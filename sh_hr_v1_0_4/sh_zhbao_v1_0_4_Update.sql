-----------------------------------------------------
-- Export file for user ZHBAO                      --
-- Created by Administrator on 2013-6-19, 14:17:43 --
-----------------------------------------------------

spool zhbao_back_v1_0_4.log
 

 drop function WL_XS_GETPAYMENT;
 drop function WL_XS_GETSALECOUNT;
 drop function WL_XS_GETSECULAR;
 drop function WL_XS_GETSHOULDON;
 drop function WL_XS_GETXSMONEY;
 drop function WL_XS_GETXS_INFO;

 drop procedure WL_GETDEPINFO;
 drop procedure WL_GETDEPINFO_1;
 drop procedure WL_XS_BINDDEP;
 drop procedure WL_XS_GETBYMSALE;
 drop procedure WL_XS_GETMYEARN;
 drop procedure WL_XS_HANDONHISTORY;
 drop procedure WL_XS_PAYMENTM;
 drop procedure WL_XS_SACH_ADDSIJI;
 drop procedure WL_XS_SACH_GETSIJI;
 drop procedure WL_XS_SACH_SELECT;
 drop procedure WL_XS_SACH_SELECT1;
 drop procedure WL_XS_SALES_ADDUPDATE;
 drop procedure WL_XS_SALES_SELECTDELE;
 drop procedure WL_XS_SALE_DEP_REG;
prompt
prompt Creating procedure WL_GETDEPINFO
prompt ==================================
prompt
create or replace procedure WL_GetDepInfo(
     PageSize in number,
     pageIndex in number,
     keys in varchar2,
     in_state in number,---1 全部 0 为上传 1 带审核 2 通过 3 未通过
     pageCount out number,
     recur out sys_refcursor--Wl_Cust_Dep_Getinfo.t_info
  )as

begin
  if in_state=-1 then
      open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department
            where CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  else
    open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department
            where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%')
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  end if;
end;
/

spool off