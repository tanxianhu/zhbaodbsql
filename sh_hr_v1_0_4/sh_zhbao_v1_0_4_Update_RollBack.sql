-----------------------------------------------------
-- Export file for user ZHBAO                      --
-- Created by Administrator on 2013-6-19, 14:17:43 --
-----------------------------------------------------

spool zhbao_back_v1_0_4.log

prompt
prompt Creating function WL_XS_GETPAYMENT
prompt ==================================
prompt
create or replace function WL_XS_GetPayMent(saleid1 in varchar2) return number is
  /*该函数获取传入销售编号的缴费总和*/
  tempMoney number(5):=0;
  tempCount number(5):=0;
begin
  select count(*) into tempCount
     from wl_xs_payment
      where saleid=to_number(saleid1);
  if tempCount!=0 then
    select sum(paymentamount) into tempMoney
      from wl_xs_payment
     where saleid=to_number(saleid1);
  end if;
  return(tempMoney);
end;
/

prompt
prompt Creating function WL_XS_GETSALECOUNT
prompt ====================================
prompt
create or replace function wl_xs_GetSaleCount(
     saleId1 in varchar2,
     grade in number,
     saledate in varchar2)
     return number is
  retcount number(5):=0;
begin
  if regexp_like(saleId1,'^\d{3}000$','i') then
   if grade=0 then
     select count(order_count) into retcount  from wl_orderinfo
        where order_operator=saleId1
        and product_id='FFF0D80E7CC344198499B3892A17499D';
      return(retcount);
   end if;
   if grade=1 then
      select count(order_count) into retcount from wl_orderinfo
         where order_operator>to_number(saleId1)
         and order_operator<to_number(saleId1)+1000
         and  product_id='FFF0D80E7CC344198499B3892A17499D';
   end if;
  elsif grade=1 then
      select count(order_count) into retcount  from wl_orderinfo
        where  order_operator=saleId1
        and  product_id='FFF0D80E7CC344198499B3892A17499D';
      return(retcount);
  end if;

  return retcount;
end;
/

prompt
prompt Creating function WL_XS_GETSECULAR
prompt ==================================
prompt
create or replace function WL_xs_Getsecular
(
   sender in varchar2,--当前身份
   obj in varchar2--下级身份
   )
   return number is
     twoToOne number(8):=70;
     oneToMe0 number(8):=70;
     oneToMe1 number(8):=60;
     Money1 number(8):=0;
     moneyTemp number(8):=0;
     temp_count number(6):=0;
   begin
       -------------超级管理员身份---------------
       if regexp_like(sender,'^admin$','i') then
          ---------------二级销售应该给该一级销售数量---------
          select count(order_count) into temp_count from wl_orderinfo
           where order_operator<to_number(Obj)+1000
           and order_operator>to_number(Obj)
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
            moneyTemp:=temp_count*oneToMe1;
           ---------------------一级销售自己的数量--------------
           select count(order_count) into temp_count from wl_orderinfo
           where order_operator=Obj
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
             moneyTemp:=moneyTemp+(temp_count*oneToMe0);
           goto ret_end;
       end if;
       -----------------一级销售身份-----------------
        if regexp_like(sender,'^[1-9]\d{2}000$','i') then
          ------------------二级销售应该给一级销售数量----------
          select count(order_count) into temp_count from wl_orderinfo
           where order_operator=to_number(Obj)
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
           moneyTemp:=temp_count*twoToOne;
       end if;
       --------------------二级销售--------------
       <<ret_end>>
  return(moneyTemp);
end;
/

prompt
prompt Creating function WL_XS_GETSHOULDON
prompt ===================================
prompt
create or replace function wl_xs_GetShouldOn(saleid1  in varchar2,grade in number,xsdate in varchar2) return number is
  ret_shouldOn number(8);
  temp_count number(5):=0;
begin
   if regexp_like(saleid1,'^\d{3}000$','i') then
     temp_count:=wl_xs_getsalecount(saleid1,grade,xsdate);
   elsif grade=1 then
     temp_count:=wl_xs_getsalecount(saleid1,grade,xsdate);
   end if;
   if grade=0 then
     ret_shouldOn:=temp_count*70;
   else
     ret_shouldOn:=temp_count*60;
   end if;
  return(ret_shouldOn);
end ;
/

prompt
prompt Creating function WL_XS_GETXSMONEY
prompt ==================================
prompt
create or replace function wl_xs_getxsMoney(saleid1 in varchar2,grade in number,Mdat in varchar2) return number is
  ret_money number(8):=0;
  temp_count number(5):=1;
begin
     --------------------一级销售获取自己的销售金额以及其二级的销售金额-----------
     if regexp_like(saleid1,'^\d{3}000$','i') then
       temp_count:=wl_xs_getsalecount(saleid1,grade,Mdat);
       ret_money:=temp_count*120;
     ----------------------二级只能获取自己的销售金额----------------------------
     elsif grade=1 then
       temp_count:=wl_xs_getsalecount(saleid1,grade,Mdat);
       ret_money:=temp_count*120;
     end if;
  return(ret_money);
end wl_xs_getxsMoney;
/

prompt
prompt Creating function WL_XS_GETXS_INFO
prompt ==================================
prompt
create or replace function wl_xs_GetXS_Info(saleId in varchar2,obj in varchar2) return varchar2 is
  ret_str varchar2(500);
  temp_sql varchar2(1000);
begin
  if obj='name' then
     select max(sname) into ret_str from wl_xs_salesman where saleid=saleId;
  end if;
  if obj='phone' then
    select max(sphone) into ret_str from wl_xs_salesman where saleid=saleId;
  end if;
  return(ret_str);
end;
/

prompt
prompt Creating procedure WL_GETDEPINFO
prompt ================================
prompt
create or replace procedure WL_GetDepInfo(
     PageSize in number,
     pageIndex in number,
     keys in varchar2,
     pageCount out number,
     recur out sys_refcursor
  )as

  begin
    open recur for
  select k.* from (
     select t.* ,rownum run from
       ( select w.*,n.* from WL_Cust_department w
           left join wl_cust_deptnote n
           on n.depid=w.cust_id
            where CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            order by n.ischecked,R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
     where (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  end;
/

prompt
prompt Creating procedure WL_GETDEPINFO_1
prompt ==================================
prompt
create or replace procedure WL_GetDepInfo_1(
     PageSize in number,
     pageIndex in number,
     keys in varchar2,
     in_state in number,---1 全部 0 为上传 1 带审核 2 通过 3 未通过
     pageCount out number,
     recur out sys_refcursor--Wl_Cust_Dep_Getinfo.t_info
  )as

begin
  if in_state=-1 then
      open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department
            where CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  else
    open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department
            where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%')
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  end if;
end;
/

prompt
prompt Creating procedure WL_XS_BINDDEP
prompt ================================
prompt
create or replace procedure wl_XS_BindDep(
  /*销售人员绑定信息部*/
  op in varchar2,
  In_saleid in number,
  In_depNbr in varchar2,
  In_depPass in varchar2,
  out_ret out varchar2
)
as
temp_count number(2):=0;
temp_str varchar2(80):='';
begin
   select count(*) into temp_count from Wl_Cust_Department where nbr=In_depNbr and c_password=In_depPass;
   if temp_count=0 then
       temp_str:='绑定失败：用户名或密码错误！';
       goto ret_end;
   end if;
   if In_saleid is null then
      temp_str:='销售编号有误！';
      goto ret_end;
   end if;
   if regexp_like(op,'bind') then
      update Wl_Cust_Department set saleId=In_saleid  where nbr=In_depNbr and c_password=In_depPass;
      commit;
      temp_str:='绑定成功！';
       goto ret_end;
   end if;
   if regexp_like(op,'exitBind') then
      update Wl_Cust_Department set saleId=''  where nbr=In_depNbr and c_password=In_depPass;
      commit;
      temp_str:='取消成功！';
   end if;
   <<ret_end>>
   out_ret:= temp_str;
end;
/

prompt
prompt Creating procedure WL_XS_GETBYMSALE
prompt ===================================
prompt
create or replace procedure WL_XS_GetByMSale(
sender in varchar2,
ret_cur out sys_refcursor)
as
   ret_num number(5):=0;
begin
   -----------------------管理员---------------------
   if regexp_like(sender,'admin','i') then
       open ret_cur for
            select saleId sID,
                   wl_xs_GetXS_Info(saleId,'name') sName,
                   wl_xs_GetXS_Info(saleId,'phone') sPhone,
                   wl_xs_getsecular('admin',saleId) shoudUP,
                   wl_Xs_getPayMent(saleId) sUped,
                   wl_xs_getsecular('admin',saleId)-wl_Xs_getPayMent(saleId) sNoup
            from wl_xs_salesach
            where xstype='司机'
            and regexp_like(saleid,'^\d{3}000$')
            group by saleId;
            goto ret_end;
   end if;
     -----------------一级代理查看二级代理销售状态级--------
   if regexp_like(sender,'^\d{3}000$','i') then
        open ret_cur for
            select saleId sID,
            wl_xs_GetXS_Info(saleId,'name') sName,
            wl_xs_GetXS_Info(saleId,'phone') sPhone,
             wl_xs_getsecular(sender,saleId) shoudUP,
            wl_Xs_getPayMent(saleId) sUped,
            wl_xs_getsecular(sender,saleId)-wl_Xs_getPayMent(saleId) sNoup
            from wl_xs_salesach
            where saleid<to_number(sender)-(mod(to_number(sender),100))+1000
            and saleid>to_number(sender)-(mod(to_number(sender),100))
            and xstype='司机'
            group by saleid ;
            goto ret_end;
    end if;
    ------------------------代理查看自己的销售量情况-----------
    <<ret_other>>
    open ret_cur for
        select c.nbr cPhone,c.c_name cName,s.xstime sDate
         from wl_xs_salesach s
         inner join wl_cust_mobile c
         on c.cust_id=s.oppsiteid
         where s.saleid=sender;
    <<ret_end>>
    ret_num:=0;
end;
/

prompt
prompt Creating procedure WL_XS_GETMYEARN
prompt ==================================
prompt
create or replace procedure WL_xs_GetMyEarn(
/*输入编号获取该销售人员盈利金额*/
 sender in varchar2,---销售ID
 outStr out varchar2--输入字符串
)as
temp_out varchar2(500):='';
temp_count number(8):=0;
temp_money number(8):=0;
temp_money_1 number(8):=0;
temp_money_up number(8):=0;
temp_all_money number(8):=0;
temp_shoud_inf number(8):=0;
begin
     /*管理员查看自己盈利状态*/
     if regexp_like(sender,'admin','i') then
          select count(*) into temp_count
             from Wl_Xs_Salesach
               where xstype='司机'
               and Translation=1;
               temp_all_money:=temp_count*120;
         temp_out:='总收入:'||to_char(temp_all_money)||'元;';
         /*获取一级销售人数销售的司机个数*/
         ---------------应收--------------------
         select count(*) into temp_count
             from Wl_Xs_Salesach
               where regexp_like(saleid,'\d{3}000','i')
               and xstype='司机'
               and Translation=1;
         temp_money:=temp_count*70;
         /*获取二级销售的销售司机个数*/
          select count(*) into temp_count
             from Wl_Xs_Salesach
               where regexp_instr(saleid,'\d{3}000')=0
               and xstype='司机'
               and Translation=1;
          temp_money:=temp_money+(temp_count*60);
          temp_shoud_inf:=temp_money;
           /*获取公司总收入*/
          temp_out:=temp_out||'应收:'||to_char(temp_money)||'元;';
          temp_out:=temp_out||'支出:'||to_char(temp_all_money-temp_money)||'元;';
           /*获取公司实际收入*/
          select sum(paymentamount) into temp_money
          from wl_xs_payment
          where regexp_like(saleid,'\d{3}000','i');
          if temp_money is null or trim(temp_money)='' then
              temp_money:=0;
          end if;
          temp_out:=temp_out||'已收:'||to_char(temp_money)||'元;';
          temp_out:=temp_out||'未收:'||to_char(temp_shoud_inf-temp_money)||'元;';
         goto ret_end;
     end if;
     /*一级销售人员查看自己销售多少个，销售金额是多少，二级销售工上交多少 一级销售应该上交多少，盈利多少*/
     if regexp_like(sender,'\d{3}000','i') then
        /*查看自己销售多少司机*/
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid=to_number(sender)
        and xstype='司机'
        and Translation=1;
        temp_out:='我总共销售司机:'||to_char(temp_count)||'个;';
        temp_out:=temp_out||'销售总金额:'||to_char(temp_count*120)||'元;';
        temp_money_1:=temp_count*50;--自己盈利金额
        temp_money_up:=temp_count*70;--应该上交
        temp_out:=temp_out||'盈利金额:'||to_char(temp_money_1)||'元;';
        temp_out:=temp_out||'应上交金额:'||to_char(temp_money_up)||'元;';
        /*查看该一级销售人员的二级销售人员销售司机个数*/
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid>to_number(sender)
        and saleid<to_number(sender)+1000
        and xstype='司机'
        and Translation=1;
        temp_out:=temp_out||'我的二级销售应该交我:'||to_char(temp_count*70)||'元;';
        temp_out:=temp_out||'我应该转交上级:'||to_char(temp_count*60)||'元;';
        temp_out:=temp_out||'从二级盈利:'||to_char(temp_count*10)||'元;';
        temp_out:=temp_out||'总上交:'||to_char(temp_count*60+temp_money_up)||'元;';
        -------------------------已经上交金额---------------------------------------
         temp_out:=temp_out||'已上交:'||to_char(wl_Xs_getPayMent(sender))||'元;';
          temp_out:=temp_out||'还未上交:'||to_char(wl_xs_getsecular('admin',sender)-wl_Xs_getPayMent(sender))||'元;';
        -------------------------未上交上交金额-------------------------------------
        temp_out:=temp_out||'我的总利润:'||to_char(temp_count*10+temp_money_1)||'元;';
        select sum(paymentamount) into temp_money
        from wl_xs_payment
        where saleid>to_number(sender)
        and saleid<to_number(sender)+1000;
        if temp_money is null or trim(temp_money)='' then
           temp_money:=0;
        end if;
        temp_out:=temp_out||'已获得的利润:'||to_char(temp_money_1+temp_money)||'元。';
        goto ret_end;
     end if;
     /*二级销售人员查看自己的销售总金额，应上交金额，已经上交金额， 未上交金额 ，总利润*/
     if regexp_instr(sender,'\d{3}000')=0 and regexp_instr(sender,'^\d{6}$')>0 then
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid=to_number(sender)
        and xstype='司机'
        and Translation=1;
        temp_out:='销售总金额: '||to_char(temp_count*120)||'元;';
        temp_money_up:=temp_count*70;
        temp_money_1:=temp_count*50;
        temp_out:=temp_out||'应上交金额:'||to_char(temp_money_up)||'元;';
        /*已经上交金额*/
        select sum(paymentamount) into temp_money
        from wl_xs_payment
        where saleid=to_number(sender);
         if temp_money is null or trim(temp_money)='' then
           temp_money:=0;
        end if;
        temp_out:=temp_out||'已经上交金额:'||to_char(temp_money)||'元;';
        temp_out:=temp_out||'未上交金额:'||to_char(temp_money_up-temp_money)||'元;';
        temp_out:=temp_out||'总利润:'||to_char(temp_money_1)||'元;';
     end if;
    <<ret_end>>
    outStr:=temp_out;
  end;
/

prompt
prompt Creating procedure WL_XS_HANDONHISTORY
prompt ======================================
prompt
create or replace procedure WL_XS_handOnHistory(
   /*获取缴费和收费账单*/
   sender in varchar2,--查询者
   op in varchar2,--操作 in 收费账单 out 缴费账单
   ret_cur out sys_refcursor--查询结果
)
as
begin

  if regexp_like(op,'in','i') then
      if regexp_like(sender,'admin','i') then
       open ret_cur for  select a.sname 姓名,a.saleid 编号,a.sphone 电话,p.paymentamount 上交金额,p.paydate 上交时间,
          case
             when p.note='admin' then '找货宝公司'
          end 收款对象,
          case
              when p.note='admin' then '029-88764408'
          end 收款电话
          from wl_xs_payment p
           inner join wl_xs_salesman a
           on a.saleid=p.saleid
            where p.note=sender;
      else
         open ret_cur for  select a.sname 姓名,a.saleid 编号,a.sphone 电话,p.paymentamount 上交金额,p.paydate 上交时间
          from wl_xs_payment p
           inner join wl_xs_salesman a
           on a.saleid=p.saleid
            where p.note=sender;
      end if;
  end if;
  if regexp_like(op,'out','i') then
     open  ret_cur for  select p.saleid 编号,p.paymentamount 上交金额,p.paydate 上交时间,
          case
             when p.note='admin' then '找货宝公司'
             else wl_xs_getxs_info(p.note,'name')
          end 收款对象,
          case
              when p.note='admin' then '029-88764408'
               else wl_xs_getxs_info(p.note,'phone')
          end 收款电话
        from wl_xs_payment p
       where p.saleid=sender;
  end if;
end;
/

prompt
prompt Creating procedure WL_XS_PAYMENTM
prompt =================================
prompt
create or replace procedure wl_xs_PayMentM(
   /*销售人员交税*/
   saleid1 in varchar2,--销售编号
   payAmount in number,--缴费金额
   note1 in varchar2,--备注
   ret_out out varchar2--输入信息--一级销售编号
)as
temp_str varchar2(800):='';
temp_count number(8):=0;
begin
    if saleid1 is null or trim(saleid1)='' then
       temp_str:='销售编号无效';
       goto ret_end;
    end if;
     if regexp_like(saleid1,'\d{6}')=false then
        temp_str:='销售编号无效';
       goto ret_end;
     end if;
     select count(*) into temp_count from wl_xs_salesman
     where saleid=saleid1;
     if temp_count=0 then
        temp_str:='销售编号无效';
         goto ret_end;
     end if;
     if regexp_like(note1,'\d{3}000') then
        if to_number(saleid1)<to_number(note1) or to_number(saleid1)>=to_number(note1)+1000 then
         temp_str:='缴费失败。该销售编号不属于您的二级代理';
         goto ret_end;
        end if;
     end if;
     if regexp_like(note1,'\d{3}000')=false and regexp_like(note1,'\d{6}') then
         temp_str:='您无权操作';
         goto ret_end;
     end if;
     insert into WL_XS_PayMent(saleid,Paymentamount,payDate,note)
       values(saleid1,payAmount,sysdate,note1);
       temp_str:='缴费成功';
    <<ret_end>>
    ret_out:=temp_str;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_ADDSIJI
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_addSiJi(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   in_phone varchar2,--司机司机手机号
   in_idCard varchar2,--司机身份证号码
   in_cname varchar2,--司机姓名
   in_carNumber varchar2,--司机车牌号
   in_carType in varchar2,--司机车类型
   in_like1 in varchar2,--司机长袍路线
   in_qq in varchar2 ,--司机QQ
   Out_ret out varchar2  --返回结果
)
as
 temp_cust_id varchar2(200):='';
 temp_str varchar2(400):='';
 temp_count number(3):=0;
begin
  if sxid is null then
     temp_str:='销售编号有误！';
      goto ret_end;
  end if;
  select count(*) into temp_count from wl_xs_salesman
  where saleid =sxid;
   if temp_count=0 then
      temp_str:='销售编号有误！';
      goto ret_end;
   end if;
   if in_phone is null then
       temp_str:='电话号码不能为空！';
      goto ret_end;
   end if;
   select max(cust_id) into temp_cust_id  from wl_cust_mobile
     where nbr =in_phone;
   if temp_cust_id is null then
        insert into Wl_Cust_Mobile(
               nbr,Id_card,c_name,car_number,
               Car_Model,Like1,qq,create_date,eff_date
         )values
         (
            in_phone,in_idCard,in_cname,in_carNumber,
            in_carType,in_like1,in_qq,sysdate,sysdate
         ) return cust_id into temp_cust_id;
         commit;
    end if;
     wl_xs_sach_add(SXID,'司机',temp_cust_id,temp_count);
         if temp_count=0 then
            temp_str:='添加成功！';
            goto ret_end;
         elsif temp_count=4 then
            temp_str:='不能重复添加！';
            goto ret_end;
         elsif temp_count=5 then
             temp_str:='该用户已经被其他用户添加！';
              goto ret_end;
         end if;
   <<ret_end>>
  Out_ret:=temp_str;
end;
/

prompt
prompt Creating procedure WL_XS_SACH_GETSIJI
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_GetSiJi(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   SiJiType number,--司机类型 0 所有用户 1 包年用户 2  包月用户 3 潜在用户
   XSType1 in varchar2,--销售类型
   keyWords in varchar2,--搜索相关的数据
   retCursor out sys_refcursor--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
         select c.* from (
           select * from wl_cust_department
               where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch
                  where SaleId=SXID and XSType='信息部'
               )
           )  c
           where c.NBR like ''||keyWords||'%'
              or c.C_NAME like ''||keyWords||'%'
              or c.ID_CARD like ''||keyWords||'%'
              or c.City like ''||keyWords||'%'
              or c.CHIEF like ''||keyWords||'%'
              or c.CONTACT like ''||keyWords||'%'
              or c.ADDRESS like ''||keyWords||'%'
              or c.SMSSIGN like ''||keyWords||'%';
      end if;
      if XSType1='司机' then
        if SiJiType=0 then
            open retCursor for
               select m.*,t.* from (
                  select a.*,b.* from wl_orderinfo a
                     inner join wl_productinfo b
                     on a.product_id=b.productid
                     ) t right join (
                        select * from Wl_Cust_Mobile where cust_id in
                    ( select oppsiteid from Wl_Xs_Salesach
                          where saleid=SXID and xstype='司机'
                    )) m
                  on t.cust_id=m.cust_id
                where m.NBR like ''||keyWords||'%'
               or m.C_NAME like ''||keyWords||'%'
               or m.City like ''||keyWords||'%'
               or m.CAR_NUMBER like ''||keyWords||'%'
               or m.CAR_MODEL like ''||keyWords||'%'
               or m.LIKE1 like ''||keyWords||'%'
               or m.QQ like ''||keyWords||'%';
           elsif SiJiType=1 then
              open retCursor for
                    select m.*,t.*
                    from wl_cust_mobile m
                         inner join
                         (
                         select o.cust_id,o.order_date,o.expiry_date,
                             o.order_count,o.order_type,o.order_operator,
                             p.product_name,p.product_type,p.product_value,
                             p.product_note from wl_orderinfo o
                             inner join wl_productinfo p
                             on p.productid=o.product_id
                             where o.product_id='FFF0D80E7CC344198499B3892A17499D'
                           ) t
                         on t.cust_id=m.cust_id
                 where t.order_operator=SXID;
            elsif SiJiType=2 then
                open retCursor for
                    select m.*,t.*
                    from wl_cust_mobile m
                         inner join
                         (
                         select o.cust_id,o.order_date,o.expiry_date,
                             o.order_count,o.order_type,o.order_operator,
                             p.product_name,p.product_type,p.product_value,
                             p.product_note from wl_orderinfo o
                             inner join wl_productinfo p
                             on p.productid=o.product_id
                             where o.product_id='96B6F04443E64C21819C9C68822E07F4'
                           ) t
                         on t.cust_id=m.cust_id
                 where t.order_operator=SXID;
            elsif SiJiType=3 then
              open retCursor for
               select m.*,t.* from (
                  select a.*,b.* from wl_orderinfo a
                     inner join wl_productinfo b
                     on a.product_id=b.productid
                     ) t right join (
                        select * from Wl_Cust_Mobile where cust_id in
                    ( select oppsiteid from Wl_Xs_Salesach
                          where saleid=SXID and xstype='司机'
                          and oppsiteid not in (
                               select cust_id from wl_orderinfo
                              where order_operator=SXID
                    ))) m
                  on t.cust_id=m.cust_id
                  ;
            end if;
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_SELECT
prompt ====================================
prompt
create or replace procedure WL_XS_SACH_Select(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   XSType1 in varchar2,--销售类型
   retCursor out sys_refcursor--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
           select * from wl_cust_department where Cust_ID in (
              select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='信息部'
           );
      end if;
      if XSType1='司机' then
        open retCursor for
           select * from wl_cust_mobile where Cust_ID in (
              select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='司机'
           );
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_SELECT1
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_Select1(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   XSType1 in varchar2,--销售类型
   keyWords in varchar2,--搜索相关的数据
   retCursor out sys_refcursor--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
         select c.* from (
           select * from wl_cust_department
               where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch
                  where SaleId=SXID and XSType='信息部'
               )
           )  c
           where c.NBR like ''||keyWords||'%'
              or c.C_NAME like ''||keyWords||'%'
              or c.ID_CARD like ''||keyWords||'%'
              or c.City like ''||keyWords||'%'
              or c.CHIEF like ''||keyWords||'%'
              or c.CONTACT like ''||keyWords||'%'
              or c.ADDRESS like ''||keyWords||'%'
              or c.SMSSIGN like ''||keyWords||'%';
      end if;
      if XSType1='司机' then
        open retCursor for
           select m.* from(
               select * from wl_cust_mobile where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='司机'
               )
           )  m
           where m.NBR like ''||keyWords||'%'
           or m.C_NAME like ''||keyWords||'%'
           or m.City like ''||keyWords||'%'
           or m.CAR_NUMBER like ''||keyWords||'%'
           or m.CAR_MODEL like ''||keyWords||'%'
           or m.LIKE1 like ''||keyWords||'%'
           or m.QQ like ''||keyWords||'%';
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SALES_ADDUPDATE
prompt ========================================
prompt
CREATE OR REPLACE PROCEDURE WL_XS_SALES_AddUpdate(
  /*销售人员的管理的存储过程*/
    snId IN NUMBER,--销售编号
    sName1 IN varchar2,--销售人员姓名
    sGender1 IN varchar2,--销售人员性别
    sIDCard1 IN varchar2,--销售人员身份证号码
    sPhone1 IN varchar2,--电话号码
    sNode1 IN varchar2,--备注
    sOp in number,--操作编号 0 添加 1 修改
    ret out varchar2--结果编号 返回编号 添加成功  0 更新成功 1 销售编号无效  2 电话号码无效
)as
retcount number(4):=0;
retStr varchar2(100):='';
begin
     /*添加*/
     if sOp=0 then
         --电话号码为空
         if sPhone1 is null or trim(sPhone1)='' then
            retStr:='电话号码为空';
            goto  ret_end;
          end if;
          select count(*) into retcount from WL_XS_salesman where saleId=snId;
          if retcount>0 then
            retStr:='该销售编号已存在！';
            goto  ret_end;
          end if;
          --添加信息并返回编号
          insert into WL_XS_salesman(saleId,sName,sGender,sIDCard,sPhone,sNode)
              values(snId,sName1,sGender1,sIDCard1,sPhone1,sNode1);
            retStr:='添加成功！';
            goto ret_end;
       end if;
       /*更新*/
       if sOp=1 then
            --电话号码为空
         if sPhone1 is null or trim(sPhone1)='' then
            retStr:='电话号码为空';
            goto  ret_end;
          end if;
          --判断该编号是否存在
           select count(saleId) into retcount from WL_XS_salesman where saleId=snId;
           --不存在
           if retcount=0 then
                retStr:='销售编号不存在';
                goto  ret_end;
           end if;
           --存在则更新
           update WL_XS_salesman set sName=sName1,sGender=sGender1,sIDCard=sIDCard1,sPhone=sPhone1,sNode=sNode1
              where saleId=snId;
              retStr:='修改成功！';
               goto ret_end;
         end if;
     <<ret_end>>
     ret:=retStr;
  end;
/

prompt
prompt Creating procedure WL_XS_SALES_SELECTDELE
prompt =========================================
prompt
create or replace procedure WL_XS_SALES_SelectDele(
    /*操作销售元列表 查询和删除*/
    snId in number,--销售人员Id
    keywords in varchar2,--搜索关键字
    Sop in number,--操作编号 0 删除 1 查询
    ret out number,--0 删除成功 1 销售编号无效 2 关键字无效
    retcur out sys_refcursor--返回的结果集
  )as
  retcount number(4):=0;
  begin
    /*执行删除操作*/
      if Sop=0 then
          --编号为空
          if snId is null then
              retcount:=1;
              goto ret_end;
            end if;
          --编号为‘’
          if trim(to_char(snId))='' then
               retcount:=1;
               goto ret_end;
            end if;
            --是否存在
            select count(saleId) into retcount from WL_XS_salesman where saleId=snId;
            --编号不存在
            if retcount=0 then
                 retcount:=1;
                 goto ret_end;
              end if;
             --删除该用户
             delete WL_XS_salesman where saleId=snId;
             delete WL_XS_salesAch where SaleId=snId;
              retcount:=0;
             goto ret_end;
        end if;
       /*执行查询操作*/
       if Sop=1 then
           --关键字为空查询全部
           if keywords is null or trim(keywords)='' then
               open retcur for select * from WL_XS_salesman order by sCreateDate desc;
               goto ret_end;
            end if;
            --关键字不为空蘑菇查询
           open retcur for
               select * from WL_XS_salesman where
               to_char(saleId) like ''||keywords||'%'
               or sName like ''||keywords||'%'
               or sGender like ''||keywords||'%'
               or sIDCard like ''||keywords||'%'
               or sPhone like ''||keywords||'%'
               or sNode like ''||keywords||'%' order by sCreateDate desc;
        end if;
      <<ret_end>>
      ret:=retcount;
    end;
/

prompt
prompt Creating procedure WL_XS_SALE_DEP_REG
prompt =====================================
prompt
create or replace procedure WL_XS_SALE_DEP_REG(
   /*改存储过程是信息部手机信息注册用户使用的*/
    phoneNum in varchar2,--手机号码
    Pwd in varchar2,--密码
    XSId in number,--销售人员ID
    Ret out number--返回值 0 成功 1 手机号码为空 2 密码为空 3 销售编号无效 4 手机号已经被注册过
)as
retcount number(6):=0;
opsiId varchar2(64):='';
begin
  /*判断手机号码*/
   if phoneNum is null then
     retcount:=1;
     goto ret_end;
   end if;
   select count(nbr) into retcount from wl_cust_department where nbr=phoneNum;
   if retcount>0 then
       retcount:=4;
       goto ret_end;
   end if;
   /*判断密码*/
   if Pwd is null then
     retcount:=2;
     goto ret_end;
   end if;
   /*注册信息部*/
   insert into wl_cust_department (nbr,contact,c_password,freesms,billingsms,issuperadmin,smssign)
     values(phoneNum,PhoneNum,Pwd,0,0,0,'【找货宝】') return CUST_ID into OpsiId;
   /*判断ID是否为空*/
    if XSId is null then
       retcount:=3;
       goto ret_end;
    end if;
    /*判断销售ID是否存在*/
    select count(SALEID) into retcount from  wl_xs_salesman where SALEID=XSId;
    if retcount=0 then
       retcount:=3;
       goto ret_end;
    end if;
    /*给销售人员插入对应的部门ID*/
    insert into WL_XS_salesAch(SaleId,XSType,OppsiteId)
        values(XSId,'信息部',opsiId);
      retcount:=0;
    /*给该部门分配一个信息销售编号*/
    select max(Saleid)+1 into retcount from Wl_Xs_Salesman where Saleid<(XSId+100000) and Saleid>=XSId;
    /*将销售编号放入销售人员列表*/
    insert into Wl_Xs_Salesman(SALEID,SPWD,Sname,Sgender,Sidcard,Sphone,Snode,Screatedate)
    values(retcount,'0000','','','',phoneNum,'',sysdate);
    /*将信息部和自身的销售编号绑定*/
    update wl_cust_department set saleId=retcount where cust_id=opsiId;
    commit;
  <<ret_end>>
  Ret:=retcount;
end;
/


spool off
