----------------------------------------------------
-- Export file for user ZHBAO                     --
-- Created by Administrator on 2013-6-7, 18:09:49 --
----------------------------------------------------

spool sh_zhbao_update_v1_0_2.log

prompt
prompt Add Table Column WL_XS_SALESMAM
prompt ==================================
prompt
alter table wl_xs_salesman add(sumsale number(10) default 0);

prompt
prompt Adding Table Column WL_XS_SALESMAN
prompt ==================================
prompt
alter table wl_xs_salesman add(sumup number(10) default 0);

prompt
prompt Creating function GETSHOULDUPCOUNT
prompt ==================================
prompt
create or replace function GetShouldUpCount(In_saleId in varchar2) return number is
  temp_Result number(8);
begin
    select sum(su) into temp_Result from (
     select sum(sumsale) su from wl_xs_salesman where saleid=In_saleId
     union
     select sum(sumsale) su from wl_xs_salesman where saleid in (
        select saleid from wl_xs_salesman where fsaleid=In_saleId
      )
    );
  return(temp_Result);
end GetShouldUpCount;
/

prompt
prompt Creating procedure SP_DOWN_INSERT
prompt =================================
prompt
create or replace procedure SP_Down_Insert(
   In_MessId in varchar2,--短信Id
   In_ReceiVer in varchar2,--接收者
   In_Sender in varchar2,--发送者
   In_content in varchar2,--发送的内容
   Priority in number,--优先级别
   ret_out out number--操作结果 0 成功 1 接收者为空 2 发送者为空 3 内容为空
)
as
temp_out number(5);
temp_Priority number(2);
begin
   temp_Priority:=Priority;
   --验证接收者
   if In_ReceiVer is null then
      temp_out:=1;
      goto ret_end;
   end if;
   --验证发送者
   if In_Sender is null then
       temp_out:=2;
      goto ret_end;
   end if;
   --验证内容
   if In_content is null then
        temp_out:=3;
      goto ret_end;
   end if;
   --验证优先级
   if temp_Priority is null then
      temp_Priority:=0;
   end if;
   insert into SP_DOWN(ID,MessageId,ReCEIVER,Sender,Content,sendDate,priority,state)
      values(sp_down_ID.Nextval,In_MessId,In_ReceiVer,In_Sender,In_content,Sysdate,temp_Priority,0);
      commit;
       temp_out:=0;
   <<ret_end>>
   ret_out:=temp_out;
end;
/

prompt
prompt Creating procedure SP_GETSPCODE
prompt ===============================
prompt
create or replace procedure SP_GetSPCODE(
  ret_cur out sys_refcursor--返回结果集
)
as
begin
  open ret_cur for select * from sp_code;
end;
/

prompt
prompt Creating procedure SP_GET_DOWNMES
prompt =================================
prompt
create or replace procedure SP_Get_downMes(
  /*获取下行预发送短信*/
  in_count in number,--一次获取条数
  ret_out out sys_refcursor--返回的结构集
)
as
begin
    /*0未发送，1发送成功，2正在发送，3发送失败,4 已发送未返回状态*/
    update sp_down set state=4 where state=2;
    commit;
    update sp_down set state=2 where sp_down.id in(
      select id from (
           select sp_down.id from sp_down where state=0
            order by priority desc
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_down where state=2;
  end;
/

prompt
prompt Creating procedure SP_GET_UPSMS
prompt ===============================
prompt
create or replace procedure SP_Get_UPSMS(
  in_count in number,--获取条数
  ret_out out sys_refcursor--返回结果集
)
as
begin
    --状态 0 为处理 1 正在处理 2 已处理
    update sp_up set state=2 where state=1;
    commit;
    update sp_up set state=1 where sp_up.id in(
      select id from (
           select sp_up.id from sp_up where state=0
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_up where state=1;
end;
/

prompt
prompt Creating procedure SP_INSERT_UP
prompt ===============================
prompt
create or replace procedure SP_Insert_Up(
 /*短信SP上行*/
  In_Sender in varchar2,--发送者
  In_SP_Code in  varchar2,--sp指令
  In_MessID in varchar2,--消息ID
  In_Content in  varchar2,--短信类容
  Out_ret out varchar2--操作结构
)
as
temp_out varchar2(50);
temp_SP_Code varchar2(5000);
begin
    temp_SP_Code:=In_SP_Code;
     --发送者不能为空
     if In_Sender is null then
       temp_out:='发送者不能为空';
       goto ret_end;
     end if;
     --从类容中获取指令
     if temp_SP_Code is null then
         if In_Content is null then
             temp_out:='指令和内容不能同时为空';
             goto ret_end;
          end if;
        select max(IN_code) into temp_SP_Code from sp_code where regexp_like(In_Content,format,'i');
     end if;
     --判断指令
     if temp_SP_Code is null then
          temp_out:='无法获取指令';
          goto ret_end;
     end if;
     --插入上行信息
     insert into sp_up(ID,MessageID,sender,sp_code,content,state,create_date)
       values(sp_up_id.nextval,In_MessID,In_Sender,temp_SP_Code,In_Content,0,sysdate);
       commit;
      temp_out:='添加成功！';
     <<ret_end>>
     Out_ret:=temp_out;
  end;
/

prompt
prompt Creating procedure SP_UPDATE_DOWNSTATE
prompt ======================================
prompt
create or replace procedure SP_Update_DownState(
   /*更新下行短信状态*/
   In_SMSID in varchar2,--短信ID
   In_state in number,--状态
   Ret_out out number--返回状态 0 成功 1 短信ID为空 2 该短信不存在 3状态为空
)
as
temp_num number(5);
begin
    ---验证短信ID 非空
    if In_SMSID is null then
       temp_num:=1;
       goto ret_end;
    end if;
    --验证状态非空
    if In_state is null then
        temp_num:=3;
       goto ret_end;
    end if;
    --验证短信是否存在
    select count(*) into temp_num from sp_down where id=In_SMSID;
    if temp_num=0 then
       temp_num:=2;
       goto ret_end;
    end if;
    ---更新短信状态
    update sp_down set state=In_state where id=In_SMSID;
    commit;
   <<ret_end>>
   Ret_out:=temp_num;
end;
/

prompt
prompt Creating procedure WL_CUST_CANNELORDER
prompt ======================================
prompt
create or replace procedure WL_CUST_CANNELORDER(
   Phones in varchar2,
   ret_out out varchar2)
as
temp_str varchar2(500):='';
begin
  select max(CUST_ID) into temp_str from Wl_Cust_Mobile where nbr=phones;
  if temp_str is not  null then
     update wl_orderinfo set Expiry_date=sysdate where CUST_ID=temp_str and Expiry_date is null;
     commit;
     update wl_cust_mobile set isorderssxy=0,eff_date=sysdate where nbr=Phones;
     commit;
  end if;
  temp_str:='取消成功！！';
  <<ret_end>>
  ret_out:=temp_str;
end;
/

prompt
prompt Creating procedure WL_CUST_ORDER
prompt ================================
prompt
create or replace procedure WL_CUST_Order
  /*用户套餐订购*/
  (custId in varchar2
  --订购用户
  ,ProductId1 in varchar2
  --商品ID
  ,OrderCount in number
  --商品数量
  ,OrderType  in varchar2
  --订购类型  1.自定购 2.销售人员代购 3.信息部代购 4.公司代购
  ,CreateDate in date
  --订购时间
  ,ExprityDate in date
  --失效时间
  ,Op_sender in varchar2
  --操作人Id
  ,ret_out  out varchar2
  --返回操作状态
  )
as
  tmp_out varchar2(500):='';
  temp_OrderType varchar2(500):='';
  temp_Op_sender varchar2(500):='';
  temp_count number(5):=0;
begin
   temp_OrderType:=OrderType;
   temp_Op_sender:=Op_sender;

  ---------订购用户验证----------
  if custId is null then
     tmp_out:='订购用户不能为空【找货宝】';
     goto ret_end;
  end if;

  -------------商品验证-------------
  if ProductId1 is null then
    tmp_out:='购买商品不能为空【找货宝】';
    goto ret_end;
  end if;

  ---------------该商品是否存在------
  select count(*) into temp_count from wl_productinfo where productid=ProductId1;
  /*if temp_count=0 then
     tmp_out:='该套餐不存在【找货宝】';
     goto ret_end;
  end if;*/

  ----------------订购类型验证---------
  if temp_OrderType is null then
     tmp_out:='订购类型验证【找货宝】';
     goto ret_end;
  end if;

  ------查看是否有销售人员绑定该号码----
  select max(saleid) into tmp_out from wl_xs_salesach where oppsiteid=custId;
  if tmp_out is not null then
      temp_Op_sender:=to_char(tmp_out);
      temp_OrderType:='销售人员代购【找货宝】';
  end if;

  -------------添加订单-----------------
   insert into Wl_Orderinfo
    (
        Cust_id,Product_Id,Order_Count,Order_Date,Expiry_Date,Order_Type,Order_Operator
    )
    values
    (
        custId,ProductId1,OrderCount,CreateDate,ExprityDate,temp_OrderType,temp_Op_sender
     );
  commit;
  tmp_out:='订购成功！！【找货宝】';
  <<ret_end>>
  ret_out:=tmp_out;
end;
/

prompt
prompt Creating procedure WL_SALE_ADD_DRIVER
prompt =====================================
prompt
create or replace procedure WL_sale_Add_driver(
   In_saleid in varchar2,--–销售员登陆账号
   In_Driver_Phone in varchar2,--–司机电话
   In_Driver_IDCard in varchar2,--司机生分证号码
   In_Driver_Name in varchar2,--—司机姓名
   In_driver_CarNum in varchar2,--—司机车牌号
   In_Driver_CarType in varchar2, --–车类型
   In_deriver_Line in varchar2, --–长跑路线
   In_Driver_QQ in varchar2, --–司机QQ
   Out_state out varchar2--— 0 成功 -1 无权添加 -2 司机号码不能为空 -3 该司机已被其他人注册为意向用户
  )
as
 temp_str varchar2(400):='';
 temp_count number(3):=0;
 temp_out varchar2(500);
 temp_saleid varchar2(500);
begin
   temp_saleid:=In_saleid;

  if In_saleid is null then
     temp_out:='-1';
     goto ret_end;
  end if;

  if In_Driver_Phone is null then
      temp_out:='-2';
     goto ret_end;
  end if;
  select count(*) into temp_count from wl_cust_mobile where nbr=In_Driver_Phone;

  if temp_count >0 then
     temp_out:='-3';
     goto ret_end;
  end if;

  insert into wl_cust_mobile(nbr,c_name,car_number,id_card,like1,isorderssxy,car_model,create_date,eff_date,qq)
          values(In_Driver_Phone,In_Driver_Name,In_driver_CarNum,In_Driver_IDCard,In_deriver_Line,0,In_Driver_CarType,sysdate,sysdate,In_Driver_QQ)
           return cust_id into temp_str;
  commit;
 
  if regexp_like(temp_saleid,'salemanager','i') then
    temp_saleid:='0';
  end if;

  insert into wl_xs_salesach(saleid,oppsiteid,xstype,xstime) values(temp_saleid,temp_str,'司机',sysdate);
  commit;
  temp_out:='0';
   <<ret_end>>
  Out_state:=temp_out;
end;
/

prompt
prompt Creating procedure WL_SALE_DELETE
prompt =================================
prompt
create or replace procedure wl_sale_Delete
(
    In_opsaleId in varchar2,--被注销的ID\
    In_Current_saleId in varchar2,--当前操作人ID
    Out_ret out varchar2--操作状态
)
as
temp_str varchar2(500);
temp_out varchar2(500);
begin
   if In_opsaleId is null or In_Current_saleId is null then
       temp_out:='注销失败,注销放和被注销放有为空的情况！';
       goto ret_end;
   end if;

   if  regexp_like(In_Current_saleId,'salemanager','i') then
       update wl_xs_salesman set state=1 where saleid=In_opsaleId or fsaleid=In_opsaleId;
       commit;
        temp_out:='注销成功';
        goto ret_end;
   else
       select max(fsaleid) into temp_str from wl_xs_salesman where saleid=In_opsaleId;
       if temp_str=In_Current_saleId then
          update wl_xs_salesman set state=1 where saleid=In_opsaleId or fsaleid=In_opsaleId;
          commit;
           temp_out:='注销成功';
           goto ret_end;
       else
           temp_out:='您没有注销权限！';
           goto ret_end;
       end if;
   end if;

   <<ret_end>>
   Out_ret:=temp_out;
  end;
/

prompt
prompt Creating procedure WL_SALE_GETCUSTMOBILE
prompt ========================================
prompt
create or replace procedure wl_sale_getCustMobile(
   In_CustIDOrPhone in varchar2,--CUSTID 或者是电话号码
   Out_state out number,--0 成功 -1 输入参数为空
   Out_CURSOR out sys_refcursor--输出结果集合
)
as
begin
     if In_CustIDOrPhone is null then
       Out_state:=-1;
     else
        open Out_CURSOR for select * from wl_cust_mobile where cust_id=In_CustIDOrPhone or nbr=In_CustIDOrPhone;
     end if;
end;
/

prompt
prompt Creating procedure WL_SALE_GETPAYMENT
prompt =====================================
prompt
create or replace procedure wl_sale_getPayment(
  In_current in varchar2,--当前ID
  In_saleId in varchar2,--要查看ID
  Ret_out out sys_refcursor--返回的结果集
)
as
begin
if In_saleId is null then
 if regexp_like(In_current,'salemanager','i') then
  open Ret_out for
    select saleid,fsaleid,loginname,spwd,sname,sgender,
      sidcard,sphone,saddress,snode,screatedate,state,
      getshouldupcount(saleid) shouldOn,sumup shoulded,
      getshouldupcount(saleid)-sumup noup from wl_xs_salesman
      where fsaleid=0;
 else
  open Ret_out for
    select saleid,fsaleid,loginname,spwd,sname,sgender,
      sidcard,sphone,saddress,snode,screatedate,state,
      getshouldupcount(saleid) shouldOn,sumup shoulded,
      getshouldupcount(saleid)-sumup noup from wl_xs_salesman
      where fsaleid=In_current;
 end if;
 else
   open Ret_out for
    select saleid,fsaleid,loginname,spwd,sname,sgender,
      sidcard,sphone,saddress,snode,screatedate,state,
      getshouldupcount(saleid) shouldOn,sumup shoulded,
      getshouldupcount(saleid)-sumup noup from wl_xs_salesman
      where saleid=In_saleId;
 end if;
end;
/

prompt
prompt Creating procedure WL_SALE_LOOK_SIJI
prompt ====================================
prompt
create or replace procedure wl_sale_look_siji
(
   In_CurrentId in varchar2,--当前登录用户ID
   in_saleid in varchar2,--被查看销售编号
   in_sijiType in varchar2,--司机类型 0 意向用户 1 包年司机 2 包月司机
   out_cursor out sys_refcursor--返回结果级
)
as
begin
    if in_sijiType='0' then
       open out_cursor for
          select * from wl_cust_mobile where cust_id in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid=in_saleid
          );
    end if;
    if in_sijiType='1' then
      if in_saleid is null then
        open out_cursor for
           select *from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator in (
                 select saleid from wl_xs_salesman  where saleid=In_CurrentId
                 or fsaleid=In_CurrentId
             )
          );
       else
         open out_cursor for
           select * from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator=in_saleid
          );
       end if;
    end if;

    if in_sijiType='2' then
       open out_cursor for
          select *from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
             where order_operator=in_saleid and
             product_id='10元包月'
          );
    end if;
  end;
/

prompt
prompt Creating procedure WL_SALE_PAYMENTM
prompt ===================================
prompt
create or replace procedure WL_sale_PAYMENTM(
  /*给销售人员充值数量*/
 In_OpearaotrId in varchar2,--操作人编号
 In_Saleid in varchar2,--被充值人编号
 In_paycount in number,--充值数量
 In_note in varchar2,--充值数量
 ret_out out number--操作结果 0 成功 -1 操作人编号有误 -2 被充值人标号有误 -3 充值数量 出错 -4 无权操作
  )
as
  temp_out number(5);
  temp_count number(8):=0;
begin

  if In_OpearaotrId is null then
    temp_out:=-1;
    goto ret_end;
  end if;

  if In_Saleid is null then
    temp_out:=-2;
    goto ret_end;
  end if;

  if In_paycount<1 then
     temp_out:=-3;
    goto ret_end;
  end if;

  select count(*) into temp_count
        from wl_xs_salesman where saleid=In_Saleid and fsaleid=In_OpearaotrId;
  if  temp_count=0 and In_OpearaotrId<>'salemanager' then
       temp_out:=-4;
        goto ret_end;
  end if;

  update wl_xs_salesman set sumup=sumup+In_paycount where saleid=In_Saleid;
  commit;

  insert into WL_XS_PayMent(saleid,u_id,p_type,Paymentamount,payDate,note)
  values(In_Saleid,In_OpearaotrId,'',In_paycount,sysdate,In_note);
   commit;
    temp_out:=0;
  <<ret_end>>
  ret_out:=temp_out;
end;
/

prompt
prompt Creating procedure WL_SALE_REGISTER
prompt ===================================
prompt
CREATE OR REPLACE Procedure wl_sale_register(
      In_phone in varchar2,--手机号码。非空
      In_Pwd in varchar2, --密码 非空
      In_Fid in number,--父级销售编号 一级时 0
      In_SName in varchar2,--销售员姓名
      In_SGender in varchar2,--性别
      In_SAddress In varchar2,--注册地址
      In_Idcard in varchar2,--身份证号码
      In_Note in varchar2,--备注
      Out_state out varchar2--操作结果 0 成功 -1 手机号码为空 -2  手机号码已被注册 -3 登陆密码为空
)
as
 temp_out varchar2(500);
 temp_SaleId number(10);
 temp_fatherId number(10);
 temp_SPwd varchar2(20);
 temp_LoginName varchar2(100);
 temp_count number(5);
begin
    temp_fatherId:=In_Fid;
    temp_SPwd:=In_Pwd;
    temp_LoginName:=In_phone;

    /*判断手机号吗(必须)*/
    if temp_LoginName is null then
        temp_out:='-1';
        goto ret_end;
     end if;

     /*判断手机号码是否已被注册*/
     select count(*) into temp_count from wl_xs_salesman where loginname=temp_LoginName;
     if temp_count>0 then
         temp_out:='-2';
        goto ret_end;
     end if;

     /*判断密码*/
     if temp_SPwd is null then
         temp_out:='-3';
        goto ret_end;
     end if;

     /*判断父ID*/
     if temp_fatherId is null then
         temp_fatherId:=0;
     end if;

     /*从序列获取销售编号*/
     temp_SaleId:=sequ_sale_Id.Nextval;
     insert into wl_xs_salesman(saleID,fsaleid,loginname,spwd,SName,SGender,
                                 SIDCard,saddress,SPhone,SNode,SCreateDate)
         values(temp_SaleId,temp_fatherId,temp_LoginName,In_Pwd,In_SName,In_SGender,
                                  In_Idcard,In_SAddress,In_phone,In_Note,sysdate);
     commit;
     temp_out:='0';
     <<ret_end>>
     Out_state:=temp_out;
end;
/

prompt
prompt Creating procedure WL_SALE_SACH_DELETE
prompt ======================================
prompt
create or replace procedure wl_sale_sach_delete(
   Cust_id in varchar2,--司机ID
   Current_id in varchar2,--当前销售编号
   Out_state out number--操作状态 0 操作成功 -1 销售编号有误 -2 该司机不属于该销售编号
)
as
temp_count number(5);
temp_out number(5);
temp_id varchar2(80);
begin
     ----检测当前销售编号
    temp_id:=Current_id;
    
    if regexp_like(temp_id,'salemanager','i') then
      goto ret_next;
    end if;  
    select count(*) into temp_count from wl_xs_salesman where saleid=temp_id;
    if temp_count=0 then
      temp_out:=-1;
      goto ret_end;
    end if;
    <<ret_next>>
    if regexp_like(temp_id,'salemanager','i') then
       temp_id:='0';
    end if;
    --
    select count(*) into temp_count from wl_xs_salesach where saleid=temp_id and oppsiteid=Cust_id;
    if temp_count=0 then
      temp_out:=-2;
      goto ret_end;
    end if;

    delete wl_xs_salesach where saleid=temp_id and oppsiteid=Cust_id;
    commit;
    Out_state:=0;
   <<ret_end>>
   Out_state:=temp_out;
end;
/

prompt
prompt Creating procedure WL_SALE_SELECTSALE
prompt =====================================
prompt
CREATE OR REPLACE Procedure WL_Sale_selectSale(
  In_SaleID in varchar2,--登陆账号 saleManager 销售经理
  In_Txt in varchar2,--查询关键字
  In_PageIndex in number,--当前页面
  In_pageSize in number,--每页显示的条收
  Out_counst out number,--总记录数
  Ret_cur out sys_refcursor--返回查询的结果集
)
as
begin
   if regexp_like(In_SaleID,'saleManager','i') then
      open Ret_cur for
        select * from
        (
         select saleid,fsaleid,loginname,spwd,sname,sgender,sidcard
         ,sphone,saddress,snode,screatedate,state,sumsale,sumup,rownum nu from (
          select * from wl_xs_salesman where fsaleid=0 
           ) t
           where t.loginname like In_Txt||'%'
           or   t.sname  like In_Txt||'%'
           or t.sgender like In_Txt||'%'
           or t.sidcard like In_Txt||'%'
           or t.saddress like In_Txt||'%'
           or t.sphone like In_Txt||'%'
           ) c
           where c.nu<=(In_PageIndex)*In_pageSize and c.nu>(In_PageIndex-1)*In_pageSize;

            select count(*) into Out_counst from
             (
              select saleid,fsaleid,loginname,spwd,sname,sgender,sidcard
              ,sphone,saddress,snode,screatedate,state,sumsale,sumup,rownum nu from (
              select * from wl_xs_salesman where fsaleid=0 
            ) t
           where t.loginname like In_Txt||'%'
           or   t.sname  like In_Txt||'%'
           or t.sgender like In_Txt||'%'
           or t.sidcard like In_Txt||'%'
           or t.saddress like In_Txt||'%'
           or t.sphone like In_Txt||'%'
           ) ;
   else
        open Ret_cur for
        select * from
        (
         select saleid,fsaleid,loginname,spwd,sname,sgender,sidcard
         ,sphone,saddress,snode,screatedate,state,sumsale,sumup,rownum nu from (
          select * from wl_xs_salesman where fsaleid=In_SaleID 
           ) t
           where t.loginname like In_Txt||'%'
           or   t.sname  like In_Txt||'%'
           or t.sgender like In_Txt||'%'
           or t.sidcard like In_Txt||'%'
           or t.saddress like In_Txt||'%'
           or t.sphone like In_Txt||'%'
           ) c
           where c.nu<=In_PageIndex*In_pageSize and c.nu>(In_PageIndex-1)*In_pageSize;

            select count(*) into Out_counst from
             (
              select saleid,fsaleid,loginname,spwd,sname,sgender,sidcard
              ,sphone,saddress,snode,screatedate,state,sumsale,sumup,rownum nu from (
              select * from wl_xs_salesman where fsaleid=In_SaleID 
            ) t
           where t.loginname like In_Txt||'%'
           or   t.sname  like In_Txt||'%'
           or t.sgender like In_Txt||'%'
           or t.sidcard like In_Txt||'%'
           or t.saddress like In_Txt||'%'
           or t.sphone like In_Txt||'%'
           ) ;
   end if;
end;
/

prompt
prompt Creating procedure WL_SALE_SELECT_LOGIN
prompt =======================================
prompt
create or replace procedure WL_sale_Select_Login(
  in_phone  in varchar2,--登陆手机号码
  In_pwd in varchar2,---登陆密码
  Ret_out out sys_refcursor--—返回结果集
  )
as
begin
  open  Ret_out for select * from wl_xs_salesman where loginname=in_phone and spwd=In_pwd and state = 0;
end;
/

prompt
prompt Creating procedure WL_SALE_UPDATE
prompt =================================
prompt
create or replace procedure WL_sale_update(
    in_saleID in  number,
    in_fsaleid in number,
    in_loginname in varchar2,
    in_spwd in varchar2,
    in_sname in varchar2,
    in_sgender in varchar2,
    in_sidcard in varchar2,
    in_sPhone in varchar2,
    in_address in varchar2,
    in_snode in varchar2,
    in_sumsale in number,
    in_sumup in varchar2,
    ret_out out  varchar2 --0 修改成功 -1 当前用户获取失败 -2 登陆名不能为空 -3 电话号码为空
)
as
  retcount number(4):=0;
  retStr varchar2(100):='';
begin
   --销售编号
    if in_saleID is null then
      retStr:='-1';
      goto  ret_end;
    end if;
    --电话号码
    if in_loginname is null then
      retStr:='-2';
      goto  ret_end;
    end if;
    --电话号码
    if in_sPhone is null then
      retStr:='-3';
      goto  ret_end;
    end if;

    select count(*) into retcount  from WL_XS_salesman where saleId=in_saleID;
    if retcount=0 then
        retStr:='-1';
      goto  ret_end;
    end if;
    --存在则更新
    update WL_XS_salesman set Fsaleid=in_fsaleid, Loginname=in_loginname, spwd=in_spwd,saddress=in_address, sName=in_sname,sGender=in_sgender,
      sIDCard=in_sidcard,sPhone=in_sPhone,sNode=in_snode,sumsale=in_sumsale,sumup=in_sumup
      where saleId=in_saleID;
      retStr:='0';
      goto ret_end;
  <<ret_end>>
  ret_out:=retStr;
end;
/

prompt
prompt Creating procedure WL_SALE_UPDATECUSTMOBILE
prompt ===========================================
prompt
create or replace procedure WL_sale_UpdateCUSTMOBILE(
  CUST_ID in varchar2,--主键编号
  Cqq in varchar2,--QQ
  runLine in varchar2,--长跑路线
  carType in varchar2,--车型
  cname in varchar2,--姓名
  Phones in varchar2,--手机
  carNum in varchar2,--车牌号
  Ret_state out number--操作状态 0 成功 -1 该号码不存在
  )
  as
   selecount number(5);
  begin
   select count(CUST_ID) into selecount from wl_cust_mobile where CUST_ID=CUST_ID;
    if selecount>0 then--存在则更新
       update wl_cust_mobile set QQ=Cqq,C_NAME=cname,CAR_NUMBER=carNum, CAR_MODEL=carType,
       LIKE1=runLine where NBR=Phones;
       commit;
       Ret_state:=0;
     else
        Ret_state:=-1;
      end if;
  end;
/

prompt
prompt Creating procedure WL_SMS_ZST_OPEARE_OPDETAIL_1
prompt ===============================================
prompt
create or replace procedure WL_SMS_ZST_OPEARE_OPDETAIL_1(
  MessID in varchar2,--消息ID
  In_phoneNum in varchar2,--手机号码
  In_Op in varchar2,--操作
  In_Mess in varchar2,--输入的内容
  Out_mess out varchar2--输出信息
  )
as
 temp_Count number(5):=0;--临时储存记录数
 temp_OutMess varchar2(300):='';--临时储存输出信息
 temp_Str varchar2(300):='';--临时储存字符串
 temp_str1 varchar2(300):='';--临时处理字符串2
 startStr varchar2(500):='';--起始地
 endStr varchar2(500):='';--目的地
 In_obj varchar2(500):='';--临时对象
 begin

  ----------------------判断该用户是否存在----------------
  select count(*) into temp_count from Wl_Cust_Mobile where nbr=In_phoneNum;

  --------------------获取客户端地址链接------------------
  if regexp_like(In_Op,'getrj','i') then
      temp_OutMess:='http://down.zhaohuobao.cn/ZHBao.apk';
      sp_down_insert(MessID,In_phoneNum ,'mandao',temp_OutMess,0 ,temp_count);
  end if;

  ------------------------订购业务------------------------
  if regexp_like(In_Op,'order','i') then
    ----该用户存在
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=1,Eff_Date=add_months(sysdate,to_number(30)) where nbr=In_phoneNum;
      select max(cust_ID) into temp_str from Wl_Cust_Mobile where nbr=In_phoneNum;
     --该用户不存在
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,add_months(sysdate,1),1) return CUST_ID into temp_str;
    end if;
     --生成订单
      wl_cust_order(temp_str,'10元包月',1,'自订购',sysdate,null,In_phoneNum,temp_OutMess);
      temp_OutMess :='感谢您订购找货宝业务!请回复“RJ”下载安卓手机软件;短信查询:请回复“某地到某地”。QQ查询:请加2577556895为好友。【找货宝】';
      sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------------取消业务--------------------------
  if regexp_like(In_Op,'exit','i') then
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=0,Eff_Date=sysdate where nbr=In_phoneNum;
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,sysdate,0);
    end if;
    insert into WL_SMS_ZST_SERVICE(PhoneNum,BizSate) values(In_phoneNum,'取消业务');
    WL_CUST_CannelOrder(In_phoneNum,temp_OutMess);
    temp_OutMess :='手机号码：'||In_phoneNum||'取消成功【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------绑定QQ----------------------------------
  if In_Op='bdqq' then
       In_obj:=regexp_substr(In_Mess,'\d+');
      ----------------查找是否存在其他人占用此QQ----------
      select count(*) into temp_Count from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------没有其他人使用此qq-----------------------
      if temp_Count=0 then
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
      ---------------查看存在该QQ的电话是否为空-----------
      select max(nbr) into temp_Str from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------存在qq 的电话为空则删除该条信息并将qq绑定到该号码下-----
      if temp_Str is null then
         delete  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum and (nbr is null);
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      else--否则去掉对方的qq并绑定自己QQ
         update Wl_Cust_Mobile set qq='' where qq=In_obj and nbr<>In_phoneNum;
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
   end if;

  ---司机的手机绑定销售编号 成为该销售编号的意向用户-----
  if regexp_like(In_op,'bdxs','i') then
     In_obj:=regexp_substr(In_Mess,'\d+');
     select max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       if temp_Str is null then
          insert into Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Last_Position_Date,Isorderssxy)values(In_phoneNum,sysdate,sysdate,sysdate,2);
           commit;
       end if;
       select max(CUST_ID) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       select count(*) into temp_Count  from wl_xs_salesach where oppsiteid=temp_Str;
       if temp_Count>0 then
           temp_OutMess:='该司机已被其他销售人员绑定';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
       else
           select max(saleid) into temp_count from wl_xs_salesman where sphone=In_obj;
           if temp_count is null then
               temp_OutMess:='此销售编号有误！';
               sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
               goto ret_end;
            else
                insert into wl_xs_salesach(saleid,xstype,oppsiteid,xstime,translation) values(temp_count,'司机',temp_Str,sysdate,0);
                temp_OutMess:='绑定成功！';
                sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
                commit;
           end if;
       end if;
   end if;

  -----------------销售人员开通司机手机号-----------------
  if regexp_like(In_op,'ktsj','i') then

       -------------验证销售编号-----------------
        select count(*) into temp_Count from Wl_Xs_Salesman where sphone=In_phoneNum and state=0;
        if temp_Count=0 then
           temp_OutMess:='您发短信的手机非找货宝销售人员手机，不能为此号码开通找货宝业务，请让销售人员帮您开通，感谢您对找货宝的支持4006282515【找货宝】';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
           goto ret_end;
        end if;
        In_obj:=regexp_substr(In_Mess,'\d+');

        -------------判断该司机是否存在本库中-----
        select Max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_obj;
        if temp_Str is null  then
           insert into Wl_Cust_Mobile(Nbr,Create_Date,Isorderssxy,Isreg,Serchtimes)values(In_obj,sysdate,0,0,0)
            return CUST_ID into temp_Str;
        end if;

        ------判断该司机是否重复订购-------------
        select count(*) into temp_Count from wl_orderinfo where
             cust_id=temp_str and (expiry_date is null or expiry_date>sysdate);
        if temp_Count>0 then
            temp_OutMess:='销售代理您好，该号码已开通找货宝业务，无需重新开通，如有疑问，请咨询总部工作人员';
            sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
            goto ret_end;
        end if;

        ----判断该手机号码是否被销售编号绑定----
        select count(*) into temp_Count from wl_xs_salesach where  oppsiteid=temp_Str;
        if temp_Count>0 then

             ---获取该的所属销售员
             select max(sphone) into temp_str1 from wl_xs_salesman where SaleId in(
                select Max(SALEID) from Wl_Xs_Salesach  where oppsiteid=temp_Str
             );

             ----给该销售员发送信息
             temp_OutMess:='手机号为15129030647已被其他销售员开通，将不再是您的意向用户';
             sp_down_insert('',temp_str1 ,'mandao' ,temp_OutMess ,0 ,temp_count);

             ---删除原来有的意向用户
             delete wl_xs_salesach where oppsiteid=temp_Str;
        end if;

         ------------------给司机开通12个月----------
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(12)), isORDERSSXY=1 where nbr=In_obj;
         commit;

         --------根据手机号码获取销售人员的销售编号--
         select max(saleid) into temp_str1 from wl_xs_salesman where sphone=In_phoneNum;

         ---------------------生成订单----------------
         wl_cust_order(temp_Str,'120元包年',1,'销售人员代购',sysdate,add_months(sysdate,12),temp_str1,temp_OutMess);

         ------------给销售代理下发信息---------------
         temp_OutMess:='销售代理您好,您已经帮手机号码为'||In_obj||'的用户开通找货宝业务，请指导用户使用此业务，感谢对找货宝的支持 ';
         sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);

         ---------------给司机下发信息-----------------
         temp_OutMess:='尊敬的用户您好！您已经成功开通找货宝业务，感谢对找货宝的支持，请让销售人员指导您使用此业务，如有疑问，请咨询4006282515';
         sp_down_insert('',In_obj ,'mandao' ,temp_OutMess ,0 ,temp_count);

         ---------------------给当前销售员的总销售量加--
         update wl_xs_salesman set sumsale=sumsale+1 where saleid=temp_str1;
         commit;

   end if;

  ------------------获取货源信息--------------------------
  if regexp_like(In_Op,'getinfo','i') then

    -----------------------判断该用户是否订购业务-------------
    select max(isorderssxy) into temp_count from wl_cust_mobile where nbr=In_phoneNum;
    if temp_count=0 or temp_count is null then
      temp_OutMess:='您尚未订购找货宝短信业务，无法使用该功能，若要使用请发送5137到10669588订购【找货宝】';
      goto ret_end;
    end if;

    -------------------------查询信息--------------------------
    WL_Cust_ZHBQQ_GetAddress(In_Mess,startStr,endStr);

    -----------------------地址格式有误------------------------
    if startStr is null or endStr is null then
      temp_OutMess :='您输入的格式有误请输入:某地到某地【找货宝】';
      goto ret_end;
    end if;

    -----------------------地址格式正确-------------------------
    temp_OutMess:=get_line_info_TXH_top(In_phoneNum,1,startStr,endStr);

    ------------------------更新最后一次查询时间----------------
    update wl_cust_mobile set
           last_position_date=sysdate ,
           last_search=startStr||'到'||endStr,
           last_serch_data=Sysdate
           where  nbr=In_phoneNum;
    commit;

    -----------------------插入到短信记录中去------------------------
    insert into WL_SMS_ZST_Messages(PHONENUM,IN_MESS,OUT_MESS)
           values(In_phoneNum,In_Mess,temp_OutMess);
    commit;

    -------------------------查询记录条数-----------------------------
    select count(*) into temp_count from WL_info_data
               where info_start like startStr||'%'
               and info_end like endStr||'%';
    temp_OutMess:=temp_OutMess||'相关信息共有'||temp_count||'条,继续发送获取其他信息【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
   end if;
   ---------------结束点-----------------
   <<ret_end>>
   Out_mess:=temp_OutMess;
 end;
/


spool off
