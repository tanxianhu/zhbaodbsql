----------------------------------------------------
-- Export file for user HR                        --
-- Created by Administrator on 2013-6-6, 12:13:06 --
----------------------------------------------------

spool zhbao_v1_0_0.log

prompt
prompt Creating package WL_CUST_DEPARTMENT_GETINFO
prompt ===========================================
prompt
create or replace package wl_cust_department_getinfo as
--3.显示用户已发送信息（显示自己所有发布的信息）
--输入参数：in_nbr 号码
--输出游标进行显示
type t_info is ref cursor;
procedure p_info(in_nbr in varchar2,cur_ref out t_info);

end wl_cust_department_getinfo;
/

prompt
prompt Creating package WL_CUST_DEPARTMENT_GETTRUCK
prompt ============================================
prompt
create or replace package wl_cust_department_gettruck as
--4.显示附近车辆信息（显示信息部客户表的一个号码,对应**公里以内的手机客户信息）
--输入参数：in_nbr,IN_KM  信息部中某一个号码,公里数
--输出游标进行显示

type t_info is ref cursor;

procedure p_truck(in_nbr in varchar2,in_km in number,cur_ref out t_info);
  tmp_ct    number(10);
  nbr_lng   number(11,8);
  nbr_lat   number(11,8);
  tmp_str   varchar2(500);
  
end wl_cust_department_gettruck;
/

prompt
prompt Creating package WL_CUST_DEP_CONTACT_GETINF
prompt ===========================================
prompt
create or replace package wl_cust_dep_contact_getInf as
type contactInfoType is ref cursor;
end;
/

prompt
prompt Creating package WL_CUST_DEP_GETINFO
prompt ====================================
prompt
create or replace package wl_cust_dep_getinfo as
--F2：显示当前位置附近信息部、停车场的信息
--输入参数：in_sim,in_nbr,in_esn,in_imsi,in_lng,in_lat,in_city,in_km,c_type,key_wd
--1输入参数in_sim或in_lng或in_lat任一为空
--2客户不存在
--0成功
  type t_info is ref cursor;
  procedure p_info
    (in_sim  in varchar2  ---用户sim卡号（必选字段）
    ,in_nbr  in varchar2  ---用户号码
    ,in_esn  in varchar2  ---用户手机设备号码
    ,in_imsi in varchar2
    ,in_lng  in number    ---用户所在位置经度信息
    ,in_lat  in number    ---用户所在位置纬度信息
    ,in_city in varchar2  ---城市名称，可为空
    ,in_km   in number    ---附近公里数
    ,in_c_type  in varchar2  ---类型表示信息部或停车场
    ,in_key_wd  in varchar2  ---信息部客户表的模糊客户名称
    ,in_send_nbr  in varchar2  ----信息发送者名字
    ,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6成功
    ,cur_ref out t_info   ---返回记录集
 );
 
end wl_cust_dep_getinfo;
/

prompt
prompt Creating package WL_CUST_FRIEND_LIST
prompt ====================================
prompt
create or replace package wl_cust_friend_list as
 --好友分组展示
 --输入参数：客户ID
 --输出参数：好友游标集 ret 1输入参数为空,2无此客户,0成功

 type t_info is ref cursor;
  procedure p_info
    (in_cust_id        in varchar2
    ,ret               out number   ---返回参数
    ,cur_ref           out t_info   ---返回记录集
    );
end wl_cust_friend_list;
/

prompt
prompt Creating package WL_CUST_FRIEND_QUERY
prompt =====================================
prompt
create or replace package wl_cust_friend_query as
 --好友列表展示
 --输入参数：客户ID
 --输出参数：好友游标集 ret 1输入参数为空,2无此客户,0成功

 type t_info is ref cursor;
  procedure p_info
    (in_cust_id        in varchar2
    ,ret               out number   ---返回参数
    ,cur_ref           out t_info   ---返回记录集
    );
end wl_cust_friend_query;
/

prompt
prompt Creating package WL_CUST_GETINFO
prompt ================================
prompt
create or replace package wl_cust_getinfo as
/*
  XueHaiFeng
  LastModified:2013-03-27
----
--获取货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     0更新用户位置成功
     9新用户创建位置成功 
--8按经纬查无货源信息
--7按线路查无货源信息
*/
  type t_info is ref cursor;
  procedure p_info
    (in_imsi in varchar2,  --用户imsi卡号（必选字段）
     in_line in varchar2,  ---指定线路:可空
     ret     out number,   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
     cur_ref out t_info   ---返回记录集
    );
end wl_cust_getinfo;
/

prompt
prompt Creating package WL_CUST_GETINFO_T
prompt ==================================
prompt
create or replace package wl_cust_getinfo_t as
/*
  XueHaiFeng
  LastModified:2013-03-27
----
--获取货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     0更新用户位置成功
     9新用户创建位置成功 
--8按经纬查无货源信息
--7按线路查无货源信息
*/
  type t_info is ref cursor;
  procedure p_info
    (in_imsi in varchar2,  --用户imsi卡号（必选字段）
     in_line in varchar2,  ---指定线路:可空
     ret     out number,   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
     cur_ref out t_info   ---返回记录集
    );
end wl_cust_getinfo_t;
/

prompt
prompt Creating package WL_CUST_MOBILE_CITYLIST
prompt ========================================
prompt
create or replace package wl_cust_mobile_citylist as
--F6:显示当前位置货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     /*0更新用户位置成功
       9新用户创建位置成功*/ 
--8按经纬查无货源信息
--7按线路查无货源信息
 type t_info is ref cursor;
  procedure p_info
    (in_sim  in varchar2  ---用户sim卡号（必选字段）
    ,in_nbr  in varchar2  ---用户号码
    ,in_esn  in varchar2  ---用户手机设备号码
    ,in_imsi in varchar2
    ,in_lng  in number    ---用户所在位置经度信息
    ,in_lat  in number    ---用户所在位置纬度信息
    ,in_line in varchar2  ---指定线路:可空
    ,in_ct   in varchar2  ---需要返回行数
    ,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
    ,cur_ref out t_info   ---返回记录集
    );
end wl_cust_mobile_citylist;
/

prompt
prompt Creating package WL_CUST_MOBILE_GETINFO
prompt =======================================
prompt
create or replace package wl_cust_mobile_getinfo as
--6.根据号码或SIM，查手机客户表的信息
--输入参数：instr 号码或SIM其中一个
--输出游标进行显示:cur_ref
type t_info is ref cursor;
procedure p_info(instr in varchar2,cur_ref out t_info);

end wl_cust_mobile_getinfo;
/

prompt
prompt Creating package WL_CUST_MOBILE_GETINFO6
prompt ========================================
prompt
create or replace package wl_cust_mobile_getinfo6 as
--F6:显示当前位置货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     /*0更新用户位置成功
       9新用户创建位置成功*/ 
--8按经纬查无货源信息
--7按线路查无货源信息
 type t_info is ref cursor;
  procedure p_info
    (in_sim  in varchar2  ---用户sim卡号（必选字段）
    ,in_nbr  in varchar2  ---用户号码
    ,in_esn  in varchar2  ---用户手机设备号码
    ,in_imsi in varchar2
    ,in_lng  in number    ---用户所在位置经度信息
    ,in_lat  in number    ---用户所在位置纬度信息
    ,in_line in varchar2  ---指定线路:可空
    ,in_ct   in varchar2  ---需要返回行数
    ,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
    ,cur_ref out t_info   ---返回记录集
    );
end wl_cust_mobile_getinfo6;
/

prompt
prompt Creating package WL_CUST_SMS_FIND
prompt =================================
prompt
create or replace package wl_cust_sms_find as
 --短信2:找出待发短信记录
 --输入参数：多少个短信ID,
 --输出参数：游标集：短信ID,收信人号码,短信内容,
 --ret 0为正常返回。1输入条数为空或小于1或大于100，2无待发数据
 type t_info is ref cursor;
  procedure p_info
    (in_ct   in number    ---输入索取待发多少条短信
    ,ret     out number   ---返回参数
    ,cur_ref out t_info   ---返回记录集
    );
end wl_cust_sms_find;
/

prompt
prompt Creating package WL_GET_CUST_DEP
prompt ================================
prompt
create or replace package wl_get_cust_dep as
 --输入cust_id,
 --输出参数：信息部客户表:wl_cust_department游标集
 --ret 0为正常返回。1无待发数据
 type t_info is ref cursor;
  procedure p_info
    (in_cust_id   in varchar2
    ,ret          out number
    ,cur_ref      out t_info   ---返回记录集
    );
end wl_get_cust_dep;
/

prompt
prompt Creating package WL_MOBILE_RING_QUERY
prompt =====================================
prompt
create or replace package wl_mobile_ring_query as
--外呼管理
--1.随机获取5条，最近外呼时间最早 并且最近外呼时间大于4小时 并且 状态字段为1
--的五条信息，然后更改此5条记录的“最近外呼时间”
--存储过程获取 联系人电话 姓名 车号 QQ号码 车型 常跑路线  手机类型  
--外呼人  最近外呼时间。

 type t_info is ref cursor;
  procedure p_info
(in_ct   in varchar2  ---需要返回行数
,ret     out number   ---返回参数0为正常返回。1输入参数错，2无符合记录
,cur_ref out t_info   ---返回记录集
 );
end wl_mobile_ring_query;
/

prompt
prompt Creating function GET_CUST_ADDRESS
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_cust_address (message in varchar2)
       RETURN VARCHAR2
--获取客户名称、电话、地址
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin

  if instr(message,'我的')>0 then
  --3.地址
     if instr(message,'地址')>0 and
        regexp_substr (message,'(地址)(是)?(:|：)?\w{1,30}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(地址)(是)?(:|：)?\w{1,30}');
        out_message   :=regexp_replace(tmp_message_1,'(地址)(是)?(:|：)?');
     end if;

  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_AMOUNT
prompt =================================
prompt
CREATE OR REPLACE FUNCTION get_cust_amount (message in varchar2)
       RETURN VARCHAR2
--获取客户预付款，单位元
  is
   out_message        varchar2(500);

  begin

  --2.电话
     if   regexp_substr(message,'(金额)\d{1,5}') is not null
     then
        out_message :=regexp_substr(message,'(金额)\d{1,5}') ;
        out_message :=substr(out_message,3);

     end if;


  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_CITY
prompt ===============================
prompt
create or replace function get_cust_city (message in varchar2)
return varchar2
--获取客户密码
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);
  begin
  --1.名称
     if regexp_substr (message,'(城市)(是)?(:|：)?\w{1,30}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(城市)(是)?(:|：)?\w{1,30}');
        out_message   :=regexp_replace(tmp_message_1,'(城市)(是)?(:|：)?');
     end if;


  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_CITY_ID
prompt ==================================
prompt
create or replace function get_cust_city_id
(in_nbr in varchar2)
   return varchar2
  --输入nbr，查出信息部客户表city，输出wl_info_data_region表中serial
  is
  tmp_city varchar2(30);
  out_city_id   varchar2(500);
  begin
  

   select max(city) into tmp_city
   from wl_cust_department
   where nbr=trim(in_nbr) or qq=trim(in_nbr);  
   
   if tmp_city is null 
   then goto ret_end;
   end if;
   
   select max(serial) into out_city_id from
   wl_info_data_region where id in(1,2) and pro||city=tmp_city;

   if out_city_id is null 
   then goto ret_end;
   end if;

   out_city_id:='#P'||out_city_id||'#';

 <<ret_end>>
   return(out_city_id);
  end;
/

prompt
prompt Creating function GET_CUST_CONTACT
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_cust_contact (message in varchar2)
       RETURN VARCHAR2
--获取客户名称、电话、地址
  is
   out_message        varchar2(500);

  begin

  if instr(message,'我的')>0 then
  --2.电话
     if (instr(message,'电话')>0  or instr(message,'手机')>0 )and
        regexp_substr(message,'1\d{10}') is not null
     then
        out_message :=regexp_substr(message,'1\d{10}');
     end if;
  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_CONTACT2
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION get_cust_contact2 (message in varchar2)
       RETURN VARCHAR2
--获取客户名称、电话、地址
  is
   out_message        varchar2(500);

  begin

  --2.电话
     if   regexp_substr(message,'((电话)|(手机))1\d{10}') is not null
     then
        out_message :=regexp_substr(message,'((电话)|(手机))1\d{10}') ;
        out_message :=substr(out_message,3);
     
     end if;


  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_CONTACT3
prompt ===================================
prompt
create or replace function get_cust_contact3
(in_nbr in varchar2)
   return varchar2
  --输入nbr，输出wl_cust_department表中contact
  is

  out_contact   varchar2(100);
  begin


    select max(contact) into out_contact
    from wl_cust_department
    where nbr=trim(in_nbr);  

    return(out_contact);
  end;
/

prompt
prompt Creating function GET_CUST_NAME
prompt ===============================
prompt
CREATE OR REPLACE FUNCTION get_cust_name (message in varchar2)
       RETURN VARCHAR2
--输入物流信息串,输出串中车型
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin

  if instr(message,'我的')>0 then
  --1.名称
     if regexp_substr (message,'(名称|名字)(是|叫)?(:|：)?\w{1,15}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(名称|名字)(是|叫)?(:|：)?\w{1,15}');
        out_message   :=regexp_replace(tmp_message_1,'(名称|名字)(是|叫)?(:|：)?');
     end if;

  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_NAME2
prompt ================================
prompt
CREATE OR REPLACE FUNCTION get_cust_name2 (message in varchar2)
       RETURN VARCHAR2
--输入物流信息串,输出串中车型
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin


  --1.名称
     if regexp_substr (message,'(名称|名字)(是|叫)?(:|：)?\w{1,15}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(名称|名字)(是|叫)?(:|：)?\w{1,15}');
        out_message   :=regexp_replace(tmp_message_1,'(名称|名字)(是|叫)?(:|：)?');
     end if;


  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_PASSWORD
prompt ===================================
prompt
create or replace function get_cust_password (message in varchar2)
return varchar2
--获取客户密码

  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin


  --1.名称
     if regexp_substr (message,'(密码)(是)?(:|：)?\w{6,30}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(密码)(是)?(:|：)?\w{6,30}');
        out_message   :=regexp_replace(tmp_message_1,'(密码)(是)?(:|：)?');
     end if;


  return (out_message);
end;
/

prompt
prompt Creating function GET_CUST_QQ
prompt =============================
prompt
CREATE OR REPLACE FUNCTION get_cust_QQ (message in varchar2)
       RETURN VARCHAR2
--获取客户名称、电话、地址
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin



     if regexp_substr (message,'([Q]{2}|[q]{2})[0-9]{1,20}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'([Q]{2}|[q]{2})[0-9]{1,20}');
        out_message   :=regexp_substr(tmp_message_1,'[0-9]{1,20}');
     end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_DEP_CITY_BYNBR
prompt ====================================
prompt
create or replace function get_dep_city_bynbr
 (in_nbr in varchar2)

/*输入参数：QQ号
输出参数：信息部客户表城市*/

 return  varchar2
 is

  out_city        varchar2(100);

 begin

     select city into out_city from wl_cust_department
     where nbr=trim(in_nbr);


  return (out_city);
end;
/

prompt
prompt Creating function GET_RAD
prompt =========================
prompt
CREATE OR REPLACE FUNCTION get_rad(d number) RETURN NUMBER
--专用于函数1.get_distance
is
PI number :=3.141592625;
begin
return d* PI/180.0;
end ;
/

prompt
prompt Creating function GET_DISTANCE
prompt ==============================
prompt
CREATE OR REPLACE FUNCTION get_distance(lng1 number,lat1 number,
                                        lng2 number,lat2 number
                                        ) RETURN NUMBER is
--输入两对经纬,输出距离KM
   earth_padius number := 6378.137;
   radLat1      number := get_rad(lat1);
   radLat2      number := get_rad(lat2);
   a            number := radLat1 - radLat2;
   b            number := get_rad(lng1) -get_rad(lng2);
   s            number := 0;
 begin
   s := 2 *
        Asin(Sqrt(power(sin(a / 2), 2) +
                  cos(radLat1) * cos(radLat2) * power(sin(b / 2), 2)));
   s := s * earth_padius;
   s := Round(s * 10000) / 10000;
   return s;

end;
/

prompt
prompt Creating function GET_DRIVER_CARNBR
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION get_driver_carnbr (message in varchar2)
       RETURN VARCHAR2
--获取司机qq号、车牌、电话
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin

  if instr(message,'司机')>0 then
  --2.车牌
     if (instr(message,'车牌')>0 or instr(message,'车号')>0)and
        regexp_substr (message,'(车牌|车号)(号)?(是)?(:|：)?\w{7}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(车牌|车号)(号)?(是)?(:|：)?\w{7}');
        out_message   :=regexp_replace(tmp_message_1,'(车牌|车号)(号)?(是)?(:|：)?');
     end if;

  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_DRIVER_NBR
prompt ================================
prompt
CREATE OR REPLACE FUNCTION get_driver_nbr (message in varchar2)
       RETURN VARCHAR2
--获取司机qq号、车牌、电话
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin

  if instr(message,'司机')>0 then
  --3.电话
     if (instr(message,'电话')>0  or instr(message,'手机')>0 )and
        regexp_substr(message,'1\d{10}') is not null
     then
        out_message :=regexp_substr(message,'1\d{10}');
     end if;
  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_DRIVER_QQ
prompt ===============================
prompt
CREATE OR REPLACE FUNCTION get_driver_qq (message in varchar2)
       RETURN VARCHAR2
--获取司机qq号、车牌、电话
  is
   out_message        varchar2(500);
   tmp_message_1      varchar2(200);

  begin

  if instr(message,'司机')>0 then
  --1.qq号
     if (instr(message,'qq')>0 or instr(message,'QQ')>0)and
        regexp_substr (message,'(QQ|qq)(号)?(是)?(:|：)?\d{1,20}') is not null
     then
        tmp_message_1 :=regexp_substr(message,'(QQ|qq)(号)?(是)?(:|：)?\d{1,20}');
        out_message   :=regexp_replace(tmp_message_1,'(QQ|qq)(号)?(是)?(:|：)?');
     end if;

  end if;

  return (out_message);
end;
/

prompt
prompt Creating function GET_HOW_LONG
prompt ==============================
prompt
create or replace function get_how_long(in_date in date)
return varchar2 
  --输入时间,输出距离现在时长
 is
    tmp_date  VARCHAR2 (200);
    out_info  VARCHAR2 (200);
begin
    tmp_date:=sysdate-in_date;
    case 
    when tmp_date>1       then out_info:=trunc(tmp_date)||'天前发布';
    when tmp_date*24>1    then out_info:=trunc(tmp_date*24)||'小时前发布';
    when tmp_date*24*60>1 then out_info:=trunc(tmp_date*24*60)||'分钟前发布';
    else out_info:='1分钟前发布';
    end case;

        return (out_info);
end;
/

prompt
prompt Creating function GET_INTERVAL
prompt ==============================
prompt
create or replace function get_interval(in_sim in varchar2,in_info_id number )
return number 
--1000分=16小时，0分1000，1分500，2分330，4分200，5分166，10分90，20分47
--16小时后变为0
 is
    ret   number;
begin
  ret:=0;
select count(*) into ret
   FROM  wl_info_data_log
  where sim=in_sim
    and info_id= in_info_id;

   if  ret <>1 then 
       return -1;
   end if;
 -- select(case when  (sysdate-create_date)*24*60<30 
 --             then  trunc(-180+(sysdate-create_date)*24*60*6) 
--              else  -2
 --        end )   into ret
 
select trunc(1000/((sysdate-create_date)*24*60+1),2) into ret
  FROM  wl_info_data_log
  where sim=in_sim
    and info_id= in_info_id;
        return (ret);
end;
/

prompt
prompt Creating function GET_SQL_TOP40
prompt ===============================
prompt
CREATE OR REPLACE FUNCTION get_sql_top40 (in_sql in varchar2)
     RETURN VARCHAR2
---输入表记录,输出前40条记录
   is
     out_info  VARCHAR2 (1000);
   begin
   out_info:='select * from ('||in_sql||') where rownum<=40  ';
   
   RETURN (out_info);
end;
/

prompt
prompt Creating function GET_INFO_ID
prompt =============================
prompt
CREATE OR REPLACE FUNCTION get_info_id(in_sim varchar2
                                      ,in_lng number
                                      ,in_lat number)
--可变参数：
--输入经纬度和信息起始地经纬度相减在'2'内
--当前时间和信息创建时间在'6'小时内
--当前时间和信息创建时间每'5'分钟时间间隔

     RETURN varchar2 
                  is               
   tmp_str  varchar2(500);
   tmp_ret_str  varchar2(500);
   tmp_id number(30);
   type cursor_type is ref cursor;
   c1 cursor_type;
 begin
 
   
tmp_str:=
'select id from wl_info_data where 1=1 and    
(lng_start-'||in_lng||'<2 and lng_start-'||in_lng||'>-2) and
(lat_start-'||in_lat||'<2 and lat_start-'||in_lat||'>-2)
order by get_info_value
(
num
,-((lng_start-'||in_lng||')*(lng_start-'||in_lng||')+
   (lat_start-'||in_lat||')*(lat_start-'||in_lat||')
  )
,-get_interval('||in_sim||',Id)
)
-(sysdate-create_date)*24*60/5 desc';

 
/*tmp_str:='';
tmp_str:=tmp_str||'select id from wl_info_data where 1=1 '; 
   tmp_str:=tmp_str||' and  abs(lng_start-'||in_lng||')<2 and  abs(lat_start-'||in_lat||')<2'; 
   --tmp_str:=tmp_str||' and (sysdate-create_date)*24<6';
   tmp_str:=tmp_str||' order by get_info_value(num,
                       -get_distance(lng_start,lat_start,'||in_lng||','||in_lat||'),
                       get_interval('||in_sim||',Id))-(sysdate-create_date)*24*60/5 desc';

*/ 



        open c1 for  get_sql_top40( tmp_str);
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound; 
            dbms_output.put_line(tmp_id||'---'||get_interval(in_sim,tmp_id)||in_sim);
            tmp_ret_str:=tmp_ret_str||','||tmp_id;
         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;
         
   return tmp_ret_str;

end;
/

prompt
prompt Creating function GET_SQL_TOP
prompt =============================
prompt
CREATE OR REPLACE FUNCTION get_sql_top (in_sql in varchar2,in_ct in varchar2)
     RETURN VARCHAR2
---输入表记录,输出前**条记录
   is
     out_info  VARCHAR2 (1000);
   begin
   out_info:='select * from ('||in_sql||') where rownum<='||in_ct;

   RETURN (out_info);
end;
/

prompt
prompt Creating function GET_INFO_ID_TOP
prompt =================================
prompt
CREATE OR REPLACE FUNCTION get_info_id_top(in_sim varchar2
                                          ,in_lng number
                                          ,in_lat number
                                          ,in_ct  varchar2)
--可变参数：
--输入经纬度和信息起始地经纬度相减在'2'内
--当前时间和信息创建时间在'6'小时内
--当前时间和信息创建时间每'5'分钟时间间隔

     RETURN varchar2
                  is
   tmp_str  varchar2(500);
   tmp_ret_str  varchar2(500);
   tmp_id number(30);
   type cursor_type is ref cursor;
   c1 cursor_type;
 begin

tmp_str:=
'select id from wl_info_data where 1=1 and    
(lng_start-'||in_lng||'<2 and lng_start-'||in_lng||'>-2) and
(lat_start-'||in_lat||'<2 and lat_start-'||in_lat||'>-2)
order by get_info_value
(
num
,-((lng_start-'||in_lng||')*(lng_start-'||in_lng||')+
   (lat_start-'||in_lat||')*(lat_start-'||in_lat||')
  )
,-get_interval('||in_sim||',Id)
)
-(sysdate-create_date)*24*60/5 desc';


/*
tmp_str:='';
tmp_str:=tmp_str||'select id from wl_info_data where 1=1 ';
   tmp_str:=tmp_str||' and  abs(lng_start-'||in_lng||')<2 and  abs(lat_start-'||in_lat||')<2';
   --tmp_str:=tmp_str||' and (sysdate-create_date)*24<6';
   tmp_str:=tmp_str||' order by get_info_value(num,
                       -get_distance(lng_start,lat_start,'||in_lng||','||in_lat||'),
                       get_interval('||in_sim||',Id))-(sysdate-create_date)*24*60/5';

*/

        open c1 for  get_sql_top(tmp_str,in_ct);
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound;
            dbms_output.put_line(tmp_id||'---'||get_interval(in_sim,tmp_id)||in_sim);
            tmp_ret_str:=tmp_ret_str||','||tmp_id;
         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;

   return tmp_ret_str;

end;
/

prompt
prompt Creating function GET_INFO_NBR_BY_CITY
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION get_info_nbr_by_city
  (from_province in varchar2
   ,from_city  in varchar2
   ,to_province  in varchar2
   ,to_city  in varchar2
    )
       RETURN number
--获取客户名称、电话、地址
  is
   info_start1     varchar2(100);
   info_end1       varchar2(100);
   ct      number(5);
  begin

     info_start1:=replace(from_province||from_city,'全部');
     info_end1  :=replace(to_province||to_city,'全部');
     
     select count(*) into ct
     from wl_info_data
     where info_start like info_start1||'%' 
       and info_end like info_end1||'%';
     
   

  return (ct);
end;
/

prompt
prompt Creating function GET_INFO_VALUE
prompt ================================
prompt
create or replace function get_info_value(in_km in number,in_num number,in_interval number )
return number 

 is
   
begin
 
        return (in_num+in_km+in_interval);
end;
/

prompt
prompt Creating function GET_LAT
prompt =========================
prompt
CREATE OR REPLACE FUNCTION get_lat(place  varchar2) RETURN NUMBER is
     ret      number(11,8) := 0;
--输入省市县区全称,输出经度
 begin
   if place is null then
      return -1 ;
   end if;
    select count(*)  into ret
     FROM  wl_info_data_region
    where lng is not null
      and lat is not null
     and pro||city||county||region=place;
      -- and pro||city||county =place;
   if  ret <>1 then 
       return -2;
   end if;

   select lat into ret
     FROM  wl_info_data_region
    where lng is not null
      and lat is not null
      and pro||city||county||region=place;
     -- and pro||city||county =place;
   return ret;
end;
/

prompt
prompt Creating function GET_LINE_INFO_ID_TOP
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION get_line_info_id_top(in_sim varchar2
                                               ,in_ct  varchar2
                                               ,from_province varchar2
                                               ,from_city varchar2
                                               ,to_province varchar2
                                               ,to_city varchar2)


--可变参数：
--输入经纬度和信息起始地经纬度相减在'2'内
--当前时间和信息创建时间在'6'小时内
--当前时间和信息创建时间每'5'分钟时间间隔

     RETURN varchar2  is
   tmp_str           varchar2(500);
   tmp_ret_str       varchar2(500);
   tmp_id            number(30);
   type cursor_type is ref cursor;
   c1 cursor_type;
   
 begin
  
   tmp_str:='select id from wl_info_data where 1=1';
   
--省,市,省,市
if from_province<>'全部' then
   tmp_str:=tmp_str||' and info_start like '||chr(39)||from_province||'%'||chr(39);
end if ;  
  

if from_city<>'全部' then
   tmp_str:=tmp_str||' and info_start like '||chr(39)||'%'||from_city||'%'||chr(39);
end if ;

if to_province<>'全部' then
   tmp_str:=tmp_str||' and info_end like '||chr(39)||to_province||'%'||chr(39);
end if ;  
  
if to_city<>'全部' then
   tmp_str:=tmp_str||' and info_end like '||chr(39)||'%'||to_city||'%'||chr(39);
end if ;


tmp_str:=tmp_str||' order by get_info_value
(
num
,-0
,-get_interval('||in_sim||',Id)
)
-(sysdate-create_date)*24*60/5 desc';



/*
tmp_str:='';
tmp_str:=tmp_str||'select id from wl_info_data where 1=1 ';
   tmp_str:=tmp_str||' and  abs(lng_start-'||in_lng||')<2 and  abs(lat_start-'||in_lat||')<2';
   --tmp_str:=tmp_str||' and (sysdate-create_date)*24<6';
   tmp_str:=tmp_str||' order by get_info_value(num,
                       -get_distance(lng_start,lat_start,'||in_lng||','||in_lat||'),
                       get_interval('||in_sim||',Id))-(sysdate-create_date)*24*60/5';

*/

        open c1 for  get_sql_top(tmp_str,in_ct);
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound;
            --dbms_output.put_line(tmp_id||'---'||get_interval(in_sim,tmp_id)||in_sim);
            tmp_ret_str:=tmp_ret_str||','||tmp_id;
            --dbms_output.put_line(tmp_ret_str);
         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;

   return tmp_ret_str;

end;
/

prompt
prompt Creating function WL_GETCOMADDRESS
prompt ==================================
prompt
create or replace function WL_GetComAddress(keyAdd in varchar2)
  return varchar2 is
  tempStr varchar2(100):='';
  temppro varchar2(100):='';
begin
  --乡村地址--
  select max(county) into tempStr
    from Wl_Info_Data_Region 
    where regexp_instr(keyAdd,county)>0;
  if tempStr is not null then
     select max(pro||city||county) into tempStr 
       from Wl_Info_Data_Region 
       where regexp_instr(keyAdd,county)>0;
       goto ret_end;
  end if;
  --城市地址--
  select max(city) into tempStr
    from Wl_Info_Data_Region 
    where regexp_instr(keyAdd,city)>0;
  if tempStr is not null then
     select max(city) into tempStr 
       from Wl_Info_Data_Region 
       where regexp_instr(keyAdd,city)>0;
     select max(pro) into temppro 
       from Wl_Info_Data_Region 
       where regexp_instr(keyAdd,city)>0;
      if tempStr<>temppro then
         tempStr:=temppro||tempStr;
      end if;
     goto ret_end;
  end if;
  --省级地址--
  select max(pro) into tempStr
    from Wl_Info_Data_Region 
    where regexp_instr(keyAdd,pro)>0;
  <<ret_end>>
  return (tempStr);
end WL_GetComAddress;
/

prompt
prompt Creating function GET_LINE_INFO_TXH_TOP
prompt =======================================
prompt
CREATE OR REPLACE FUNCTION get_line_info_TXH_top(in_sim varchar2
                                               ,in_ct  varchar2
                                               ,from_address varchar2
                                               ,end_address varchar2)


--可变参数：
--输入经纬度和信息起始地经纬度相减在'2'内
--当前时间和信息创建时间在'6'小时内
--当前时间和信息创建时间每'5'分钟时间间隔

     RETURN varchar2  is
   tmp_str           varchar2(500);
   tmp_ret_str       varchar2(500);
   tmp_id            number(30);
   start_add varchar2(100);
   end_add varchar2(100);
   tmp_out_message varchar2(4000);
   tmp_row_message varchar2(2000);
   type cursor_type is ref cursor;
   c1 cursor_type;

 begin
   /*获取地址全称省市县。。*/
   start_add:=WL_GetComAddress(from_address);
   end_add:=WL_GetComAddress(end_address);
   --dbms_output.put_line('tart'||start_add);
  -- dbms_output.put_line('tart'||end_add);
   tmp_str:='select id from wl_info_data where 1=1';
   tmp_str:=tmp_str||' and info_start like '||chr(39)||start_add||'%'||chr(39);
   tmp_str:=tmp_str||' and info_end like '||chr(39)||end_add||'%'||chr(39);
   --if HasNbr>0 then
       tmp_str:=tmp_str||' order by get_info_value(num,-0,-get_interval('||in_sim||',Id))-(sysdate-create_date)*24*60/5 desc';
  -- else
      -- tmp_str:=tmp_str||'order by Create_date asc';
  -- end if;
   open c1 for  get_sql_top(tmp_str,in_ct);
   loop
     fetch c1 into tmp_id;
     exit when c1%notfound;
     tmp_ret_str:=tmp_ret_str||','||tmp_id;
   end loop;
   if tmp_ret_str is not null then
       tmp_ret_str:=substr(tmp_ret_str,2);
   end if ;
   if tmp_ret_str is null then
        tmp_out_message:='';
       return (tmp_out_message);
   end if;
  -- dbms_output.put_line('temp'||tmp_ret_str);
   --取值--
  tmp_str:='';
  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_ret_str||')
  order by create_date desc';
  open c1 for tmp_str;
  loop
    fetch c1 into tmp_row_message;
    exit when c1%notfound;
    tmp_out_message:=tmp_out_message||chr(13)||tmp_row_message||';';
  end loop;
  close c1;
  tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_ret_str||' )';
  execute immediate tmp_str;
  commit;
  ------记录访问信息------无记录的插入，有记录的更新时间-----------------------
   tmp_str:='';
   tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
   tmp_str:=tmp_str||'select id, '||chr(39)||in_sim||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_ret_str||' )';
   tmp_str:=tmp_str||' minus ';
   tmp_str:=tmp_str||'select info_id, '||chr(39)||in_sim||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_ret_str||')
                      and   sim='||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_ret_str||' ) and sim= '||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;
  return (tmp_out_message);
end;
/

prompt
prompt Creating function GET_LINE_MSG_ID
prompt =================================
prompt
CREATE OR REPLACE FUNCTION get_line_msg_id
--输出待发的前100条短信ID：

return varchar2  is
   tmp_str           varchar2(5000);
   tmp_ret_str       varchar2(5000);
   tmp_id            varchar2(64);
   type cursor_type is ref cursor;
   c1 cursor_type;

begin

   tmp_str:= 'select chr(39)||msg_id||chr(39) from wl_cust_sms
      where state=0 or (state=-1 and fail>0)';

--dbms_output.put_line(tmp_str);
        open c1 for  tmp_str;
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound;

            tmp_ret_str:=tmp_ret_str||','||tmp_id;

         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;

   return tmp_ret_str;

end;
/

prompt
prompt Creating function GET_LINE_MSG_ID_TOP
prompt =====================================
prompt
CREATE OR REPLACE FUNCTION get_line_msg_id_top(in_ct number)
--输出待发的前100条短信ID：

return varchar2  is
   tmp_str           varchar2(5000);
   tmp_ret_str       varchar2(5000);
   tmp_id            varchar2(64);
   type cursor_type is ref cursor;
   c1 cursor_type;

begin
--state:-1待发,0成功,2正在发送,1失败,-2过期
   tmp_str:='select chr(39)||msg_id||chr(39) from 
     (select msg_id from wl_cust_sms where state=-1 group by msg_id)
      where rownum<='||in_ct;


/*   tmp_str:='select chr(39)||msg_id||chr(39) from 
     (select msg_id from wl_cust_sms 
      where state=0 or (state=-1 and fail>0) group by msg_id)
      where rownum<='||in_ct;*/
      

--dbms_output.put_line(tmp_str);
        open c1 for  tmp_str;
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound;
            
            tmp_ret_str:=tmp_ret_str||','||tmp_id;

         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;

   return tmp_ret_str;

end;
/

prompt
prompt Creating function GET_LINE_MSG_ID_TOP_OLD
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION get_line_msg_id_top_old(in_ct number)
--输出待发的前100条短信ID：

return varchar2  is
   tmp_str           varchar2(5000);
   tmp_ret_str       varchar2(5000);
   tmp_id            varchar2(64);
   type cursor_type is ref cursor;
   c1 cursor_type;

begin

   tmp_str:='select ''''''''||msg_id||'''''''' from
     (select msg_id from wl_cust_sms
      where state=0 or (state=-1 and fail>0) order by create_date )
      where rownum<='||in_ct;

--dbms_output.put_line(tmp_str);
        open c1 for  tmp_str;
        loop
            fetch c1 into tmp_id;
            exit when c1%notfound;

            tmp_ret_str:=tmp_ret_str||','||tmp_id;

         end loop;
         if tmp_ret_str is not null then
            tmp_ret_str:=substr(tmp_ret_str,2);
         end if ;

   return tmp_ret_str;

end;
/

prompt
prompt Creating function GET_LNG
prompt =========================
prompt
create or replace function get_lng(place  in varchar2)
return number 

 is
    ret   number(11,8);
begin

select count(*) into ret
   FROM  wl_info_data_region
  where lng is not null
    and lat is not null
   and pro||city||county||region=place;
 --  and pro||city||county=place;

   if  ret <>1 then 
       return -2;
   end if;


select lng into ret
   FROM  wl_info_data_region
  where lng is not null
    and lat is not null
   and pro||city||county||region=place;
 --  and pro||city||county=place;

        return (ret);
end;
/

prompt
prompt Creating function GET_MESS_ZST_TXH_TOP
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION get_Mess_ZST_TXH_top(in_sim varchar2
                                               ,in_ct  varchar2
                                               ,from_address varchar2
                                               ,end_address varchar2)


--可变参数：
--输入经纬度和信息起始地经纬度相减在'2'内
--当前时间和信息创建时间在'6'小时内
--当前时间和信息创建时间每'5'分钟时间间隔

     RETURN varchar2  is
   tmp_str           varchar2(500);
   tmp_ret_str       varchar2(500);
   tmp_id            number(30);
   start_add varchar2(100);
   end_add varchar2(100);
   tmp_out_message varchar2(4000);
   tmp_row_message varchar2(2000);
   type cursor_type is ref cursor;
   c1 cursor_type;

 begin
   /*获取地址全称省市县。。*/
   start_add:=WL_GetComAddress(from_address);
   end_add:=WL_GetComAddress(end_address);
   tmp_str:='select id from wl_info_data where 1=1';
   tmp_str:=tmp_str||' and info_start like '||chr(39)||start_add||'%'||chr(39);
   tmp_str:=tmp_str||' and info_end like '||chr(39)||end_add||'%'||chr(39);
   tmp_str:=tmp_str||' order by get_info_value(num,-0,-get_interval('||in_sim||',Id))-(sysdate-create_date)*24*60/5 desc';
   open c1 for  get_sql_top(tmp_str,in_ct);
   loop
     fetch c1 into tmp_id;
     exit when c1%notfound;
     tmp_ret_str:=tmp_ret_str||','||tmp_id;
   end loop;
   if tmp_ret_str is not null then
       tmp_ret_str:=substr(tmp_ret_str,2);
   end if ;
   --取值--
  tmp_str:='';
  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_ret_str||')
  order by create_date desc';
  open c1 for tmp_str;
  loop
    fetch c1 into tmp_row_message;
    exit when c1%notfound;
    tmp_out_message:=tmp_out_message||chr(13)||tmp_row_message||';';
  end loop;
  close c1;
  tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_ret_str||' )';
  execute immediate tmp_str;
  commit;
  ------记录访问信息------无记录的插入，有记录的更新时间-----------------------
   tmp_str:='';
   tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
   tmp_str:=tmp_str||'select id, '||chr(39)||in_sim||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_ret_str||' )';
   tmp_str:=tmp_str||' minus ';
   tmp_str:=tmp_str||'select info_id, '||chr(39)||in_sim||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_ret_str||')
                      and   sim='||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_ret_str||' ) and sim= '||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;
  return (tmp_out_message);
end;
/

prompt
prompt Creating function GET_MOBILE
prompt ============================
prompt
CREATE OR REPLACE FUNCTION get_mobile (info in varchar2)
   RETURN VARCHAR2
  --输入字符串,输出手机号
  is
    out_info  VARCHAR2 (200);
  begin

 out_info :=regexp_substr(info,'[1][3-9]\d{9}');

    return(out_info);
  end;
/

prompt
prompt Creating function GET_NBR_TYPE
prompt ==============================
prompt
CREATE OR REPLACE FUNCTION get_nbr_type (v_acc_nbr IN NUMBER)
   RETURN VARCHAR2
IS
   nbr_type   NUMBER (5);
   acc_nbr    VARCHAR2 (20);
BEGIN
   nbr_type := 0;
   acc_nbr := SUBSTR (v_acc_nbr, LENGTH (v_acc_nbr) - 7 + 1, 7);

-------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '([[6,9,8]{1})\1{6}')) = 7
   THEN
      RETURN (1);
   END IF;

------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '([[:digit:]]{1}+)\1{6}')) = 7
   THEN
      RETURN (2);
   END IF;

------------------------
   IF INSTR ('0123456789', acc_nbr) > 0
   THEN
      RETURN (3);
   END IF;

------------------------
   IF INSTR ('9876543210', acc_nbr) > 0
   THEN
      RETURN (4);
   END IF;

------------------------
   IF    LENGTH (REGEXP_SUBSTR (acc_nbr,
                                '([[:digit:]]{1}+)\1{3}([[:digit:]]{1}+)\2{2}'
                               )
                ) = 7
      OR LENGTH (REGEXP_SUBSTR (acc_nbr,
                                '([[:digit:]]{1}+)\1{2}([[:digit:]]{1}+)\2{3}'
                               )
                ) = 7
   THEN
      RETURN (5);
   END IF;

------------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{1}([[6,9,8]{1})\1{5}')) = 7
   THEN
      RETURN (6);
   END IF;

------------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{1}([[:digit:]]{1})\1{5}')) =
                                                                             7
   THEN
      RETURN (7);
   END IF;

   IF INSTR ('0123456789', SUBSTR (acc_nbr, 2)) > 0
   THEN
      RETURN (8);
   END IF;

------------------------
   IF INSTR ('9876543210', SUBSTR (acc_nbr, 2)) > 0
   THEN
      RETURN (9);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{2}([[6,9,8]{1})\1{4}')) = 7
   THEN
      RETURN (10);
   END IF;

------------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{2}([[:digit:]]{1})\1{4}')) =
                                                                             7
   THEN
      RETURN (11);
   END IF;

   IF LENGTH
          (REGEXP_SUBSTR (acc_nbr,
                          '[[:digit:]]{1}([6,9,8]{1}+)\1{2}([6,9,8]{1}+)\2{2}'
                         )
          ) = 7
   THEN
      RETURN (12);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{1}([[:digit:]]{1}+)\1{2}([[:digit:]]{1}+)\2{2}'
                 )
         ) = 7
   THEN
      RETURN (13);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr,
                             '[[:digit:]]{1}([6,9,8][6,9,8]{1})\1{2}')
             ) = 7
   THEN
      RETURN (14);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr,
                             '[[:digit:]]{1}([[:digit:]][[:digit:]]{1})\1{2}'
                            )
             ) = 7
   THEN
      RETURN (15);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                   (acc_nbr,
                    '[[:digit:]]{1}([6,9,8])\1{1}([6,9,8])\2{1}([6,9,8])\3{1}'
                   )
         ) = 7
   THEN
      RETURN (16);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                   (acc_nbr,
                    '[[:digit:]]{1}([6,9,8])\1{1}([6,9,8])\2{1}([6,9,8])\3{1}'
                   )
         ) = 7
   THEN
      RETURN (16);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
             (acc_nbr,
              '[[:digit:]]{1}([[:digit:]])\1{1}([[:digit:]])\2{1}([[:digit:]])\3{1}'
             )
         ) = 7
   THEN
      RETURN (17);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{1}([6,9,8]{3})\1{1}')) = 7
   THEN
      RETURN (18);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{1}([[:digit:]]{3})\1{1}')) =
                                                                             7
   THEN
      RETURN (19);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
             (acc_nbr,
              '[[:digit:]]{1}([6,9,8])\1{1}([6,9,8])\2{1}\1{1}'
             )
         ) = 7
   THEN
      RETURN (20);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
             (acc_nbr,
              '[[:digit:]]{1}([[:digit:]])\1{1}([[:digit:]])\2{1}\1{1}'
             )
         ) = 7
   THEN
      RETURN (21);
   END IF;

  IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}([6,9,8]{1})\1{3}')) =
                                                                             7
   THEN
      RETURN (22);
   END IF;

  IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}([[:digit:]]{1})\1{3}')) =
                                                                             7
   THEN
      RETURN (23);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (SUBSTR (v_acc_nbr, LENGTH (v_acc_nbr) - 8 + 1, 8), '([[:digit:]]{4})\1{1}')) =
                                                                             8
   THEN
      RETURN (24);
   END IF;

  IF INSTR ('0123456789', SUBSTR (acc_nbr, 3)) > 0
   THEN
      RETURN (25);
   END IF;

------------------------
   IF INSTR ('9876543210', SUBSTR (acc_nbr, 3)) > 0
   THEN
      RETURN (26);
   END IF;

  IF LENGTH
          (REGEXP_SUBSTR (acc_nbr,
                          '[[:digit:]]{2}([6,9,8]{1}+)\1{1}([6,9,8]{1}+)\2{2}'
                         )
          ) = 7
   THEN
      RETURN (27);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{2}([[:digit:]]{1}+)\1{1}([[:digit:]]{1}+)\2{2}'
                 )
         ) = 7
   THEN
      RETURN (28);
   END IF;

  IF LENGTH
          (REGEXP_SUBSTR (acc_nbr,
                          '[[:digit:]]{2}([6,9,8]{1}+)\1{2}([6,9,8]{1}+)\2{1}'
                         )
          ) = 7
   THEN
      RETURN (29);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{2}([[:digit:]]{1}+)\1{2}([[:digit:]]{1}+)\2{1}'
                 )
         ) = 7
   THEN
      RETURN (30);
   END IF;

 IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{4}([[:digit:]]{1})\1{2}')) =
                                                                             7
   THEN
      RETURN (31);
   END IF;

 IF INSTR ('0123456789', SUBSTR (acc_nbr, 4)) > 0
   THEN
      RETURN (32);
   END IF;

------------------------
   IF INSTR ('9876543210', SUBSTR (acc_nbr, 4)) > 0
   THEN
      RETURN (33);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{3}([6,9,8]{1}+)\1{1}([6,9,8]{1}+)\2{1}'
                 )
         ) = 7
   THEN
      RETURN (34);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{3}([[:digit:]]{1}+)\1{1}([[:digit:]]{1}+)\2{1}'
                 )
         ) = 7
   THEN
      RETURN (35);
   END IF;

 IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}([6,9,8]{2})\1{1}')) = 7
   THEN
      RETURN (36);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}([[:digit:]]{2})\1{1}')) =
                                                                             7
   THEN
      RETURN (37);
   END IF;

   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}[0]{3}[1,2,3,5,6,7,8,9,]{1}')) =
                                                                             7
   THEN
      RETURN (38);
   END IF;

   IF LENGTH
         (REGEXP_SUBSTR
                 (acc_nbr,
                  '[[:digit:]]{2}([[:digit:]]{1})\1{1}[[:digit:]]{1}\1{2}'
                 )
         ) = 7
   THEN
      RETURN (39);
   END IF;

  IF INSTR ('0123456789', SUBSTR (acc_nbr, 5)) > 0
   THEN
      RETURN (40);
   END IF;
------------------------
   IF INSTR ('9876543210', SUBSTR (acc_nbr, 5)) > 0
   THEN
      RETURN (41);
   END IF;
--------------------------
   IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{3}[0]{2}[1,2,3,4,5,6,7,8,9,0]{2}')) =
                                                                             7
   THEN
      RETURN (42);
   END IF;

 IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{5}([6,9,8]{1})\1{1}')) =
                                                                             7
   THEN
      RETURN (43);
   END IF;

  IF LENGTH (REGEXP_SUBSTR (acc_nbr, '[[:digit:]]{5}([[:digit:]]{1})\1{1}')) =
                                                                             7
   THEN
      RETURN (44);
   END IF;

   RETURN (45);
END;
/

prompt
prompt Creating function GET_ONLINE
prompt ============================
prompt
create or replace function Get_OnLIne return varchar2 is
  mess varchar2(500);
  VisiToDay number(5):=0;
  VisiLeaveline number(5):=0;
  allvisiData number(6):=0;
  Online number(5):=0;
  WebPubCount number(5):=0;
begin
  --总访问人数--
  select count(*) into allvisiData from wl_web_visited_log
        where INTIME is not null;
  --今日访问人数--
  select count(*) into visiToDay from wl_web_visited_log
        where to_char(sysdate,'yyyy/mm/dd')=to_char(INTIME,'yyyy/mm/dd');
  --今日在离线人数--
  select count(*) into VisiLeaveline from wl_web_visited_log
       where to_char(sysdate,'yyyy/mm/dd')=to_char(OUTTIME,'yyyy/mm/dd');
   if visiToDay-VisiLeaveline>0 then
      Online:=visiToDay-VisiLeaveline;
    else
      Online:=0;
      end if;
    --------获取web平台发送信息数量---------
  select count(*)  into WebPubCount from Wl_Info_Data where info_from like 'Web%';
   mess:='网站总访问人数:'||to_char(allvisiData)||',今日访问用户:'||to_char(visiToDay)||',在线人数:'||to_char(Online)||'。';
   mess:=mess||'网站发布信息数:'||to_char(WebPubCount)||'条。';
  return(mess);
end Get_OnLIne;
/

prompt
prompt Creating function GET_QQ_GROUP_CITY
prompt ===================================
prompt
create or replace function get_qq_group_city
(in_nbr in varchar2)
   return varchar2
  --输入nbr，查出信息部客户表city，输出wl_info_data_region表中serial
  is
  tmp_city varchar2(30);
  out_city_id   varchar2(500);
  begin


   select max(start_place) into tmp_city
   from  WL_CUST_QQ_GROUP_CITY
   where QQ_GROUP_NBR=trim(in_nbr);

   if tmp_city is null
   then goto ret_end;
   end if;

   select max(serial) into out_city_id from
   wl_info_data_region where id in(1,2) and pro||city=tmp_city;

   if out_city_id is null
   then goto ret_end;
   end if;

   out_city_id:='#P'||out_city_id||'#';

 <<ret_end>>
   return(out_city_id);
  end;
/

prompt
prompt Creating function GET_WEB_WL_INFOID
prompt ===================================
prompt
create or replace function get_web_wl_infoid
 (in_qq in number,in_info_start in varchar2,
  in_info_end in varchar2,in_info in varchar2,
  in_nbr in varchar2,in_info_truck in varchar2,
  in_info_goods in varchar2,in_info_note in varchar2)

/*输入参数：QQ号,群号,起始地,目的地,信息主体,信息联系电话,车辆类型
,货物类型,信息备注
输出参数：输出信息ID或-1*/

 return  number
 is
  pragma autonomous_transaction;--自治事务
  out_info_id        number(30);
  tmp_ct             number(10);
  tmp_c_name         varchar2(500);
  tmp_info_id        number(10);
  tmp_create_date    date;
  tmp_info           varchar2(500);
  tmp_info_nbr1      varchar2(30);
  tmp_lng_start      number(11,8);
  tmp_lat_start      number(11,8);
  tmp_lng_end        number(11,8);
  tmp_lat_end        number(11,8);

 begin

/*QQ号->c_name—起始城市—起点---联系电话
群号
起始地---à经纬度
目的地---》经纬度
信息主体
信息联系电话
车辆类型
货物类型
信息备注*/
  -----输入参数空值校验---------
  if in_qq is null or in_info_start is null or
  in_info_end is null or in_info is null
  then goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(in_info)>500 then
     goto ret_end;
  end if;
  -----号码校验----------------
  tmp_info_nbr1  :=in_nbr;

  if tmp_info_nbr1  is null then
     select contact into tmp_info_nbr1 from wl_cust_department
     where nbr=trim(in_qq);
  end if;

  if  tmp_info_nbr1 is null then
     goto ret_end;
  end if;

  -----生成时间与信息序列----------------
  tmp_create_date :=sysdate;

  -----信息主体判断-----------
  tmp_info :=in_info;

  tmp_info :=replace(tmp_info,'上海上海','上海');
  tmp_info :=replace(tmp_info,'北京北京','北京');
  tmp_info :=replace(tmp_info,'天津天津','天津');
  tmp_info :=replace(tmp_info,'重庆重庆','重庆');


  ----更新或插入目的表wl_info_data---------------------------
  select count(*) into tmp_ct
  from wl_info_data
  where info=tmp_info and info_nbr1=tmp_info_nbr1
  and nbr=to_char(in_qq);


  if tmp_ct >0 then
    update  wl_info_data set create_date=tmp_create_date
    where info=tmp_info and info_nbr1=tmp_info_nbr1
    and nbr=to_char(in_qq);
    commit;

    select id into out_info_id
    from wl_info_data
    where info=tmp_info and info_nbr1=tmp_info_nbr1
    and nbr=to_char(in_qq);


/*  dbms_output.put_line(tmp_info_out);*/

  goto real_end;
  end if;

  -----生成经纬、信息部名称、信息ID----------------
  tmp_lng_start  :=get_lng(in_info_start);
  tmp_lat_start  :=get_lat(in_info_start);
  tmp_lng_end    :=get_lng(in_info_end);
  tmp_lat_end    :=get_lat(in_info_end);

  select c_name into tmp_c_name from wl_cust_department
  where nbr=trim(in_qq);

  tmp_info_id    :=wl_info_data_id.nextval;
  -----插入信息主表----------------
  insert into wl_info_data
   (id,nbr,c_name,create_date,info,
    info_start,info_end,info_nbr1,
    info_truck,info_goods,info_note,
    info_from,note,
    lng_start,lat_start,lng_end,lat_end
   )
  values
   (tmp_info_id,to_char(in_qq),tmp_c_name,tmp_create_date,tmp_info,
    in_info_start,in_info_end,tmp_info_nbr1,
    in_info_truck,in_info_goods,in_info_note,
    'Web','',
    tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;

 out_info_id:=tmp_info_id;

 insert into wl_flag(name,create_date,note) values
 (tmp_info_id,tmp_create_date,'WEB处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 insert into  wl_info_from_in_early (text1,note)
 values (in_info,'Web'||'_'||in_qq);
 commit;
 out_info_id:=-1;

 <<real_end>>
  return (out_info_id);
end;
/

prompt
prompt Creating function GET_WL_INFOID
prompt ===============================
prompt
create or replace function get_wl_infoid
 (in_qq in number,in_group in number,in_info_start in varchar2,
  in_info_end in varchar2,in_info in varchar2,
  in_nbr in varchar2,in_info_truck in varchar2,
  in_info_goods in varchar2,in_info_note in varchar2)

/*输入参数：QQ号,群号,起始地,目的地,信息主体,信息联系电话,车辆类型
,货物类型,信息备注
输出参数：输出信息ID或-1*/

 return  number
 is
  pragma autonomous_transaction;--自治事务
  out_info_id        number(30);
  tmp_ct             number(10);
  tmp_c_name         varchar2(500);
  tmp_info_id        number(10);
  tmp_create_date    date;
  tmp_info           varchar2(500);
  tmp_info_nbr1      varchar2(30);
  tmp_lng_start      number(11,8);
  tmp_lat_start      number(11,8);
  tmp_lng_end        number(11,8);
  tmp_lat_end        number(11,8);

 begin
 
/*QQ号->c_name—起始城市—起点---联系电话
群号
起始地---à经纬度
目的地---》经纬度
信息主体
信息联系电话
车辆类型
货物类型
信息备注*/
  -----输入参数空值校验---------
  if in_qq is null or in_info_start is null or 
  in_info_end is null or in_info is null 
  then goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(in_info)>500 then
     goto ret_end;
  end if;
  -----号码校验----------------
  tmp_info_nbr1  :=in_nbr;

  if tmp_info_nbr1  is null then
     select contact into tmp_info_nbr1 from wl_cust_department
     where qq=trim(in_qq);
  end if;
  
  if  tmp_info_nbr1 is null then
     goto ret_end;
  end if;

  -----生成时间与信息序列----------------
  tmp_create_date :=sysdate;

  -----信息主体判断-----------
  tmp_info :=in_info;

  tmp_info :=replace(tmp_info,'上海上海','上海');
  tmp_info :=replace(tmp_info,'北京北京','北京');
  tmp_info :=replace(tmp_info,'天津天津','天津');
  tmp_info :=replace(tmp_info,'重庆重庆','重庆');


  ----更新或插入目的表wl_info_data---------------------------
  select count(*) into tmp_ct
  from wl_info_data
  where info=tmp_info and info_nbr1=tmp_info_nbr1
  and nbr=to_char(in_qq);


  if tmp_ct >0 then
    update  wl_info_data set create_date=tmp_create_date
    where info=tmp_info and info_nbr1=tmp_info_nbr1
    and nbr=to_char(in_qq);
    commit;
    
    select id into out_info_id
    from wl_info_data
    where info=tmp_info and info_nbr1=tmp_info_nbr1
    and nbr=to_char(in_qq);


/*  dbms_output.put_line(tmp_info_out);*/

  goto real_end;
  end if;

  -----生成经纬、信息部名称、信息ID----------------
  tmp_lng_start  :=get_lng(in_info_start);
  tmp_lat_start  :=get_lat(in_info_start);
  tmp_lng_end    :=get_lng(in_info_end);
  tmp_lat_end    :=get_lat(in_info_end);

  select c_name into tmp_c_name from wl_cust_department
  where qq=trim(in_qq);

  tmp_info_id    :=wl_info_data_id.nextval;
  -----插入信息主表----------------
  insert into wl_info_data
   (id,nbr,c_name,create_date,info,
    info_start,info_end,info_nbr1,
    info_truck,info_goods,info_note,
    info_from,note,
    lng_start,lat_start,lng_end,lat_end
   )
  values
   (tmp_info_id,to_char(in_qq),tmp_c_name,tmp_create_date,tmp_info,
    in_info_start,in_info_end,tmp_info_nbr1,
    in_info_truck,in_info_goods,in_info_note,
    'qq',to_char(in_group),
    tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;

   
 insert into  wl_info_from_in_early (text1,note,info_id)
 values (tmp_info,'qq-'||in_group||'-'||in_qq,tmp_info_id);
 commit;

 out_info_id:=tmp_info_id;

 insert into wl_flag(name,create_date,note) values
 (tmp_info_id,tmp_create_date,'qq处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 insert into  wl_info_from_in_early (text1,note)
 values (in_info,'qq-'||in_group||'-'||in_qq);
 commit;
 out_info_id:=-1;

 <<real_end>>
  return (out_info_id);
end;
/

prompt
prompt Creating function GET_WL_INFOID_TXT
prompt ===================================
prompt
create or replace function get_wl_infoid_txt
 (in_nbr in varchar2
 ,in_city in varchar2
 ,in_info_start in varchar2
 ,in_info_end in varchar2
 ,in_info in varchar2
 ,in_info_truck in varchar2
 ,in_info_goods in varchar2
 ,in_info_note in varchar2)

/*输入参数：QQ号,群号,起始地,目的地,信息主体,信息联系电话,车辆类型
,货物类型,信息备注
输出参数：输出信息ID或-1*/

 return  number
 is
  pragma autonomous_transaction;--自治事务
  out_info_id        number(30);
  tmp_ct             number(10);
  tmp_c_name         varchar2(500);
  tmp_info_id        number(10);
  tmp_create_date    date;
  tmp_info           varchar2(500);
  tmp_info_nbr1      varchar2(30);
  tmp_lng_start      number(11,8);
  tmp_lat_start      number(11,8);
  tmp_lng_end        number(11,8);
  tmp_lat_end        number(11,8);

 begin

/*QQ号->c_name—起始城市—起点---联系电话
群号
起始地---à经纬度
目的地---》经纬度
信息主体
信息联系电话
车辆类型
货物类型
信息备注*/
  -----输入参数空值校验---------
  if in_info_start is null  or in_info_end is null  or in_info is null then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(in_info)>500 then
     goto ret_end;
  end if;
  -----生成时间与信息序列----------------
  tmp_create_date :=sysdate;

  -----信息主体判断-----------
  tmp_info :=in_info;

  tmp_info :=replace(tmp_info,'上海上海','上海');
  tmp_info :=replace(tmp_info,'北京北京','北京');
  tmp_info :=replace(tmp_info,'天津天津','天津');
  tmp_info :=replace(tmp_info,'重庆重庆','重庆');


  ----更新或插入目的表wl_info_data---------------------------
  select count(*) into tmp_ct
  from wl_info_data
  where info=tmp_info and info_nbr1=in_nbr and nbr=in_nbr;


  if tmp_ct >0 then
    update  wl_info_data set create_date=tmp_create_date
     where info=tmp_info and info_nbr1=in_nbr and nbr=in_nbr;
    commit;

    select id into out_info_id
    from wl_info_data
    where info=tmp_info and info_nbr1=in_nbr and nbr=in_nbr;


/*  dbms_output.put_line(tmp_info_out);*/

  goto real_end;
  end if;

  -----生成经纬、信息部名称、信息ID----------------
  tmp_lng_start  :=get_lng(in_info_start);
  tmp_lat_start  :=get_lat(in_info_start);
  tmp_lng_end    :=get_lng(in_info_end);
  tmp_lat_end    :=get_lat(in_info_end);

  select count(*) into tmp_ct from wl_cust_department
  where nbr=trim(in_nbr);
  if tmp_ct=1  then
      select c_name into tmp_c_name from wl_cust_department
      where nbr=trim(in_nbr);
     else
       tmp_c_name:=' ';
  end if ;
  tmp_info_id    :=wl_info_data_id.nextval;
  tmp_info_nbr1:=in_nbr;
  -----插入信息主表----------------
  insert into wl_info_data
 (id         ,nbr            ,c_name    ,city   ,create_date    ,info    ,info_start   ,info_end   ,info_nbr1    ,info_truck   ,info_goods   ,info_note   ,info_from,note  ,lng_start    ,lat_start    ,lng_end    ,lat_end
  ) values
 (tmp_info_id,to_char(in_nbr),tmp_c_name,in_city,tmp_create_date,tmp_info,in_info_start,in_info_end,tmp_info_nbr1,in_info_truck,in_info_goods,in_info_note,'qq_net' ,in_nbr,tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;
 out_info_id:=tmp_info_id;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 out_info_id:=-1;

 <<real_end>>
  return (out_info_id);
end;
/

prompt
prompt Creating function GET_WL_INFO_GOODS
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_goods(info in varchar2) RETURN VARCHAR2 IS
    out_info VARCHAR2(200);
--输入物流信息串,输出串中货物
  begin
    out_info := '';
  declare
    cursor job_goods is
    select id,goods,desc1 from wl_info_data_goods order by id desc;
    c_row job_goods%rowtype;
  begin
    open job_goods;
    loop
      if REGEXP_SUBSTR(info, c_row.goods) IS not null
         Then
           out_info := c_row.desc1;
           return(c_row.desc1);
      end if;
      fetch job_goods  into c_row;
       exit when job_goods%notfound;
    end loop;
    close job_goods;
  end;
  RETURN(out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_NBR1
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_nbr1 (info in varchar2)
   RETURN VARCHAR2
  --输入物流信息串,输出串中第1个联系电话
  is
    out_info  VARCHAR2 (200);
  begin
    --out_info :=REGEXP_SUBSTR(info,'((1\d{10}\D?)|((2|3|4|5|6|7|8)\d{6,7}))',1,1);
out_info :=REGEXP_SUBSTR(info,'((1\d{10})|((2|3|4|5|6|7|8)\d{6,7}))',1,1);

    return(out_info);
  end;
/

prompt
prompt Creating function GET_WL_INFO_NBR2
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_nbr2 (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出串中第2个联系电话
IS
   out_info  VARCHAR2 (200);

begin

--out_info :=REGEXP_SUBSTR(info,'((1\d{10}\D?)|((2|3|4|5|6|7|8)\d{6,7}))',1,2);
out_info :=REGEXP_SUBSTR(info,'((1\d{10})|((2|3|4|5|6|7|8)\d{6,7}))',1,2);

RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_NBR3
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_nbr3 (info in varchar2)
     RETURN VARCHAR2
--输入物流信息串,输出串中第3个联系电话 
  IS
     out_info  VARCHAR2 (200);
  begin
      --out_info :=REGEXP_SUBSTR(info,'((1\d{10}\D?)|((2|3|4|5|6|7|8)\d{6,7}))',1,3);
 out_info :=REGEXP_SUBSTR(info,'((1\d{10})|((2|3|4|5|6|7|8)\d{6,7}))',1,3);

     RETURN (out_info);
  end;
/

prompt
prompt Creating function GET_WL_INFO_NOTE
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_note (info in varchar2)
     RETURN VARCHAR2
---输入物流信息串,输出串中备注
   is
     out_info  VARCHAR2 (200);
   begin
     out_info :='';
   declare  
      cursor c_job_note
       is
      select id,note,desc1 
      from wl_info_data_note
      order by id desc;
      c_row c_job_note%rowtype;
   begin
      open c_job_note;
       loop
           if REGEXP_SUBSTR(info,c_row.note)IS NOT NULL
              Then out_info :=out_info||'、'||c_row.desc1;
           end if;
       --提取一行数据到c_row
         fetch c_job_note into c_row;
         exit when c_job_note%notfound;
       end loop;
      close c_job_note;
    end;
    IF SUBSTR(out_info,1,1)='、' Then 
       out_info :=SUBSTR(out_info,2);
    end if;
   RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_END
prompt ========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_end(info in varchar2,in_city in varchar2)
   RETURN VARCHAR2
--输入物流信息串和城市,输出非该城市中的省市县区全称目的地
     IS
   out_info  VARCHAR2 (8000);
  begin
     out_info :='';
     declare
     cursor job_region_end
        is
     select state||'\s?(\d{3})?' state,pro||city||county place 
       from wl_info_data_region 
      where (city<>in_city OR CITY IS NULL)  order by id DESC;
     c_row job_region_end%rowtype;
     begin
        open job_region_end;
        loop
          if  REGEXP_SUBSTR(info,c_row.STATE) IS NOT NULL then 
              --Then out_info :=REGEXP_SUBSTR(info,c_row.STATE);
               out_info :=c_row.place||REGEXP_SUBSTR(REGEXP_SUBSTR(info,c_row.STATE),'(\d{3})?');
               /*if  REGEXP_SUBSTR(info,c_row.STATE) IS NOT NULL
                 Then out_info :=out_info||'、'||REGEXP_SUBSTR(info,c_row.STATE);*/
                --else  out_info :=out_info||REGEXP_SUBSTR(info,c_row.truck);
                RETURN (out_info);
          end if;
                --提取一行数据到c_row
         fetch job_region_end into c_row;
          --判读是否提取到值，没取到值就退出
          --取到值job_region_end%notfound 是false
          --取不到值job_region_end%notfound 是true
         exit when job_region_end%notfound;
        end loop;
   close job_region_end;
  end;
--IF SUBSTR(out_info,1,1)='、' 
----Then out_info :=SUBSTR(out_info,2);
---end if;
RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_END_LINE
prompt =============================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_end_line
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
   --in_str    varchar (500);
  begin
  tmp_info :=substr(info,1,15);
  flag     :=0;

 declare
  cursor job_region_from
    is
  select state,serial from wl_info_data_region order by id desc;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;

<<ret_end_1>>
   tmp_info:=
   regexp_substr(tmp_info,
   '~\d+!((一)|(到)|(至)|(去)|(-)|( )){0,2}~\d+!');


   tmp_info:=
   regexp_replace(tmp_info,
   '(~{1})(\d+)(!{1})(.{0,2})(~{1})(\d+)(!{1})','\6');

   if tmp_info is null then
   goto ret_end;
   end if ;

   select
   (case when city is null then pro||',全部'
    else pro||','||city
    end) into tmp_info
   from wl_info_data_region
   where serial=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_END_N
prompt ==========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_end_n
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   out_info  varchar (500);
  begin

   tmp_info:=
   regexp_replace(info,
   '(~{1})(\d+)(!{1})(.{1})(~{1})(\d+)(!{1})','\6');

   select pro||city||county||region into out_info
   from wl_info_data_region
   where serial=to_number(tmp_info);

  return (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_END_NEW
prompt ============================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_end_new
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
   --in_str    varchar (500);
  begin
  tmp_info :=substr(info,1,15);
  flag     :=0;

 declare
  cursor job_region_from
    is
  select state,serial from wl_info_data_region order by id desc;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;

<<ret_end_1>>
   tmp_info:=
   regexp_substr(tmp_info,
   '~\d+!((一)|(到)|(至)|(去)|(-)|( )){0,2}~\d+!');


   tmp_info:=
   regexp_replace(tmp_info,
   '(~{1})(\d+)(!{1})(.{0,2})(~{1})(\d+)(!{1})','\6');

   if tmp_info is null then
   goto ret_end;
   end if ;

   select pro||city||county||region into tmp_info
   from wl_info_data_region
   where serial=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_END_S
prompt ==========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_end_s(info in varchar2,in_city in varchar2)
   RETURN VARCHAR2
--输入物流信息串和城市,输出非该城市中的自带目的地
     IS
   out_info  VARCHAR2 (8000);
  begin
     out_info :='';
     declare
     cursor job_region_end
        is
     select state||'\s?(\d{3})?' state,pro||city||county place 
       from wl_info_data_region 
      where (city<>in_city OR CITY IS NULL)  order by id DESC;
     c_row job_region_end%rowtype;
     begin
        open job_region_end;
        loop
          if  REGEXP_SUBSTR(info,c_row.STATE) IS NOT NULL then 
              --Then out_info :=REGEXP_SUBSTR(info,c_row.STATE);
               out_info :=REGEXP_SUBSTR(info,c_row.STATE);
               /*if  REGEXP_SUBSTR(info,c_row.STATE) IS NOT NULL
                 Then out_info :=out_info||'、'||REGEXP_SUBSTR(info,c_row.STATE);*/
                --else  out_info :=out_info||REGEXP_SUBSTR(info,c_row.truck);
                RETURN (out_info);
          end if;
                --提取一行数据到c_row
         fetch job_region_end into c_row;
          --判读是否提取到值，没取到值就退出
          --取到值job_region_end%notfound 是false
          --取不到值job_region_end%notfound 是true
         exit when job_region_end%notfound;
        end loop;
   close job_region_end;
  end;
--IF SUBSTR(out_info,1,1)='、' 
----Then out_info :=SUBSTR(out_info,2);
---end if;
RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_FROM
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_from
  (info in varchar2,in_city in varchar2)
--输入物流信息串和城市,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
--加CITY 输入参数，FROM：只找榆林、NULL替换为榆林  END：<>榆林
   out_info  VARCHAR2 (8000);
  begin
  out_info :='';
 declare
  cursor job_region_from
    is
  select STATE,pro||city||county place from wl_info_data_region where city=in_city order by id DESC;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if REGEXP_SUBSTR(info,c_row.state) is not null then 
        out_info :=c_row.place;
        RETURN (out_info);
    end if;
   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;
  if out_info is null 
     then out_info :=IN_CITY;
  end if;
  RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_FROM_LINE
prompt ==============================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_from_line
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
   --in_str    varchar (500);
  begin
  tmp_info :=substr(info,1,15);
  flag     :=0;

 declare
  cursor job_region_from
    is
  select state,serial from wl_info_data_region order by id desc;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;

<<ret_end_1>>
   tmp_info:=
   regexp_substr(tmp_info,
   '~\d+!((一)|(到)|(至)|(去)|(-)|( )){0,2}~\d+!');


   tmp_info:=
   regexp_replace(tmp_info,
   '(~{1})(\d+)(!{1})(.{0,2})(~{1})(\d+)(!{1})','\2');

   if tmp_info is null then
   goto ret_end;
   end if ;

   select
   (case when city is null then pro||',全部'
    else pro||','||city
    end) into tmp_info
   from wl_info_data_region
   where serial=to_number(tmp_info);


 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_FROM_N
prompt ===========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_from_n
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   out_info  varchar (500);
  begin
  
   tmp_info:=
   regexp_replace(info,
   '(~{1})(\d+)(!{1})(.{1})(~{1})(\d+)(!{1})','\2');
   
   select pro||city||county||region into out_info
   from wl_info_data_region 
   where serial=to_number(tmp_info);
   
  return (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_FROM_NEW
prompt =============================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_from_new
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
   --in_str    varchar (500);
  begin
  tmp_info :=substr(info,1,15);
  flag     :=0;
  
 declare
  cursor job_region_from
    is
  select state,serial from wl_info_data_region order by id desc;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;
 
<<ret_end_1>>
   tmp_info:=
   regexp_substr(tmp_info,
   '~\d+!((一)|(到)|(至)|(去)|(-)|( )){0,2}~\d+!');


   tmp_info:=
   regexp_replace(tmp_info,
   '(~{1})(\d+)(!{1})(.{0,2})(~{1})(\d+)(!{1})','\2');
   
   if tmp_info is null then
   goto ret_end;
   end if ;
   
   select pro||city||county||region into tmp_info
   from wl_info_data_region
   where serial=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_FROM_S
prompt ===========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_from_s
  (info in varchar2,in_city in varchar2)
--输入物流信息串和城市,输出该城市中的自带起始地
   RETURN VARCHAR2
   IS
--加CITY 输入参数，FROM：只找榆林、NULL替换为榆林  END：<>榆林
   out_info  VARCHAR2 (8000);
  begin
  out_info :='';
 declare
  cursor job_region_from
    is
  select STATE,pro||city||county place from wl_info_data_region where city=in_city order by id DESC;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if REGEXP_SUBSTR(info,c_row.state) is not null then 
        out_info :=REGEXP_SUBSTR(info,c_row.state);
        RETURN (out_info);
    end if;
   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;
  if out_info is null 
     then out_info :=IN_CITY;
  end if;
  RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_SERIAL
prompt ===========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_serial
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag number(1);
  begin
  tmp_info :=substr(info,1,15);
  flag     :=0;
 declare
  cursor job_region_from
    is
  select state,serial from wl_info_data_region order by id desc;
  c_row job_region_from%rowtype;
 begin
  open job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then   
        return (tmp_info);
        end if ;
    
    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;

  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_REGION_SIMPLE
prompt ===========================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_region_simple
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
  begin
   tmp_info:=
   regexp_substr(info,'~\d+!((一)|(到)|(至)){1}~\d+!');

  return (tmp_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_RULE
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_rule (info in varchar2)
RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
out_info  VARCHAR2 (2000);

begin
  out_info := info;
  
  declare
  cursor c_job is select * from wl_info_data_rule order by id;
  c_row c_job%rowtype;
  
  begin
    open c_job;
    loop
      --提取一行数据到c_row
      fetch c_job into c_row;
      --判读是否提取到值，没取到值就退出
      --取到值c_job%notfound 是false
       --取不到值c_job%notfound 是true
       exit when c_job%notfound;
       out_info :=REGEXP_REPLACE(out_info,c_row.find,c_row.REPLACE);
       
    end loop;
    close c_job;
  end;
  
RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_INFO_TRUCK
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION get_wl_info_truck (info in varchar2)
       RETURN VARCHAR2
--输入物流信息串,输出串中车型
  is
   out_info  VARCHAR2 (200);
  begin
    out_info :='';
  declare
    cursor job_truck
      is
    select id,truck,desc1 
    from wl_info_data_truck 
    order by id desc;
    c_row job_truck%rowtype;
  begin
    open job_truck;
  
    if regexp_substr(info,'\d{1,2}(吨){1}') is not null then 
       out_info :=regexp_substr(info,'\d{1,2}(吨){1}');
    end if;

 
    if regexp_substr(info,'\d{1,2}(\.\d{1})?((米)|(m)|(M)){1}') is not null then 
       out_info :=out_info||','||regexp_substr(info,'\d{1,2}(\.\d{1})?((米)|(m)|(M)){1}');
    end if;
    
   loop
      if regexp_substr(info,c_row.truck) is not null
         Then 
             out_info :=regexp_substr(info,c_row.truck);
             return (out_info||','||c_row.desc1);
      end if;
      --提取一行数据到c_row
      fetch job_truck into c_row;
      --判读是否提取到值，没取到值就退出
      --取到值job_truck%notfound 是false
      --取不到值job_truck%notfound 是true
      exit when job_truck%notfound;
    end loop;
    
  
  close job_truck;
  end;
  RETURN (out_info);
end;
/

prompt
prompt Creating function GET_WL_KM_CITY
prompt ================================
prompt
CREATE OR REPLACE FUNCTION get_wl_km_city
(in_lng in number,in_lat in number) RETURN VARCHAR2 IS
--输入经度和纬度,输出城市名
out_info varchar2(64);
  begin
  out_info := '';
  declare
    cursor job is
------select trunc(get_distance(lng,lat,in_lng,in_lat),2) distance,t.pro||t.city place
      select t.pro||t.city place
      from wl_info_data_region t
      where lng is not null
      and lat is not null
      and county is null
      and get_distance(lng,lat,in_lng,in_lat)<300
      order by get_distance(lng,lat,in_lng,in_lat);

    c_row job%rowtype;
  begin
    open job;
      loop

         fetch job into c_row;
         exit when job%notfound;
         if out_info IS null
         Then out_info := c_row.place;
              return(out_info);
         end if;
       end loop;

    close job;
  end;
  RETURN(out_info);
end;
/

prompt
prompt Creating function GET_WL_KM_CITY1
prompt =================================
prompt
CREATE OR REPLACE FUNCTION get_wl_km_city1
(in_lng IN number,in_lat in number,in_km in number) RETURN VARCHAR2 IS

out_info varchar2(2000);
  begin
  out_info := '';
  declare
    cursor job is
      select trunc(get_distance(lng,lat,121,31),2) distance,t.pro||t.city place
      from wl_info_data_region t
      where lng is not null
      and lat is not null
      and county is null
      and get_distance(lng,lat,121,31)<100
      order by trunc(get_distance(lng,lat,121,31),2);

    c_row job%rowtype;
  begin
    open job;
      loop
             out_info := out_info||c_row.distance||'KM----城市:'||c_row.place;
            --  return(out_info);
 
         fetch job into c_row;
         exit when job%notfound;
       end loop;

    close job;
  end;
  RETURN(out_info);
end;
/

prompt
prompt Creating function GET_WL_REPLACE_LOCAL_PLACE
prompt ============================================
prompt
create or replace function get_wl_replace_local_place (info in varchar2)
   return varchar2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
is
   out_info  VARCHAR2 (2000);
begin
out_info := info;
declare
cursor c_job
is

select *--state,lng,lat,
--pro||city||county||region full_name
--county||region name1
--,to_char(serial) serial
from lxf_local_place order by id  desc;

c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true

out_info :=regexp_replace(out_info,c_row.state,
'<full_name>'||c_row.pro||c_row.city||c_row.county||c_row.region||'<\full_name>');
fetch c_job into c_row;

exit when c_job%notfound;
end loop;
close c_job;
end;
return (out_info);
end;
/

prompt
prompt Creating function GET_XML_ELEMENT
prompt =================================
prompt
CREATE OR REPLACE FUNCTION get_xml_element
(in_str in varchar2,pattern in varchar2,in_start in varchar2,how in varchar2)
   RETURN VARCHAR2
  --输入字符串、关键字、起始位置、第几个，输出取到的字符
  is
   pattern_0  varchar2 (200);
   pattern_1  varchar2 (200);
   pattern_2  varchar2 (200);
   id_1       varchar2 (200);
   id_2       varchar2 (200);
   id_3       varchar2 (200);
   id_4       varchar2 (200);
   out_str    varchar2 (200);
  begin
  
   pattern_0:=lower(pattern);
   pattern_1:='<' ||pattern_0||'>';
   pattern_2:='<\'||pattern_0||'>';
   id_1     :=instr(in_str,pattern_1,in_start,how);
   id_2     :=instr(in_str,pattern_2,in_start,how);
   id_3     :=id_1+length(pattern_1);
   id_4     :=id_2-id_1-length(pattern_1);--<place>7个长度
   out_str  :=substr(in_str,id_3,id_4);

/*

if pattern='place' then
   pattern_1:='<place>' ;
   pattern_2:='<\place>';
   id_1     :=instr(in_str,pattern_1,in_start,how);
   id_2     :=instr(in_str,pattern_2,in_start,how);
   id_3     :=id_1+7;
   id_4     :=id_2-id_1-7;--<place>7个长度
   dbms_output.put_line(id_1||id_2||id_3||id_4);
   out_str  :=substr(in_str,id_3,id_4);
end if;

if pattern='id' then
   pattern_1:='<id>' ;
   pattern_2:='<\id>';
   id_1     :=instr(in_str,pattern_1,in_start,how);
   id_2     :=instr(in_str,pattern_2,in_start,how);
   id_3     :=id_1+4;
   id_4     :=id_2-id_1-4;--<id>4个长度
   dbms_output.put_line(id_1||id_2||id_3||id_4);
   out_str  :=substr(in_str,id_3,id_4);
end if;

if pattern='name' then
   pattern_1:='<name>' ;
   pattern_2:='<\name>';
   id_1     :=instr(in_str,pattern_1,in_start,how);
   id_2     :=instr(in_str,pattern_2,in_start,how);
   id_3     :=id_1+6;
   id_4     :=id_2-id_1-6;--<name>6个长度
   dbms_output.put_line(id_1||id_2||id_3||id_4);
   out_str  :=substr(in_str,id_3,id_4);
end if;
*/
    return(out_str);
  end;
/

prompt
prompt Creating function LXF_CLEAR_HEAD
prompt ================================
prompt
CREATE OR REPLACE FUNCTION lxf_clear_head
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (4000);

  begin

---cut one words
 tmp_info:=substr(info, regexp_instr(info,'#P\d+#'));

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_CUT_FIRST_PLACE_NAME
prompt ==========================================
prompt
CREATE OR REPLACE FUNCTION lxf_cut_first_place_name
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (4000);

  begin

---cut one words
 tmp_info:=substr(info, regexp_instr(info,'#P\d+#')+length(regexp_substr(info,'#P\d+#')));

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_GET_0912_PLACE_NAME
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_0912_place_name
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   out_info  varchar (500); 
   ct      number(5);
  begin

  tmp_info :=regexp_substr(info,'\d+');
  

     if tmp_info is null then
     goto ret_end;
   end if;
  
  


select count(*) into ct 
from lxf_local_place
where serial=to_number(tmp_info);

   if ct = 0  then 
     goto ret_end;
   end if;


   select pro||city||county||region into out_info
   from lxf_local_place
   where serial=to_number(tmp_info);

 <<ret_end>>
  return (out_info);
end;
/

prompt
prompt Creating function LXF_GET_GOODS
prompt ===============================
prompt
CREATE OR REPLACE FUNCTION lxf_get_goods
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
  begin

  tmp_info :=regexp_substr(info,'\d+(\.\d{1})?');
  flag     :=0;
  if  length(tmp_info) >5 then

 if substr(tmp_info,1,5)='10001' then 
       return (substr(tmp_info,6)||'吨');
     end if ;
  if substr(tmp_info,1,5)='10002' then 
       return (substr(tmp_info,6)||'公斤');
     end if ;
      if substr(tmp_info,1,5)='10003' then 
       return (substr(tmp_info,6)||'立方');
     end if ;
      if substr(tmp_info,1,5)='10004' then 
       return (substr(tmp_info,6)||'台');
     end if ;
      if substr(tmp_info,1,5)='10005' then 
       return (substr(tmp_info,6)||'部');
     end if ;
      if substr(tmp_info,1,5)='10006' then 
       return (substr(tmp_info,6)||'件');
     end if ;
      if substr(tmp_info,1,5)='10007' then 
       return (substr(tmp_info,6)||'车');
     end if ;
      if substr(tmp_info,1,5)='10008' then 
       return (substr(tmp_info,6)||'米');
     end if ;
      if substr(tmp_info,1,5)='10009' then 
       return (substr(tmp_info,6)||'部');
     end if ;
      if substr(tmp_info,1,5)='10010' then 
       return (substr(tmp_info,6)||'箱');
     end if ;
     if substr(tmp_info,1,5)='10011' then 
       return (substr(tmp_info,6)||'辆');
     end if ; 
  
    /*   select  regexp_replace( '305.8吨       206吨','([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?吨',  '#G10001\1\2#')from dual;
       select  regexp_replace( '305.8公斤   206公斤','([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?公斤','#G10002\1\2#')from dual;
       select  regexp_replace( '305.8立方   206立方','([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?立方','#G10003\1\2#')from dual;
       select  regexp_replace( '305.8台       006台','([1|2|3|4|5|6|7|8|9]\d{0,2})台'            ,'#G10004\1\2#')from dual;
       select  regexp_replace( '305.8部       006部','([1|2|3|4|5|6|7|8|9]\d{0,2})部'            ,'#G10005\1\2#')from dual;
       select  regexp_replace( '305.8件       006件','([1|2|3|4|5|6|7|8|9]\d{0,2})件'            ,'#G10006\1\2#')from dual;
       select  regexp_replace( '305.8车       006车','([1|2|3|4|5|6|7|8|9]\d{0,2})车'            ,'#G10007\1\2#')from dual;
       select  regexp_replace( '305.8米       006米','([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?米'  ,'#G10008\1\2#')from dual;
       select  regexp_replace( '305.8部       006部','([1|2|3|4|5|6|7|8|9]\d{0,2})部'            ,'#G10009\1\2#')from dual;
       select  regexp_replace( '305.8张       006箱','([1|2|3|4|5|6|7|8|9]\d{0,2})箱'            ,'#G10010\1\2#')from dual;
*/
tmp_info :=tmp_info;



  end if;
  if tmp_info is null then
     goto ret_end;
   end if;

   select desc1 into tmp_info
   from wl_info_data_goods
   where ID=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_GET_NOTE
prompt ==============================
prompt
CREATE OR REPLACE FUNCTION lxf_get_note
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
  begin

  tmp_info :=regexp_substr(info,'\d+');
  flag     :=0;

   if tmp_info is null then
     goto ret_end;
   end if;

   select desc1 into tmp_info
   from wl_info_data_note
   where ID=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_GOODS
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_goods (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS

   out_info  VARCHAR2 (2000);

begin
out_info := info;

declare
cursor c_job
is
 select id,goods,desc1 from wl_info_data_goods order by id;

c_row c_job%rowtype;
begin

open c_job;
loop

--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=REGEXP_REPLACE(out_info,c_row.goods,' #G'||c_row.id||'# ');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_LOCAL_PLACE
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_local_place (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (2000);
begin
out_info := info;
declare
cursor c_job
is
select * from lxf_local_place order by id  desc ,serial desc ;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=regexp_REPLACE(out_info,c_row.state,'#P'||to_char(c_row.serial)||'#');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_NOTE
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_note (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (2000);
begin
out_info := info;
declare
cursor c_job
is
  select id,note,desc1 
      from wl_info_data_note
      order by id;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=regexp_REPLACE(out_info,c_row.note,' #N'||to_char(c_row.id)||'# ');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_PLACE_NAME
prompt ========================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_place_name (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (2000);
begin
out_info := info;
declare
cursor c_job
is
select * from wl_info_data_region order by id  desc;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=regexp_REPLACE(out_info,c_row.state,' #P'||to_char(c_row.serial)||'#');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_TRUNK
prompt ===================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_trunk (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (2000);
begin
out_info := info;
declare
cursor c_job
is
    select id,truck,desc1 
    from wl_info_data_truck 
    order by id;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=regexp_REPLACE(out_info,c_row.truck,' #T'||to_char(c_row.id)||'# ');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_REPLACE_WRONG_WORD
prompt ========================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_wrong_word (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS

   out_info  VARCHAR2 (2000);

begin
out_info := info;

declare
cursor c_job
is
select * from wl_info_data_rule order by id;

c_row c_job%rowtype;
begin

open c_job;
loop

--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=REGEXP_REPLACE(out_info,c_row.find,c_row.REPLACE);
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function LXF_GET_PARAMETER_STRING
prompt ==========================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_parameter_string
  (in_str in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   in_str1   varchar (2000);
   new_str1  varchar (2000);
   flag      number(1);
   v1 number(5)  :=1;
 begin

 in_str1:=in_str;
      -----置换系统内处理标志--
     in_str1:=replace(in_str1,'#','');
     --------错别字|别名替换-----------
       in_str1:=lxf_replace_wrong_word(in_str1);
       -------本地地名替换P----------------
       in_str1:=lxf_replace_local_place(in_str1);
       -------国标地名替换P----------------
       in_str1:=lxf_replace_place_name(in_str1);
       -------货物类型替换G----------------
       in_str1:=lxf_replace_goods(in_str1);
       -------特殊单位替换----------
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?吨'  ,'#G10001\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?公斤','#G10002\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})(\.\d{1})?立方','#G10003\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})台'            ,'#G10004\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})部'            ,'#G10005\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})件'            ,'#G10006\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})车'            ,'#G10007\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,1})(\.\d{1})?米'  ,'#G10008\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})部'            ,'#G10009\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})箱'            ,'#G10010\1\2#');
       in_str1:=regexp_replace( in_str1,'([1|2|3|4|5|6|7|8|9]\d{0,2})辆'            ,'#G10011\1\2#');
       ------车辆类型替换T-----------------
       in_str1:=lxf_replace_trunk(in_str1);
       ------备注信息替换N-----------------

       in_str1:=lxf_replace_note(in_str1);
  --       dbms_output.put_line(in_str1);

       -----运输价格识别M2---
      -- in_str1:=regexp_replace( in_str1, '([1|2|3|4|5|6|7|8|9]\d{2})((/吨)|(元))','#M\1#');
       in_str1:=regexp_replace( in_str1, '([1|2|3|4|5|6|7|8|9]\d{2})((元/吨)|(元))','#M\1#');
       -----电话号码识别C------
      -- in_str1:=regexp_replace( in_str1, '([[:digit:]]{11}|[[:digit:]]{7,8})','#C\1#');
       in_str1:=regexp_replace( in_str1, '(1[3|4|5|7|8][[:digit:]]{9})','#C\1#');
        -----替换所有空格----------
       in_str1:=replace(in_str1,' ','');

   --   dbms_output.put_line(in_str1);
       -----运输价格识别M---地名后2-4位数字为价格--------------
       in_str1:=regexp_replace( in_str1,'#P([[:digit:]]+)#([1|2|3|4|5|6|7|8|9]\d{2})','#P\1##M\2#');
       -----地名连接符号识别X-------------
       in_str1:=regexp_replace( in_str1,'#P([[:digit:]]+)#(煤场)?(到|>|》|一|二|道|至|一|=|-| )+#P([[:digit:]]+)#','#P\1##X1##P\4#');
  --  dbms_output.put_line(in_str1);
        -----地名连接符号识别X-------------
       in_str1:=regexp_replace( in_str1,'#P([[:digit:]]+)#(煤场)?(到|>|》|一|二|道|至|一|=|-| )+#P([[:digit:]]+)#','#P\1##X1##P\4#');
   -- dbms_output.put_line(in_str1||'1');
       -----地名连接符号识别X2-------------
       in_str1:=regexp_replace( in_str1,'#P([[:digit:]]+)##M([[:digit:]]{2,4})#在?#P([[:digit:]]+)#装','#P\3##X1##P\1##M\2#');
  --  dbms_output.put_line(in_str1||'2');
            -----地名连接符号识别X2-------------
       in_str1:=regexp_replace( in_str1,'#P([[:digit:]]+)#(在)?#P([[:digit:]]+)#装','#P\3##X1##P\1#');
   -- dbms_output.put_line(in_str1||'3');

       -----得到编码格式--------------
       loop
         new_str1 := new_str1 || regexp_substr(in_str1,'#[[:upper:]]([[:digit:]]|\.)+#',1,v1);
         v1:=v1+1;
         exit when v1=200 or     regexp_instr(in_str1,'#[[:upper:]]([[:digit:]]|\.)+#',1,v1)=0;
       end loop;



 <<ret_end>>
  return (new_str1);
end;
/

prompt
prompt Creating function LXF_GET_PLACE_NAME
prompt ====================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_place_name
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   ct      number(5);
  begin

  tmp_info :=regexp_substr(info,'\d+');
  ct     :=0;

   if tmp_info is null then
     goto ret_end;
   end if;

select count(*) into ct 
from wl_info_data_region
where serial=to_number(tmp_info);
   if ct = 0 then 
     goto ret_end;
   end if;



   select pro||city||county||region into tmp_info
   from wl_info_data_region
   where serial=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating procedure LXF_WORDS_CLIPPER
prompt ====================================
prompt
create or replace procedure lxf_words_clipper


    (sub_sentence       in out  varchar2
     ,out_words            out  varchar2
    )
is
  begin
  
  out_words:=regexp_substr(sub_sentence,'#\w\d+(\.\d{1})?#');
  sub_sentence:=substr(sub_sentence,regexp_instr(sub_sentence,'#\w\d+(\.\d{1})?#')+length(out_words));
       
end;
/

prompt
prompt Creating function LXF_GET_TRUNK
prompt ===============================
prompt
CREATE OR REPLACE FUNCTION lxf_get_trunk
  (info in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (500);
   flag      number(1);
  begin

  tmp_info :=regexp_substr(info,'\d+');
  flag     :=0;

   if tmp_info is null then
     goto ret_end;
   end if;

   select desc1 into tmp_info
   from wl_info_data_truck
   where ID=to_number(tmp_info);

 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_RECOVER_SENTENCE
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION lxf_recover_sentence
  (in_qq in varchar2,in_qq_group_nbr in varchar2,in_sub_sentence in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   sub_sentence  varchar (4000);
   new_word     varchar2(100);
   out_sub_sentence  varchar (4000);
   ------------------------------------
   new_from_place  varchar2(500);
   new_end_place  varchar2(500);
   out_info  varchar2(500);
   new_cell_number  varchar2(500);
   new_trunk  varchar2(500);
   new_goods  varchar2(500);
   new_note  varchar2(500);
   info_id  number(10);
  begin
  sub_sentence:=in_sub_sentence;
   -----解析起始城市-----------
    lxf_words_clipper(sub_sentence,new_word);
         if regexp_instr(new_word,'#P[[:digit:]]+#')>0  then
              if   lxf_get_0912_place_name(new_word)  is null
                 then
                   new_from_place:=lxf_get_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_from_place;
                    
                    else
                    new_from_place:=lxf_get_0912_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_from_place;
                
               end if;
          end if;
      -----解析连接符-----------
     lxf_words_clipper(sub_sentence,new_word);
          if regexp_instr(new_word,'#X1#')>0 then
               out_sub_sentence:=out_sub_sentence||'-->>';
          end if;
      -----解析目的城市-----------
      lxf_words_clipper(sub_sentence,new_word);
         if regexp_instr(new_word,'#P[[:digit:]]+#')>0  then
              if   lxf_get_0912_place_name(new_word)  is null
                 then
                   new_end_place:=lxf_get_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_end_place;
                    
                    else
                    new_end_place:=lxf_get_0912_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_end_place;
                
               end if;
          end if;
  --------解析货物、车辆、联系电话、运价信息------------
  loop
  lxf_words_clipper(sub_sentence,new_word);
  exit when  new_word is null;
          if regexp_instr(new_word,'#G\d+(\.\d{1})?#')>0 then
           new_goods:=lxf_get_goods(new_word);
           out_sub_sentence:=out_sub_sentence||','||new_goods;
          end if;
          if regexp_instr(new_word,'#N[[:digit:]]+#')>0 then
             new_note:= lxf_get_note(new_word);   
             out_sub_sentence:=out_sub_sentence||','||new_note;
          end if;
          if regexp_instr(new_word,'#T[[:digit:]]+#')>0 then
               new_trunk:=lxf_get_trunk(new_word);
              out_sub_sentence:=out_sub_sentence||','||new_trunk;
          end if;
          if regexp_instr(new_word,'#C[[:digit:]]+#')>0 then
             new_cell_number:=regexp_substr(new_word,'[[:digit:]]+');  
          --  out_sub_sentence:=out_sub_sentence||',联系电话'||new_cell_number;
          end if;
          if regexp_instr(new_word,'#M[[:digit:]]+#')>0 then
              out_sub_sentence:=out_sub_sentence||',运价'||regexp_substr(new_word,'[[:digit:]]+')||'元/吨';
          end if;

 end loop;
 if  new_from_place is not null and new_end_place is not null
 then
--out_sub_sentence:=out_sub_sentence;
    info_id:=get_wl_infoid(in_qq,in_qq_group_nbr,new_from_place,new_end_place,out_sub_sentence
              ,new_cell_number,new_trunk,new_goods,new_note);
   
 end if;
   if new_cell_number is not null 
     then 
       out_sub_sentence:=out_sub_sentence||'，联系电话'||new_cell_number;
    end if ;
 <<ret_end>>
  return (out_sub_sentence);
end;
/

prompt
prompt Creating function LXF_REPLACE_STR1
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_str1
  (str1 in varchar2,str2 in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (4000);

  begin
  tmp_info:=str1;
---cut one words
if str2 is not null then
    tmp_info:=str2;
  end if ;
 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating function LXF_SENTENCE_IS_USELESS
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION lxf_sentence_is_useless
  (sentence in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN number
   IS
   ret  number (1);

  begin
   ret:=0;
---
if  regexp_instr( sentence, '#P\d+#')>0  then
     ret:=1;
  end if ;
 <<ret_end>>
  return (ret);
end;
/

prompt
prompt Creating function LXF_STRING_MINUS
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION lxf_string_minus
  (str1 in varchar2,str2 in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   tmp_info  varchar (4000);

  begin
tmp_info:=str1;
---cut one words
if substr(tmp_info,1,length(str2))=str2 then
    tmp_info:=substr(tmp_info,length(str2)+1);
  end if ;
 <<ret_end>>
  return (tmp_info);
end;
/

prompt
prompt Creating procedure LXF_CONBINE_SUB_SENTENCE
prompt ===========================================
prompt
create or replace procedure lxf_conbine_sub_sentence


    (sub_sentence       in out  varchar2
     ,default_cell_phone  in out  varchar2
     ,default_start_city  in out  varchar2
    )
is
begin
    -----替换默认联系电话-----
    if regexp_substr(sub_sentence,'#C[[:digit:]]+#') is not null then
        default_cell_phone:=regexp_substr(sub_sentence,'#C[[:digit:]]+#');
    else
        sub_sentence:= sub_sentence||default_cell_phone;   
    end if;
    ----替换默认起始城市---- 
    if regexp_substr(sub_sentence,'^#P[[:digit:]]+##X1#') is not null then
        default_start_city:=regexp_substr(sub_sentence,'^#P[[:digit:]]+#');
    else
        sub_sentence:=default_start_city||'#X1#'||sub_sentence;   
    end if;    
end;
/

prompt
prompt Creating function LXF_GET_QQ_MSG_BACK
prompt =====================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_qq_msg_back(in_qq varchar2
                                      ,in_qq_group varchar2
                                      ,in_str varchar2)

     RETURN varchar2
                  is
    in_sentence varchar2(4000);
    sub_sentence varchar2(4000);
    out_sentence varchar2(4000);
----------------------------------
    default_start_city  varchar2(500);
    default_cell_phone  varchar2(500);
    attempt_flag  number(2);
    ct  number(2);--子句条数
    tmp_str varchar2(4000);--存放子句
 begin
   ct:=0;
   in_sentence:=lxf_get_parameter_string(in_str);
     ---------分割句子----------
     --dbms_output.put_line( 'sentence1:'||in_sentence);
     -----设置默认起始地和默认联系电话-------------
     default_start_city:=lxf_replace_str1(get_qq_group_city(in_qq_group),get_cust_city_id(in_qq));
     default_cell_phone:=lxf_replace_str1(get_cust_contact3(in_qq),regexp_substr(in_sentence,'#C[[:digit:]]+#'));
     if default_cell_phone is null then return '无联系电话，信息识别失败.'; end if;
     attempt_flag:=2;
    loop
    if attempt_flag<=0 then 
       in_sentence:= lxf_cut_first_place_name(in_sentence);
       attempt_flag:=2;
     end if ;
    
    ----模式一-----------------------------
    in_sentence:=lxf_clear_head(in_sentence);
    sub_sentence:=regexp_substr(in_sentence,'^#P\d+##X\d+##P\d+#((#N\d+#)|(#G\d+(\.\d{1})?#)|(#T\d+#)|(#C\d+#)|(#M\d+#))+');
    if sub_sentence is not null                                            
     then
       in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
        attempt_flag:=2;
        ct:=ct+1;
       lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
     ------- 
      dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
       if ct<=5 then 
            out_sentence:=out_sentence||lxf_recover_sentence(in_qq,in_qq_group,sub_sentence)||';'||chr(13)||chr(10);
        else
            tmp_str:=lxf_recover_sentence(in_qq,in_qq_group,sub_sentence);
        end if;
       
       
       else 
         attempt_flag:=attempt_flag-1;
    end if ;
----模式二------------------------------
    in_sentence:=lxf_clear_head(in_sentence);
    sub_sentence:=regexp_substr(in_sentence,'^#P\d+##M\d+#((#N\d+#)|(#G\d+(\.\d{1})?#)|(#T\d+#)|(#C\d+#))*');
    if sub_sentence is not null
      then
       in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
        attempt_flag:=2;
        ct:=ct+1;
       lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
    -------- 
     dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
      if ct<=5 then 
            out_sentence:=out_sentence||lxf_recover_sentence(in_qq,in_qq_group,sub_sentence)||';'||chr(13)||chr(10);
        else
            tmp_str:=lxf_recover_sentence(in_qq,in_qq_group,sub_sentence);
        end if ;
       else 
         attempt_flag:=attempt_flag-1;
    end if ;    
----------------------------------  

    exit when  lxf_sentence_is_useless(in_sentence)=0;
   end loop;
   if ct>5 then 
    out_sentence:=out_sentence||'.....共发送'||ct||'条信息。';
   end if ;
   
   return out_sentence;

end;
/

prompt
prompt Creating function LXF_GET_QQ_MSG_BACK1
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_qq_msg_back1(in_qq varchar2
                                      ,in_qq_group varchar2,in_city varchar2
                                      ,in_str varchar2)

     RETURN varchar2
     /*
     -10 城市参数错误
     -11 没有默认的电话号码
     返回值大于0，正确返回，其中返回值为识别记录的条数
     */
                  is
    in_sentence varchar2(4000);
    sub_sentence varchar2(4000);
----------------------------------
    default_start_city  varchar2(500);
    default_cell_phone  varchar2(500); 
    attempt_flag  number(2);
    ct  number(2);--子句条数
    tmp_str varchar2(4000);--存放子句
    ret number(5);
 begin
   ct:=0;
   in_sentence:=lxf_get_parameter_string(in_str);
     ---------分割句子----------
     --dbms_output.put_line( 'sentence1:'||in_sentence);
     -----设置默认起始地和默认联系电话-------------
     
        ----信息列表-----
   if regexp_instr(trim(lxf_replace_place_name(trim(in_city))),'^#P\d+#$')>0 then
     default_start_city:=trim(lxf_replace_place_name(trim(in_city)));
     else
       ret:=-10;
       goto ret_end;
    end if ;    
     default_cell_phone:=regexp_substr(in_sentence,'#C[[:digit:]]+#');
     if default_cell_phone is null then 
        ret:=-11;
        goto ret_end;
      end if;
     attempt_flag:=2;
     ----------开始识别子句----------------
    loop
      if attempt_flag<=0 then 
         in_sentence:= lxf_cut_first_place_name(in_sentence);
         attempt_flag:=2;
      end if ;
    ----模式一-----------------------------
      in_sentence:=lxf_clear_head(in_sentence);
      sub_sentence:=regexp_substr(in_sentence,'^#P\d+##X\d+##P\d+#((#N\d+#)|(#G\d+#)|(#T\d+#)|(#C\d+#)|(#M\d+#))+');
      if sub_sentence is not null  then
         in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
         attempt_flag:=2;
         ct:=ct+1;
         lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
       --dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
         tmp_str:=lxf_recover_sentence(in_qq,in_qq_group,sub_sentence);
        else 
         attempt_flag:=attempt_flag-1;
      end if ;
----模式二------------------------------
      in_sentence:=lxf_clear_head(in_sentence);
      sub_sentence:=regexp_substr(in_sentence,'^#P\d+##M\d+#((#N\d+#)|(#G\d+#)|(#T\d+#)|(#C\d+#))*');
      if sub_sentence is not null
           then
              in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
              attempt_flag:=2;
              ct:=ct+1;
              lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
              -- dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
                   tmp_str:=lxf_recover_sentence(in_qq,in_qq_group,sub_sentence);
           else 
              attempt_flag:=attempt_flag-1;
       end if ;    
----------------------------------  
       exit when  lxf_sentence_is_useless(in_sentence)=0;
   end loop;

   ret:=ct;
   
   
   <<ret_end>>
   
   return ret;

end;
/

prompt
prompt Creating function LXF_GET_SUB_SENTENCE
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_sub_sentence
  (in_sentence in varchar2,model_str in varchar2 )
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   out_sentence  varchar (4000);
  begin
    out_sentence:= regexp_substr(in_sentence,model_str);

 <<ret_end>>
  return (out_sentence);
end;
/

prompt
prompt Creating function LXF_WEB_RECOVER_SENTENCE
prompt ==========================================
prompt
CREATE OR REPLACE FUNCTION lxf_Web_recover_sentence
  (in_qq in varchar2,in_sub_sentence in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   sub_sentence  varchar (4000);
   new_word     varchar2(100);
   out_sub_sentence  varchar (4000);
   ------------------------------------
   new_from_place  varchar2(500);
   new_end_place  varchar2(500);
   out_info  varchar2(500);
   new_cell_number  varchar2(500);
   new_trunk  varchar2(500);
   new_goods  varchar2(500);
   new_note  varchar2(500);
   info_id  number(10);
  begin
  sub_sentence:=in_sub_sentence;
   -----解析起始城市-----------
    lxf_words_clipper(sub_sentence,new_word);
         if regexp_instr(new_word,'#P[[:digit:]]+#')>0  then
              if   lxf_get_0912_place_name(new_word)  is null
                 then
                   new_from_place:=lxf_get_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_from_place;

                    else
                    new_from_place:=lxf_get_0912_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_from_place;

               end if;
          end if;
      -----解析连接符-----------
     lxf_words_clipper(sub_sentence,new_word);
          if regexp_instr(new_word,'#X1#')>0 then
               out_sub_sentence:=out_sub_sentence||'-->>';
          end if;
      -----解析目的城市-----------
      lxf_words_clipper(sub_sentence,new_word);
         if regexp_instr(new_word,'#P[[:digit:]]+#')>0  then
              if   lxf_get_0912_place_name(new_word)  is null
                 then
                   new_end_place:=lxf_get_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_end_place;

                    else
                    new_end_place:=lxf_get_0912_place_name(new_word);
                    out_sub_sentence:=out_sub_sentence||new_end_place;

               end if;
          end if;
  --------解析货物、车辆、联系电话、运价信息------------
  loop
  lxf_words_clipper(sub_sentence,new_word);
  exit when  new_word is null;
          if regexp_instr(new_word,'#G\d+(\.\d{1})?#')>0 then
           new_goods:=lxf_get_goods(new_word);
           out_sub_sentence:=out_sub_sentence||','||new_goods;
          end if;
          if regexp_instr(new_word,'#N[[:digit:]]+#')>0 then
             new_note:= lxf_get_note(new_word);
             out_sub_sentence:=out_sub_sentence||','||new_note;
          end if;
          if regexp_instr(new_word,'#T[[:digit:]]+#')>0 then
               new_trunk:=lxf_get_trunk(new_word);
              out_sub_sentence:=out_sub_sentence||','||new_trunk;
          end if;
          if regexp_instr(new_word,'#C[[:digit:]]+#')>0 then
             new_cell_number:=regexp_substr(new_word,'[[:digit:]]+');
          --  out_sub_sentence:=out_sub_sentence||',联系电话'||new_cell_number;
          end if;
          if regexp_instr(new_word,'#M[[:digit:]]+#')>0 then
              out_sub_sentence:=out_sub_sentence||',运价'||regexp_substr(new_word,'[[:digit:]]+')||'元/吨';
          end if;

 end loop;
 if  new_from_place is not null and new_end_place is not null
 then
--out_sub_sentence:=out_sub_sentence;
    info_id:=get_Web_wl_infoid(in_qq,new_from_place,new_end_place,out_sub_sentence
              ,new_cell_number,new_trunk,new_goods,new_note);

 end if;
   if new_cell_number is not null
     then
       out_sub_sentence:=out_sub_sentence||'，联系电话'||new_cell_number;
    end if ;
 <<ret_end>>
  return (out_sub_sentence);
end;
/

prompt
prompt Creating function LXF_GET_WEB_MSG_BACK
prompt ======================================
prompt
CREATE OR REPLACE FUNCTION lxf_get_Web_msg_back(in_qq varchar2
                                      ,in_str varchar2)

     RETURN varchar2
                  is
    in_sentence varchar2(4000);
    sub_sentence varchar2(4000);
    out_sentence varchar2(4000);
----------------------------------
    default_start_city  varchar2(500);
    default_cell_phone  varchar2(500);
    attempt_flag  number(2);
    ct  number(2);--子句条数
 begin
   ct:=0;
   in_sentence:=lxf_get_parameter_string(in_str);
     ---------分割句子----------
     --dbms_output.put_line( 'sentence1:'||in_sentence);
     -----设置默认起始地和默认联系电话-------------
     default_cell_phone:=lxf_replace_str1(get_cust_contact3(in_qq),regexp_substr(in_sentence,'#C[[:digit:]]+#'));
     if default_cell_phone is null then return '无联系电话，信息识别失败.'; end if;
     attempt_flag:=2;
    loop
    if attempt_flag<=0 then
       in_sentence:= lxf_cut_first_place_name(in_sentence);
       attempt_flag:=2;
     end if ;

    ----模式一-----------------------------
    in_sentence:=lxf_clear_head(in_sentence);
    sub_sentence:=regexp_substr(in_sentence,'^#P\d+##X\d+##P\d+#((#N\d+#)|(#G\d+(\.\d{1})?#)|(#T\d+#)|(#C\d+#)|(#M\d+#))+');
    if sub_sentence is not null
     then
       in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
        attempt_flag:=2;
        ct:=ct+1;
       lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
     -------
      --dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
       if ct<=5 then
            out_sentence:=out_sentence||lxf_web_recover_sentence(in_qq,sub_sentence)||';'||chr(13)||chr(10);
        else
            out_sentence:=lxf_web_recover_sentence(in_qq,sub_sentence);
        end if;
       else
         attempt_flag:=attempt_flag-1;
    end if ;
----模式二------------------------------
    in_sentence:=lxf_clear_head(in_sentence);
    sub_sentence:=regexp_substr(in_sentence,'^#P\d+##M\d+#((#N\d+#)|(#G\d+(\.\d{1})?#)|(#T\d+#)|(#C\d+#))*');
    if sub_sentence is not null
      then
       in_sentence:=lxf_string_minus(in_sentence,sub_sentence);
        attempt_flag:=2;
        ct:=ct+1;
       lxf_conbine_sub_sentence(sub_sentence,default_cell_phone,default_start_city );
    --------
     --dbms_output.put_line( 'sub_sentence1:'||sub_sentence);
      if ct<=5 then
            out_sentence:=out_sentence||lxf_Web_recover_sentence(in_qq,sub_sentence)||';'||chr(13)||chr(10);
        else
            out_sentence:=lxf_web_recover_sentence(in_qq,sub_sentence);
        end if ;
       else
         attempt_flag:=attempt_flag-1;
    end if ;
----------------------------------
    exit when  lxf_sentence_is_useless(in_sentence)=0;
   end loop;
   if ct>5 then
      out_sentence:=out_sentence||'.....共发送'||ct||'条信息。';
   end if ;
  return out_sentence;

end;
/

prompt
prompt Creating function LXF_REPLACE_UNIT
prompt ==================================
prompt
CREATE OR REPLACE FUNCTION lxf_replace_unit (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (4000);
begin
out_info := info;
declare
cursor c_job
is
  select id,note,desc1
      from wl_info_data_note
      order by id;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
out_info :=regexp_REPLACE(out_info,c_row.note,' #N'||to_char(c_row.id)||'# ');
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating function WL_CUST_IS_EFF_MOBLIE
prompt =======================================
prompt
CREATE OR REPLACE FUNCTION wl_cust_is_eff_moblie
  ( in_nbr  in varchar2)
--输入物流信息串,输出该城市中的省市县区全称起始地
   RETURN VARCHAR2
   IS
   ct  number(2);
   ret number (2);

  begin
---cut one words
  select count(*) into ct
  from wl_cust_mobile where qq=trim(in_nbr) and sysdate between create_date and eff_date;

  if ct> 0 then
     ret:=1;
  else
     ret:=0;
  end if;
  return (ret);
end;
/

prompt
prompt Creating function WL_XS_GETPAYMENT
prompt ==================================
prompt
create or replace function WL_XS_GetPayMent(saleid1 in varchar2) return number is
  /*该函数获取传入销售编号的缴费总和*/
  tempMoney number(5):=0;
  tempCount number(5):=0;
begin
  select count(*) into tempCount 
     from wl_xs_payment 
      where saleid=to_number(saleid1);
  if tempCount!=0 then
    select sum(paymentamount) into tempMoney  
      from wl_xs_payment 
     where saleid=to_number(saleid1);
  end if;
  return(tempMoney);
end;
/

prompt
prompt Creating function WL_XS_GETSALECOUNT
prompt ====================================
prompt
create or replace function wl_xs_GetSaleCount(
     saleId1 in varchar2,
     grade in number, 
     saledate in varchar2) 
     return number is
  retcount number(5):=0;
begin
  if regexp_like(saleId1,'^\d{3}000$','i') then
   if grade=0 then
     select count(order_count) into retcount  from wl_orderinfo 
        where order_operator=saleId1 
        and product_id='FFF0D80E7CC344198499B3892A17499D';
      return(retcount);
   end if;
   if grade=1 then
      select count(order_count) into retcount from wl_orderinfo
         where order_operator>to_number(saleId1) 
         and order_operator<to_number(saleId1)+1000
         and  product_id='FFF0D80E7CC344198499B3892A17499D';
   end if;
  elsif grade=1 then
      select count(order_count) into retcount  from wl_orderinfo 
        where  order_operator=saleId1 
        and  product_id='FFF0D80E7CC344198499B3892A17499D';
      return(retcount);
  end if;
  
  return retcount;
end;
/

prompt
prompt Creating function WL_XS_GETSECULAR
prompt ==================================
prompt
create or replace function WL_xs_Getsecular
(
   sender in varchar2,--当前身份
   obj in varchar2--下级身份
   ) 
   return number is
     twoToOne number(8):=70;
     oneToMe0 number(8):=70;
     oneToMe1 number(8):=60;
     Money1 number(8):=0;
     moneyTemp number(8):=0;
     temp_count number(6):=0;
   begin
       -------------超级管理员身份---------------
       if regexp_like(sender,'^admin$','i') then
          ---------------二级销售应该给该一级销售数量---------
          select count(order_count) into temp_count from wl_orderinfo
           where order_operator<to_number(Obj)+1000
           and order_operator>to_number(Obj)
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
            moneyTemp:=temp_count*oneToMe1;  
           ---------------------一级销售自己的数量--------------
           select count(order_count) into temp_count from wl_orderinfo
           where order_operator=Obj
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
             moneyTemp:=moneyTemp+(temp_count*oneToMe0);
           goto ret_end;
       end if;
       -----------------一级销售身份-----------------
        if regexp_like(sender,'^[1-9]\d{2}000$','i') then
          ------------------二级销售应该给一级销售数量----------
          select count(order_count) into temp_count from wl_orderinfo
           where order_operator=to_number(Obj)
           and  product_id='FFF0D80E7CC344198499B3892A17499D';
           moneyTemp:=temp_count*twoToOne;
       end if;
       --------------------二级销售--------------
       <<ret_end>>
  return(moneyTemp);
end;
/

prompt
prompt Creating function WL_XS_GETSHOULDON
prompt ===================================
prompt
create or replace function wl_xs_GetShouldOn(saleid1  in varchar2,grade in number,xsdate in varchar2) return number is
  ret_shouldOn number(8);
  temp_count number(5):=0;
begin
   if regexp_like(saleid1,'^\d{3}000$','i') then
     temp_count:=wl_xs_getsalecount(saleid1,grade,xsdate);
   elsif grade=1 then
     temp_count:=wl_xs_getsalecount(saleid1,grade,xsdate);
   end if;
   if grade=0 then
     ret_shouldOn:=temp_count*70;
   else
     ret_shouldOn:=temp_count*60;
   end if;
  return(ret_shouldOn);
end ;
/

prompt
prompt Creating function WL_XS_GETXSMONEY
prompt ==================================
prompt
create or replace function wl_xs_getxsMoney(saleid1 in varchar2,grade in number,Mdat in varchar2) return number is
  ret_money number(8):=0;
  temp_count number(5):=1;
begin
     --------------------一级销售获取自己的销售金额以及其二级的销售金额-----------
     if regexp_like(saleid1,'^\d{3}000$','i') then
       temp_count:=wl_xs_getsalecount(saleid1,grade,Mdat);
       ret_money:=temp_count*120;
     ----------------------二级只能获取自己的销售金额----------------------------
     elsif grade=1 then
       temp_count:=wl_xs_getsalecount(saleid1,grade,Mdat);
       ret_money:=temp_count*120;
     end if;
  return(ret_money);
end wl_xs_getxsMoney;
/

prompt
prompt Creating function WL_XS_GETXS_INFO
prompt ==================================
prompt
create or replace function wl_xs_GetXS_Info(saleId in varchar2,obj in varchar2) return varchar2 is
  ret_str varchar2(500);
  temp_sql varchar2(1000);
begin
  if obj='name' then
     select max(sname) into ret_str from wl_xs_salesman where saleid=saleId;
  end if;
  if obj='phone' then 
    select max(sphone) into ret_str from wl_xs_salesman where saleid=saleId;
  end if;
  return(ret_str);
end;
/

prompt
prompt Creating function XML_REPLACE_LOCAL_PLACE
prompt =========================================
prompt
CREATE OR REPLACE FUNCTION xml_replace_local_place (info in varchar2)
   RETURN VARCHAR2
--输入物流信息串,输出按处理规则表执行后的新物流信息串
IS
   out_info  VARCHAR2 (2000);
   start_pos  number(5);
   place_len  number(5);
   place_word varchar2(200);
   some_sharp varchar2(200);
begin
out_info := info;
out_info :=replace(out_info,chr(10),'');
out_info :=replace(out_info,chr(13),'');

start_pos:=0;
declare
cursor c_job
is

select  serial id, pro||city||county||region full_name
,(case when id=1 then pro when id=2 then city when id=3 then county when id=4 then county||region else '' end) name
,state
from lxf_local_place  order by id  desc;
c_row c_job%rowtype;
begin
open c_job;
loop
--提取一行数据到c_row
fetch c_job into c_row;

--判读是否提取到值，没取到值就退出
--取到值c_job%notfound 是false
 --取不到值c_job%notfound 是true
exit when c_job%notfound;
start_pos:=regexp_instr(out_info,c_row.state);
if start_pos>0 then
 place_word:=regexp_substr(out_info,c_row.state);
 some_sharp:=regexp_substr(place_word,'.+','#');
 out_info:=regexp_replace(out_info,place_word,some_sharp,start_pos,1);
---out_info :=regexp_REPLACE(out_info,c_row.state,'<place>'||'<id>'||to_char(c_row.id)||'</id>'||'<name>'||c_row.name||'<\name>'||'<full_name>'||c_row.full_name||'<\full_name>'||'<\place>');

end if;
  end loop;
close c_job;
end;
RETURN (out_info);
end;
/

prompt
prompt Creating procedure CREATE_UPDATE_PRODUCT
prompt ========================================
prompt
create or replace procedure Create_Update_Product(
   pro_name in varchar2,--商品名称
   pro_type in varchar2,--商品类型
   pro_value in varchar2,--商品单价
   pro_sate in varchar2,--0 为商家 1 正在销售 2 停止销售
   pro_create_date in varchar2,--开始时间
   pro_expiry_date in varchar2,--到期时间
   pro_note  in varchar2,--产品说明
   out_ret out varchar2--返回状态
)
as
begin
    insert into  wl_productinfo(product_name,product_type,product_value,
    product_state,product_create_date,product_expiry_date,product_note) values
    (
      pro_name,pro_type,pro_value,pro_sate,sysdate,pro_expiry_date,pro_note
    )
    ;
    out_ret:='成功';
end;
/

prompt
prompt Creating procedure PRO_WEB_VISIT_LOG
prompt ====================================
prompt
create or replace procedure Pro_Web_VisiT_Log(
  Ip in varchar2,--对方IP
  city in varchar2,--地址
  Op in number,--操作 0 离线 1 上线
  ret out varchar2--返回值
)
as
 --counSign number(9):=0;
 retStr varchar2(500):='';
begin
   if Ip is null or trim(Ip)='' then
      retStr:='Ip为空';
      goto ret_end;
   end if;
   if Op is null then
      retStr:='操作为空';
      goto ret_end;
   end if;
   if Op=1 then
      insert into WL_Web_Visited_log(VIP,FromCity,IsOnLine,InTime)
        values(Ip,city,1,sysdate);
      retStr:='添加成功!';
      goto ret_end;
   end if;
   if Op=0 then
      insert into WL_Web_Visited_log(VIP,FromCity,IsOnLine,OutTime)
        values(Ip,city,0,sysdate);
      retStr:='操作成!';
      goto ret_end;
   end if;
   <<ret_end>>
   ret:=retStr;
end;
/

prompt
prompt Creating procedure SECURE_DML
prompt =============================
prompt
CREATE OR REPLACE PROCEDURE secure_dml
IS
BEGIN
  IF TO_CHAR (SYSDATE, 'HH24:MI') NOT BETWEEN '08:00' AND '18:00'
        OR TO_CHAR (SYSDATE, 'DY') IN ('SAT', 'SUN') THEN
	RAISE_APPLICATION_ERROR (-20205,
		'You may only make changes during normal office hours');
  END IF;
END secure_dml;
/

prompt
prompt Creating procedure SP_DOWN_INSERT
prompt =================================
prompt
create or replace procedure SP_Down_Insert(
   In_MessId in varchar2,--短信Id
   In_ReceiVer in varchar2,--接收者
   In_Sender in varchar2,--发送者
   In_content in varchar2,--发送的内容
   Priority in number,--优先级别
   ret_out out number--操作结果 0 成功 1 接收者为空 2 发送者为空 3 内容为空
)
as
temp_out number(5);
temp_Priority number(2);
begin
   temp_Priority:=Priority;
   --验证接收者
   if In_ReceiVer is null then
      temp_out:=1;
      goto ret_end;
   end if;
   --验证发送者
   if In_Sender is null then
       temp_out:=2;
      goto ret_end;
   end if;
   --验证内容
   if In_content is null then
        temp_out:=3;
      goto ret_end;
   end if;
   --验证优先级
   if temp_Priority is null then
      temp_Priority:=0;
   end if;
   insert into SP_DOWN(ID,MessageId,ReCEIVER,Sender,Content,sendDate,priority,state)
      values(sp_down_ID.Nextval,In_MessId,In_ReceiVer,In_Sender,In_content,Sysdate,temp_Priority,0);
      commit;
       temp_out:=0;
   <<ret_end>>
   ret_out:=temp_out;
end;
/

prompt
prompt Creating procedure SP_GETSPCODE
prompt ===============================
prompt
create or replace procedure SP_GetSPCODE(
  ret_cur out sys_refcursor--返回结果集
)
as
begin
  open ret_cur for select * from sp_code;
end;
/

prompt
prompt Creating procedure SP_GET_DOWNMES
prompt =================================
prompt
create or replace procedure SP_Get_downMes(
  /*获取下行预发送短信*/
  in_count in number,--一次获取条数
  ret_out out sys_refcursor--返回的结构集
)
as
begin
    /*0未发送，1发送成功，2正在发送，3发送失败,4 已发送未返回状态*/
    update sp_down set state=4 where state=2;
    commit;
    update sp_down set state=2 where sp_down.id in(
      select id from (
           select sp_down.id from sp_down where state=0
            order by priority desc
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_down where state=2;
  end;
/

prompt
prompt Creating procedure SP_GET_UPSMS
prompt ===============================
prompt
create or replace procedure SP_Get_UPSMS(
  in_count in number,--获取条数
  ret_out out sys_refcursor--返回结果集
)
as
begin
    --状态 0 为处理 1 正在处理 2 已处理
    update sp_up set state=2 where state=1;
    commit;
    update sp_up set state=1 where sp_up.id in(
      select id from (
           select sp_up.id from sp_up where state=0
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_up where state=1;
end;
/

prompt
prompt Creating procedure SP_INSERT_UP
prompt ===============================
prompt
create or replace procedure SP_Insert_Up(
 /*短信SP上行*/
  In_Sender in varchar2,--发送者
  In_SP_Code in  varchar2,--sp指令
  In_MessID in varchar2,--消息ID
  In_Content in  varchar2,--短信类容
  Out_ret out varchar2--操作结构
)
as
temp_out varchar2(50);
temp_SP_Code varchar2(5000);
begin
    temp_SP_Code:=In_SP_Code;
     --发送者不能为空
     if In_Sender is null then
       temp_out:='发送者不能为空';
       goto ret_end;
     end if;
     --从类容中获取指令
     if temp_SP_Code is null then
         if In_Content is null then
             temp_out:='指令和内容不能同时为空';
             goto ret_end;
          end if;
        select max(IN_code) into temp_SP_Code from sp_code where regexp_like(In_Content,format,'i');
     end if;
     --判断指令
     if temp_SP_Code is null then
          temp_out:='无法获取指令';
          goto ret_end;
     end if;
     --插入上行信息
     insert into sp_up(ID,MessageID,sender,sp_code,content,state,create_date)
       values(sp_up_id.nextval,In_MessID,In_Sender,temp_SP_Code,In_Content,0,sysdate);
       commit;
      temp_out:='添加成功！';
     <<ret_end>>
     Out_ret:=temp_out;
  end;
/

prompt
prompt Creating procedure SP_UPDATE_DOWNSTATE
prompt ======================================
prompt
create or replace procedure SP_Update_DownState(
   /*更新下行短信状态*/
   In_SMSID in varchar2,--短信ID
   In_state in number,--状态
   Ret_out out number--返回状态 0 成功 1 短信ID为空 2 该短信不存在 3状态为空
)
as
temp_num number(5);
begin
    ---验证短信ID 非空
    if In_SMSID is null then
       temp_num:=1;
       goto ret_end;
    end if;
    --验证状态非空
    if In_state is null then
        temp_num:=3;
       goto ret_end;
    end if;
    --验证短信是否存在
    select count(*) into temp_num from sp_down where id=In_SMSID;
    if temp_num=0 then
       temp_num:=2;
       goto ret_end;
    end if;
    ---更新短信状态
    update sp_down set state=In_state where id=In_SMSID;
    commit;
   <<ret_end>>
   Ret_out:=temp_num;
end;
/

prompt
prompt Creating procedure WL_CITY_LIST
prompt ===============================
prompt
create or replace procedure wl_city_list
(in_city in varchar2   
,city_list out varchar2   
)           
  is            
   tmp_str varchar2(2000);
   tmp_pro varchar2(500);
   tmp_max_pro varchar2(500);
   tmp_info_number number(10);
   tmp_out_str varchar2(2000);
   ct number(10);
   type cursor_type is ref cursor;
   c1 cursor_type;
  begin
   ct:=0;
tmp_str:='select (case when substr(info_end,1,2)='||chr(39)||'黑龙%'||Chr(39)||' then '||Chr(39)||'黑龙江'||Chr(39)
       ||'             when substr(info_end,1,2)='||chr(39)||'内蒙%'||Chr(39)||' then '||Chr(39)||'内蒙古'||Chr(39)||'  
                        else substr(info_end,1,2) end) 省份,count(*)  条数
          from wl_info_data 
          where info_start like '||chr(39)||'%'||in_city||'%'||Chr(39)||'
          group by  substr(info_end,1,2)
          order by count(*) desc';


        open c1 for  tmp_str;
        loop
            fetch c1 into tmp_pro,tmp_info_number;
            exit when c1%notfound; 
           -- dbms_output.put_line(tmp_pro||'---'||tmp_info_number);
           if  tmp_out_str is null then  tmp_max_pro:=tmp_pro; end if ; 
          tmp_out_str:=tmp_out_str||'到'||tmp_pro||to_char(tmp_info_number)||'条信息; ';
          ct:=ct+tmp_info_number;
         end loop;
         if tmp_out_str is not null then
             tmp_out_str:=in_city||'货源信息如下：'||chr(13)||chr(10)||'  '||tmp_out_str
                         || '目前共有信息'||ct||'条。'||chr(13)||chr(10)|| '  提醒您：发送： '||in_city||'到'||tmp_max_pro||' ,就能查询到货运信息';
           else 
             tmp_out_str:='该城市暂无新信息';
         end if ;
      city_list:=   tmp_out_str;
end;
/

prompt
prompt Creating procedure WL_XS_SACH_ADD
prompt =================================
prompt
create or replace procedure WL_XS_sAch_Add(
  /*添加销售业绩*/
  SaleId1 in number,--销售人员Id
  XSType1 in varchar2 ,--销售类型 信息部、司机、。。
  OppsiteId1 in varchar2,--销售对象Id
  ret out number--还回值 0 成功 1 销售编号无效 2 销售类型为空 3 销售对象无效 4 重复绑定  5已被绑定
)
as
retcount number(5):=0;
begin
    /*销售编号验证*/
    if SaleId1 is null then
       retcount:=1;
       goto ret_end;
    end if;
    if trim(to_char(SaleId1))='' then
        retcount:=1;
       goto ret_end;
    end if;
    --销售编号是否存在
    select count(saleId) into retcount from WL_XS_salesman where saleId=SaleId1;
    if retcount=0 then
        retcount:=1;
       goto ret_end;
     end if;
    /*销售类型验证*/
    if XSType1 is null then
       retcount:=2;
       goto ret_end;
    end if;
    if XSType1='' then
        retcount:=2;
       goto ret_end;
    end if;
    /*销售对象验证*/
    if OppsiteId1 is null then
        retcount:=3;
       goto ret_end;
    end if;
    if OppsiteId1='' then
        retcount:=3;
       goto ret_end;
    end if;
    /*销售对象是否存在*/
    if XSType1='信息部' then
          select count(CUST_ID) into retcount from wl_cust_department where CUST_ID=OppsiteId1;
          if retcount=0 then
             retcount:=3;
             goto ret_end;
          end if;
     end if;
     if XSType1='司机' then
          select count(CUST_ID) into retcount from wl_cust_mobile where CUST_ID=OppsiteId1;
          if retcount=0 then
             retcount:=3;
             goto ret_end;
          end if;
     end if;
     /*验证是否重复绑定*/
     select count(*) into retcount from WL_XS_salesAch 
         where SaleId=SaleId1
         and XSType=XSType1 
         and OppsiteId=OppsiteId1;
      if retcount>0 then
           retcount:=4;
           goto ret_end;
       end if;
       /*是否已经被绑定*/
       select count(*) into retcount from WL_XS_salesAch 
         where XSType=XSType1 
         and OppsiteId=OppsiteId1;
      if retcount>0 then
           retcount:=5;
           goto ret_end;
       end if; 
     /*验证同过后添加数据*/
     insert into WL_XS_salesAch(SaleId,XSType,OppsiteId)
        values(SaleId1,XSType1,OppsiteId1);
        retcount:=0;
    <<ret_end>>
    ret:=retcount;
end;
/

prompt
prompt Creating procedure WL_CONTACT_ADDSIJI
prompt =====================================
prompt
CREATE OR REPLACE Procedure wl_Contact_AddSiji(
   In_SaleId in number,--销售编号
   In_PhoneNum in varchar2, --司机电话号码
   In_CName in varchar2,--司机姓名
   In_CarNum in varchar2,--司机车牌
   In_CarType in varchar2,--车类型
   In_CarLine in varchar2,--路线
   Out_ret out varchar2--输出结果
)
as
temp_count number(8):=0;
temp_str varchar2(50):='';
temp_CustId varchar2(64):='';
begin
    
    if regexp_instr(In_PhoneNUm,'1(3|4|5|7|8){1}\d{9}')<1 then
        temp_str:='手机号码有误！';
      goto ret_end;
    end if;
    select count(*) into temp_count from wl_cust_mobile where nbr=In_PhoneNUm;
    if temp_count>0 then
      temp_str:='绑定失败？原因:该号码已被注册！';
      goto ret_end;
    end if;
    insert into wl_cust_mobile(
           nbr,c_Name,car_number,car_model,like1,
           create_date,eff_date,ISORDERSSxy)
       values(
           In_PhoneNUm,In_CName,In_CarNum,In_CarType,In_CarLine,
           sysdate,sysdate,0
       ) return cust_Id into temp_CustId;
       commit;
       WL_XS_sAch_Add(In_SaleId,'司机',temp_CustId,temp_count);
       if temp_count=0 then
          temp_str:='绑定成功！';
          goto ret_end;
       end if;
       if temp_count=1 then
           temp_str:='销售编号无效！';
          goto ret_end;
        end if;
        if temp_count=4 then
           temp_str:='您已经添加该司机！';
          goto ret_end;
        end if;
        if temp_count=5 then
           temp_str:='该司机已被其他销售人员绑定！';
          goto ret_end;
        end if;
    <<ret_end>>
    out_ret:=temp_str;
  end;
/

prompt
prompt Creating procedure WL_CUST_CANNELORDER
prompt ======================================
prompt
create or replace procedure WL_CUST_CANNELORDER(
   Phones in varchar2,
   ret_out out varchar2)
as
temp_str varchar2(500):='';
begin
  select max(CUST_ID) into temp_str from Wl_Cust_Mobile where nbr=phones;
  if temp_str is not  null then
     update wl_orderinfo set Expiry_date=sysdate where CUST_ID=temp_str and Expiry_date is null;
     commit;
     update wl_cust_mobile set isorderssxy=0,eff_date=sysdate where nbr=Phones;
     commit;
  end if;
  temp_str:='取消成功！！';
  <<ret_end>>
  ret_out:=temp_str;
end;
/

prompt
prompt Creating procedure WL_CUST_CBIADDUPDATE
prompt =======================================
prompt
create or replace procedure WL_CUST_CBIAddUpdate(
   /*上传营业执照或者其他内容*/
   DeId in varchar2,--信息部ID
   con in varchar2,--内容
   condesc in varchar2,--对内容的描述
   ret out number--返回值 0 成功 1 信息部无效 2 内容无效
)as
retcount number(4):=0;
begin
   /*验证信息部*/
   if DeId is null then
      retcount:=1;
      goto ret_end;
   end if;
   /*验证内容*/
   if con is null then
      retcount:=2;
      goto ret_end;
    end if;
    /*该表中是否有记录*/
    select count(*) into retcount from Wl_Cust_Deptnote where DEPID=DeId and NOTEDESC=condesc;
    /*存在则修改*/
    if retcount=1 then
        update Wl_Cust_Deptnote set CONM=con where DEPID=DeId and NOTEDESC=condesc;
        retcount:=0;
        goto ret_end;
     end if;
    /*不存在则添加*/
    if retcount=0 then
        insert into Wl_Cust_Deptnote(DEPID,CONM,NOTEDESC) values(DeId,con,condesc);
        retcount:=0;
        goto ret_end;
     end if;
   <<ret_end>>
   ret:=retcount;
  end;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_ADDNEW
prompt ============================================
prompt
create or replace procedure wl_cust_department_addnew
( in_nbr in varchar2
 ,in_c_password varchar2
 ,ret out number)--带输入输出参数的存储过程
--2.新增用户
as
   begin
    if in_nbr is null or length(trim(in_nbr))<>11 then
       ret:=1;
       goto ret_end;
    end if;
    
    if trim(in_c_password) is null  then
       ret:=2;
       goto ret_end;
    end if;
    
    insert into wl_cust_department (nbr,c_password,contact)
    values(in_nbr,in_c_password,in_nbr);
    commit;
   ret:=0;

<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_CG_PASS
prompt =============================================
prompt
create or replace procedure wl_cust_department_cg_pass
( in_nbr in varchar2
 ,in_c_password varchar2
 ,ret out number)
 --M2\F1\S1 修改密码
 --输入参数：in_nbr,in_c_password   号码,密码
 --输出参数：1输入号码为空或号码不是11位，2输入密码为空，
 --3信息部客户表无此号码，0修改成功
as
tmp_ct number(10);
   begin
    if in_nbr is null or length(trim(in_nbr))<>11 then
       ret:=1;
       goto ret_end;
    end if;
    
   if trim(in_c_password) is null then
       ret:=2;
       goto ret_end;
    end if;
    
    select count(*) into tmp_ct
    from  wl_cust_department
    where nbr= in_nbr;
    
    if tmp_ct<>1 then ret:=3;
       goto ret_end;
    end if;
    
  update wl_cust_department set c_password=in_c_password where nbr=in_nbr;
  commit;
 
   ret:=0;

<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_PASS
prompt ==========================================
prompt
create or replace procedure wl_cust_department_pass
(in_nbr in varchar2,in_c_password in varchar2,
 out_cust_id out varchar2,ret out number)--带输入输出参数的存储过程

--1.登陆系统：信息部客户表的客户验证
--输入参数：in_nbr，in_c_password        号码，密码
--输出：0或1      0有记录，1无

as

ct number(10);

begin


select count(*) into ct from
wl_cust_department where nbr=in_nbr and c_password=in_c_password;

if ct > 0 
then select cust_id into out_cust_id from wl_cust_department
     where nbr=in_nbr and c_password=in_c_password;
     ret:=0; --把查询结果赋值个记录类型
else ret:=1;
end if;

end;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_REGIST
prompt ============================================
prompt
create or replace procedure wl_cust_department_regist
(in_nbr in varchar2,ret out number)--带输入输出参数的存储过程

--1.注册时测试账号是否存在
--输入参数：in_nbr，in_c_password        号码，密码
--输出：0或1      0有记录，1无

as

ct number(10);

begin


select count(*) into ct FROM
wl_cust_department where nbr=in_nbr;

if ct = 0 then 
   ret:=0; --把查询结果赋值个记录类型
else ret:=1;
end if;
end;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_SENDMSG
prompt =============================================
prompt
create or replace procedure wl_cust_department_sendmsg
--2.信息发送：发布信息后，插入至信息部发布的信息,再经过处理最终插入wl_info_data
--输入参数：in_nbr,in_info    号码,信息
--输出参数：1输入号码或信息为空，2信息部客户表无此号码，3信息过短或过长
--4目的地为空，6\7三小时内发布过相同信息，0成功插入


(in_nbr in varchar2,in_info in varchar2,ret out number)

as

  tmp_city varchar2(64);
  tmp_name varchar2(64);

  tmp_ct number(10);
  tmp_id number(10);
  tmp_create_date date;
  tmp_info_start varchar2(500);
  tmp_info_end   varchar2(500);
  tmp_info_start_s varchar2(500);
  tmp_info_end_s   varchar2(500);
  tmp_info_truck varchar2(500);
  tmp_info_goods varchar2(500);
  tmp_info_note varchar2(500);
  tmp_info_nbr1 varchar2(30);
  tmp_info_nbr2 varchar2(30);
  tmp_info_nbr3 varchar2(30);
  tmp_info_out   varchar2(500);
  tmp_lng_start  number(11,8);
  tmp_lat_start  number(11,8);
  tmp_lng_end  number(11,8);
  tmp_lat_end  number(11,8);
  
begin
  -----输入号码、信息空值校验---------
  if in_nbr is null or in_info is null then
     ret:=1;
     goto ret_end;
  end if;
  -----按号码查询信息部客户表，是否为空---------
  select count(*) into tmp_ct  from
  wl_cust_department where nbr=in_nbr;
 
  if tmp_ct = 0 then
       ret:=2;
       goto ret_end;
  end if;  
  -----输入信息长度校验---------
  if length(in_info)<5 or  length(in_info)>500 then
     ret:=3;
     goto ret_end;
  end if;
  -----目的地校验----------------
  select city,c_name into tmp_city,tmp_name FROM
  wl_cust_department where nbr=in_nbr;
  
  tmp_info_start  :=get_wl_info_region_from  (in_info,tmp_city);
  tmp_info_end    :=get_wl_info_region_end   (in_info,tmp_city);
  tmp_info_start_s:=get_wl_info_region_from_s(in_info,tmp_city);
  tmp_info_end_s  :=get_wl_info_region_end_s (in_info,tmp_city);
  
 if  tmp_info_end is null  then
     ret:=4;
     goto ret_end;
 end if;

  -----插入信息至wl_info_from_department------- 
  
  select count(*) into tmp_ct 
  from wl_info_from_department
  where info=in_info and info_nbr1=in_nbr; 
--and (sysdate-create_date)*24<3;
  
 if tmp_ct >0 then
    update wl_info_from_department set create_date=sysdate
    where info=in_info and info_nbr1=in_nbr;
    commit;
       ret:=6;
       goto ret_end_1;
 end if;
  
 
  tmp_info_nbr1   :=in_nbr;
  tmp_info_nbr2   :=get_wl_info_nbr1(in_info);
  tmp_info_nbr3   :=get_wl_info_nbr2(in_info);  
  
  tmp_info_truck  :=get_wl_info_truck(in_info);
  tmp_info_goods  :=get_wl_info_goods(in_info);
  tmp_info_note   :=get_wl_info_note (in_info);
  
  tmp_id          :=wl_info_data_id.nextval;
  tmp_create_date :=sysdate;
 
insert into wl_info_from_department
       (id,nbr,c_name,city,create_date,info,
        info_start,info_end,info_nbr1,info_nbr2,
        info_nbr3,info_truck,info_goods,
        info_note,info_from)
values 
       (tmp_id,in_nbr,tmp_name,tmp_city,tmp_create_date,in_info,
        tmp_info_start,tmp_info_end,tmp_info_nbr1,tmp_info_nbr2,
        tmp_info_nbr3,tmp_info_truck,tmp_info_goods,
        tmp_info_note,'department');
commit;

  -----生成输出信息-----------
 <<ret_end_1>> ----------
 
 tmp_info_out    :=tmp_info_start_s||'到'||tmp_info_end_s;
 
 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;
 
 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;
 
 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;

 ----插入目的表wl_info_data--------------------------- 
 
  tmp_lng_start  :=get_lng(tmp_info_start);
  tmp_lat_start  :=get_lat(tmp_info_start); 
  tmp_lng_end    :=get_lng(tmp_info_end)  ;
  tmp_lat_end    :=get_lat(tmp_info_end)  ;
 

  select count(*) into tmp_ct 
  from wl_info_data 
  where info=tmp_info_out and info_nbr1=tmp_info_nbr1;

  if tmp_ct >0 then
    update  wl_info_data set  create_date=sysdate
    where info=tmp_info_out and info_nbr1=tmp_info_nbr1;
    commit;
       ret:=7;
       goto ret_end;
  end if;
 
 
 insert into wl_info_data
   (id,nbr,c_name,city,create_date,info,
    info_start,info_end,info_nbr1,info_nbr2,
    info_nbr3,info_truck,info_goods,info_note,
    info_from,lng_start,lat_start,lng_end,lat_end
   )
 values 
   (tmp_id,in_nbr,tmp_name,tmp_city,tmp_create_date,tmp_info_out,
    tmp_info_start,tmp_info_end,tmp_info_nbr1,tmp_info_nbr2,
    tmp_info_nbr3,tmp_info_truck,tmp_info_goods,tmp_info_note,
    'department',tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;
  ret:=0;
  
 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'department处理数据成功');
 commit;
 
 <<ret_end>>
 ret:=ret;
 
end ;
/

prompt
prompt Creating procedure WL_CUST_DEPARTMENT_SIJI_GET
prompt ==============================================
prompt
create or replace procedure Wl_Cust_Department_SIJI_get(
   Keys in varchar2,--搜索关键字
   DepId1 in varchar2,--信息部ID
   PageIndex in number,--当前页索引
   PageSize in number,--每页显示的条数
   Counts out number,--总记录数
   ret_cur out sys_refcursor--返回的记录数
)
as
  temp_saleId number(9):=0;
begin
     if DepId1 is null then
        goto ret_end;
     end if;
     /*查询返回结果集带有信息部Id*/
    open ret_cur for
     select t.* from (
      select m.*,rownum num from
       ( 
         select * from wl_cust_mobile
         where cust_id in(
           select MObileId cust_id from Wl_Cust_Department_SIJI
              where Depid=DepId1
         )
       ) m
      where nbr like keys||'%'
            or c_Name like keys||'%'
            or city like keys||'%'
            or car_NumBer like keys||'%'
            or car_model like keys||'%'
            or like1 like keys||'%'
     ) t
     where t.num<PageIndex*PageSize
     and t.num>(pageIndex-1)*pagesize;

     /*查询总记录数带有信息部Id*/
    select count(*) into Counts from (
       select * from
       (select * from wl_cust_mobile
         where cust_id in(
           select MObileId from Wl_Cust_Department_SIJI
              where Depid=DepId1
         )) temp
      where  temp.nbr like keys||'%'
            or temp.c_Name like keys||'%'
            or temp.city like keys||'%'
            or temp.car_NumBer like keys||'%'
            or temp.car_model like keys||'%'
            or temp.like1 like keys||'%'
     ) ;
     
     <<ret_end>>


    /*查询返回记录集信息部ID为空*/
     open ret_cur for
       select temp.* from
          (select * from wl_cust_mobile
            where nbr like keys||'%'
               or c_Name like keys||'%'
               or city like keys||'%'
               or car_NumBer like keys||'%'
               or car_model like keys||'%'
               or like1 like keys||'%'
               or qq like keys||'%') temp
        where rownum<PageIndex*pageSize
          and rownum>(PageIndex-1)*pageSize ;
    /*查询总记录数信息部Id 为空*/
    select count(*) into Counts from wl_cust_mobile
     where  nbr like keys||'%'
            or c_Name like keys||'%'
            or city like keys||'%'
            or car_NumBer like keys||'%'
            or car_model like keys||'%'
            or like1 like keys||'%'
            or qq like keys||'%';
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_ADDCHONGZHIINFO
prompt ==============================================
prompt
create or replace procedure wl_cust_dep_AddChongzhiInfo(
  depId in varchar2,--部门编号
  inAmount in number--充值数量
  )
  as
  begin
     insert into wl_cust_dep_chongZhiInfo(DepartId,InAmount) values(depId,inAmount);
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_ADDUPDATE
prompt ========================================
prompt
create or replace procedure WL_cust_dep_Addupdate(
   Op in varchar2,--操作 add update
   nbr1 in varchar2,--电话号码
   Id_card1 in varchar2,--身份证号码
   C_Name1 in varchar2,--信息部名称
   C_passWord1 in varchar2,--密码
   city1 in varchar2,--注册城市
   Chief1 in varchar2,--负责人
   Contact1 in varchar2,--联系人
   Address1 in varchar2,--注册地址
   Note1 in varchar2,--备注
   FreeSMS1 in number,--免费短信条数
   BillingSMS1 in number,--计费短信条数
   ISSUPER1 in number,--是否是管理员
   Smssign1 in varchar2,--短信签名
   QQ1 in varchar2,--QQ号码
   retStr out varchar2--返回字符串
)
as
tempCount number(6):=0;
tempStr varchar2(200):='';
begin
   if Op is null then
      tempStr:='操作为空';
      goto ret_end;
   end if;
   --检验QQ号码
   if QQ1 is not null and trim(QQ1)<>'' then
     select count(*) into tempCount
      from Wl_Cust_Department
      where qq=QQ1;
     if tempCount>0 then
        select max(nbr) into tempStr from Wl_Cust_Department
        where qq=QQ1 or nbr=QQ1;
        if tempStr is not null and trim(tempStr)<>'' then
           tempStr:='该QQ号已经被绑定。如有觉得有异常。请联系我们：400-628-2515';
           goto ret_end;
        else
          delete Wl_Cust_Department where qq=QQ1 or nbr=QQ1;
        end if;
     end if;
   end if;
   --更新--
   if op='update' then
      update Wl_Cust_Department
          set Id_card=Id_card1,c_name=C_Name1,c_password=C_passWord1,
          city=city1,Chief=Chief1,contact=Contact1,address=Address1,
          Note=Note1,FreeSMS=FreeSMS1,BillingSMS=BillingSMS1,IsSuperAdmin=ISSUPER1,
          SMSSIGN=Smssign1,qq=QQ1 where nbr=nbr1;
       tempStr:='更新成功';
      goto ret_end;
   end if;
   --添加--
   if op='add' then
      select count(*) into tempCount from Wl_Cust_Department where nbr=nbr1;
      if tempCount>0 then
        tempStr:='该账号已存在请更换账号';
        goto ret_end;
      else
        insert into Wl_Cust_Department(nbr,ID_CARd,C_name,c_password,Chief,ConTact,Address,QQ)
        values(nbr1,Id_card1,C_Name1,C_passWord1,Chief1,Contact1,Address1,QQ1);
        tempStr:='添加成功!';
      end if;
   end if;
   <<ret_end>>
   retStr:=tempStr;
end;
/

prompt
prompt Creating procedure WL_CUST_DEP_AUTOZHONGZHI
prompt ===========================================
prompt
create or replace procedure wl_cust_dep_AutoZhongzhi(
SDN in varchar2,--SDN好管理员的ID编号
inCount in number,--充值金额
refState out number--充值状态0 失败 1 成功 2 SDN号有误
)as
/*
  系统自动每月自动充值300条免费信息
  无累计
*/
selecount number(3):=0;
begin
  select count(CUST_ID) into selecount 
        from WL_Cust_department
         where CUST_ID=SDN and ISSUPERADMIN=1;
   if selecount=0 then
        refState:=2;
    elsif selecount=1 then
       update WL_Cust_department set FREESMS=inCount where ISSUPERADMIN=0;
       refState:=1;
    else
       refState:=0;
   end if;
end;
/

prompt
prompt Creating procedure WL_CUST_DEP_CHANGEPWD
prompt ========================================
prompt
create or replace procedure wl_cust_dep_changePwd(
    dipId in varchar2,--部门Id
    oldpwd in varchar2,--旧密码
    newPwd in varchar2,--新密码
    stateref out number)---状态 0：成功；1：旧密码有误
    as
    /*
       更改部门的登陆密码
    */
    usercount number;
    begin
       select count(nbr) into usercount
          from WL_Cust_department
          where CUST_ID=dipId
          and C_PASSWORD=oldpwd;
    if usercount=0 then
          stateref:=1;
       else
          update WL_Cust_department set C_PASSWORD=newPwd where CUST_ID=dipId;
          stateref:=0;
       end if;
    end;
/

prompt
prompt Creating procedure WL_CUST_DEP_CHONGZHI
prompt =======================================
prompt
create or replace procedure wl_cust_dep_ChongZhi(
      nbr in varchar2,--注册账号
      amountBillingSMS in number,--要充值的计费短信数量
      stateref out number---状态0：成功 ；1：失败
      )as
      /*
         给部门账户充值
      */
      Did varchar2(200);
      countResult number;
      begin
         select count(CUST_ID) into countResult from WL_Cust_department where NBR=nbr;
         if countResult>0 then
           update WL_Cust_department
              set BILLINGSMS=BILLINGSMS+amountBillingSMS
              where CUST_ID=Did;
              stateref:=0;
         end if;
      end;
/

prompt
prompt Creating procedure WL_CUST_DEP_FINDPWD
prompt ======================================
prompt
create or replace procedure WL_CUST_DEP_findPwd(
    phones in varchar2,--手机号码
    restate out number,--状态 0 成功 1 同一天之内重复发送 2 手机号码有误 3 改用户不存在
    refStr out varchar2--状态对应的信息
  )
  /*用户找回密码*/
  as
  lastDate date:=sysdate;--最后一次获取密码时间
  rcount number(3):=0;--查询满足条件的数量
  state number(3):=0;--状态 0 成功 1 同一天之内重复发送 2 手机号码有误 3 改用户不存在
  strref varchar2(100):='';--状态对应的信息
  dayTimes number:=1;
  begin
    ----判断手机号码是否为空
    if phones is null then
        state:=2;
        strref:='手机号码有误';
        goto ref_end;
      end if;
      ----判断手机格式是否正确
      if length(trim(phones))!=11 then
        state:=2;
        strref:='手机号码有误';
        goto ref_end;
      end if;
      --查询该号码所对应得信息部是否存在
     select count(*) into rcount from wl_cust_department where nbr=phones;
     --该用户不存在
     if rcount=0 then
        state:=3;
        strref:='用户不存在';
        goto ref_end;
      end if;
      --该用户存在时判断该用户当天是否已通过手机方式找回密码
      select count(*) into rcount from WL_CUST_DEP_FR_log where phone=phones;
      --临时表中没有该手机的记录通过手机方式找回密码 则返回密码
      if rcount=0 then
          state:=0;
          select c_password into strref from wl_cust_department where nbr=phones;
          insert into WL_CUST_DEP_FR_log(BizType,phone) values('找回密码',phones);
          goto ref_end;
      end if;
      --查询该手机号码最后一次找回密码的时间
      select tradeDate into lastDate from WL_CUST_DEP_FR_log where phone=phones;
      ---判断是否在一天内是否重复访问
      if sysdate-lastDate>=dayTimes then
           state:=0;
           select c_password into strref from wl_cust_department where nbr=phones;
           update WL_CUST_DEP_FR_log set tradeDate=sysdate where phone=phones;
           goto ref_end;
      end if;
      --同一天内重复发送
      if sysdate-lastDate<dayTimes then
          state:=1;
          strref:='不能再同一天之内重复发送';
      end if;
    <<ref_end>>
    restate:=state;
    refstr:=strref;
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_GETCHONGZHIINFO
prompt ==============================================
prompt
create or replace procedure wl_cust_dep_getChongzhiInfo(
   depId in varchar2,--部门编号
   refcur out WL_CUST_DEPARTMENT_GETINFO.t_info)--返回的结果集
  as
  begin
    if depId is null or depId=''then
        open refcur for select * from wl_cust_dep_chongZhiInfo;
     else
        open refcur for select * from wl_cust_dep_chongZhiInfo where DepartId=depId;
     end if;
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_GETDEPARTINFO
prompt ============================================
prompt
create or replace procedure WL_cust_dep_GetDepartInfo(
consid in varchar2,--部门编号可为''
phoneNum in varchar2,--注册账号（注册电话号码）
refResult out wl_cust_dep_getinfo.t_info)--返回结果集
as
/*查询根据部门ID或者注册账号部门信息*/
begin
  if consid=''and phoneNum='' then
     open refResult for select * from WL_Cust_department;
  elsif consid='' and phoneNum!=''then
     open refResult for select * from WL_Cust_department where NBR=phoneNum;
  elsif consid!=''and phoneNum='' then
     open refResult for select * from WL_Cust_department where CUST_ID=consid;
  elsif consid!=''and phoneNum='' then
     open refResult for select * from WL_Cust_department where CUST_ID=consid and NBR=phoneNum;
  end if;
end;
/

prompt
prompt Creating procedure WL_CUST_DEP_GETDEPARTINFOBYNBR
prompt =================================================
prompt
create or replace procedure wl_cust_dep_GetDepartInfoByNbr(
    nbr in varchar2,--注册编号
    depID out varchar2--返回部门ID
  )as
  infoCount number;
  begin
     select count(cust_ID) into infoCount from WL_Cust_department where NBR=nbr;
     if infoCount>0 then
        select cust_ID into depID from WL_Cust_department where NBR=nbr;
     else
       depID:='';
     end if;
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_GETINFOBYKEYS
prompt ============================================
prompt
create or replace procedure wl_cust_dep_GetInfoByKeys(
 keys in varchar2,--关键字
 refcure out wl_cust_department_getinfo.t_info)--输入结果集
 as
 /*查询部门信息根据所输入的关键字*/
 begin
   if keys is null or keys='' then
      open refcure for select * from WL_Cust_department;
   else
    open refcure for
      select * from WL_Cust_department
        where CUST_ID like '%'||keys||'%'
        or NBR like '%'||keys||'%'
        or C_NAME like '%'||keys||'%'
        or CONTACT like '%'||keys||'%' ;
     end if;
 end;
/

prompt
prompt Creating procedure WL_CUST_DEP_LOGIN
prompt ====================================
prompt
create or replace procedure WL_CUST_DEP_Login(
  UNBR in varchar2,--账户
  UPWd in varchar2,--密码
  recur out Wl_Cust_Dep_Getinfo.t_info,--返回查询数据集
  retSign out number--0 成功 1 用户名或密码有误
)as
retcount number(10):=0;
begin
   /*验证用户名密码*/
   if UNBR is null then
      retcount:=1;
      goto ret_end;
     end if;
    if trim(UNBR)='' then
      retcount:=1;
      goto ret_end;
    end if;
    if UPWd is null then
      retcount:=1;
      goto ret_end;
     end if;
    if trim(UPWd)='' then
      retcount:=1;
      goto ret_end;
    end if;
    /*查询该用户是否存在*/
   select count(*) into retSign from wl_cust_department where nbr=UNBR and C_PASSWord=UPWD;
   if retSign=0 then
      retcount:=1;
      goto ret_end;
    end if;
    if retSign>0 then
       open recur for select w.*,n.CONM from wl_cust_department w
          left join wl_cust_deptnote n
          on n.depid=w.cust_id
          where w.nbr=UNBR and w.c_password=UPWD;
          retcount:=0;
      goto ret_end;
    end if;
  <<ret_end>>
  retSign:=retcount;
  end;
/

prompt
prompt Creating procedure WL_CUST_DEP_MESSAGEGETBYGROUP
prompt ================================================
prompt
create or replace procedure WL_CUST_DEP_MESSAGEGETBYGroup(
    did in varchar2,--部门ID
    pageIndex in number,--当前页数
    pages out number,---总页数
    refcur out wl_cust_dep_getinfo.t_info)--返回结果
    as
    /*
    查询部门发送信息的历史记录
    */
    begin
      --获取结果集
      open refcur for
          select u.* from (select rownum rnum,k.* from( select mr.*,m.M_CONTENT,m.Senddate
                 from (
                      select r.M_ID,count(M_ID) con
                  from wl_cust_dep_mes_recever r  group by M_ID
                   ) mr
                  inner join WL_CUST_DEP_MESSAGE m on mr.M_ID=m.M_ID
                   where m.depid=did order by m.senddate desc
                 )k
              )u
            
             where u.rnum>(pageIndex-1)*8 and u.rnum<=pageIndex*8 order by u.senddate desc;
       --获取总页数
       select count(k.M_ID) into pages from (select rownum rnum, mr.*,m.M_CONTENT,m.Senddate
            from (select r.M_ID,count(M_ID) con
                from wl_cust_dep_mes_recever r  group by M_ID) mr
                inner join WL_CUST_DEP_MESSAGE m on mr.M_ID=m.M_ID
            where m.depid=did order by m.senddate desc)k;
    end;
/

prompt
prompt Creating procedure WL_CUST_DEP_MESSAGE_ADD
prompt ==========================================
prompt
create or replace procedure WL_CUST_DEP_MESSAGE_ADD(
dep in varchar2,--部门ID
MContent in varchar2,--短信内容
refcur out wl_cust_dep_getinfo.t_info--返回主键ID
)as
/*
插入短信信息，返回短信主键
*/
reMid varchar2(200);
begin
  insert into  WL_CUST_DEP_MESSAGE(DepId,M_content)values(dep,Mcontent) return M_ID into reMid;
  open refcur for select M_ID from WL_CUST_DEP_MESSAGE where M_ID=reMid;
end;
/

prompt
prompt Creating procedure WL_CUST_DEP_MES_RECEVER_ADD
prompt ==============================================
prompt
create or replace procedure WL_CUST_DEP_MES_Recever_add(
  rphone in varchar2,--手机号码
  MID in varchar2--短信ID好
)as
/*
  添加短信的接收着
*/
begin
  insert into  WL_CUST_DEP_MES_Recever(R_PHONE,M_ID)values(rphone,MID);
end;
/

prompt
prompt Creating procedure WL_CUST_DEP_SENDMESSUPDATE
prompt =============================================
prompt
create or replace procedure WL_CUST_DEP_SendMessUpdate(
  depId in varchar2,--部门ID
  state out number--状态0:成功 1：余额不足
)
as
/*
  本存储过程将在发送信息
  时候更改概部门的免费短信条数和
  计费短信条数的
*/
free number:=0;
biling number:=0;
begin
   select FREESMS into free from WL_Cust_department where CUST_ID=depId;
   select BILLINGSMS into biling from WL_Cust_department where CUST_ID=depId;
   if free>0 then
      update WL_Cust_department set FREESMS=FREESMS-1 where CUST_ID=depId;
      state:=0;
   else 
       if biling>0 then
          update WL_Cust_department set BILLINGSMS=BILLINGSMS-1 where CUST_ID=depId;
          state:=0;
       else
          state:=1;
       end if;
   end if;
end;
/

prompt
prompt Creating procedure WL_CUST_GETINFODATA
prompt ======================================
prompt
create or replace procedure WL_CUST_GETINFODATA(
   /*更具信息编号获取信息以及发布信息的执照*/
   MID in number,--信息编号
   retcur out wl_cust_dep_getinfo.t_info,--查询结果集
   ret out number--返回编号 0 成功 1 信息编号无效
)as
retcount number:=0;
begin
    /*验证信息编号*/
    if MID is null then
       retcount:=1;
       goto ret_end;
    end if;
     open retcur for  select w.C_name,
           w.Create_Date,
           w.INfo,w.Info_NBR1,
           w.Info_TRUCK,w.INFO_note,d.conm
      from wl_info_data w
      left join (
              select e.conm, c.c_Name,c.nbr,c.contact
              from wl_Cust_department c
              inner join wl_cust_deptnote e
              on c.cust_id=e.depid
           ) d
      on d.nbr=w.info_nbr1 or d.nbr=w.nbr
      where W.id=MID;
       retcount:=0;
       goto ret_end;
    <<ret_end>>
    ret:=retcount;
end;
/

prompt
prompt Creating procedure WL_CUST_INTERFACE_SENDMSG
prompt ============================================
prompt
create or replace procedure wl_cust_interface_sendmsg
--Mc:发布外部信息(从天下通上取到发布信息,插入至天下通信息表,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：ret:1城市或信息或号码1或来源为空,2城市无效,3信息过短或过长
--4目的地为空，6\7三小时内发布过相同信息，0成功插入


/*\*NBR                               ---可空  
C_NAME	                          ---可空 
CITY	榆林                        ---必填
CREATE_DATE	2012-4-9 上午 10:34:01---可空，默认为系统当前时间  
INFO	榆林到江宁,运化工原料       ---必填 
INFO_START	陕西榆林              ---可空
INFO_END	江苏南京江宁            ---可空
INFO_NBR1	15619929456             ---必填
INFO_NBR2	                        ---可空  
INFO_NBR3	                        ---可空  
INFO_TRUCK	                      ---可空  
INFO_GOODS	运化工原料            ---可空  
INFO_FROM XXXX                    ---必填
LNG_START	109.45000000            ---可空  
LAT_START	38.17000000             ---可空               
LNG_END	118.50000000              ---可空 *\
LAT_END	31.57000000               ---可空 
*/

(in_nbr in varchar2,in_c_name in varchar2,in_city in varchar2,
 in_create_date in date,in_info in varchar2,in_info_start in varchar2,
 in_info_end in varchar2,in_info_nbr1 in varchar2,in_info_nbr2 in varchar2,
 in_info_nbr3 in varchar2,in_info_truck in varchar2,
 in_info_goods in varchar2,in_info_note in varchar2,in_info_from in varchar2,
 in_lng_start in number,in_lat_start in number,
 in_lng_end in number,in_lat_end in number,
 ret out number)
as


  tmp_ct number(10);
  tmp_id number(10);
  tmp_create_date date;
  tmp_info_start varchar2(500);
  tmp_info_end   varchar2(500);
  tmp_info_truck varchar2(500);
  tmp_info_goods varchar2(500);
  tmp_info_note varchar2(500);
  tmp_info_nbr2 varchar2(30);
  tmp_info_nbr3 varchar2(30);
  tmp_lng_start  number(11,8);
  tmp_lat_start  number(11,8);
  tmp_lng_end  number(11,8);
  tmp_lat_end  number(11,8);

begin
  -----输入城市、信息、号码1空值校验---------
  if in_city is null or in_info is null or 
     in_info_nbr1 is null or in_info_from is null then
     ret:=1;
     goto ret_end;
  end if; 
  -----输入城市有效性校验--------- 
  select count(*) into tmp_ct
  from  wl_info_data_region 
  where city is not null and city=in_city;

  if tmp_ct =0 then
       ret:=2;
       goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(in_info)<5 or  length(in_info)>500 then
     ret:=3;
     goto ret_end;
  end if;
   -----目的地校验----------------
  if in_info_end is not null  then 
     tmp_info_end :=in_info_end;
  else tmp_info_end :=get_wl_info_region_end (in_info,in_city);
  end if; 
   
  
  if  tmp_info_end is null  then
     ret:=4;
     goto ret_end;
  end if;
  -----附值：时间、起始地、号码2、号码3、车、货、备注信息----

  --时间
  if in_create_date is not null  then 
     tmp_create_date :=in_create_date;
  else tmp_create_date :=sysdate;
  end if;
  --起始地
  if in_info_start is not null  then 
     tmp_info_start :=in_info_start;
  else tmp_info_start :=get_wl_info_region_from (in_info,in_city);
  end if;

  --号码2
  if in_info_nbr2 is not null  then 
     tmp_info_nbr2 :=in_info_nbr2;
  else tmp_info_nbr2 :=get_wl_info_nbr1 (in_info);
  end if;
  --号码3
  if in_info_nbr3 is not null  then 
     tmp_info_nbr3 :=in_info_nbr3;
  else tmp_info_nbr3 :=get_wl_info_nbr2 (in_info);
  end if;
  --车
  if in_info_truck is not null  then 
     tmp_info_truck :=in_info_truck;
  else tmp_info_truck :=get_wl_info_truck (in_info);
  end if;
  --货
  if in_info_goods is not null  then 
     tmp_info_goods :=in_info_goods;
  else tmp_info_goods :=get_wl_info_goods (in_info);
  end if;
  --备注信息
  if in_info_note is not null  then 
     tmp_info_note :=in_info_note;
  else tmp_info_note :=get_wl_info_note (in_info);
  end if;
    

  -----插入信息至wl_info_from_interface-------
  select count(*) into tmp_ct
  from wl_info_from_interface
  where city=in_city           and info=in_info and
        info_nbr1=in_info_nbr1 and info_from=in_info_from;

 if tmp_ct >0 then
    update wl_info_from_interface set create_date=tmp_create_date
    where city=in_city           and info=in_info and
          info_nbr1=in_info_nbr1 and info_from=in_info_from;
  commit;
       ret:=6;
       goto ret_end_1;
 end if;

  --
  tmp_id          :=wl_info_data_id.nextval;

insert into wl_info_from_interface
       (id,nbr,c_name,city,create_date,info,
        info_start,info_end,info_nbr1,info_nbr2,
        info_nbr3,info_truck,info_goods,
        info_note,info_from)
values
       (tmp_id,in_nbr,in_c_name,in_city,tmp_create_date,in_info,
        tmp_info_start,tmp_info_end,in_info_nbr1,tmp_info_nbr2,
        tmp_info_nbr3,tmp_info_truck,tmp_info_goods,
        tmp_info_note,in_info_from);
commit;



 <<ret_end_1>> ----------


 ----插入目的表wl_info_data---------------------------

/*LNG_START	109.45000000            ---可空  
LAT_START	38.17000000             ---可空               
LNG_END	118.50000000              ---可空 *\
LAT_END	31.57000000               ---可空 */

  --起始地经度
  if in_lng_start is not null  then 
     tmp_lng_start :=in_lng_start;
  else tmp_lng_start :=get_lng(tmp_info_start);
  end if;
  --起始地纬度
  if in_lat_start is not null  then 
     tmp_lat_start :=in_lat_start;
  else tmp_lat_start :=get_lat(tmp_info_start);
  end if;
  --结束地经度
  if in_lng_end is not null  then 
     tmp_lng_end :=in_lng_end;
  else tmp_lng_end :=get_lng(tmp_info_end);
  end if;  
  --结束地纬度
  if in_lat_end is not null  then 
     tmp_lat_end :=in_lat_end;
  else tmp_lat_end :=get_lat(tmp_info_end);
  end if;  
  

  select count(*) into tmp_ct
  from wl_info_data
  where city=in_city           and info=in_info and
        info_nbr1=in_info_nbr1 and info_from=in_info_from;
        

  if tmp_ct >0 then
    update  wl_info_data set create_date=sysdate
    where city=in_city           and info=in_info and
          info_nbr1=in_info_nbr1 and info_from=in_info_from;
    commit;
       ret:=7;
       goto ret_end;
  end if;

--
 insert into wl_info_data
   (id,nbr,c_name,city,create_date,info,
    info_start,info_end,info_nbr1,info_nbr2,
    info_nbr3,info_truck,info_goods,info_note,
    info_from,lng_start,lat_start,lng_end,lat_end
   )
 values
   (tmp_id,in_nbr,in_c_name,in_city,tmp_create_date,in_info,
    tmp_info_start,tmp_info_end,in_info_nbr1,tmp_info_nbr2,
    tmp_info_nbr3,tmp_info_truck,tmp_info_goods,tmp_info_note,
    in_info_from,tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;
  ret:=0;

 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'interface处理数据成功');
 commit;

 <<ret_end>>
ret:=ret;

end ;
/

prompt
prompt Creating procedure WL_CUST_MOBILE_RECORD5
prompt =========================================
prompt
create or replace procedure wl_cust_mobile_record5
(in_sim        in varchar2
,in_call_nbr   in varchar2
,in_call_date  in date
,ret           out number)

--F5:拨打电话记录
--输入参数：in_sim,in_call_nbr,in_date
--输出参数：ret:1输入为空,2SIM不存在,0插入成功
--日期格式：2012/05/03 13:00:00
as
  tmp_ct number(10);
begin

  -----输入参数空值校验---------
  if trim(in_sim)  is null or trim(in_call_nbr) is null or
     trim(in_call_date) is null then
     ret:=1;
     goto ret_end;
  end if;
  
  -----记录校验-------------
  select count(*) into tmp_ct from wl_cust_mobile where 
    sim=trim(in_sim);

  if tmp_ct = 0 then
     ret:=2;
     goto ret_end;
  end if;

  -----插入记录--------------
  insert into wl_cust_mobile_record(sim,call_nbr,call_date)
  values (in_sim,in_call_nbr,in_call_date);
  commit;

  ret:=0;
<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_MOBILE_UPDATEADD
prompt ===========================================
prompt
create or replace procedure WL_CUST_MObile_UpdateAdd(
   Cqq in varchar2,--QQ
   runLine in varchar2,--长跑路线
   carType in varchar2,--车型
   cname in varchar2,--姓名
   Phones in varchar2,--手机
   carNum in varchar2,--车牌号
   createTime in date--创建时间
)as
/*
  导入数据，以QQ为准，如存在则更新，不存在则添加
*/
selecount number(5);
begin
  --查看该QQ号是否存在
 select count(CUST_ID) into selecount from wl_cust_mobile where NBR=Phones;
 if selecount>0 then--存在则更新
   update wl_cust_mobile set QQ=Cqq,
      C_NAME=cname,CAR_NUMBER=carNum,
      CAR_MODEL=carType,LIKE1=runLine
       where NBR=Phones;
  elsif selecount=0 then --不存在则添加
    insert into wl_cust_mobile(NBR,c_Name,Car_Number,car_model,like1,qq,CREATE_DATE)
       values(Phones,cname,carNum,carType,runLine,Cqq,createTime);
 end if;
end;
/

prompt
prompt Creating procedure WL_CUST_ORDER
prompt ================================
prompt
create or replace procedure WL_CUST_Order
  /*用户套餐订购*/
  (custId in varchar2
  --订购用户
  ,ProductId1 in varchar2
  --商品ID
  ,OrderCount in number
  --商品数量
  ,OrderType  in varchar2
  --订购类型  1.自定购 2.销售人员代购 3.信息部代购 4.公司代购
  ,CreateDate in date
  --订购时间
  ,ExprityDate in date
  --失效时间
  ,Op_sender in varchar2
  --操作人Id
  ,ret_out  out varchar2
  --返回操作状态
  )
as
  tmp_out varchar2(500):='';
  temp_OrderType varchar2(500):='';
  temp_Op_sender varchar2(500):='';
  temp_count number(5):=0;
begin
   temp_OrderType:=OrderType;
   temp_Op_sender:=Op_sender;
  ---------订购用户验证----------
  if custId is null then
     tmp_out:='订购用户不能为空【找货宝】';
     goto ret_end;
  end if;
  -------------商品验证-------------
  if ProductId1 is null then
    tmp_out:='购买商品不能为空【找货宝】';
    goto ret_end;
  end if;
  ---------------该商品是否存在------
  select count(*) into temp_count from wl_productinfo
     where productid=ProductId1;
  /*if temp_count=0 then
     tmp_out:='该套餐不存在【找货宝】';
     goto ret_end;
  end if;*/
  ----------------订购类型验证---------
  if temp_OrderType is null then
     tmp_out:='订购类型验证【找货宝】';
     goto ret_end;
  end if;
  ------查看是否有销售人员绑定该号码----
  select max(saleid) into tmp_out from wl_xs_salesach where oppsiteid=custId;
  if tmp_out is not null then
      temp_Op_sender:=to_char(tmp_out);
      temp_OrderType:='销售人员代购【找货宝】';
  end if;
  -------------添加订单-----------------
   insert into Wl_Orderinfo
    (
        Cust_id,Product_Id,Order_Count,Order_Date,Expiry_Date,Order_Type,Order_Operator
    )
    values
    (
        custId,ProductId1,OrderCount,CreateDate,ExprityDate,temp_OrderType,temp_Op_sender
     );
  commit;
  tmp_out:='订购成功！！【找货宝】';
  <<ret_end>>
  ret_out:=tmp_out;
end;
/

prompt
prompt Creating procedure WL_CUST_ZHBQQ_GETADDRESS
prompt ===========================================
prompt
create or replace procedure WL_Cust_ZHBQQ_GetAddress(
  message in varchar2,--输入的信息
  startStr out varchar2,--输出起始地
  endStr out varchar2--输出目的地
)
as
Pi number:=0;
begin
    Pi:=regexp_instr(message,'到|去|——>');
  if pi>0 then
    startStr:=wl_getcomaddress(substr(message,0,pi));
    endStr:= wl_getcomaddress(substr(message,pi));
   else
      startStr:=null;
      endStr:=null;
  end if;
end;
/

prompt
prompt Creating procedure WL_CUST_QQ_SENDINFO
prompt ======================================
prompt
create or replace procedure wl_cust_qq_sendinfo
--在QQ上展示货运信息
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

    (externalid       in  number
     ,sender          in  number
     ,message         in  varchar2
     ,out_externalid  out number
     ,out_sender      out number
     ,out_message     out varchar2
    )

is
  ct number(10);
  tmp_out_message varchar2(8000);
  in_sim          varchar2(30);
  tmp_infoid_str  varchar2(1000);
  tmp_in_ct       varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  info_number    number(5);
  tmp_str1        varchar2(500);
  temp_nbr varchar2(20);
  startStr varchar2(100):='';
  endStr varchar2(100):='';
  temp_sign number(6):=0;
  begin
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;

  -----判断用户，老用户更新时间，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where qq=to_char(sender);

  if ct> 0 then
     update wl_cust_mobile set last_position_date=sysdate
     where  sim=to_char(sender) or qq=to_char(sender);
     commit;  ---更新用户
  else
     insert into wl_cust_mobile(sim,qq,last_position_date,note,create_Date,eff_date)
     values (to_char(sender),to_char(sender),sysdate,'qq:群号'||to_char(externalid),sysdate,sysdate+30);
     commit;  ---插入用户
  end if;
 
 -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid>0 then
     goto ret_end;
  end if;
   ----信息列表-----
  if regexp_instr(trim(lxf_replace_place_name(trim(message))),'^#P\d+#$')>0 then
     wl_city_list(trim(message), tmp_out_message);
     goto ret_end;
  end if ;
 --------------------判断是否绑定手机号-------------------------
 /*WL_CUST_ZHBQQ_HasPhone(trim(sender), message, tmp_out_message ,temp_sign);
 --if temp_sign=1 then
   --  goto ret_end;
 --end if;
 /*---------------------试用是否到期----------------------------
 if  wl_cust_is_eff_moblie(sender)=0 then
      tmp_out_message :='您好，您的试用期已到，请发送用手机发送短信5137到10669588订购后继续使用，包月费10元/月；';
     goto ret_end;
 end if;*/
  -----输入信息校验---------
  if message='?' or message='？' then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;
  --按指定线路返回
  WL_Cust_ZHBQQ_GetAddress(message,startStr,endStr);
  if startStr is null or endStr is null then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;

   ----------返回记录集，sender=QQ号=SIM
   tmp_in_ct :='5';
   in_sim   :=to_char(sender);
   ----------------该用户是否订购掌上通业务------------------
  /* 
   select nbr into temp_nbr from Wl_Cust_Mobile where qq=to_char(sender);
   if temp_nbr is null or trim(temp_nbr)='' then
        ------------------今天是否已经查询购的----------------------------
        select to_char(Last_Serch_Data,'yyyy:mm:dd') into temp_nbr  from Wl_Cust_Mobile where qq=to_char(sender);
        if temp_nbr=to_char(sysdate,'yyyy:mm:dd') then
            tmp_in_ct:=1;
        end if;
   end if;
    select Isorderssxy into ct from Wl_Cust_Mobile where qq=to_char(sender);
    dbms_output.put_line(to_char(ct)||'ct');
    if ct=0 then
       tmp_in_ct:=1;
     elsif ct=1 then
        tmp_in_ct:=5;
    end if;*/
   tmp_infoid_str:=get_line_info_TXH_top(in_sim,tmp_in_ct,startStr,endStr);
-------更新最后查询的条件--------------------------
   if from_city is not null  and  to_city is not null  then
      update wl_cust_mobile set last_position_date=sysdate ,last_search=from_province||from_city||'到'||to_province||to_city,
      last_serch_data=Sysdate 
      where  sim=to_char(sender) or qq=to_char(sender);
      commit;  ---更新用户
   end if ;

   if tmp_infoid_str is null then
      tmp_out_message :='暂时没有您要查询的信息';
      --tmp_out_message :='春节期间放假十天，信息数较少，请谅解!';
      goto ret_end;
   end if;
   tmp_out_message:=tmp_infoid_str;
-----查询记录条数-----------
   select count(*) into info_number from WL_info_data where info_start like startStr||'%' and info_end like endStr||'%';
   tmp_out_message:=startStr||'到'||endStr||'共有'||to_char(info_number)||'条信息:'||tmp_out_message;
   if tmp_in_ct=1 then
      tmp_out_message:=tmp_out_message||chr(10)||'您是试用用户,可查看的条数有限。'||chr(10)||'如需订购找货宝业务请用以下方式开通,每月收费10元,'||chr(10)||'1.手机发送短信5137到10669588订购该业务,根据提示操作相应业务。'||chr(10)||
      '2.登陆http://zhaohuobao.taobao.com/购买该服务,购买后我们工作人员将与您联系,3.如果您已订购成功。请告诉我您的手机号【找货宝】';
   else
      tmp_out_message:=tmp_out_message||chr(13)||chr(10)||'*继续发送 '||startStr||'到'||endStr||' 查询剩余信息。';
   end if;
  insert into wl_cust_moblie_qq_log ( externalid,sender,message) values (externalid,sender,message );
  commit;
   -------结束点---------
 <<ret_end>>
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;
end;
/

prompt
prompt Creating procedure WL_CUST_QQ_SENDINFO_20120708
prompt ===============================================
prompt
create or replace procedure wl_cust_qq_sendinfo_20120708

--在QQ上展示货运信息
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

    (externalid       in  number
     ,sender          in  number
     ,message         in  varchar2
     ,out_externalid  out number
     ,out_sender      out number
     ,out_message     out varchar2
    )

is
   type cursor_type is ref cursor;
   c1 cursor_type;

  ct number(10);
  tmp_out_message varchar2(8000);
  tmp_row_message varchar2(2000);
  in_sim          varchar2(30);
  tmp_line        varchar2(500);
  tmp_str         varchar2(2000);
  tmp_infoid_str  varchar2(1000);
  tmp_in_ct       varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市

  begin
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;

  -----判断用户，老用户更新时间，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where qq=to_char(sender);

  if ct> 0 then
     update wl_cust_mobile set last_position_date=sysdate
     where  sim=to_char(sender) or qq=to_char(sender);
     commit;  ---更新用户
  else
     insert into wl_cust_mobile(sim,qq,last_position_date,note)
     values (to_char(sender),to_char(sender),sysdate,'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if message='?' or message='？' then
     tmp_out_message :='查找货运信息，请发送：出发地到目的地的城市名，如发送：“上海到石家庄”。';
     goto ret_end;
  end if;
  --按指定线路返回
  tmp_line:=get_wl_info_region_from_line (message)||','||
            get_wl_info_region_end_line  (message);


  if trim(tmp_line)=',' then
     tmp_out_message :='城市输入错误，如需要帮助请回复”？“';
     goto ret_end;
  end if;

      ----------返回记录集，sender=QQ号=SIM
   tmp_in_ct :='5';
   in_sim   :=to_char(sender);
      -------tmp_line输入字符串拆分----------
      --格式:陕西,榆林,陕西,西安 (陕西,全部,全部,全部)
   from_province:=trim(substr(tmp_line,1,instr(tmp_line,',',1,1)-1));
   from_city:=trim(substr(tmp_line,instr(tmp_line,',',1,1)+1,instr(tmp_line,',',1,2)-instr(tmp_line,',',1,1)-1));
   to_province:=trim(substr(tmp_line,instr(tmp_line,',',1,2)+1,instr(tmp_line,',',1,3)-instr(tmp_line,',',1,2)-1));
   to_city:=trim(substr(tmp_line,instr(tmp_line,',',1,3)+1,instr(tmp_line,',',1,3)-instr(tmp_line,',',1,2)-1));

   tmp_infoid_str:=get_line_info_id_top
     (in_sim,tmp_in_ct,from_province,from_city,to_province,to_city);

    --dbms_output.put_line(tmp_infoid_str);

      --if from_province is null or from_province='全部' then
         --ret:=2;
         --goto ret_end;
      --end if ;

   if tmp_infoid_str is null then
      tmp_out_message :='无最新货源';
      goto ret_end;
   end if;


  tmp_str:='';
  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)
  ||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_infoid_str||')
  order by create_date desc';


/*  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)
  ||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_infoid_str||')
  order by create_date desc';*/

open c1 for tmp_str;

 loop
    fetch c1 into tmp_row_message;
    exit when c1%notfound;

    tmp_out_message:=tmp_out_message||chr(13)||tmp_row_message||';';
     --判读是否提取到值，没取到值就退出
     --取到值job_region_from%notfound 是false
     --取不到值job_region_from%notfound 是true

 end loop;
close c1;

--dbms_output.put_line(tmp_out_message);


/*c_row job_region_from%rowtype;
 begin

  job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;
*/


  --tmp_out_message :=c_row.info||c_row.info_nbr1;


   ------调整信息权值-----------------------------
  tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_infoid_str||' )';
  execute immediate tmp_str;
  commit;

   ------记录访问信息------无记录的插入，有记录的更新时间-----------------------
   tmp_str:='';
   tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
   tmp_str:=tmp_str||'select id, '||chr(39)||in_sim||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_infoid_str||' )';
   tmp_str:=tmp_str||' minus ';
   tmp_str:=tmp_str||'select info_id, '||chr(39)||in_sim||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_infoid_str||')
                      and   sim='||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_infoid_str||' ) and sim= '||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;
   -------结束点---------

 <<ret_end>>
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;



end;
/

prompt
prompt Creating procedure WL_CUST_YLBQQ_OPCLBIZ
prompt ========================================
prompt
create or replace procedure WL_CUST_YLBQQ_OPCLBIZ(
  /*开通或者注销手机订购业务*/
  in_op in varchar2,--操作
  in_num in varchar2,--手机号
  in_month in varchar2,--开通几月
  in_Saler in varchar2,--销售编号
  in_bingQQ in varchar2,--绑定QQ号
  out_mess out varchar2--输出信息
)as
TempCount number(5):=0;--临时储存记录数
TempStr varchar2(1000):='';--临时储存动态sql
Temp_OutMess varchar2(1000):='';--临时储存输出信息
begin
   ------------------表中是否存在该手机号码------------------------
   select count(*) into TempCount from Wl_Cust_Mobile where nbr=in_num;
  ---------------------开通手机号--------------------------------------
   if in_op='开通' and TempCount=0 then--开通的手机号码不存在
      if in_bingQQ is null or  in_bingQQ=''  then --带有绑定QQ
            insert into wl_cust_mobile(nbr,create_date,eff_date,isorderssxy) values(in_num,sysdate,add_months(sysdate,to_number(in_month)),1);
          Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！';
          goto ret_end;
      else--不带有QQ绑定
          select count(*) into  TempCount from Wl_Cust_Mobile where qq=in_bingQQ and nbr<>in_num;
         if TempCount=0 then --该QQ 唯一
             insert into wl_cust_mobile(nbr,create_date,eff_date,isorderssxy,qq) values(in_num,sysdate,add_months(sysdate,to_number(in_month)),1,in_bingQQ);
             Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定成功！';
             goto ret_end;
         else---gaiQQ不唯一
             select max(nbr) into TempStr  from wl_cust_mobile where qq=in_bingQQ and nbr<>in_num;
             if TempStr is null or trim(TempStr)='' then --其他QQ对应的电话号码为空
                 delete wl_cust_mobile where qq=in_bingQQ and nbr<>in_num;
                 insert into wl_cust_mobile(nbr,create_date,eff_date,isorderssxy,qq) values(in_num,sysdate,add_months(sysdate,to_number(in_month)),1,in_bingQQ);
                 Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定成功！';
                 goto ret_end;
             else--其他QQ对应的电话号码不为空
                 insert into wl_cust_mobile(nbr,create_date,eff_date,isorderssxy) values(in_num,sysdate,add_months(sysdate,to_number(in_month)),1);
                 Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定失败！原因:该QQ号已被其他用户绑定！';
                 goto ret_end;
             end if;
         end if;
      end if;
   elsif in_op='开通' and TempCount<>0 then --手机号码存在
      if in_bingQQ='' or in_bingQQ is null  then --带有绑定QQ
          update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)),isorderssxy=1 where nbr=in_num; 
          Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！';
          goto ret_end;
          ----------
      else--不带有QQ绑定
         select count(*) into  TempCount from Wl_Cust_Mobile where qq=in_bingQQ and nbr<>in_num;
         if TempCount=0 then --该QQ 唯一
             update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)),isorderssxy=1,qq=in_bingQQ where nbr=in_num; 
             Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定成功！';
             goto ret_end;
         else---gaiQQ不唯一
             select max(nbr) into TempStr  from wl_cust_mobile where qq=in_bingQQ and nbr<>in_num;
             if TempStr is null or trim(TempStr)='' then --其他QQ对应的电话号码为空
                 delete wl_cust_mobile where qq=in_bingQQ and nbr<>in_num;
                 update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)),isorderssxy=1,qq=in_bingQQ where nbr=in_num; 
                  Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定成功！';
                 goto ret_end;
             else--其他QQ对应的电话号码不为空
                update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)),isorderssxy=1 where nbr=in_num; 
                  Temp_OutMess:='手机号码'||in_num||'已成功开通'||in_month||'个月！'||'qq号码:'||in_bingQQ||'绑定失败！原因:该QQ号已被其他用户绑定！';
                 goto ret_end;
             end if;
         end if;
      end if;
   end if;
   ----------------------------试用个体------------------------------
   if in_op='试用' then
      if (in_num is null or trim(in_num)='') and (in_bingQQ is null or trim(in_bingQQ)='') then
         Temp_OutMess:='试用失败。无法判断试用对象';
         goto app_end;
      end if;
      if (in_num is not null or trim(in_num)<>'') then
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)) where nbr=in_num;
         commit;
         Temp_OutMess:='手机号码:'||in_num||'已成功拥有'||to_char(in_month)||'的试用期！'; 
         goto app_end;
      end if;
      if (in_bingQQ is not null or trim(in_bingQQ)<>'') then
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month)) where qq=in_bingQQ;
         commit;
         Temp_OutMess:='QQ号码:'||in_bingQQ||'已成功拥有'||to_char(in_month)||'的试用期！'; 
         goto app_end;
      end if;
   end if;
   --------------------------试用全部---------------------------
   if in_op='试用全部' then
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(in_month));
         commit;
         Temp_OutMess:='全部账号已成功拥有'||to_char(in_month)||'的试用期！'; 
         goto app_end;
   end if;
   -------------------------注销---------------------------------
   select count(*) into TempCount from Wl_Cust_Mobile where nbr=in_num;
   if in_op='注销' and TempCount=0 then
       Temp_OutMess:='该号码不存在。无法完成注销！';
       goto app_end;
   end if;
    if in_op='注销' and TempCount<>0 then
       update Wl_Cust_Mobile set nbr='' where nbr=in_num;
       commit;
       Temp_OutMess:='手机号码:'||in_num||'注销成功！';
       goto app_end;
   end if;
   ----------------------取消业务------------------------
    if in_op='取消' and TempCount=0 then
       Temp_OutMess:='该号码不存在。无法完成业务取消！';
       goto app_end;
   end if;
    if in_op='取消' and TempCount<>0 then
       update Wl_Cust_Mobile set isorderssxy=0,eff_date=sysdate where nbr=in_num;
       commit;
       Temp_OutMess:='手机号码:'||in_num||'业务取消成功！';
       goto app_end;
   end if;
   -------------------------开通结束点-----------------------------
   <<ret_end>>
   commit;
    if in_Saler='' or in_Saler is null then
      goto app_end;
    else--0 成功 1 销售编号无效 2 销售类型为空 3 销售对象无效 4 重复绑定  5已被绑定
       select max(CUST_ID) into TempStr from Wl_Cust_Mobile where nbr=in_num;
       WL_XS_sAch_Add(to_number(in_Saler),'司机',TempStr,TempCount);
       if TempCount=0 then
          Temp_OutMess:=Temp_OutMess||' 销售编号'||in_Saler||'绑定成功！';
       elsif TempCount=3 then
          Temp_OutMess:=Temp_OutMess||' 司机无效！。销售编号'||in_Saler||'绑定失败！';
       elsif TempCount=4 then
           Temp_OutMess:=Temp_OutMess||' 。销售编号'||in_Saler||'已经绑定该号码！';
       elsif TempCount=5 then
           Temp_OutMess:=Temp_OutMess||'该号码已被其他销售人员绑定 。销售编号'||in_Saler||'绑定失败！';
       end if;
    end if;
   <<app_end>>
   out_mess:=Temp_OutMess;
end;
/

prompt
prompt Creating procedure WL_CUST_YLBQQ_OPENBIZ
prompt ========================================
prompt
create or replace procedure WL_cust_YLBQQ_OpenBiz(
   /*依赖宝QQ开通手机号，QQ号*/
   mess in varchar2,--输入的信息类容
   OutMess out varchar2--输出信息
)as
tempOpeare varchar2(150):='';--操作标示 开通 ,注销
TempPhone varchar2(150):='';--临时储存手机号码
TempQQ varchar2(100):='';--临时储存QQ号码
TemPSaleer varchar2(100):='';--临时储存销售ID
TempMonth varchar2(100):='12';--临时储存充值月数
TempOutMess varchar2(150):='';---临时储存输出字符串
TempInMess varchar2(500):='';--临时储存输入字符串
begin
  -------------判断操作符------------------------
  TempInMess:=mess;
  if regexp_instr(TempInMess,'开通|注销|取消|试用全部|试用')=0 then
     TempOutMess:='操作符无效,请重新输入';
     goto ret_end;
  end if;
  ---------------获取操作符-----------------------
  tempOpeare:=regexp_substr(TempInMess,'开通|注销|取消|试用全部|试用');
  dbms_output.put_line(tempOpeare);
  -------------判断手机号码------------------------
  if tempOpeare not in('试用','试用全部') then
    if regexp_instr(TempInMess,'(开通|注销|取消|试用)\s*1(3|4|5|6|7|8)\d{9}|(手机号|号码|电话)?\s*1(3|4|5|6|7|8)\d{9}\s*')=0 then
       TempOutMess:='手机号码有误,请重新输入';
       goto ret_end;
    end if;
  end if;
  -------------------获取手机号码-------------------
  TempPhone:=regexp_substr(TempInMess,'(开通|注销|取消|试用)\s*1(3|4|5|6|7|8)\d{9}|(手机号|号码|电话)?\s*1(3|4|5|6|7|8)\d{9}\s*');
  TempInMess:=regexp_replace(TempInMess,'(开通|注销|取消|试用)\s*1(3|4|5|6|7|8)\d{9}|(手机号|号码|电话)?\s*1(3|4|5|6|7|8)\d{9}\s*','');
  TempPhone:=regexp_replace(TempPhone,'手机号|号码|电话|开通|注销|取消|试用|\s+','');
  
  dbms_output.put_line('电话号码:'||TempPhone);
  -------------------获取充值月数--------------------
   if regexp_instr(TempInMess,'月\s*\d{1,2}')>0 then
      TempMonth:=regexp_substr(TempInMess,'月\s*\d{1,2}');
      TempInMess:=regexp_replace(TempInMess,'月\s*\d{1,2}','');
      TempMonth:=regexp_replace(TempMonth,'月|\s+','');
   end if;
   dbms_output.put_line('月份:'||TempMonth);
   -------------------获取销售编号--------------------
   if regexp_instr(TempInMess,'销售编号\s*\d{6}')>0 then
      TemPSaleer:=regexp_substr(TempInMess,'销售编号\s*\d{6}');
      TempInMess:=regexp_replace(TempInMess,'销售编号\s*\d{6}','');
      TemPSaleer:=regexp_replace(TemPSaleer,'销售编号|\s+','');
   end if;
   dbms_output.put_line('销售编号:'||TemPSaleer);
    -------------------获取绑定QQ号--------------------
   if regexp_instr(TempInMess,'(绑定)?\s*(QQ|qq|Qq|qQ)\s*\d{5,12}\s*')>0 then
      TempQQ:=regexp_substr(TempInMess,'(绑定)?\s*(QQ|qq|Qq|qQ)?\s*\d{5,12}\s*');
      TempQQ:=regexp_replace(TempQQ,'(绑定)?\s*(QQ|qq|Qq|qQ)?|\s+','');
   end if;
   dbms_output.put_line('qq号:'||TempQQ);
   ----------------------执行开通货注销操作------------------
   WL_cust_YLBQQ_OPCLBIZ(tempOpeare,TempPhone,TempMonth,TemPSaleer,TempQQ,TempOutMess);
  <<ret_end>>
  OutMess:=TempOutMess;
end;
/

prompt
prompt Creating procedure WL_GET_ACTIVE_USER
prompt =====================================
prompt
create or replace procedure wl_get_active_user
(ret out varchar2)      


--1.输入字符串拆分 格式  陕西,榆林,陕西,西安 ，省份和起始城市不能为空，目的城市可以为空
  as
active_driver number(5); 
active_info  number(5);
active_dep   number(5);
 begin
  
  -----活跃司机数量
select count(*)  into  active_driver
from wl_cust_mobile  where sysdate -last_position_date<1 and last_search is not null ;
----榆林活跃信息条数
select count(*) into active_info
from( 
select info,info_nbr1,count(*) ct
from wl_info_data 
group by info,info_nbr1);
-----活跃信息部个数---------
select count( distinct nbr)  into active_dep
from wl_info_data where info_start like '陕西榆林%';  
 ret:='24小时内活跃司机用户'||active_driver||'个，有效信息数'||active_info||'条，活跃信息部'||active_dep||'家。'||get_OnLine;
end;
/

prompt
prompt Creating procedure WL_CUST_QQ_SENDMSG
prompt =====================================
prompt
create or replace procedure wl_cust_qq_sendmsg
--Mb:从QQ上获(从QQ上取到发布信息,插入至QQ发布的信息表,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功
(externalid in number,sender in number,message in varchar2,
 out_externalid out number,out_sender out number,out_message out varchar2)
as
  tmp_message        varchar2(4000);
  ct                 number(10);
  tmp_c_name         varchar2(500);
  tmp_c_contact      varchar2(500);
  tmp_c_address      varchar2(500);
  tmp_driver_qq      varchar2(500);
  tmp_driver_carnbr  varchar2(500);
  tmp_driver_nbr     varchar2(500);
  tmp_id             number(30);
  tmp_out_message    varchar2(4000);
  tmp_create_date    date;
  tmp_info_out       varchar2(4000);
  tmp_amount         number(10,2);
  tmp_cust_id        varchar2(64);
  tmp_nbr            varchar2(30);
  tmp_c_password     varchar2(64);
  tmp_c_city         varchar2(200);
  tmp_str            varchar2(500);
begin
  tmp_out_message:='';
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>4000 then
     goto ret_end;
  end if;
  -----预处理规则--------
  tmp_message:=get_wl_info_rule(message);
  if tmp_message is null or  length(tmp_message)>4000 then
       goto ret_end;
  end if;
  -----判断发布货源信息部，新用户插入记录-----
  select count(*) into ct
  from wl_cust_department where qq=to_char(sender);

  if ct<= 0 then
     insert into wl_cust_department(qq,note)
     values (to_char(sender),'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if tmp_message='?' or tmp_message='？' then
     tmp_out_message :='货运信息发布格式为：起始城市到目的城市，货源类型，车源类型，联系电话等内容。如：府谷到运城，运煤炭 联系电话13310924210';
     goto ret_end;
  end if;
  -----统计信息--------------
  if trim(tmp_message)='统计'  then
     wl_get_active_user(tmp_out_message);
     goto ret_end;
  end if;
tmp_create_date :=sysdate;
tmp_info_out:=substr(lxf_get_qq_msg_back(to_char(sender),to_char(externalid),tmp_message),1,3000);
tmp_out_message:=tmp_info_out;


if regexp_instr(trim(lxf_replace_place_name(tmp_message)),'^#P\d+#$')>0 then
  wl_city_list(trim(tmp_message), tmp_info_out);
    tmp_out_message:=tmp_info_out;
end if ;
 insert into  wl_info_from_in_early (text1,note,info_id)
 values (message,'qq-'||externalid||'-'||sender,tmp_id);
 commit;

 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'qq处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 tmp_create_date:=sysdate;
 insert into  wl_info_from_in_early (text1,note)
 values (message,'qq-'||externalid||'-'||sender);
 commit;

 <<real_end>>

---一、更新信息部名称、电话、地址-----信息部客户表---
 if instr(tmp_message,'我的')>0 then
      tmp_out_message :='';
      tmp_c_name      :=get_cust_name   (tmp_message);
      tmp_c_contact   :=get_cust_contact(tmp_message);
      tmp_c_address   :=get_cust_address(tmp_message);
    --1.名称
    if tmp_c_name is not null
    then
       update wl_cust_department set c_name=tmp_c_name where nbr=trim(sender);
       commit;
       tmp_out_message :='已记录您的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);
    end if;
    --2.电话
    if tmp_c_contact is not null
    then
       update wl_cust_department set contact=tmp_c_contact where nbr=trim(sender);
       commit;
       tmp_out_message :=tmp_out_message||'已记录您的联系电话为：'||tmp_c_contact||'--好3G，用天翼'||chr(10);
    end if;
    --3.地址
    if tmp_c_address is not null
    then
       update wl_cust_department set address=tmp_c_address where nbr=trim(sender);
       tmp_out_message :=tmp_out_message||'已记录您的地址为：'||tmp_c_address||'--好3G，用天翼'||chr(10);
       commit;
    end if;
 end if;


---二、更新司机qq号、车牌、电话-----手机客户表---
 if instr(tmp_message,'司机')>0
 then
      tmp_out_message :='';
      tmp_driver_qq       :=get_driver_qq   (tmp_message);
      tmp_driver_carnbr   :=get_driver_carnbr(tmp_message);
      tmp_driver_nbr      :=get_cust_contact2(tmp_message);
    --1.qq号
    if tmp_driver_qq is not null and tmp_driver_nbr is not null
    then
       select count(*) into ct from wl_cust_mobile where
       sim=to_char(tmp_driver_qq);

       if ct= 0
       then
          insert into wl_cust_mobile(sim,qq,note,nbr)
          values (to_char(tmp_driver_qq),
                  to_char(tmp_driver_qq),
                  'qq:'||tmp_driver_qq||',来自qq:'||trim(sender),
                  tmp_driver_nbr
                  );
          commit;  ---插入用户
          tmp_out_message :='已记录司机的qq号为：'||tmp_driver_qq||'--好3G，用天翼'||chr(10);
       end if;

       if ct> 0
       then
          tmp_out_message :='已经有qq号为：'||tmp_driver_qq||'的司机--好3G，用天翼'||chr(10);
       end if;
    end if;
    --2.车牌
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
     update wl_cust_mobile set car_number=tmp_driver_carnbr
     where sim=to_char(tmp_driver_qq) ;--and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的车牌号为：'||tmp_driver_carnbr||'--好3G，用天翼'||chr(10);
    end if;
     --3.电话
    if tmp_driver_qq is not null
       and tmp_driver_nbr is not null
    then
     update wl_cust_mobile set nbr=tmp_driver_nbr
     where sim=to_char(tmp_driver_qq);-- and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的号码号为：'||tmp_driver_nbr||'--好3G，用天翼'||chr(10);
    end if;

 end if;


---三、-----客服人员用---信息部客户表
--客服QQ106074832，密码：123456，名字：王婷11，电话15332592881
 if instr(tmp_message,'客服')>0  and trim(sender) in
    ('18646500','106074835','49631833','7349238',
    '549748914','501943805','447658593','860299303','342143923')
 then
     tmp_out_message :='';
     tmp_c_name      :=get_cust_name2(tmp_message);
     tmp_c_contact   :=get_cust_contact2(tmp_message);
     tmp_c_address   :=get_cust_qq(tmp_message); --qq
     tmp_c_password  :=get_cust_password(tmp_message);
     tmp_c_city      :=get_cust_city(tmp_message);
    --QQ、密码、名称、电话
    if tmp_c_address is not null
    then select count(*) into ct from wl_cust_department where
         nbr=to_char(tmp_c_address);
         if ct=0
         then insert into wl_cust_department(nbr) values(tmp_c_address);
              commit;---插入新建QQ用户
              tmp_out_message :='已创建信息部的qq号为：'||tmp_c_address||chr(10);
         end if;
         ---给司机用户停机
        if tmp_c_address is not null  and  instr(tmp_message,'停机')>0 then
             execute immediate 'update wl_cust_mobile set eff_date=sysdate where sim=to_char('||tmp_c_address||')';
              commit;
             tmp_out_message :=tmp_out_message||tmp_c_address||'用户已经停机';

         end if;
          ---给司机用户开机
         if tmp_c_address is not null  and  instr(tmp_message,'开机')>0 then
              execute immediate 'update wl_cust_mobile set eff_date=sysdate
              +365 where sim=to_char('||tmp_c_address||')';
              commit;
              tmp_out_message :=tmp_out_message||tmp_c_address||'用户已经开机';
         end if;


         tmp_str:='update wl_cust_department set ';

         if tmp_c_password is not null --密码
         then tmp_str:=tmp_str||'c_password='||chr(39)||tmp_c_password||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的密码为：'||tmp_c_password||chr(10);
         end if;
         if tmp_c_name is not null     --名称
         then tmp_str:=tmp_str||'c_name='||chr(39)||tmp_c_name||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的名称为：'||tmp_c_name||chr(10);
         end if;
         if tmp_c_contact is not null  --电话
         then tmp_str:=tmp_str||'contact='||chr(39)||tmp_c_contact||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的联系电话为：'||tmp_c_contact||chr(10);
         end if;
         if tmp_c_city is not null  --城市
         then tmp_str:=tmp_str||'city='||chr(39)||tmp_c_city||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的默认城市为：'||tmp_c_city||chr(10);
         end if;
         tmp_str:=tmp_str||'note='||chr(39)||'qq:'||tmp_c_address||',来自qq客服修改'
         ||chr(39)||' where nbr=to_char('||tmp_c_address||')';
         execute immediate tmp_str;
         commit;
    end if;
 end if;


---四、客户充值-----客服人员用------
--客户充值,QQ106074832,金额100
 if instr(tmp_message,'客户充值')>0 and trim(sender) in
   ('1605073514','106074835','18646500','7349238','49631833',
   '1605073514','577911432') then
      tmp_out_message :='';
      tmp_amount      :=get_cust_amount(tmp_message);
      tmp_c_address   :=get_cust_qq(tmp_message); --qq

    if tmp_c_address is not null
    then
       select count(*) into ct from wl_cust_department where
       nbr=to_char(tmp_c_address);

       if ct=1
       then
       select cust_id,nbr,c_name,contact into
       tmp_cust_id,tmp_nbr,tmp_c_name,tmp_c_contact
       from wl_cust_department where
       nbr=to_char(tmp_c_address);

       insert into wl_cust_dep_balance_log
       (cust_id,amount,use,payee,payee_name,nbr,c_name,contact,create_date)
       values
       (tmp_cust_id,tmp_amount,'客服充值',sender,'王婷',tmp_nbr,
        tmp_c_name,tmp_c_contact,tmp_create_date);
       commit;  ---插入余额日志表

       select count(*) into ct from wl_cust_dep_balance where
       cust_id=tmp_cust_id; --在客户余额表中增加余额
         if ct=0
         then insert into wl_cust_dep_balance(cust_id,advance,
              balance,nbr,c_name,contact)values
              (tmp_cust_id,tmp_amount,tmp_amount,
              tmp_nbr,tmp_c_name,tmp_c_contact);
              commit;
         else update wl_cust_dep_balance set
              advance=advance+tmp_amount,balance=balance+tmp_amount
              where cust_id=tmp_cust_id;
              commit;
         end if;

       tmp_out_message :='成功为qq号：'||tmp_c_address||'充值'||
       tmp_amount||'元。该用户在系统中联系电话是：'||
       tmp_c_contact||',名称是：'||tmp_c_name;

       end if;
    end if;
 end if;
 ---五、开通业务-----管理员------
--开通[注销|取消] 电话 15129030647 月 12  销售编号 1010
 if regexp_instr(tmp_message,'开通|注销|取消|试用|试用全部')>0 and trim(sender) in('577911432','791465191','860299303','342143923') then
    WL_cust_YLBQQ_OPENBIZ(tmp_message,tmp_out_message);
 end if;
 -----输入参数externalid校验,大于0收到群消息,回复内容为空
  if externalid>0 then
     tmp_out_message:='';
  end if;
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;

end ;
/

prompt
prompt Creating procedure WL_CUST_QQ_SENDMSG_20121015
prompt ==============================================
prompt
create or replace procedure wl_cust_qq_sendmsg_20121015
--Mb:从QQ上获(从QQ上取到发布信息,插入至QQ发布的信息表,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

(externalid in number,sender in number,message in varchar2,
 out_externalid out number,out_sender out number,out_message out varchar2)
as
  tmp_message        varchar2(500);
  ct                 number(10);
  tmp_c_name         varchar2(500);
  tmp_c_contact      varchar2(500);
  tmp_c_address      varchar2(500);
  tmp_driver_qq      varchar2(500);
  tmp_driver_carnbr  varchar2(500);
  tmp_driver_nbr     varchar2(500);
  tmp_id             number(30);
  tmp_out_message    varchar2(500);
  tmp_create_date    date;
  tmp_info_start     varchar2(500);
  tmp_info_end       varchar2(500);
  tmp_info_truck     varchar2(500);
  tmp_info_goods     varchar2(500);
  tmp_info_note      varchar2(500);
  tmp_info_nbr1      varchar2(30);
  tmp_info_nbr2      varchar2(30);
  tmp_info_nbr3      varchar2(30);
  tmp_info_out       varchar2(500);
  tmp_lng_start      number(11,8);
  tmp_lat_start      number(11,8);
  tmp_lng_end        number(11,8);
  tmp_lat_end        number(11,8);
  tmp_amount         number(10,2);
  tmp_cust_id        varchar2(64);
  tmp_nbr            varchar2(30);
begin
  tmp_out_message:='';
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;
  -----预处理规则--------
  tmp_message:=get_wl_info_rule(message);
  if tmp_message is null or  length(tmp_message)>500 then
       goto ret_end;
  end if;
  -----判断发布货源信息部，新用户插入记录-----
  select count(*) into ct
  from wl_cust_department where nbr=to_char(sender);

  if ct<= 0 then
     insert into wl_cust_department(nbr,note)
     values (to_char(sender),'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if tmp_message='?' or tmp_message='？' then
     tmp_out_message :='货运信息发布格式为：起始城市到目的城市，货源类型，车源类型，联系电话等内容。如：府谷到运城，运煤炭 联系电话13310924210';
     goto ret_end;
  end if;
 /* -----目的地和号码校验----------------
  tmp_info_end    :=get_wl_info_region_end_new(tmp_message);
  tmp_info_nbr1   :=get_wl_info_nbr1(tmp_message);

  if tmp_info_nbr1 is null then
    select contact into tmp_info_nbr1 from wl_cust_department
    where nbr=trim(sender);
  end if;

  if  tmp_info_end is null or tmp_info_nbr1 is null then
      tmp_out_message :='发布的货源信息无效，如需要帮助请回复”?“。--好3G，用天翼';
      goto ret_end;
  end if;
*/
tmp_create_date :=sysdate;
tmp_info_out:=lxf_get_qq_msg_back(to_char(sender),to_char(externalid),tmp_message);
tmp_out_message:=tmp_info_out||' 已发布';

/*
 ------取信息值------------------------
  tmp_info_start  :=get_wl_info_region_from_new(tmp_message);

  tmp_info_nbr2   :=get_wl_info_nbr2(tmp_message);
  tmp_info_nbr3   :=get_wl_info_nbr3(tmp_message);

  tmp_info_truck  :=get_wl_info_truck(tmp_message);
  tmp_info_goods  :=get_wl_info_goods(tmp_message);
  tmp_info_note   :=get_wl_info_note (tmp_message);

  tmp_create_date :=sysdate;
  tmp_id          :=wl_info_data_id.nextval;



-----生成输出信息-----------
 tmp_info_out :=tmp_info_start||'到'||tmp_info_end;

 tmp_info_out :=replace(tmp_info_out,'上海上海','上海');
 tmp_info_out :=replace(tmp_info_out,'北京北京','北京');
 tmp_info_out :=replace(tmp_info_out,'天津天津','天津');
 tmp_info_out :=replace(tmp_info_out,'重庆重庆','重庆');

 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;

 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;

 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;

 ----更新或插入目的表wl_info_data---------------------------
  select count(*) into ct
  from wl_info_data
  where info=tmp_info_out and info_nbr1=tmp_info_nbr1
  and note=to_char(externalid)
  and nbr=to_char(sender)  ;


  if ct >0 then
    update  wl_info_data set create_date=tmp_create_date
    where info=tmp_info_out and info_nbr1=tmp_info_nbr1
    and note=to_char(externalid)
    and nbr=to_char(sender)  ;
    commit;

-- dbms_output.put_line(tmp_info_out);

    tmp_out_message:=tmp_message||' 已更新--好3G，用天翼';
  goto real_end;
  end if;


  tmp_lng_start  :=get_lng(tmp_info_start);
  tmp_lat_start  :=get_lat(tmp_info_start);
  tmp_lng_end    :=get_lng(tmp_info_end)  ;
  tmp_lat_end    :=get_lat(tmp_info_end)  ;

  select c_name into tmp_c_name from wl_cust_department
  where nbr=trim(sender);


--c_name=to_char(sender) info=message note=to_char(externalid)
insert into wl_info_data
   (id,nbr,c_name,create_date,info,
    info_start,info_end,info_nbr1,info_nbr2,
    info_nbr3,info_truck,info_goods,info_note,
    info_from,note,
    lng_start,lat_start,lng_end,lat_end
   )
 values
   (tmp_id,to_char(sender),tmp_c_name,tmp_create_date,tmp_info_out,
    tmp_info_start,tmp_info_end,tmp_info_nbr1,tmp_info_nbr2,
    tmp_info_nbr3,tmp_info_truck,tmp_info_goods,tmp_info_note,
    'qq',to_char(externalid),
    tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;
*/
 --tmp_out_message:=tmp_out_message||tmp_message||' 已发布';

 insert into  wl_info_from_in_early (text1,note,info_id)
 values (message,'qq-'||externalid||'-'||sender,tmp_id);
 commit;

 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'qq处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 tmp_create_date:=sysdate;
 insert into  wl_info_from_in_early (text1,note)
 values (message,'qq-'||externalid||'-'||sender);
 commit;

 <<real_end>>

  -----更新信息部名称、电话、地址-----信息部客户表---
 if instr(tmp_message,'我的')>0 then
      tmp_out_message :='';
      tmp_c_name      :=get_cust_name   (tmp_message);
      tmp_c_contact   :=get_cust_contact(tmp_message);
      tmp_c_address   :=get_cust_address(tmp_message);
    --1.名称
    if tmp_c_name is not null
    then
       update wl_cust_department set c_name=tmp_c_name where nbr=trim(sender);
       commit;
       tmp_out_message :='已记录您的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);
    end if;
    --2.电话
    if tmp_c_contact is not null
    then
       update wl_cust_department set contact=tmp_c_contact where nbr=trim(sender);
       commit;
       tmp_out_message :=tmp_out_message||'已记录您的联系电话为：'||tmp_c_contact||'--好3G，用天翼'||chr(10);
    end if;
    --3.地址
    if tmp_c_address is not null
    then
       update wl_cust_department set address=tmp_c_address where nbr=trim(sender);
       tmp_out_message :=tmp_out_message||'已记录您的地址为：'||tmp_c_address||'--好3G，用天翼'||chr(10);
       commit;
    end if;
 end if;


  -----更新司机qq号、车牌、电话-----手机客户表---
 if instr(tmp_message,'司机')>0 then
      tmp_out_message :='';
      tmp_driver_qq       :=get_driver_qq   (tmp_message);
      tmp_driver_carnbr   :=get_driver_carnbr(tmp_message);
      tmp_driver_nbr      :=get_driver_nbr(tmp_message);
    --1.qq号
    --2.车牌
    --3.电话
/*    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
       select count(*) into ct from wl_cust_mobile where
       qq=to_char(tmp_driver_qq) and car_number=tmp_driver_carnbr;

       if ct= 0
       then
          insert into wl_cust_mobile(sim,nbr,qq,car_number,note)
          values (to_char(tmp_driver_qq),
                  to_char(tmp_driver_nbr),
                  to_char(tmp_driver_qq),
                  tmp_driver_carnbr,'qq:'||tmp_driver_qq||',来自qq:'||trim(sender)
                 );
          commit;  ---插入用户
          tmp_out_message :='已记录司机的qq号为：'||tmp_driver_qq||
                            ',车牌为：'||tmp_driver_carnbr||
                            ',电话为：'||tmp_driver_nbr||chr(10);


       else
          tmp_out_message :='已经有qq号为：'||tmp_driver_qq||
                            ',车牌为：'||tmp_driver_carnbr||'的司机记录'||chr(10);
       end if;

     end if;*/

    --1.qq号
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
       select count(*) into ct from wl_cust_mobile where
       qq=to_char(tmp_driver_qq);

       if ct= 0
       then
          insert into wl_cust_mobile(sim,qq,note)
          values (to_char(tmp_driver_qq),
                  to_char(tmp_driver_qq),
                  'qq:'||tmp_driver_qq||',来自qq:'||trim(sender)
                  );
          commit;  ---插入用户
          tmp_out_message :='已记录司机的qq号为：'||tmp_driver_qq||'--好3G，用天翼'||chr(10);
       end if;

       if ct> 0
       then
          tmp_out_message :='已经有qq号为：'||tmp_driver_qq||'的司机--好3G，用天翼'||chr(10);
       end if;
     end if;
     --2.车牌
     if tmp_driver_qq is not null and tmp_driver_carnbr is not null
     then
     update wl_cust_mobile set car_number=tmp_driver_carnbr
     where sim=to_char(tmp_driver_qq) and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的车牌号为：'||tmp_driver_carnbr||'--好3G，用天翼'||chr(10);
     end if;
     --3.电话
     if tmp_driver_qq is not null and tmp_driver_carnbr is not null
        and tmp_driver_nbr is not null
     then
     update wl_cust_mobile set nbr=tmp_driver_nbr
     where sim=to_char(tmp_driver_qq) and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的号码号为：'||tmp_driver_nbr||'--好3G，用天翼'||chr(10);
     end if;

  end if;


--客服QQ106074832，名字：王婷11，电话15332592881
 -----客服人员用---
 if instr(tmp_message,'客服')>0  and trim(sender) in
   ('18646500','106074835','49631833','7349238',
    '549748914','501943805') then
      tmp_out_message :='';
      tmp_c_name      :=get_cust_name2   (tmp_message);
      tmp_c_contact   :=get_cust_contact2(tmp_message);
      tmp_c_address   :=get_cust_qq(tmp_message); --qq
    --1.QQ、名称
    if tmp_c_address is not null and tmp_c_name is not null
    then
       select count(*) into ct from wl_cust_department where
       nbr=to_char(tmp_c_address);

       if ct= 0
       then
          insert into wl_cust_department(nbr,c_name,note)
          values (to_char(tmp_c_address),
                  to_char(tmp_c_name),
                  'qq:'||tmp_c_address||',来自qq客服创建'
                  );
          commit;  ---插入用户
          tmp_out_message :='已记录信息部的qq号为：'||tmp_c_address||'--好3G，用天翼'||chr(10);
          tmp_out_message :=tmp_out_message||'已记录信息部的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);
       else
          update wl_cust_department set
          c_name=to_char(tmp_c_name),
          note='qq:'||tmp_c_address||',来自qq客服修改'
          where nbr=to_char(tmp_c_address);
          commit;  ---修改用户
          tmp_out_message :='已经有qq号为：'||tmp_c_address||'的信息部记录--好3G，用天翼'||chr(10);
          tmp_out_message :=tmp_out_message||'已修改信息部的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);

       end if;
     end if;

   --2.电话
     if tmp_c_address is not null and tmp_c_name is not null
        and tmp_c_contact is not null
     then
        update wl_cust_department set
        contact=to_char(tmp_c_contact),
        note='qq:'||tmp_c_address||',来自qq客服修改'
        where nbr=to_char(tmp_c_address);
        commit;  ---修改用户
        tmp_out_message :=tmp_out_message||'已修改信息部的联系电话为：'||tmp_c_contact||'--好3G，用天翼'||chr(10);
     end if;

 end if;


 --客户充值,QQ106074832,金额100
 -----客服人员用---
 if instr(tmp_message,'客户充值')>0 and trim(sender) in
   ('1605073514','106074835') then
      tmp_out_message :='';
      tmp_amount      :=get_cust_amount(tmp_message);
      tmp_c_address   :=get_cust_qq(tmp_message); --qq

    if tmp_c_address is not null
    then
       select count(*) into ct from wl_cust_department where
       nbr=to_char(tmp_c_address);

       if ct=1
       then
       select cust_id,nbr,c_name,contact into
       tmp_cust_id,tmp_nbr,tmp_c_name,tmp_c_contact
       from wl_cust_department where
       nbr=to_char(tmp_c_address);

       insert into wl_cust_dep_balance_log
       (cust_id,amount,use,payee,payee_name,nbr,c_name,contact,create_date)
       values
       (tmp_cust_id,tmp_amount,'客服充值',sender,'王婷',tmp_nbr,
        tmp_c_name,tmp_c_contact,tmp_create_date);
       commit;  ---插入余额日志表

       select count(*) into ct from wl_cust_dep_balance where
       cust_id=tmp_cust_id; --在客户余额表中增加余额
         if ct=0
         then insert into wl_cust_dep_balance(cust_id,advance,
              balance,nbr,c_name,contact)values
              (tmp_cust_id,tmp_amount,tmp_amount,
              tmp_nbr,tmp_c_name,tmp_c_contact);
              commit;
         else update wl_cust_dep_balance set
              advance=advance+tmp_amount,balance=balance+tmp_amount
              where cust_id=tmp_cust_id;
              commit;
         end if;

       tmp_out_message :='成功为qq号：'||tmp_c_address||'充值'||
       tmp_amount||'元。该用户在系统中联系电话是：'||
       tmp_c_contact||',名称是：'||tmp_c_name;

       end if;
    end if;
 end if;


  -----输入参数externalid校验,大于0收到群消息,回复内容为空
  if externalid>0 then
     tmp_out_message:='';
  end if;

out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;

end ;
/

prompt
prompt Creating procedure WL_CUST_QQ_SENDMSG_N
prompt =======================================
prompt
create or replace procedure wl_cust_qq_sendmsg_n
--Mb:从QQ上获(从QQ上取到发布信息,插入至QQ发布的信息表,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

(externalid in number,sender in number,message in varchar2,
 out_externalid out number,out_sender out number,out_message out varchar2)
as
  tmp_message        varchar2(500);
  ct                 number(10);
  tmp_c_name         varchar2(500);
  tmp_c_contact      varchar2(500);
  tmp_c_address      varchar2(500);
  tmp_driver_qq      varchar2(500);
  tmp_driver_carnbr  varchar2(500);
  tmp_driver_nbr     varchar2(500);
  tmp_id             number(30);
  tmp_out_message    varchar2(500);
  tmp_create_date    date;
  tmp_info_start     varchar2(500);
  tmp_info_end       varchar2(500);
  tmp_info_truck     varchar2(500);
  tmp_info_goods     varchar2(500);
  tmp_info_note      varchar2(500);
  tmp_info_nbr1      varchar2(30);
  tmp_info_nbr2      varchar2(30);
  tmp_info_nbr3      varchar2(30);
  tmp_info_out       varchar2(500);
  tmp_lng_start      number(11,8);
  tmp_lat_start      number(11,8);
  tmp_lng_end        number(11,8);
  tmp_lat_end        number(11,8);
  tmp_amount         number(10,2);
  tmp_cust_id        varchar2(64);
  tmp_nbr            varchar2(30);
  tmp_c_password     varchar2(64);
  tmp_str            varchar2(500);
begin
  tmp_out_message:='';
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;
  -----预处理规则--------
  tmp_message:=get_wl_info_rule(message);
  if tmp_message is null or  length(tmp_message)>500 then
       goto ret_end;
  end if;
  -----判断发布货源信息部，新用户插入记录-----
  select count(*) into ct
  from wl_cust_department where nbr=to_char(sender);

  if ct<= 0 then
     insert into wl_cust_department(nbr,note)
     values (to_char(sender),'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if tmp_message='?' or tmp_message='？' then
     tmp_out_message :='货运信息发布格式为：起始城市到目的城市，货源类型，车源类型，联系电话等内容。如：府谷到运城，运煤炭 联系电话13310924210';
     goto ret_end;
  end if;
 /* -----目的地和号码校验----------------
  tmp_info_end    :=get_wl_info_region_end_new(tmp_message);
  tmp_info_nbr1   :=get_wl_info_nbr1(tmp_message);

  if tmp_info_nbr1 is null then
    select contact into tmp_info_nbr1 from wl_cust_department
    where nbr=trim(sender);
  end if;

  if  tmp_info_end is null or tmp_info_nbr1 is null then
      tmp_out_message :='发布的货源信息无效，如需要帮助请回复”?“。--好3G，用天翼';
      goto ret_end;
  end if;
*/
tmp_create_date :=sysdate;
tmp_info_out:=lxf_get_qq_msg_back(to_char(sender),to_char(externalid),tmp_message);
tmp_out_message:=tmp_info_out||' 已发布';

/*
 ------取信息值------------------------
  tmp_info_start  :=get_wl_info_region_from_new(tmp_message);

  tmp_info_nbr2   :=get_wl_info_nbr2(tmp_message);
  tmp_info_nbr3   :=get_wl_info_nbr3(tmp_message);

  tmp_info_truck  :=get_wl_info_truck(tmp_message);
  tmp_info_goods  :=get_wl_info_goods(tmp_message);
  tmp_info_note   :=get_wl_info_note (tmp_message);

  tmp_create_date :=sysdate;
  tmp_id          :=wl_info_data_id.nextval;



-----生成输出信息-----------
 tmp_info_out :=tmp_info_start||'到'||tmp_info_end;

 tmp_info_out :=replace(tmp_info_out,'上海上海','上海');
 tmp_info_out :=replace(tmp_info_out,'北京北京','北京');
 tmp_info_out :=replace(tmp_info_out,'天津天津','天津');
 tmp_info_out :=replace(tmp_info_out,'重庆重庆','重庆');

 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;

 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;

 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;

 ----更新或插入目的表wl_info_data---------------------------
  select count(*) into ct
  from wl_info_data
  where info=tmp_info_out and info_nbr1=tmp_info_nbr1
  and note=to_char(externalid)
  and nbr=to_char(sender)  ;


  if ct >0 then
    update  wl_info_data set create_date=tmp_create_date
    where info=tmp_info_out and info_nbr1=tmp_info_nbr1
    and note=to_char(externalid)
    and nbr=to_char(sender)  ;
    commit;

-- dbms_output.put_line(tmp_info_out);

    tmp_out_message:=tmp_message||' 已更新--好3G，用天翼';
  goto real_end;
  end if;


  tmp_lng_start  :=get_lng(tmp_info_start);
  tmp_lat_start  :=get_lat(tmp_info_start);
  tmp_lng_end    :=get_lng(tmp_info_end)  ;
  tmp_lat_end    :=get_lat(tmp_info_end)  ;

  select c_name into tmp_c_name from wl_cust_department
  where nbr=trim(sender);


--c_name=to_char(sender) info=message note=to_char(externalid)
insert into wl_info_data
   (id,nbr,c_name,create_date,info,
    info_start,info_end,info_nbr1,info_nbr2,
    info_nbr3,info_truck,info_goods,info_note,
    info_from,note,
    lng_start,lat_start,lng_end,lat_end
   )
 values
   (tmp_id,to_char(sender),tmp_c_name,tmp_create_date,tmp_info_out,
    tmp_info_start,tmp_info_end,tmp_info_nbr1,tmp_info_nbr2,
    tmp_info_nbr3,tmp_info_truck,tmp_info_goods,tmp_info_note,
    'qq',to_char(externalid),
    tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end
    );
 commit;
*/
 --tmp_out_message:=tmp_out_message||tmp_message||' 已发布';

 insert into  wl_info_from_in_early (text1,note,info_id)
 values (message,'qq-'||externalid||'-'||sender,tmp_id);
 commit;

 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'qq处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 tmp_create_date:=sysdate;
 insert into  wl_info_from_in_early (text1,note)
 values (message,'qq-'||externalid||'-'||sender);
 commit;

 <<real_end>>

---一、更新信息部名称、电话、地址-----信息部客户表---
 if instr(tmp_message,'我的')>0 then
      tmp_out_message :='';
      tmp_c_name      :=get_cust_name   (tmp_message);
      tmp_c_contact   :=get_cust_contact(tmp_message);
      tmp_c_address   :=get_cust_address(tmp_message);
    --1.名称
    if tmp_c_name is not null
    then
       update wl_cust_department set c_name=tmp_c_name where nbr=trim(sender);
       commit;
       tmp_out_message :='已记录您的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);
    end if;
    --2.电话
    if tmp_c_contact is not null
    then
       update wl_cust_department set contact=tmp_c_contact where nbr=trim(sender);
       commit;
       tmp_out_message :=tmp_out_message||'已记录您的联系电话为：'||tmp_c_contact||'--好3G，用天翼'||chr(10);
    end if;
    --3.地址
    if tmp_c_address is not null
    then
       update wl_cust_department set address=tmp_c_address where nbr=trim(sender);
       tmp_out_message :=tmp_out_message||'已记录您的地址为：'||tmp_c_address||'--好3G，用天翼'||chr(10);
       commit;
    end if;
 end if;


---二、更新司机qq号、车牌、电话-----手机客户表---
 if instr(tmp_message,'司机')>0 
 then
      tmp_out_message :='';
      tmp_driver_qq       :=get_driver_qq   (tmp_message);
      tmp_driver_carnbr   :=get_driver_carnbr(tmp_message);
      tmp_driver_nbr      :=get_driver_nbr(tmp_message);
    --1.qq号
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
       select count(*) into ct from wl_cust_mobile where
       qq=to_char(tmp_driver_qq);

       if ct= 0
       then
          insert into wl_cust_mobile(sim,qq,note)
          values (to_char(tmp_driver_qq),
                  to_char(tmp_driver_qq),
                  'qq:'||tmp_driver_qq||',来自qq:'||trim(sender)
                  );
          commit;  ---插入用户
          tmp_out_message :='已记录司机的qq号为：'||tmp_driver_qq||'--好3G，用天翼'||chr(10);
       end if;

       if ct> 0
       then
          tmp_out_message :='已经有qq号为：'||tmp_driver_qq||'的司机--好3G，用天翼'||chr(10);
       end if;
    end if;
    --2.车牌
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
     update wl_cust_mobile set car_number=tmp_driver_carnbr
     where sim=to_char(tmp_driver_qq) and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的车牌号为：'||tmp_driver_carnbr||'--好3G，用天翼'||chr(10);
    end if;
     --3.电话
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
       and tmp_driver_nbr is not null
    then
     update wl_cust_mobile set nbr=tmp_driver_nbr
     where sim=to_char(tmp_driver_qq) and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的号码号为：'||tmp_driver_nbr||'--好3G，用天翼'||chr(10);
    end if;

 end if;


---三、-----客服人员用---信息部客户表
--客服QQ106074832，密码：123456，名字：王婷11，电话15332592881
 if instr(tmp_message,'客服')>0  and trim(sender) in
    ('18646500','106074835','49631833','7349238',
    '549748914','501943805') 
 then
     tmp_out_message :='';
     tmp_c_name      :=get_cust_name2(tmp_message);
     tmp_c_contact   :=get_cust_contact2(tmp_message);
     tmp_c_address   :=get_cust_qq(tmp_message); --qq
     tmp_c_password  :=get_cust_password(tmp_message);
    --QQ、密码、名称、电话
    if tmp_c_address is not null 
    then select count(*) into ct from wl_cust_department where
         nbr=to_char(tmp_c_address);
         if ct=0
         then insert into wl_cust_department(nbr,note) values 
              (tmp_c_address,'qq:'||tmp_c_address||',来自qq客服创建');
              commit;---插入QQ用户
              tmp_out_message :='已创建信息部的qq号为：'||tmp_c_address||chr(10);
         end if;
         
         tmp_str:='update wl_cust_department set ';
         
         if tmp_c_password is not null --密码
         then tmp_str:=tmp_str||'c_password='||chr(39)||tmp_c_password||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的密码为：'||tmp_c_password||chr(10);
         end if;
         if tmp_c_name is not null     --名称
         then tmp_str:=tmp_str||'c_name='||chr(39)||tmp_c_name||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的名称为：'||tmp_c_name||chr(10);
         end if;
         if tmp_c_contact is not null  --电话
         then tmp_str:=tmp_str||'contact='||chr(39)||tmp_c_contact||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的联系电话为：'||tmp_c_contact||chr(10);
         end if;

         tmp_str:=tmp_str||'note='||chr(39)||'qq:'||tmp_c_address||',来自qq客服修改'||chr(39)||' where nbr=to_char('||tmp_c_address||')';
         execute immediate tmp_str;
         dbms_output.put_line(tmp_str);
         commit;
    end if;
 end if;


---四、客户充值-----客服人员用------
--客户充值,QQ106074832,金额100
 if instr(tmp_message,'客户充值')>0 and trim(sender) in
   ('1605073514','106074835') then
      tmp_out_message :='';
      tmp_amount      :=get_cust_amount(tmp_message);
      tmp_c_address   :=get_cust_qq(tmp_message); --qq

    if tmp_c_address is not null
    then
       select count(*) into ct from wl_cust_department where
       nbr=to_char(tmp_c_address);

       if ct=1
       then
       select cust_id,nbr,c_name,contact into
       tmp_cust_id,tmp_nbr,tmp_c_name,tmp_c_contact
       from wl_cust_department where
       nbr=to_char(tmp_c_address);

       insert into wl_cust_dep_balance_log
       (cust_id,amount,use,payee,payee_name,nbr,c_name,contact,create_date)
       values
       (tmp_cust_id,tmp_amount,'客服充值',sender,'王婷',tmp_nbr,
        tmp_c_name,tmp_c_contact,tmp_create_date);
       commit;  ---插入余额日志表

       select count(*) into ct from wl_cust_dep_balance where
       cust_id=tmp_cust_id; --在客户余额表中增加余额
         if ct=0
         then insert into wl_cust_dep_balance(cust_id,advance,
              balance,nbr,c_name,contact)values
              (tmp_cust_id,tmp_amount,tmp_amount,
              tmp_nbr,tmp_c_name,tmp_c_contact);
              commit;
         else update wl_cust_dep_balance set
              advance=advance+tmp_amount,balance=balance+tmp_amount
              where cust_id=tmp_cust_id;
              commit;
         end if;

       tmp_out_message :='成功为qq号：'||tmp_c_address||'充值'||
       tmp_amount||'元。该用户在系统中联系电话是：'||
       tmp_c_contact||',名称是：'||tmp_c_name;

       end if;
    end if;
 end if;


  -----输入参数externalid校验,大于0收到群消息,回复内容为空
  if externalid>0 then
     tmp_out_message:='';
  end if;

out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;

end ;
/

prompt
prompt Creating procedure WL_CUST_SMS_INSERT
prompt =====================================
prompt
create or replace procedure wl_cust_sms_insert
( in_receiver_nbr   in varchar2
 ,in_contents       in varchar2
 ,in_sender_id      in varchar2
 ,in_priority       in varchar2
 ,ret               out number  )
 --短信1:插入需要发送短信的信息
 --输入参数：收信人号码，短信内容，发信人ID，优先级
 --输出参数：ret 1输入参数为空,2内容过长,3无发信人ID，
 --4预存款不足，0成功插入待发送

as
tmp_ct              number(10);
tmp_msg_id          varchar2(64);
tmp_sender_name     varchar2(64);
tmp_sender_nbr      varchar2(30);
tmp_date            date;
flag                number(10);
inser_position      varchar2(5000);
tmp_in_receiver_nbr varchar2(5000);
tmp_advance         number(10,2);
tmp_receiver_nbr    varchar2(5000);
begin
  -----输入参数空值校验---------
  if in_receiver_nbr is null or in_contents is null or 
     in_sender_id is null or in_priority is null 
  then ret:=1;
       goto ret_end;
  end if;
  -----短信长度校验-------------
  if length(in_contents)>500 
  then ret:=2;
     goto ret_end;
  end if;
  -----记录校验-----------------
  select count(*) into tmp_ct from  wl_cust_department
  where cust_id=in_sender_id;

  if tmp_ct<>1
  then ret:=3;
       goto ret_end;
  end if;
  -----插入第一个接收号码--------------------- 
  tmp_in_receiver_nbr:=replace(in_receiver_nbr,'，',',');

  select c_name,contact
  into tmp_sender_name,tmp_sender_nbr from wl_cust_department
  where cust_id=in_sender_id;
  tmp_msg_id       :=sys_guid();
  tmp_receiver_nbr :=substr(tmp_in_receiver_nbr,1,11);--11
  tmp_receiver_nbr :=get_mobile(tmp_receiver_nbr);
  tmp_date         :=sysdate;
  flag             :=1;
  inser_position   :=1;
  
  if tmp_receiver_nbr is not null --接收号码不为空时
  then 
     --判断预付款
     select max(advance) into tmp_advance from wl_cust_dep_balance
     where cust_id=in_sender_id;
        if tmp_advance is null or tmp_advance<0.08
        then ret:=4;
             goto ret_end;
        end if;
     --插入短信表    
     insert into wl_cust_sms
     (msg_id,receiver_nbr,contents,sender_id,
     sender_name,sender_nbr,create_date,state,fail,priority)
     values
     (tmp_msg_id,tmp_receiver_nbr,in_contents,in_sender_id,
     tmp_sender_name,tmp_sender_nbr,tmp_date,'-1','3',in_priority);
     commit;
     --减少余额表
     update wl_cust_dep_balance set advance=advance-0.08,
     last_create_date=tmp_date where cust_id=in_sender_id;
     commit;
     
  end if;
  -----从第二个号码起循环插入,最多100次---------------------  
  loop
    inser_position   :=instr(tmp_in_receiver_nbr,',',1,flag);
    tmp_receiver_nbr :=substr(tmp_in_receiver_nbr,inser_position+1,11);--11
    tmp_receiver_nbr :=get_mobile(tmp_receiver_nbr);
    flag             :=flag+1;
    
    exit when flag>100 or inser_position=0;
    
    if tmp_receiver_nbr is not null 
    then 
       --判断预付款是否够用
       select max(advance) into tmp_advance from wl_cust_dep_balance
       where cust_id=in_sender_id;
         if tmp_advance is null or tmp_advance<0.08
         then ret:=4;
              goto ret_end;
         end if;
       --插入短信表   
       insert into wl_cust_sms
       (msg_id,receiver_nbr,contents,sender_id,
       sender_name,sender_nbr,create_date,state,fail,priority)
       values
       (tmp_msg_id,tmp_receiver_nbr,in_contents,in_sender_id,
       tmp_sender_name,tmp_sender_nbr,tmp_date,'-1','3',in_priority);
       commit;
       --扣除余额表中的预付款（虚扣）
       update wl_cust_dep_balance set advance=advance-0.08,
       last_create_date=tmp_date where cust_id=in_sender_id;
       commit;
       
    end if;
  
  end loop;
--dbms_output.put_line(flag);
ret:=0;

<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_SMS_RETURN
prompt =====================================
prompt
create or replace procedure wl_cust_sms_return
( in_msg_id        in varchar2
 ,in_receiver_nbr  in varchar2
 ,in_state         in number
 ,ret              out number)
 --短信3:返回短信ID和接收号码和状态,数据库状态更新扣除余额
 --输入参数：短信ID,接收号码,状态(0成功,1失败,真正失败后才送失败，失败后不再重发)
 --输出参数：ret 1输入参数为空或状态不对，2无此短信ID和接收号码的记录，
 --3已经是成功或失败状态，0成功更新
as
tmp_ct      number(10);
tmp_state   number(1);
tmp_date    date;
tmp_cust_id varchar2(64);
tmp_c_name  varchar2(64);
tmp_contact varchar2(500);

begin
  -----输入参数空值校验---------
  if in_msg_id is null or in_state is null or in_state not in(0,1)
  then ret:=1;
       goto ret_end;
  end if;
  -----记录校验----------------
  select count(*) into tmp_ct from wl_cust_sms
  where msg_id=in_msg_id and receiver_nbr=in_receiver_nbr;
  
  if tmp_ct=0
  then ret:=2;
       goto ret_end;
  end if; 
  -----现有状态校验----------------
  select state,sender_id,sender_name,sender_nbr into
  tmp_state,tmp_cust_id,tmp_c_name,tmp_contact
  from wl_cust_sms
  where msg_id=in_msg_id and receiver_nbr=in_receiver_nbr;       
        
  if tmp_state=0 or tmp_state=1
  then ret:=3;
       goto ret_end;
  end if;
 -----更新短信表---------------------
  tmp_date:=sysdate;
  update wl_cust_sms set send_date=tmp_date,state=in_state
  where msg_id=in_msg_id and receiver_nbr=in_receiver_nbr;
  commit;  
  -----成功时减少余额（实扣余额）--------
  if in_state=0
  then update wl_cust_dep_balance set balance=balance-0.08,
       last_send_date=tmp_date where cust_id=tmp_cust_id;
       commit;

       insert into wl_cust_dep_balance_log
       (cust_id,amount,use,payee,payee_name,
        c_name,contact,create_date,note)
       values
       (tmp_cust_id,-0.08,'发送短信','system','system',
        tmp_c_name,tmp_contact,tmp_date,in_msg_id||','||in_receiver_nbr);
       commit;  ---插入余额日志表      
  end if;
 
  -----失败时增加预付款（恢复预付款）---------
  if in_state=1
  then update wl_cust_dep_balance set advance=advance+0.08,
       last_send_date=tmp_date where cust_id=tmp_cust_id;
       commit;
  end if;
  ------------------------------------
  ret:=0;

<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_SMS_SENDINFO
prompt =======================================
prompt
create or replace procedure wl_cust_sms_sendinfo

--在短信上展示货运信息
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

    (externalid       in  number
     ,sender          in  number
     ,message         in  varchar2
     ,out_externalid  out number
     ,out_sender      out number
     ,out_message     out varchar2
    )

is
   type cursor_type is ref cursor;
   c1 cursor_type;

  ct number(10);
  tmp_out_message varchar2(8000);
  tmp_row_message varchar2(2000);
  in_sim          varchar2(30);
  tmp_line        varchar2(500);
  tmp_str         varchar2(2000);
  tmp_infoid_str  varchar2(1000);
  tmp_in_ct       varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市

  begin
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;

  -----判断用户，老用户更新时间，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where qq=to_char(sender);

  if ct> 0 then
     update wl_cust_mobile set last_position_date=sysdate
     where  sim=to_char(sender) or qq=to_char(sender);
     commit;  ---更新用户
  else
     insert into wl_cust_mobile(sim,qq,last_position_date,note)
     values (to_char(sender),to_char(sender),sysdate,'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

 -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid>0 then
     goto ret_end;
  end if;


  -----输入信息校验---------
  if message='?' or message='？' then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;
  --按指定线路返回
  tmp_line:=get_wl_info_region_from_line (message)||','||
            get_wl_info_region_end_line  (message);


  if trim(tmp_line)=',' then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;

      ----------返回记录集，sender=QQ号=SIM
   tmp_in_ct :='2';
   in_sim   :=to_char(sender);
      -------tmp_line输入字符串拆分----------
      --格式:陕西,榆林,陕西,西安 (陕西,全部,全部,全部)
   from_province:=trim(substr(tmp_line,1,instr(tmp_line,',',1,1)-1));
   from_city:=trim(substr(tmp_line,instr(tmp_line,',',1,1)+1,instr(tmp_line,',',1,2)-instr(tmp_line,',',1,1)-1));
   to_province:=trim(substr(tmp_line,instr(tmp_line,',',1,2)+1,instr(tmp_line,',',1,3)-instr(tmp_line,',',1,2)-1));
   to_city:=trim(substr(tmp_line,instr(tmp_line,',',1,3)+1,instr(tmp_line,',',1,3)-instr(tmp_line,',',1,2)-1));

   tmp_infoid_str:=get_line_info_id_top
     (in_sim,tmp_in_ct,from_province,from_city,to_province,to_city);

    --dbms_output.put_line(tmp_infoid_str);

      --if from_province is null or from_province='全部' then
         --ret:=2;
         --goto ret_end;
      --end if ;

   if tmp_infoid_str is null then
      tmp_out_message :='暂时没有您要查询的信息';
      goto ret_end;
   end if;


  tmp_str:='';
  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)
  ||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_infoid_str||')
  order by create_date desc';


/*  tmp_str:=tmp_str||'select info||chr(44)||info_nbr1||chr(44)
  ||c_name||get_how_long(create_date)
  from wl_info_data where id in ('||tmp_infoid_str||')
  order by create_date desc';*/
  
open c1 for tmp_str;

 loop
    fetch c1 into tmp_row_message;
    exit when c1%notfound;
    
    tmp_out_message:=tmp_out_message||chr(13)||tmp_row_message||';';
     --判读是否提取到值，没取到值就退出
     --取到值job_region_from%notfound 是false
     --取不到值job_region_from%notfound 是true

 end loop;
close c1;

--dbms_output.put_line(tmp_out_message);


/*c_row job_region_from%rowtype;
 begin
  
  job_region_from;
  loop
    if regexp_substr(tmp_info,c_row.state) is not null then
       flag:=flag+1;
       tmp_info :=regexp_replace
       (tmp_info,c_row.state,'~'||c_row.serial||'!');
        if flag>=3 then
        goto ret_end_1;
        end if ;

    end if;

   --提取一行数据到c_row
  fetch job_region_from into c_row;
   --判读是否提取到值，没取到值就退出
   --取到值job_region_from%notfound 是false
   --取不到值job_region_from%notfound 是true
   exit when job_region_from%notfound;
  end loop;
  close job_region_from;
 end;
*/


  --tmp_out_message :=c_row.info||c_row.info_nbr1;


   ------调整信息权值-----------------------------
  tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_infoid_str||' )';
  execute immediate tmp_str;
  commit;

   ------记录访问信息------无记录的插入，有记录的更新时间-----------------------
   tmp_str:='';
   tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
   tmp_str:=tmp_str||'select id, '||chr(39)||in_sim||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_infoid_str||' )';
   tmp_str:=tmp_str||' minus ';
   tmp_str:=tmp_str||'select info_id, '||chr(39)||in_sim||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_infoid_str||')
                      and   sim='||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_infoid_str||' ) and sim= '||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;
   -------结束点---------

 <<ret_end>>
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;



end;
/

prompt
prompt Creating procedure WL_CUST_UPDATE_POSITION
prompt ==========================================
prompt
create or replace procedure wl_cust_update_position
--更新用户地理位置信息
    (in_sim       in  varchar2
     ,in_cell_id  in  varchar2
     ,in_lng      in  number
     ,in_lat      in  number
     ,ret         out number
    )

is

  ct        number(5);   ----目的城市

  begin
    --------------------入口参数-----------------------------------
  if trim(in_sim) is null or in_lng is null or in_lng<=0
                    or in_lat is null or in_lat<=0
                    or in_cell_id is null 
     then
       ret:=1;
       goto ret_end;
  end if;
  -----判断用户，老用户更新位置，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where sim=in_sim;
  if ct> 0 then
     update wl_cust_mobile set last_lng=in_lng,last_lat=in_lat,last_position_date=sysdate
     where sim=in_sim;
     commit;
     ret:=0;---更新用户
  else
     insert into wl_cust_mobile(sim,last_lng,last_lat,last_position_date,create_date,eff_date,last_cell_id)
     values (in_sim,in_lng,in_lat,sysdate,sysdate,sysdate+30,in_cell_id);
     commit;
     ret:=9;---插入用户
  end if;

 <<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_CUST_WEB_SENDMSG
prompt ======================================
prompt
create or replace procedure wl_cust_Web_sendmsg
--Mb:从web上获(从web上取到发布信息,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,信息部账号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功
(
 sender in varchar2,
 message in varchar2,
 out_message out varchar2)
as
  tmp_message        varchar2(4000);
  ct                 number(10);
  tmp_out_message    varchar2(4000);
  tmp_info_out       varchar2(4000);
begin
  tmp_out_message:='';
  -----输入参数externalid和sender空值校验---------
  if  sender is null or message is null then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>4000 then
     goto ret_end;
  end if;
  -----预处理规则--------
  tmp_message:=get_wl_info_rule(message);
  if tmp_message is null or  length(tmp_message)>4000 then
       goto ret_end;
  end if;
  -----判断发布货源信息部，新用户插入记录-----
  select count(*) into ct
  from wl_cust_department where nbr=to_char(sender);

  if ct<= 0 then
     insert into wl_cust_department(nbr,note)
     values (to_char(sender),'Web_'||to_char(sender));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if tmp_message='?' or tmp_message='？' then
     tmp_out_message :='货运信息发布格式为：起始城市到目的城市，货源类型,车源类型，联系电话等内容。如：府谷到运城，运煤炭 联系电话13310924210';
     goto ret_end;
  end if;
tmp_info_out:=substr(lxf_get_Web_msg_back(to_char(sender),tmp_message),1,3000);
tmp_out_message:=tmp_info_out;
 <<ret_end>>
  out_message   :=tmp_out_message;

end ;
/

prompt
prompt Creating procedure WL_CUST_YLBQQ_SENDMSG
prompt ========================================
prompt
create or replace procedure wl_cust_YLBQQ_sendmsg
--Mb:从QQ上获(从QQ上取到发布信息,插入至QQ发布的信息表,再经过处理最终插入wl_info_data)
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功
(externalid in number,sender in number,message in varchar2,
 out_externalid out number,out_sender out number,out_message out varchar2)
as
  tmp_message        varchar2(4000);
  ct                 number(10);
  tmp_c_name         varchar2(500);
  tmp_c_contact      varchar2(500);
  tmp_c_address      varchar2(500);
  tmp_driver_qq      varchar2(500);
  tmp_driver_carnbr  varchar2(500);
  tmp_driver_nbr     varchar2(500);
  tmp_id             number(30);
  tmp_out_message    varchar2(4000);
  tmp_create_date    date;
  tmp_info_out       varchar2(4000);
  tmp_amount         number(10,2);
  tmp_cust_id        varchar2(64);
  tmp_nbr            varchar2(30);
  tmp_c_password     varchar2(64);
  tmp_c_city         varchar2(200);
  tmp_str            varchar2(500);
begin
  tmp_out_message:='';
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>4000 then
     goto ret_end;
  end if;
  -----预处理规则--------
  tmp_message:=get_wl_info_rule(message);
  if tmp_message is null or  length(tmp_message)>4000 then
       goto ret_end;
  end if;
  -----判断发布货源信息部，新用户插入记录-----
  select count(*) into ct
  from wl_cust_department where qq=to_char(sender);

  if ct<= 0 then
     insert into wl_cust_department(qq,note)
     values (to_char(sender),'qq:群号'||to_char(externalid));
     commit;  ---插入用户
  end if;

  -----输入信息校验---------
  if tmp_message='?' or tmp_message='？' then
     tmp_out_message :='货运信息发布格式为：起始城市到目的城市，货源类型，车源类型，联系电话等内容。如：府谷到运城，运煤炭 联系电话13310924210';
     goto ret_end;
  end if;
  -----统计信息--------------
  if trim(tmp_message)='统计'  then
     wl_get_active_user(tmp_out_message);
     goto ret_end;
  end if;
tmp_create_date :=sysdate;
tmp_info_out:=substr(lxf_get_qq_msg_back(to_char(sender),to_char(externalid),tmp_message),1,3000);
tmp_out_message:=tmp_info_out;


if regexp_instr(trim(lxf_replace_place_name(tmp_message)),'^#P\d+#$')>0 then
  wl_city_list(trim(tmp_message), tmp_info_out);
    tmp_out_message:=tmp_info_out;
end if ;
 insert into  wl_info_from_in_early (text1,note,info_id)
 values (message,'qq-'||externalid||'-'||sender,tmp_id);
 commit;

 insert into wl_flag(name,create_date,note) values
 (tmp_id,tmp_create_date,'qq处理数据成功');
 commit;

 goto real_end;

 <<ret_end>>
 -----插入原始表--------------------------
 tmp_create_date:=sysdate;
 insert into  wl_info_from_in_early (text1,note)
 values (message,'qq-'||externalid||'-'||sender);
 commit;

 <<real_end>>

---一、更新信息部名称、电话、地址-----信息部客户表---
 if instr(tmp_message,'我的')>0 then
      tmp_out_message :='';
      tmp_c_name      :=get_cust_name   (tmp_message);
      tmp_c_contact   :=get_cust_contact(tmp_message);
      tmp_c_address   :=get_cust_address(tmp_message);
    --1.名称
    if tmp_c_name is not null
    then
       update wl_cust_department set c_name=tmp_c_name where nbr=trim(sender);
       commit;
       tmp_out_message :='已记录您的名称为：'||tmp_c_name||'--好3G，用天翼'||chr(10);
    end if;
    --2.电话
    if tmp_c_contact is not null
    then
       update wl_cust_department set contact=tmp_c_contact where nbr=trim(sender);
       commit;
       tmp_out_message :=tmp_out_message||'已记录您的联系电话为：'||tmp_c_contact||'--好3G，用天翼'||chr(10);
    end if;
    --3.地址
    if tmp_c_address is not null
    then
       update wl_cust_department set address=tmp_c_address where nbr=trim(sender);
       tmp_out_message :=tmp_out_message||'已记录您的地址为：'||tmp_c_address||'--好3G，用天翼'||chr(10);
       commit;
    end if;
 end if;


---二、更新司机qq号、车牌、电话-----手机客户表---
 if instr(tmp_message,'司机')>0
 then
      tmp_out_message :='';
      tmp_driver_qq       :=get_driver_qq   (tmp_message);
      tmp_driver_carnbr   :=get_driver_carnbr(tmp_message);
      tmp_driver_nbr      :=get_cust_contact2(tmp_message);
    --1.qq号
    if tmp_driver_qq is not null and tmp_driver_nbr is not null
    then
       select count(*) into ct from wl_cust_mobile where
       sim=to_char(tmp_driver_qq);

       if ct= 0
       then
          insert into wl_cust_mobile(sim,qq,note,nbr)
          values (to_char(tmp_driver_qq),
                  to_char(tmp_driver_qq),
                  'qq:'||tmp_driver_qq||',来自qq:'||trim(sender),
                  tmp_driver_nbr
                  );
          commit;  ---插入用户
          tmp_out_message :='已记录司机的qq号为：'||tmp_driver_qq||'--好3G，用天翼'||chr(10);
       end if;

       if ct> 0
       then
          tmp_out_message :='已经有qq号为：'||tmp_driver_qq||'的司机--好3G，用天翼'||chr(10);
       end if;
    end if;
    --2.车牌
    if tmp_driver_qq is not null and tmp_driver_carnbr is not null
    then
     update wl_cust_mobile set car_number=tmp_driver_carnbr
     where sim=to_char(tmp_driver_qq) ;--and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的车牌号为：'||tmp_driver_carnbr||'--好3G，用天翼'||chr(10);
    end if;
     --3.电话
    if tmp_driver_qq is not null
       and tmp_driver_nbr is not null
    then
     update wl_cust_mobile set nbr=tmp_driver_nbr
     where sim=to_char(tmp_driver_qq);-- and qq=to_char(tmp_driver_qq);
     commit;
     tmp_out_message :=tmp_out_message||'已记录司机的号码号为：'||tmp_driver_nbr||'--好3G，用天翼'||chr(10);
    end if;

 end if;


---三、-----客服人员用---信息部客户表
--客服QQ106074832，密码：123456，名字：王婷11，电话15332592881
 if instr(tmp_message,'客服')>0  and trim(sender) in
    ('18646500','106074835','49631833','7349238',
    '549748914','501943805','447658593','860299303','342143923')
 then
     tmp_out_message :='';
     tmp_c_name      :=get_cust_name2(tmp_message);
     tmp_c_contact   :=get_cust_contact2(tmp_message);
     tmp_c_address   :=get_cust_qq(tmp_message); --qq
     tmp_c_password  :=get_cust_password(tmp_message);
     tmp_c_city      :=get_cust_city(tmp_message);
    --QQ、密码、名称、电话
    if tmp_c_address is not null
    then select count(*) into ct from wl_cust_department where
         nbr=to_char(tmp_c_address);
         if ct=0
         then insert into wl_cust_department(nbr) values(tmp_c_address);
              commit;---插入新建QQ用户
              tmp_out_message :='已创建信息部的qq号为：'||tmp_c_address||chr(10);
         end if;
         ---给司机用户停机
        if tmp_c_address is not null  and  instr(tmp_message,'停机')>0 then
             execute immediate 'update wl_cust_mobile set eff_date=sysdate where sim=to_char('||tmp_c_address||')';
              commit;
             tmp_out_message :=tmp_out_message||tmp_c_address||'用户已经停机';

         end if;
          ---给司机用户开机
         if tmp_c_address is not null  and  instr(tmp_message,'开机')>0 then
              execute immediate 'update wl_cust_mobile set eff_date=sysdate
              +365 where sim=to_char('||tmp_c_address||')';
              commit;
              tmp_out_message :=tmp_out_message||tmp_c_address||'用户已经开机';
         end if;


         tmp_str:='update wl_cust_department set ';

         if tmp_c_password is not null --密码
         then tmp_str:=tmp_str||'c_password='||chr(39)||tmp_c_password||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的密码为：'||tmp_c_password||chr(10);
         end if;
         if tmp_c_name is not null     --名称
         then tmp_str:=tmp_str||'c_name='||chr(39)||tmp_c_name||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的名称为：'||tmp_c_name||chr(10);
         end if;
         if tmp_c_contact is not null  --电话
         then tmp_str:=tmp_str||'contact='||chr(39)||tmp_c_contact||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的联系电话为：'||tmp_c_contact||chr(10);
         end if;
         if tmp_c_city is not null  --城市
         then tmp_str:=tmp_str||'city='||chr(39)||tmp_c_city||chr(39)||',';
              tmp_out_message :=tmp_out_message||'已修改信息部的默认城市为：'||tmp_c_city||chr(10);
         end if;
         tmp_str:=tmp_str||'note='||chr(39)||'qq:'||tmp_c_address||',来自qq客服修改'
         ||chr(39)||' where nbr=to_char('||tmp_c_address||')';
         execute immediate tmp_str;
         commit;
    end if;
 end if;


---四、客户充值-----客服人员用------
--客户充值,QQ106074832,金额100
 if instr(tmp_message,'客户充值')>0 and trim(sender) in
   ('1605073514','106074835','18646500','7349238','49631833',
   '1605073514','577911432') then
      tmp_out_message :='';
      tmp_amount      :=get_cust_amount(tmp_message);
      tmp_c_address   :=get_cust_qq(tmp_message); --qq

    if tmp_c_address is not null
    then
       select count(*) into ct from wl_cust_department where
       nbr=to_char(tmp_c_address);

       if ct=1
       then
       select cust_id,nbr,c_name,contact into
       tmp_cust_id,tmp_nbr,tmp_c_name,tmp_c_contact
       from wl_cust_department where
       nbr=to_char(tmp_c_address);

       insert into wl_cust_dep_balance_log
       (cust_id,amount,use,payee,payee_name,nbr,c_name,contact,create_date)
       values
       (tmp_cust_id,tmp_amount,'客服充值',sender,'王婷',tmp_nbr,
        tmp_c_name,tmp_c_contact,tmp_create_date);
       commit;  ---插入余额日志表

       select count(*) into ct from wl_cust_dep_balance where
       cust_id=tmp_cust_id; --在客户余额表中增加余额
         if ct=0
         then insert into wl_cust_dep_balance(cust_id,advance,
              balance,nbr,c_name,contact)values
              (tmp_cust_id,tmp_amount,tmp_amount,
              tmp_nbr,tmp_c_name,tmp_c_contact);
              commit;
         else update wl_cust_dep_balance set
              advance=advance+tmp_amount,balance=balance+tmp_amount
              where cust_id=tmp_cust_id;
              commit;
         end if;

       tmp_out_message :='成功为qq号：'||tmp_c_address||'充值'||
       tmp_amount||'元。该用户在系统中联系电话是：'||
       tmp_c_contact||',名称是：'||tmp_c_name;

       end if;
    end if;
 end if;
 ---五、开通业务-----管理员------
--开通[注销|取消] 电话 15129030647 月 12  销售编号 1010
 if regexp_instr(tmp_message,'开通|注销|取消|试用|试用全部')>0 and trim(sender) in('577911432','791465191') then
    WL_cust_YLBQQ_OPENBIZ(tmp_message,tmp_out_message);
 end if;
 -----输入参数externalid校验,大于0收到群消息,回复内容为空
  if externalid>0 then
     tmp_out_message:='';
  end if;
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;
end ;
/

prompt
prompt Creating procedure WL_CUST_ZHBQQ_HASPHONE
prompt =========================================
prompt
create or replace procedure WL_CUST_ZHBQQ_HasPhone(
 /*判断是否有手机号码，是否绑定信息*/
 In_QQ in varchar2,--消息来源QQ
 In_Mess in varchar2,--消息
 Out_Mess out varchar2,--输出消息
 out_sign out number--输出标示 1 信息绑定 0 普通信息
)
as
Temp_Out_Mess varchar2(1000);--临时储存输出信息
Temp_count number(6);---临时绑定数量
temp_Str varchar2(100); --临时储存字符串
temp_phone varchar2(20);--临时储存电话号码
begin
  ------------------------是否有绑定手机号码业务--------------------------
   out_sign:=0;
  if regexp_instr(In_Mess,'^\s*1(3|4|5|6|7|8)\d{9}\s*$')>0 then
     out_sign:=1;
    ----------------------------格式正确进行qq电话绑定-----------------------
    temp_phone:=regexp_substr(In_Mess,'1(3|4|5|6|7|8)\d{9}');
    ---------------------------判断该手机号码是否存在---------------------------
    select count(*) into Temp_count from wl_cust_mobile  where nbr=temp_phone;
    if Temp_count>0 then
         ---------------------------获取该号码对应的QQ号------------------------
         select max(qq) into temp_Str from wl_cust_mobile where nbr=temp_phone;
         if temp_Str is null or trim(temp_Str)='' then--------------该号码对应的QQ为空
            update wl_cust_mobile set qq=In_QQ where  nbr=temp_phone;
            commit;
            delete wl_cust_mobile where qq=In_QQ and nbr<>temp_phone;
            commit;
            select isorderssxy into Temp_count from wl_cust_mobile where qq=In_QQ and nbr=temp_phone;
            if Temp_count=0 then
               Temp_Out_Mess:='您是试用用户,信息条数有限。'||chr(10)||'如需订购找货宝业务请用以下方式开通,'||chr(10)||'1.手机发送5137到10669588订购该业务,根据提示操作相应业务。'||chr(10)||
      '2.登陆http://zhaohuobao.taobao.com/购买该服务,购买后我们工作人员将与您联系【找货宝】';
            else
                Temp_Out_Mess:='您已经订购找货宝业务：'||chr(10)||'查询货运信息请发送:某地到某地，如：榆林到上海';
            end if;
            goto ret_end;
         end if;
         if temp_Str=In_QQ then---该号码对应的QQ为本QQ
           select isorderssxy into Temp_count from wl_cust_mobile where qq=In_QQ and nbr=temp_phone;
            if Temp_count=0 then
               Temp_Out_Mess:='您是试用用户,信息条数有限。'||chr(10)||'如需订购找货宝业务请用以下方式开通,'||chr(10)||'1.手机发送5137到10669588订购该业务,根据提示操作相应业务。'||chr(10)||
      '2.登陆http://zhaohuobao.taobao.com/购买该服务,购买后我们工作人员将与您联系【找货宝】';
            else
                Temp_Out_Mess:='您已经订购找货宝业务：'||chr(10)||'查询货运信息请发送:某地到某地，如：榆林到上海';
            end if;
            goto ret_end;
         end if;
          Temp_Out_Mess:='手机号码:'||temp_phone||'已被其他QQ绑定：'||chr(10)||'如果这是您的手机号；请用您的手机发送:'||chr(10)||'bdqq 您的QQ号 到106695887036'||chr(10)||' 进行绑定绑定!';
          goto ret_end;
    end if;
    ----------------------------------绑定QQ对应的手机号不存在--------------------------------------
    if  Temp_count=0 then
        update wl_cust_mobile set nbr=temp_phone where qq=In_QQ;
        commit;
        select isorderssxy into Temp_count from wl_cust_mobile where qq=In_QQ and nbr=temp_phone;
            if Temp_count=0 then
               Temp_Out_Mess:='您是试用用户,信息条数有限。'||chr(10)||'如需订购找货宝业务请用以下方式开通,'||chr(10)||'1.手机发送5137到10669588订购该业务,根据提示操作相应业务。'||chr(10)||
      '2.登陆http://zhaohuobao.taobao.com/购买该服务,购买后我们工作人员将与您联系【找货宝】';
            else
                Temp_Out_Mess:='您已经订购找货宝业务：'||chr(10)||'查询货运信息请发送:某地到某地，如：榆林到上海';
            end if;
        goto ret_end;
    end if;
  end if;
  select max(nbr) into temp_Str from wl_cust_mobile where qq=In_QQ;
  
    ----------------------------qq号码对应的手机号码为空----------------------
    if temp_Str is null or trim(temp_Str)='' then
        out_sign:=1;
       Temp_Out_Mess:='您未绑定手机号:'||chr(10)||'为了更方便您后期的使用，请输入手机号码';
       goto ret_end;
    end if;
  <<ret_end>>
  Out_Mess:=Temp_Out_Mess;
end;
/

prompt
prompt Creating procedure WL_CUST_ZHBQQ_SENDINFO
prompt =========================================
prompt
create or replace procedure wl_cust_ZHBQQ_sendinfo
--在QQ上展示货运信息
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功

    (externalid       in  number
     ,sender          in  number
     ,message         in  varchar2
     ,out_externalid  out number
     ,out_sender      out number
     ,out_message     out varchar2
    )

is
  ct number(10);
  tmp_out_message varchar2(8000);
  in_sim          varchar2(30);
  tmp_infoid_str  varchar2(1000);
  tmp_in_ct       varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  info_number    number(5);
  tmp_str1        varchar2(500);
  temp_nbr varchar2(20);
  startStr varchar2(100):='';
  endStr varchar2(100):='';
  temp_sign number(6):=0;
  begin
  -----输入参数externalid和sender空值校验---------
  if externalid is null or sender is null or message is null then
     goto ret_end;
  end if;
  -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid<0 then
     goto ret_end;
  end if;
  -----输入信息长度校验---------
  if length(message)>500 then
     goto ret_end;
  end if;

  -----判断用户，老用户更新时间，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where qq=to_char(sender);

  if ct> 0 then
     update wl_cust_mobile set last_position_date=sysdate
     where  sim=to_char(sender) or qq=to_char(sender);
     commit;  ---更新用户
  else
     insert into wl_cust_mobile(sim,qq,last_position_date,note,create_Date,eff_date)
     values (to_char(sender),to_char(sender),sysdate,'qq:群号'||to_char(externalid),sysdate,sysdate+30);
     commit;  ---插入用户
  end if;
 
 -----输入参数externalid校验,0收到好友消息,大于0收到群消息
  if externalid>0 then
     goto ret_end;
  end if;
   ----信息列表-----
  if regexp_instr(trim(lxf_replace_place_name(trim(message))),'^#P\d+#$')>0 then
     wl_city_list(trim(message), tmp_out_message);
     goto ret_end;
  end if ;
  --------------验证是否有手机号码------------
 WL_CUST_ZHBQQ_HasPhone(trim(sender), message, tmp_out_message ,temp_sign);
 if temp_sign=1 then
    goto ret_end;
 end if;
 if  wl_cust_is_eff_moblie(sender)=0 then
      tmp_out_message :='您好，您的试用期已到，请发送qq号码到106XXXXX继续使用，包月费10元/月；';
     goto ret_end;
 end if;
  -----输入信息校验---------
  if message='?' or message='？' then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;
  --按指定线路返回
  WL_Cust_ZHBQQ_GetAddress(message,startStr,endStr);
  if startStr is null or endStr is null then
     tmp_out_message :='您好，我是中国电信”找货宝“，可以帮您查找货源信息，发送出发地到目的地的城市，如发送：“榆林到河北”';
     goto ret_end;
  end if;

   ----------返回记录集，sender=QQ号=SIM
   tmp_in_ct :='5';
   in_sim   :=to_char(sender);
   ----------------该用户是否订购掌上通业务------------------
   select nbr into temp_nbr from Wl_Cust_Mobile where qq=to_char(sender);
   if temp_nbr is null or trim(temp_nbr)='' then
        ------------------今天是否已经查询购的----------------------------
        select to_char(Last_Serch_Data,'yyyy:mm:dd') into temp_nbr  from Wl_Cust_Mobile where qq=to_char(sender);
        if temp_nbr=to_char(sysdate,'yyyy:mm:dd') then
            tmp_in_ct:=1;
        end if;
   end if;
    select Isorderssxy into ct from Wl_Cust_Mobile where qq=to_char(sender);
    dbms_output.put_line(to_char(ct)||'ct');
    if ct=0 then
       tmp_in_ct:=1;
     elsif ct=1 then
        tmp_in_ct:=5;
    end if;
   tmp_infoid_str:=get_line_info_TXH_top(in_sim,tmp_in_ct,startStr,endStr);
-------更新最后查询的条件--------------------------
   if from_city is not null  and  to_city is not null  then
      update wl_cust_mobile set last_position_date=sysdate ,last_search=from_province||from_city||'到'||to_province||to_city,
      last_serch_data=Sysdate 
      where  sim=to_char(sender) or qq=to_char(sender);
      commit;  ---更新用户
   end if ;

   if tmp_infoid_str is null then
      tmp_out_message :='暂时没有您要查询的信息';
      --tmp_out_message :='春节期间放假十天，信息数较少，请谅解!';
      goto ret_end;
   end if;
   tmp_out_message:=tmp_infoid_str;
-----查询记录条数-----------
   select count(*) into info_number from WL_info_data where info_start like startStr||'%' and info_end like endStr||'%';
   tmp_out_message:=startStr||'到'||endStr||'共有'||to_char(info_number)||'条信息:'||tmp_out_message;
   if tmp_in_ct=1 then
      tmp_out_message:=tmp_out_message||'  您尚未订购找货宝业务,请手机发送5137到10669588订购该业务,';
      tmp_out_message:=tmp_out_message||'根据提示操作相应业务。';
   else
      tmp_out_message:=tmp_out_message||chr(13)||chr(10)||'*继续发送 '||tmp_str1||' 查询剩余信息。';
   end if;
  insert into wl_cust_moblie_qq_log ( externalid,sender,message) values (externalid,sender,message );
  commit;
   -------结束点---------
 <<ret_end>>
out_externalid:=externalid;
out_sender    :=sender;
out_message   :=tmp_out_message;
end;
/

prompt
prompt Creating procedure WL_CUS_CONTACT_ADD
prompt =====================================
prompt
create or replace procedure WL_cus_contact_add(
  depId in varchar,--部门ID
  phoneNum in varchar2,--手机号码
  cname in varchar2,--司机姓名
  car_num in varchar2,--车牌号码
  car_type in varchar2,--货车类型
  car_line in varchar2)--常跑路线
  as
  /*
     该存储过程为WL_CUS_Contact表添加数据
  */
  countcons number;
  begin
    select  count(*) into countcons 
      from wl_cust_dep_contact 
      where wl_cust_dep_contact.c_phonenum=phoneNum and wl_cust_departid=depId;
    if countcons=0 then
        insert into wl_cust_dep_contact (wl_cust_departID,c_phonenum,c_name,c_car_num,c_car_type,c_car_line)
         values(depId,phoneNum,cname,car_num,car_type,car_line);
    end if;
  end;
/

prompt
prompt Creating procedure WL_CUS_CONTACT_DELETE
prompt ========================================
prompt
create or replace procedure WL_cus_contact_delete(conID in varchar2)as--主键
/*
  根据conId删除wl_cus_contact的数据
*/
  sqlstr varchar(200);--动态SQl
  begin
    sqlstr:='delete from WL_cust_dep_contact where con_Id in ('||conID||')';
    execute immediate sqlstr;
  end;
/

prompt
prompt Creating procedure WL_CUS_CONTACT_GET
prompt =====================================
prompt
create or replace procedure WL_cus_contact_get(
  depId in varchar,--部门ID
  Strwords in varchar2,--输入的关键字
  pageIndex in number,--第几页
  pageSize in number,--每页显示几条数据
  allCount out number,--总记录数
  ResuleInfo out wl_cust_dep_contact_getInf.contactInfoType)--还回结果集
  as
  /*
    根据部门Id和关键字查询该部门的数据
  */
  begin
    select count(*) into allCount 
    from WL_cust_dep_contact 
    where WL_cust_dep_contact.Wl_Cust_Departid=depId;
    open ResuleInfo for  
         select WL_cust_dep_contact.*,rownum  
          from WL_cust_dep_contact 
         where wl_cust_departId=depId 
          and rownum<=pageIndex*pageSize 
          and rownum>(pageIndex-1)*pageSize 
          and (c_phoneNum like '%'||Strwords||'%' 
            or c_name like '%'||Strwords||'%'
            or c_car_type like '%'||Strwords||'%' 
            or c_car_line like '%'||Strwords||'%');
  end;
/

prompt
prompt Creating procedure WL_CUS_CONTACT_UPDATE
prompt ========================================
prompt
create or replace procedure WL_cus_contact_update(
  conID in varchar,--司机的主键
  phoneNum in varchar2,--电话号码
  cname in varchar2,--司机名称
  car_num in varchar2,--车牌号码
  car_type in varchar2,--货车类型
  car_line in varchar2)--常跑路线
  as
  /*
   依据司机conID号 更新WL_cus_contact的数据
  */
  begin
    update WL_cust_dep_contact set c_phoneNum=phoneNum,c_name=cname,c_car_num=car_num,
    c_car_type=car_type,c_car_line=car_line where con_Id=conID;
  end;
/

prompt
prompt Creating procedure WL_EVENT_CUST
prompt ================================
prompt
create or replace procedure WL_Event_Cust(
   in_imsi in varchar2, --imsi
   in_even in varchar2,--事件
   in_eved in varchar2,--事件描述
   ret out number
   --返回值 0 true；1、imsi为空或不存在；2、无效事件；3、事件描述为空;
   )
   as
   rsign number(5):=0;
   nbrsign number(5):=0;
   imsisign number(5):=0;
   simsign number(5):=0;
   isordersign number(5):=0;
   tnbr varchar2(32):='';
   tsim varchar2(32):='';
   tdev varchar2(32):='';
   
   begin
     if in_imsi is null or trim(in_imsi)='' then
         rsign:=-1;
         goto ref_end;
     end if;

     if in_even is null or trim(in_even)='' then
         rsign:=-2;
         goto ref_end;
     end if;

     if in_eved is null or trim(in_eved)='' then
        rsign:=-3;
        goto ref_end;
     end if;

    --写入日志信息记录
    insert into WL_Event_Cust_Log(Imsi,Event,Event_desc) values(in_imsi,in_even,in_eved);
    
    if in_even = 'Register' then
      --判断imsi 是否存在
      tnbr := trim(substr(in_eved,1,instr(in_eved,',',1,1)-1));
      tsim := trim(substr(in_eved,instr(in_eved,',',1,1)+1,instr(in_eved,',',1,2)-instr(in_eved,',',1,1)-1));
      tdev := trim(substr(in_eved,instr(in_eved,',',1,2)+1));
      Select count(Nbr) into nbrsign from Wl_Cust_Mobile where Nbr=tnbr;
      Select count(Imsi) into imsisign from Wl_Cust_Mobile where Imsi=in_imsi;
      if nbrsign=0 then
        if imsisign=0 then
          insert into Wl_Cust_Mobile (sim, nbr, esn, imsi,create_date,Isreg) values(tsim,tnbr,tdev,in_imsi,sysdate,1);
          commit;
        else
          update Wl_Cust_Mobile set Nbr=tnbr, Esn=tdev, Sim=tsim, Isreg =1 where Imsi=in_imsi;
          commit;
        end if;
      else
        update Wl_Cust_Mobile set Imsi=in_imsi, Esn=tdev, Sim=tsim, Isreg =1 where Nbr=tnbr;
        commit;
      end if;
      rsign:=0;
      goto ref_end;
      
    elsif in_even = 'isVip' then
      select isorderssxy into isordersign from Wl_Cust_Mobile where Imsi=in_imsi;
      if isordersign = 0 then
        rsign:=0;
        goto ref_end;
      elsif isordersign = 1 then
        rsign:=1;
        goto ref_end;
      end if;
    end if;


    <<ref_end>>
    ret:=rsign;
  end;
/

prompt
prompt Creating procedure WL_EVENT_CUST_T
prompt ==================================
prompt
create or replace procedure WL_Event_Cust_t(
   in_imsi in varchar2, --imsi
   in_even in varchar2,--事件
   in_eved in varchar2,--事件描述
   ret out number
   --返回值 0 true；1、imsi为空或不存在；2、无效事件；3、事件描述为空;
   )
   as
   rsign number(5):=0;
   nbrsign number(5):=0;
   imsisign number(5):=0;
   simsign number(5):=0;
   tnbr varchar2(32):='';
   tsim varchar2(32):='';
   tdev varchar2(32):='';
   
   begin
     if in_imsi is null or trim(in_imsi)='' then
         rsign:=1;
         goto ref_end;
     end if;

     if in_even is null or trim(in_even)='' then
         rsign:=2;
         goto ref_end;
     end if;

     if in_eved is null or trim(in_eved)='' then
        rsign:=3;
        goto ref_end;
     end if;

    --写入日志信息记录
    insert into WL_Event_Cust_Log(Imsi,Event,Event_desc) values(in_imsi,in_even,in_eved);

    --判断imsi 是否存在
    tnbr := trim(substr(in_eved,1,instr(in_eved,',',1,1)-1));
    tsim := trim(substr(in_eved,instr(in_eved,',',1,1)+1,instr(in_eved,',',1,2)-instr(in_eved,',',1,1)-1));
    tdev := trim(substr(in_eved,instr(in_eved,',',1,2)+1));
    Select count(Nbr) into nbrsign from Wl_Cust_Mobile where Nbr=tnbr;
    Select count(Imsi) into imsisign from Wl_Cust_Mobile where Imsi=in_imsi;
    if nbrsign=0 then
      if imsisign=0 then
        insert into Wl_Cust_Mobile (sim, nbr, esn, imsi,create_date) values(tsim,tnbr,tdev,in_imsi,sysdate);
        commit;
      else
        update Wl_Cust_Mobile set Nbr=tnbr, Esn=tdev, Sim=tsim where Imsi=in_imsi;
        commit;
      end if;
    else
      update Wl_Cust_Mobile set Imsi=in_imsi, Esn=tdev, Sim=tsim where Nbr=tnbr;
      commit;
    end if;
    rsign:=0;


    <<ref_end>>
    ret:=rsign;
  end;
/

prompt
prompt Creating procedure WL_EVENT_CUST_TEST
prompt =====================================
prompt
create or replace procedure WL_Event_Cust_test(
      /*对用户的认证，用户的行为、事件的记录*/
      simIn in varchar2,--sim/uim卡号
      event in varchar2,--事件
      event_desc in varchar2,--事件描述
      ret out number--返回值 0 true ,1 uim或sim不存在,2 对应的手机号码不存在, 3 无效事件 ,4 事件描述为空 5 该用户未订购时尚星语业务;
      )
       as
    rsign number(2):=0;
    tempone varchar2(20):='';
  begin
    --判断Sim是否为空
    if simIn is null or trim(simIn)='' then
       rsign:=1;
       goto ref_end;
    end if;
    --判断Sim 是否存在
    Select count(Sim) into rsign from Wl_Cust_Mobile where Sim=simIn;
    --不存在
    if rsign=0 then 
       rsign:=1;
       goto ref_end;
     end if;
     --存在判断手机号码是否为空
     select NBR into tempone from Wl_Cust_Mobile where Sim=simIn;
     --手机号码为空
     if tempone is null or trim(tempone)='' then
          rsign:=2;
         goto ref_end;
      end if;
     --判断事件是否为空
     if event is null or trim(event)='' then
         rsign:=3;
         goto ref_end;
     end if;
     
     /*************事件名为认证*********************************/
    if trim(event) = '认证' then
       select ISORDERSSXY into rsign from Wl_Cust_Mobile where Sim=simIn;
       --已订购定购时尚心语业务
       if rsign=1  then
          rsign:=0;
          goto ref_end;
       end if;
       --未订购时尚心语业务
       rsign:=5;
       goto ref_end;
    end if; 
    
    /*****************事件名为拨打电话 ****************/
    if trim(event) = '拨打电话' then
      --事件描述为空
      if event_desc is null or trim(event_desc)='' then
        rsign:=4;
        goto ref_end;
      end if;
      --插入信息记录
        insert into WL_Event_Cust_Log(Imsi,Event,Event_desc) values(simIn,event,event_desc);
        rsign:=0;
        goto ref_end;
      end if;
      
     /**************事件名为查询信息************/
    if trim(event) = '查询信息' then
       select ISORDERSSXY into rsign from Wl_Cust_Mobile where Sim=simIn;
       --未订购定购时尚心语业务
       if rsign=0  then
          rsign:=5;
          goto ref_end;
       end if;
       --订购时尚心语业务
       --且事件描述为空
       if event_desc is null or trim(event_desc)='' then
           rsign:=4;
           goto ref_end;
        end if;
        --插入信息记录
        insert into WL_Event_Cust_Log(Imsi,Event,Event_desc) values(simIn,event,event_desc);
        --成功
       rsign:=0;
       goto ref_end;
    end if; 
    
     --rsign:=99;
     <<ref_end>>
    ret:=rsign;
  end;
/

prompt
prompt Creating procedure WL_GETDEPINFO
prompt ================================
prompt
create or replace procedure WL_GetDepInfo(
     PageSize in number,
     pageIndex in number,
     keys in varchar2,
     pageCount out number,
     recur out Wl_Cust_Dep_Getinfo.t_info
  )as

  begin
    open recur for
  select k.* from (
     select t.* ,rownum run from
       ( select w.*,n.* from WL_Cust_department w
           left join wl_cust_deptnote n
           on n.depid=w.cust_id
            where CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            order by n.ischecked,R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
     where (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%' 
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  end;
/

prompt
prompt Creating procedure WL_GET_INFO_DATE
prompt ===================================
prompt
create or replace procedure wl_get_info_date
(out_date out number,ret out number)

--现在距最后一条信息源发布的时间（分）
--输出参数：out_date：分钟数     ret:0正常，-1异常

as
  info_date  date;
  sysdate_hh varchar2 (200);
begin

  -----信息最后发布时间
  select max(create_date) into info_date from wl_info_data;
  
  -----当前距信息发布时间
  out_date:=trunc((sysdate-info_date)*24*60);

  -----校验-------------
  /*8--11点 超出20分钟内没有信息
    11--18  超出60分钟内没有信息
    异常-1*/
  sysdate_hh:=to_char(sysdate,'hh24');--当前几点

  if sysdate_hh>=8  and sysdate_hh<11 and out_date>20
  then ret:=-1;
  end if;

  if sysdate_hh>=11  and sysdate_hh<18 and out_date>60
  then ret:=-1;
  end if;
     
  if ret is null
  then ret:=0;
  end if;
-----------------ret:=-1;
end;
/

prompt
prompt Creating procedure WL_GET_INFO_TEST
prompt ===================================
prompt
create or replace procedure wl_get_info_test
--输入参数：in_text,in_city 信息,城市
--输出ret:1输入信息串为空或过长过短,2输入城市为空，3经预处理规则后信息串过长过短
--4无目的地，5无联系电话，6已有记录修改时间为当前时间
(in_text  in varchar2,in_city  in varchar2,ret   out number)
is
  tmp_ct number(10);
  tmp_id number(10);
  tmp_pre_treatment varchar2(500);
  tmp_info_start varchar2(500);
  tmp_info_end   varchar2(500);
  tmp_info_truck varchar2(500);
  tmp_info_goods varchar2(500);
  tmp_info_note varchar2(500);
  tmp_info_nbr1 varchar2(30);
  tmp_info_nbr2 varchar2(30);
  tmp_info_nbr3 varchar2(30);
  tmp_info_out   varchar2(500);
  tmp_lng_start  number(11,8);
  tmp_lat_start  number(11,8);
  tmp_lng_end  number(11,8);
  tmp_lat_end  number(11,8);
  tmp_c_name varchar2(64);
  begin
    -----参数空值校验---------
    if in_text is null  or  length(in_text)<5 or  length(in_text)>500  then
       ret:=-100;-------
       goto ret_end;
    end if;
  if  trim(in_city) is null then
       ret:=-2;
       goto ret_end;
  end if;
  -----预处理规则--------
 tmp_pre_treatment:=get_wl_info_rule(in_text);
  if tmp_pre_treatment is null  or  length(tmp_pre_treatment)<5 or  length(tmp_pre_treatment)>500 then
       ret:=3;
       goto ret_end;
  end if;
  
  ret:= lxf_get_qq_msg_back1('342143923','0',in_city,in_text);
  --ret:= lxf_get_qq_msg_back1('342143923','0',in_city,tmp_pre_treatment);
  if ret<0 then 
     goto ret_end;
  end if;
  
 /* -----目的地和号码校验------
 tmp_info_start:=get_wl_info_region_from_new(tmp_pre_treatment);
 tmp_info_end :=get_wl_info_region_end_new(tmp_pre_treatment);
 -----------------------------------------------------------
 tmp_info_nbr1:=get_wl_info_nbr1(tmp_pre_treatment);
 tmp_info_nbr2:=get_wl_info_nbr2(tmp_pre_treatment);
 tmp_info_nbr3:=get_wl_info_nbr3(tmp_pre_treatment);
 if  tmp_info_end is null  then
     ret:=4;
     goto ret_end;
 end if;
 if  tmp_info_nbr1 is null then
     ret:=5;
     goto ret_end;
 end if;

 tmp_info_truck :=get_wl_info_truck(tmp_pre_treatment);
 tmp_info_goods :=get_wl_info_goods(tmp_pre_treatment);
 tmp_info_note :=get_wl_info_note(tmp_pre_treatment);

 tmp_info_out :=tmp_info_start||'到'||tmp_info_end;

 tmp_info_out :=replace(tmp_info_out,'上海上海','上海');
 tmp_info_out :=replace(tmp_info_out,'北京北京','北京');
 tmp_info_out :=replace(tmp_info_out,'天津天津','天津');
 tmp_info_out :=replace(tmp_info_out,'重庆重庆','重庆');


 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;
 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;
 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;

  tmp_lng_start:=get_lng(tmp_info_start);
  tmp_lat_start:=get_lat(tmp_info_start);
  tmp_lng_end :=get_lng(tmp_info_end)  ;
  tmp_lat_end  :=get_lat(tmp_info_end) ;


 ----插入目的表---------------------------
 select count(*) into tmp_ct
 from wl_info_data
 where info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;

 if tmp_ct >0 then
   update  wl_info_data set   create_date=sysdate
   where  info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;
   commit;
       ret:=6;
       goto real_end;
 end if;

 tmp_id:=wl_info_data_id.nextval;
 select max(c_name) into tmp_c_name from wl_cust_department
 where contact=tmp_info_nbr1 or contact=tmp_info_nbr2;


 insert into wl_info_data(id,c_name,city,create_date,info,info_start,info_end,
                          info_nbr1,info_nbr2,info_nbr3,info_truck,
                          info_goods,info_note,info_from,
                          lng_start,lat_start,lng_end,lat_end)
  values (tmp_id,tmp_c_name,in_city,sysdate,tmp_info_out,tmp_info_start,tmp_info_end,
          tmp_info_nbr1,tmp_info_nbr2,tmp_info_nbr3,tmp_info_truck,
          tmp_info_goods,tmp_info_note,'XXXX',
          tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end);
 commit;
 ret:=0;

 insert into  wl_info_from_in_early (text1,city,note,info_id)
 values (in_text,in_city,'一点点',tmp_id);
 commit;

 insert into wl_flag(name,create_date,note) values(tmp_id,sysdate,'**处理数据成功');
 commit;
*/

 goto real_end;


 <<ret_end>>

 -----插入原始表--------------------------
 insert into  wl_info_from_in_early (text1,city,note)
 values (in_text,in_city,'一点点');
 commit;

 <<real_end>>
 ret:=ret;
end ;
/

prompt
prompt Creating procedure WL_GET_INFO_TEST_N
prompt =====================================
prompt
create or replace procedure wl_get_info_test_n
--输入参数：in_text,in_city 信息,城市
--输出ret:1输入信息串为空或过长过短,2输入城市为空，3经预处理规则后信息串过长过短
--4无目的地，5无联系电话，6已有记录修改时间为当前时间
(in_text  in varchar2,in_city  in varchar2,ret   out number)
is
  tmp_ct number(10);
  tmp_id number(10);
  tmp_pre_treatment varchar2(500);
  tmp_info_start varchar2(500);
  tmp_info_end   varchar2(500);
  tmp_info_truck varchar2(500);
  tmp_info_goods varchar2(500);
  tmp_info_note varchar2(500);
  tmp_info_nbr1 varchar2(30);
  tmp_info_nbr2 varchar2(30);
  tmp_info_nbr3 varchar2(30);
  tmp_info_out   varchar2(500);
  tmp_lng_start  number(11,8);
  tmp_lat_start  number(11,8);
  tmp_lng_end  number(11,8);
  tmp_lat_end  number(11,8);
  tmp_c_name varchar2(64);
  begin
  -----参数空值校验---------
  if in_text is null  or  length(in_text)<5 or  length(in_text)>500  then
       ret:=1;-------
       goto ret_end;
  end if;
  if  trim(in_city) is null then
       ret:=2;
       goto ret_end;
  end if;
  -----预处理规则--------
 tmp_pre_treatment:=get_wl_info_rule(in_text);
  if tmp_pre_treatment is null  or  length(tmp_pre_treatment)<5 or  length(tmp_pre_treatment)>500 then
       ret:=3;
       goto ret_end;
  end if;
  -----目的地和号码校验------
 tmp_info_start:=get_wl_info_region_from_new(tmp_pre_treatment);
 tmp_info_end :=get_wl_info_region_end_new(tmp_pre_treatment);
 -----------------------------------------------------------
 tmp_info_nbr1:=get_wl_info_nbr1(tmp_pre_treatment);
 tmp_info_nbr2:=get_wl_info_nbr2(tmp_pre_treatment);
 tmp_info_nbr3:=get_wl_info_nbr3(tmp_pre_treatment);
 if  tmp_info_end is null  then
     ret:=4;
     goto ret_end;
 end if;
 if  tmp_info_nbr1 is null then
     ret:=5;
     goto ret_end;
 end if;

 tmp_info_truck :=get_wl_info_truck(tmp_pre_treatment);
 tmp_info_goods :=get_wl_info_goods(tmp_pre_treatment);
 tmp_info_note :=get_wl_info_note(tmp_pre_treatment);

 tmp_info_out :=tmp_info_start||'到'||tmp_info_end;

 tmp_info_out :=replace(tmp_info_out,'上海上海','上海');
 tmp_info_out :=replace(tmp_info_out,'北京北京','北京');
 tmp_info_out :=replace(tmp_info_out,'天津天津','天津');
 tmp_info_out :=replace(tmp_info_out,'重庆重庆','重庆');


 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;
 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;
 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;

  tmp_lng_start:=get_lng(tmp_info_start);
  tmp_lat_start:=get_lat(tmp_info_start);
  tmp_lng_end :=get_lng(tmp_info_end)  ;
  tmp_lat_end  :=get_lat(tmp_info_end) ;


 ----插入目的表---------------------------
 select count(*) into tmp_ct
 from wl_info_data
 where info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;

 if tmp_ct >0 then
   update  wl_info_data set   create_date=sysdate
   where  info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;
   commit;
       ret:=6;
       goto real_end;
 end if;

 tmp_id:=wl_info_data_id.nextval;
 select max(c_name) into tmp_c_name from wl_cust_department
 where contact=tmp_info_nbr1 or contact=tmp_info_nbr2;


 insert into wl_info_data(id,c_name,city,create_date,info,info_start,info_end,
                          info_nbr1,info_nbr2,info_nbr3,info_truck,
                          info_goods,info_note,info_from,
                          lng_start,lat_start,lng_end,lat_end)
  values (tmp_id,tmp_c_name,in_city,sysdate,tmp_info_out,tmp_info_start,tmp_info_end,
          tmp_info_nbr1,tmp_info_nbr2,tmp_info_nbr3,tmp_info_truck,
          tmp_info_goods,tmp_info_note,'XXXX',
          tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end);
 commit;
 ret:=0;

 insert into  wl_info_from_in_early (text1,city,note,info_id)
 values (in_text,in_city,'一点点',tmp_id);
 commit;



 goto real_end;

 <<ret_end>>

 -----插入原始表--------------------------
 insert into  wl_info_from_in_early (text1,city,note)
 values (in_text,in_city,'一点点');
 commit;

 <<real_end>>
 ret:=ret;
end ;
/

prompt
prompt Creating procedure WL_GET_INFO_TEST_T
prompt =====================================
prompt
create or replace procedure wl_get_info_test_t
--输入参数：in_text,in_city 信息,城市
--输出ret:1输入信息串为空或过长过短,2输入城市为空，3经预处理规则后信息串过长过短
--4无目的地，5无联系电话，6已有记录修改时间为当前时间
(in_text  in varchar2,in_city  in varchar2,ret   out number)
is
  tmp_ct number(10);
  tmp_id number(10);
  tmp_pre_treatment varchar2(500);
  tmp_info_start varchar2(500);
  tmp_info_end   varchar2(500);
  tmp_info_start_s varchar2(500);
  tmp_info_end_s   varchar2(500);
  tmp_info_truck varchar2(500);
  tmp_info_goods varchar2(500);
  tmp_info_note varchar2(500);
  tmp_info_nbr1 varchar2(30);
  tmp_info_nbr2 varchar2(30);
  tmp_info_nbr3 varchar2(30);
  tmp_info_out   varchar2(500);
  tmp_lng_start  number(11,8);
  tmp_lat_start  number(11,8);
  tmp_lng_end  number(11,8);
  tmp_lat_end  number(11,8);

  begin
  -----参数空值校验---------
  if in_text is null  or  length(in_text)<5 or  length(in_text)>500  then
       ret:=1;-------
       goto ret_end;
  end if;
  if  trim(in_city) is null then
       ret:=2;
       goto ret_end;
  end if;
  -----预处理规则--------
 tmp_pre_treatment:=get_wl_info_rule(in_text);
  if tmp_pre_treatment is null  or  length(tmp_pre_treatment)<5 or  length(tmp_pre_treatment)>500 then
       ret:=3;
       goto ret_end;
  end if;
  -----目的地和号码校验------
 tmp_info_start:=get_wl_info_region_from(tmp_pre_treatment,in_city);
 tmp_info_end :=get_wl_info_region_end(tmp_pre_treatment,in_city);
 tmp_info_start_s:=get_wl_info_region_from_s(tmp_pre_treatment,in_city);
 tmp_info_end_s :=get_wl_info_region_end_s(tmp_pre_treatment,in_city);
 -----------------------------------------------------------
 tmp_info_nbr1:=get_wl_info_nbr1(tmp_pre_treatment);
 tmp_info_nbr2:=get_wl_info_nbr2(tmp_pre_treatment);
 tmp_info_nbr3:=get_wl_info_nbr3(tmp_pre_treatment);
 if  tmp_info_end is null  then
     ret:=4;
     goto ret_end;
 end if;
 if  tmp_info_nbr1 is null then
     ret:=5;
     goto ret_end;
 end if;

 tmp_info_truck :=get_wl_info_truck(tmp_pre_treatment);
 tmp_info_goods :=get_wl_info_goods(tmp_pre_treatment);
 tmp_info_note :=get_wl_info_note(tmp_pre_treatment);

 tmp_info_out :=tmp_info_start_s||'到'||tmp_info_end_s;
 if tmp_info_truck is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_truck;
 end if;
 if tmp_info_goods is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_goods;
 end if;
 if tmp_info_note is not null then
    tmp_info_out :=tmp_info_out||','||tmp_info_note;
 end if;
  tmp_lng_start:=get_lng(tmp_info_start);
  tmp_lat_start:=get_lat(tmp_info_start);
  tmp_lng_end :=get_lng(tmp_info_end)  ;
  tmp_lat_end  :=get_lat(tmp_info_end) ;




 -----插入原始表--------------------------
 insert into  wl_info_from_in_early (text1,city) values (in_text,in_city);
 commit;
 ----插入目的表---------------------------
 select count(*) into tmp_ct
 from wl_info_data
 where info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;

 if tmp_ct >0 then
   update  wl_info_data set   create_date=sysdate
   where  info=tmp_info_out and info_nbr1=tmp_info_nbr1 ;
   commit;
       ret:=6;
       goto ret_end;
  end if;
  tmp_id:=wl_info_data_id.nextval;
 insert into wl_info_data(id,city,create_date,info,info_start,info_end,
                          info_nbr1,info_nbr2,info_nbr3,info_truck,
                          info_goods,info_note,info_from,
                          lng_start,lat_start,lng_end,lat_end)
  values (tmp_id,in_city,sysdate,tmp_info_out,tmp_info_start,tmp_info_end,
          tmp_info_nbr1,tmp_info_nbr2,tmp_info_nbr3,tmp_info_truck,
          tmp_info_goods,tmp_info_note,'XXXX',
          tmp_lng_start,tmp_lat_start,tmp_lng_end,tmp_lat_end);
 commit;
 ret:=0;
 insert into wl_flag(name,create_date,note) valueS(tmp_id,sysdate,'**处理数据成功');
 commit;
 <<ret_end>>
 ret:=ret;
end ;
/

prompt
prompt Creating procedure WL_GET_LINE_STR
prompt ==================================
prompt
create or replace procedure wl_get_line_str
(in_str in varchar2    -----输入字符串
,from_province out varchar2 ----起始省份
,from_city out varchar2     ----起始城市
,to_province out varchar2   ----目的省份
,to_city out varchar2       ----目的城市
,ret out number)            ----返回值


--1.输入字符串拆分 格式  陕西,榆林,陕西,西安 ，省份和起始城市不能为空，目的城市可以为空
  as
  begin
  if in_str is null then 
    ret:=1;
    goto ret_end;
  end if;
  from_province:=trim(substr(in_str,1,instr(in_str,',',1,1)-1));
  from_city:=trim(substr(in_str,instr(in_str,',',1,1)+1,instr(in_str,',',1,2)-instr(in_str,',',1,1)-1));
  to_province:=trim(substr(in_str,instr(in_str,',',1,2)+1,instr(in_str,',',1,3)-instr(in_str,',',1,2)-1));
  to_city:=trim(substr(in_str,instr(in_str,',',1,3)+1,instr(in_str,',',1,3)-instr(in_str,',',1,2)-1));
  if from_province is null or from_city is null or to_province is null then 
    ret:=2;
    goto ret_end;
  end if ;
 ret:=0;
<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_IF_QQ_MSG
prompt ===============================
prompt
create or replace procedure wl_if_qq_msg
--输入参数：externalid ,sender ,message  类型,QQ号,信息
--输出参数：out_externalid,out_sender,out_message 类型,QQ号,信息加成功
  (externalid      in  number
  ,sender          in  number
  ,message         in  varchar2
  ,out_externalid  out number
  ,out_sender      out number
  ,out_message     out varchar2
  )
is

begin
  
  if sender= '1554987499' then
    wl_cust_qq_sendmsg(externalid,sender,message,out_externalid,out_sender,out_message);
  elsif sender= '2577556895' then
    Wl_Cust_Qq_Sendinfo(externalid,sender,message,out_externalid,out_sender,out_message);
  end if;

end;
/

prompt
prompt Creating procedure WL_INFO_DATA_GETDATA
prompt =======================================
prompt
create or replace procedure WL_Info_data_GetData(
  /*找货宝首页获取信息*/
  pageIndex in number,--当前第几页
  pageSize in number,--每页显示的条数
  keyStart in varchar2,--起点城市
  keyEnd in varchar2,--目的城市
  retCount out number,--总记录数
  retCur out Wl_Cust_Dep_Getinfo.t_info--输出信息游标
)as
tempStart varchar2(100):='';
tempEnd varchar2(100):='';
begin
  ----获取起始地地址----
     if keyStart is not null then
       tempStart:=WL_GetComAddress(keyStart);
     end if;
     
     if keyEnd is not null then
       tempEnd:=WL_GetComAddress(keyEnd);
     end if;
  -----获取总记录数-----
  select count(*) Into retCount from
        (
            select b.*, rownum run
                from
                   (
                     select id,c_name,create_date,info_nbr1,info
                         from Wl_Info_Data
                             where 1=1  and info_start
                               like tempStart||'%' and info_end like tempEnd||'%'
                   ) b
       ) c ;
  ----查询该数据---
  open retCur for
    select * from
        (
            select b.*, rownum run
                from
                   (
                     select id,c_name,create_date,info_nbr1,info
                         from Wl_Info_Data
                             where 1=1  and info_start
                               like tempStart||'%' and info_end like tempEnd||'%'  order by Create_date desc
                   ) b
       ) c
    where c.run<=pageIndex*pageSize  and c.run>(pageIndex-1)*pageSize;
end;
/

prompt
prompt Creating procedure WL_INFO_GETCITYLIST
prompt ======================================
prompt
create or replace procedure WL_Info_GetCityList(
   in_city in varchar2,--城市名称
   ret out number,--状态：1 城市名称为空  0 成功 
   refcur out sys_refcursor--wl_cust_mobile_getinfo6.t_info--返回结果集
 )as
 /*输入城市名称。返回起始地问本城市的其他货运消息条数*/
 ct number(5);
 city varchar2(100);
 city_length number(3);
 begin
    city:=trim(in_city);
    city_length:=length(city);
    -----输入参数为null或者是空返回-1
    if city is null or trim(city)='' then
         open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where 0=1
               group by substr(info_end,1,2);
         ret:=-1;
         goto ret_end;
    end if;
    ----------查询输入参数是否存在----------
    ct:=0;
    select count(*) into ct 
      from wl_info_data 
     where instr(info_start,in_city) >0;
     -----------查询城市对应的信息数量----------
     if ct>0 then
         open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where  instr(info_start,in_city) >0
               group by substr(info_end,1,2);
               ret:=0;
          else
            open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where instr(info_start,in_city) >0 and 1=0 
               group by substr(info_end,1,2);
              ret:=1;
      end if;
    
   
 
 
      -----结束点-----
       <<ret_end>>
       ret:=ret;   
 end;
/

prompt
prompt Creating procedure WL_INFO_GETCITYLIST_T
prompt ========================================
prompt
create or replace procedure WL_Info_GetCityList_t(
   in_city in varchar2,--城市名称
   ret out number,--状态：1 城市名称为空  0 成功 
   refcur out sys_refcursor-- wl_cust_mobile_getinfo6.t_info--返回结果集
 )as
 /*输入城市名称。返回起始地问本城市的其他货运消息条数*/
 ct number(5);
 city varchar2(100);
 city_length number(3);
 begin
    city:=trim(in_city);
    city_length:=length(city);
    -----输入参数为null或者是空返回-1
    if city is null or trim(city)='' then
         open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where 0=1
               group by substr(info_end,1,2);
         ret:=-1;
         goto ret_end;
    end if;
    ----------查询输入参数是否存在----------
    ct:=0;
    select count(*) into ct 
      from wl_info_data 
     where instr(info_start,in_city) >0;
     -----------查询城市对应的信息数量----------
     if ct>0 then
         open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where  instr(info_start,in_city) >0
               group by substr(info_end,1,2);
               ret:=0;
          else
            open refcur for select substr(info_end,1,2) addressEnd,count(*) Addresscount 
                from wl_info_data 
                 where instr(info_start,in_city) >0 and 1=0 
               group by substr(info_end,1,2);
              ret:=1;
      end if;
    
   
 
 
      -----结束点-----
       <<ret_end>>
       ret:=ret;   
 end;
/

prompt
prompt Creating procedure WL_INFO_TOP
prompt ==============================
prompt
create or replace procedure wl_info_top
--Authid Current_User
is
--acct_sql varchar2(2000);
begin
--num指分享次数，默认为0，每调用一次存储过程创建一次视图，NUM减1
update wl_info_data set num=num-1 where id 
in(select id from wl_info_data_top) ;
commit;

--显示视图：视图中有分享次数-（发布时间距现在多少个5分钟）排名最前及分数最高的前20条记录

/*
update wt_wl_data_1_test set num=num-1 where id in(select id from wt_wl_data_test) ;
commit;

create or replace view wt_wl_data_test as
select "ID","NAME","INFORMATION","REGION","CREATE_DATE","NUM","STATE","CITY" from
(select * from wt_wl_data_1_test order by
num-((sysdate-create_date)*24*60/5) desc)where rownum<=20*/

--acct_sql:='select * from wt_wl_data';
--EXECUTE IMMEDIATE acct_sql;
--commit;
end;
/

prompt
prompt Creating procedure WL_MOBILE_RING_RETURN
prompt ========================================
prompt
create or replace procedure wl_mobile_ring_return
(in_nbr         in varchar2
,in_name        in varchar2
,in_car_number  in varchar2
,in_qq          in varchar2
,in_car_model   in varchar2
,in_like1       in varchar2
,in_mobile_type in number
,in_staff       in varchar2
,ret            out number)


--外呼管理
--2.修改状态信息：用户提交 “联系人电话 姓名 车号 QQ号码 车型 常跑路线
--手机类型  外呼人”存储过程按照用户提交信息修改表中的信息。
--若 QQ号码 车型 常跑路线 手机类型  为非空字段时，update表中状态字段为3
--输入参数:联系人电话,姓名,车号,QQ号码,车型,常跑路线,手机类型,外呼人
--输出参数:ret 0成功修改,1号码或QQ或车型或常跑路线或手机类型为空,2无此号码
as

tmp_ct      number(10);




begin
  
  -----输入参数空值校验---------
  if in_nbr is null or in_qq is null or in_car_model is null 
  or in_like1 is null or in_mobile_type is null 
  
  then ret:=1;
       goto ret_end;
  end if;
  
  -----记录校验----------------
  select count(*) into tmp_ct from wl_mobile_ring
  where nbr=in_nbr;

  if tmp_ct=0
  then ret:=2;
       goto ret_end;
  end if;
  -----更新外呼司机表-------------------
  update wl_mobile_ring set name=in_name,car_number=in_car_number,
  qq=in_qq,car_model=in_car_model,like1=in_like1,
  mobile_type=in_mobile_type,state=3,staff=in_staff where nbr=in_nbr;
  commit;
  ------------------------------------
  ret:=0;

<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_OUTSIDE_CUST_DEPARTMENT_CG
prompt ================================================
prompt
create or replace procedure wl_outside_cust_department_cg
(in_nbr in varchar2,in_id_card in varchar2,in_c_name in varchar2,
 in_c_password in varchar2,in_city in varchar2,
 in_c_type in varchar2,in_chief in varchar2,
 in_contact in varchar2,in_address in varchar2,
 in_lng in number, in_lat in number,in_note in number,
 ret out number)


--outside2.外部接口:更改信息部用户信息
--输入参数：in_nbr 等等(in_note可为qq号)
--输出参数：1号码无效,2密码无效,3c_type输入必须是信息部或停车场,
--4至少修改一项,5该号码不存在,0修改成功

as

tmp_ct           number(10);
tmp_nbr          varchar2(30);
tmp_id_card      varchar2(64);
tmp_c_name       varchar2(64);
tmp_c_password   varchar2(64);
tmp_city         varchar2(64);
tmp_c_type       varchar2(64);
tmp_chief        varchar2(64);
tmp_contact      varchar2(500);
tmp_address      varchar2(500);
tmp_note         varchar2(500);

begin

tmp_nbr       :=trim(in_nbr);
tmp_id_card   :=trim(in_id_card);
tmp_c_name    :=trim(in_c_name);
tmp_c_password:=trim(in_c_password);
tmp_city      :=trim(in_city);
tmp_c_type    :=trim(in_c_type);
tmp_chief     :=trim(in_chief);
tmp_contact   :=trim(in_contact);
tmp_address   :=trim(in_address);
tmp_note      :=trim(in_note);
  
  -----输入号码校验--------- 
  if tmp_nbr is null or length(tmp_nbr)<>11 or 
      substr(tmp_nbr,1,2)not in('13','14','15','18')then
     ret:=1;
     goto ret_end;
  end if;
  
  -----输入密码校验-------------
  if tmp_c_password is not null and 
      (length(in_c_password)<6 or length(in_c_password)>20)then     
     ret:=2;
     goto ret_end;
  end if;
  
  -----输入c_type校验-------------
  if tmp_c_type is not null and 
      tmp_c_type not in('信息部','停车场') then     
     ret:=3;
     goto ret_end;
  end if;
  
  -----至少有一项修改内容------------
  if tmp_id_card    is null and 
     tmp_c_name     is null and
     tmp_c_password is null and
     tmp_city       is null and 
     tmp_c_type     is null and 
     tmp_chief      is null and 
     tmp_contact    is null and 
     tmp_address    is null and 
     (in_lng        is null or  in_lng<=0) and
     (in_lat        is null or  in_lat<=0) and
     tmp_note       is null                      then
     ret:=4;
     goto ret_end;
  end if;

  -----记录校验------------
  select count(*) into tmp_ct from wl_cust_department where nbr=tmp_nbr ;

  if tmp_ct = 0 then
     ret:=5;
     goto ret_end;
  end if;
  
  -----更改记录id_card-------------
  if tmp_id_card is not null then 
     update wl_cust_department set id_card=tmp_id_card
       where nbr=tmp_nbr;
     commit;
  end if;

  -----更改记录c_name-------------
  if tmp_c_name is not null then 
     update wl_cust_department set c_name=tmp_c_name
       where nbr=tmp_nbr;
     commit;
  end if;
 
  -----更改记录c_password-------------
  if tmp_c_password is not null and length(in_c_password)>=6
      and length(in_c_password)<=20 then  
     update wl_cust_department set c_password=in_c_password
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录city-------------
  if tmp_city is not null then 
     update wl_cust_department set city=tmp_city
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录c_type-------------
  if tmp_c_type is not null then 
     update wl_cust_department set c_type=tmp_c_type
       where nbr=tmp_nbr;
     commit;
  end if;

  -----更改记录chief-------------
  if tmp_chief is not null then 
     update wl_cust_department set chief=tmp_chief
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录contact-------------
  if tmp_contact is not null then 
     update wl_cust_department set contact=tmp_contact
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录address-------------
  if tmp_address is not null then 
     update wl_cust_department set address=tmp_address
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录lng-------------
  if in_lng is not null and in_lng>0 then 
     update wl_cust_department set lng=in_lng
       where nbr=tmp_nbr;
     commit;
  end if;

  -----更改记录lat-------------
  if in_lat is not null and in_lat>0 then 
     update wl_cust_department set lat=in_lat
       where nbr=tmp_nbr;
     commit;
  end if;
  
  -----更改记录contact-------------
  if tmp_note is not null then 
     update wl_cust_department set note=tmp_note
       where nbr=tmp_nbr;
     commit;
  end if;
  

ret:=0;
<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_OUTSIDE_CUST_DEPARTMENT_CR
prompt ================================================
prompt
create or replace procedure wl_outside_cust_department_cr
(in_nbr in varchar2,in_c_password in varchar2,ret out number)

--outside1.外部接口:新建信息部用户
--输入参数：in_nbr,in_c_password   号码,密码
--输出参数：1号码无效,2密码无效,3该号码已存在,0创建成功

as

tmp_ct number(10);
tmp_nbr varchar2(30);
tmp_c_password   varchar2(64);

begin
  
tmp_nbr:=trim(in_nbr);
tmp_c_password:=trim(in_c_password);
  
  -----输入号码校验--------- 
  if tmp_nbr is null or length(tmp_nbr)<>11 or 
      substr(tmp_nbr,1,2)not in('13','14','15','18')then
     ret:=1;
     goto ret_end;
  end if;

  -----输入密码校验---------
  if tmp_c_password is null or length(in_c_password)<6 or 
      length(in_c_password)>20 then     
     ret:=2;
     goto ret_end;
  end if;

  -----记录校验---------
  select count(*) into tmp_ct from wl_cust_department where nbr=tmp_nbr ;

  if tmp_ct > 0 then 
     ret:=3; 
     goto ret_end;
  end if;
  
  -----插入记录---------
  insert into wl_cust_department (nbr,c_password)
                           values(tmp_nbr,in_c_password);
  commit;
  ret:=0;

   
<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_OUTSIDE_CUST_MOBILE_CG
prompt ============================================
prompt
create or replace procedure wl_outside_cust_mobile_cg
(in_sim in varchar2,in_id_card in varchar2,in_c_name in varchar2,
 in_c_password in varchar2,in_city in varchar2,
 in_car_number in varchar2,in_car_model in varchar2,
 in_like1 in varchar2,in_like2 in varchar2,in_like3 in varchar2,
 in_like4 in varchar2,in_like5 in varchar2,in_like6 in varchar2,
 in_like7 in varchar2,in_like8 in varchar2,in_like9 in varchar2,
 in_like10 in varchar2,
 ret out number)

--outside3.外部接口:更改手机用户信息
--输入参数：in_sim 等等(in_note可为qq号)
--输出参数：1sim不能为空,2密码无效,3至少修改一项,4该sim不存在,
--0修改成功
--update update wl_cust_mobile  (id_card,c_name,c_password,city,car_number,car_model,like1,like2,like3.....like10)   where sim= 89860


as

tmp_ct           number(10);
tmp_sim          varchar2(30);
tmp_id_card      varchar2(64);
tmp_c_name       varchar2(64);
tmp_c_password   varchar2(64);
tmp_city         varchar2(64);
tmp_car_number   varchar2(64);
tmp_car_model    varchar2(64);
tmp_like1        varchar2(500);
tmp_like2        varchar2(500);
tmp_like3        varchar2(500);
tmp_like4        varchar2(500);
tmp_like5        varchar2(500);
tmp_like6        varchar2(500);
tmp_like7        varchar2(500);
tmp_like8        varchar2(500);
tmp_like9        varchar2(500);
tmp_like10       varchar2(500);


begin

tmp_sim       :=trim(in_sim);
tmp_id_card   :=trim(in_id_card);
tmp_c_name    :=trim(in_c_name);
tmp_c_password:=trim(in_c_password);
tmp_city      :=trim(in_city);
tmp_car_number:=trim(in_car_number);
tmp_car_model :=trim(in_car_model);
tmp_like1     :=trim(in_like1);
tmp_like2     :=trim(in_like2);
tmp_like3     :=trim(in_like3);
tmp_like4     :=trim(in_like4);
tmp_like5     :=trim(in_like5);
tmp_like6     :=trim(in_like6);
tmp_like7     :=trim(in_like7);
tmp_like8     :=trim(in_like8);
tmp_like9     :=trim(in_like9);
tmp_like10    :=trim(in_like10);

  -----输入sim校验---------
  if tmp_sim is null then
     ret:=1;
     goto ret_end;
  end if;
  
  -----输入密码校验-------------
  if tmp_c_password is not null and
      (length(in_c_password)<6 or length(in_c_password)>20)then
     ret:=2;
     goto ret_end;
  end if;
  
  -----至少有一项修改内容------------
  if tmp_id_card    is null and
     tmp_c_name     is null and
     tmp_c_password is null and
     tmp_city       is null and
     tmp_car_number is null and
     tmp_car_model  is null and
     tmp_like1      is null and
     tmp_like2      is null and
     tmp_like3      is null and
     tmp_like4      is null and
     tmp_like5      is null and 
     tmp_like6      is null and
     tmp_like7      is null and
     tmp_like8      is null and
     tmp_like9      is null and
     tmp_like10     is null          then
     ret:=3;
     goto ret_end;
  end if;
  -----记录校验------------
  select count(*) into tmp_ct from wl_cust_mobile where sim=tmp_sim ;

  if tmp_ct = 0 then
     ret:=4;
     goto ret_end;
  end if;

  -----更改记录id_card-------------
  if tmp_id_card is not null then
     update wl_cust_mobile set id_card=tmp_id_card
       where sim=tmp_sim;
     commit;
  end if;
  
  -----更改记录c_name-------------
  if tmp_c_name is not null then
     update wl_cust_mobile set c_name=tmp_c_name
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录c_password-------------
  if tmp_c_password is not null and length(in_c_password)>=6
      and length(in_c_password)<=20 then
     update wl_cust_mobile set c_password=in_c_password
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录city-------------
  if tmp_city is not null then
     update wl_cust_mobile set city=tmp_city
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录car_number-------------
  if tmp_car_number is not null then
     update wl_cust_mobile set car_number=tmp_car_number
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录car_model-------------
  if tmp_car_model is not null then
     update wl_cust_mobile set car_model=tmp_car_model
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like1-------------
  if tmp_like1 is not null then
     update wl_cust_mobile set like1=tmp_like1
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like2-------------
  if tmp_like2 is not null then
     update wl_cust_mobile set like2=tmp_like2
       where sim=tmp_sim;
     commit;
  end if;
  -----更改记录like3-------------
  if tmp_like3 is not null then
     update wl_cust_mobile set like3=tmp_like3
       where sim=tmp_sim;
     commit;
  end if;
  -----更改记录like4-------------
  if tmp_like4 is not null then
     update wl_cust_mobile set like4=tmp_like4
       where sim=tmp_sim;
     commit;
  end if;
  
  -----更改记录like5-------------
  if tmp_like5 is not null then
     update wl_cust_mobile set like5=tmp_like5
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like6-------------
  if tmp_like6 is not null then
     update wl_cust_mobile set like6=tmp_like6
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like7-------------
  if tmp_like7 is not null then
     update wl_cust_mobile set like7=tmp_like7
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like8-------------
  if tmp_like8 is not null then
     update wl_cust_mobile set like8=tmp_like8
       where sim=tmp_sim;
     commit;
  end if;
  -----更改记录like9-------------
  if tmp_like9 is not null then
     update wl_cust_mobile set like9=tmp_like9
       where sim=tmp_sim;
     commit;
  end if;

  -----更改记录like10-------------
  if tmp_like10 is not null then
     update wl_cust_mobile set like10=tmp_like10
       where sim=tmp_sim;
     commit;
  end if;
  
ret:=0;
<<ret_end>>
ret:=ret;
end;
/

prompt
prompt Creating procedure WL_SMS_ZST_OPEARE_OPDETAIL
prompt =============================================
prompt
create or replace procedure WL_SMS_ZST_OPEARE_OpDetail(
    In_phoneNum in varchar2,--手机号码
    In_Op in varchar2,--操作
    In_QQ in varchar2,--qq号
    In_Saler in varchar2,--销售编号
    Out_mess out varchar2--输入信息
 )
 as
 temp_Count number(5):=0;--临时储存记录数
 temp_OutMess varchar2(300):='';--临时储存输出信息
 temp_Str varchar2(300):='';--临时储存字符串
 begin
   ----------------绑定QQ----------------
   if In_Op='bdqq' then
      ----------------查找是否存在其他人占用此QQ--------------------
      select count(*) into temp_Count from  Wl_Cust_Mobile where qq=In_QQ and nbr<>In_phoneNum;
      -----------没有其他人使用此qq------------
      if temp_Count=0 then
         update Wl_Cust_Mobile set qq=In_QQ where nbr=In_phoneNum;
         temp_OutMess:='QQ号:'||In_QQ||'绑定成功！【找货宝】';
         commit;
         goto ret_end;
      end if;
      ---------------查看存在该QQ的电话是否为空--------------------
      select max(nbr) into temp_Str from  Wl_Cust_Mobile where qq=In_QQ and nbr<>In_phoneNum;
      -----------存在qq 的电话为空则删除该条信息并将qq绑定到该号码下-----
      if temp_Str is null then
         delete  Wl_Cust_Mobile where qq=In_QQ and nbr<>In_phoneNum and (nbr is null);
         update Wl_Cust_Mobile set qq=In_QQ where nbr=In_phoneNum;
         temp_OutMess:='QQ号:'||In_QQ||'绑定成功！【找货宝】';
         commit;
         goto ret_end;
      else--否则去掉对方的qq并绑定自己QQ
         update Wl_Cust_Mobile set qq='' where qq=In_QQ and nbr<>In_phoneNum;
         update Wl_Cust_Mobile set qq=In_QQ where nbr=In_phoneNum;
         temp_OutMess:='QQ号:'||In_QQ||'绑定成功！【找货宝】';
         commit;
         goto ret_end;
      end if;
   end if;
   ---------------绑定销售人员（销售人员获取该用户单未开通 isorderssxy=2）-----------
   if In_Op='bdxs' then
       select max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       if temp_Str is null then
          insert into Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Last_Position_Date,Isorderssxy)values(In_phoneNum,sysdate,sysdate,sysdate,2); 
           commit;
       end if;
       select max(CUST_ID) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       ----------------将该号码放入该销售编号名下----------------
       WL_XS_sAch_Add(to_number(In_Saler),'司机',temp_Str,temp_Count);
       if temp_Count=0 then
          Temp_OutMess:=Temp_OutMess||' 销售编号'||In_Saler||'绑定成功！【找货宝】';
          update wl_orderinfo set order_type='销售人员代购',order_operator=In_Saler 
           where cust_id=temp_Str;
           commit;
       elsif temp_Count=1 then
          Temp_OutMess:=Temp_OutMess||' 销售编号'||In_Saler||'无效！【找货宝】';
       elsif temp_Count=3 then
          Temp_OutMess:=Temp_OutMess||' 司机无效！。销售编号'||In_Saler||'绑定失败！【找货宝】';
       elsif temp_Count=4 then
           Temp_OutMess:=Temp_OutMess||' 。销售编号'||In_Saler||'已经绑定该号码！【找货宝】';
       elsif temp_Count=5 then
           Temp_OutMess:=Temp_OutMess||'该号码已被其他销售人员绑定 。销售编号'||In_Saler||'绑定失败！【找货宝】';
       end if;
   end if;
   -----------------销售人员开通司机手机号---------
   if In_op='ktsj' then
       -------------验证销售编号-----------------
        select count(*) into temp_Count 
        from Wl_Xs_Salesman where saleId=In_Saler and sphone=In_phoneNum;
        if temp_Count=0 then
           temp_OutMess:='本手机号码与销售编号未绑定【找货宝】';
           goto ret_end;
        end if;
        -------------判断该司机是否属于该销售--------
        select Max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_QQ;
        if temp_Str is null  then
           insert into Wl_Cust_Mobile(Nbr,Create_Date,Isorderssxy,Isreg,Serchtimes)values(In_QQ,sysdate,0,0,0)
            return CUST_ID into temp_Str;
        end if;
        --------------判断该手机号码是否被销售编号绑定----
        select count(*) into temp_Count
         from wl_xs_salesach where  oppsiteid=temp_Str;
        if temp_Count=0 then
            wl_xs_sach_add(In_Saler,'司机',temp_Str,temp_Count);
        end if;
        select count(*) into temp_Count
         from wl_xs_salesach where saleId=In_Saler and oppsiteid=temp_Str;
        if temp_Count=0 then
           temp_OutMess:='该手机号码不属于该销售编号【找货宝】';
           goto ret_end;
        end if;
        ------------------开通该用户的业务12月----------
        update wl_cust_mobile 
          set eff_date=add_months(sysdate,to_number(12)),
           isORDERSSXY=1
         where nbr=In_QQ;
         temp_OutMess:='手机号码：'||In_QQ||'已成功开通12个月！【找货宝】';
        ---------------------更新该手机号为已经交费------
        update wl_xs_salesach set translation=1 
        where saleId=In_Saler and oppsiteid=temp_Str;
        commit;
        wl_cust_order(temp_Str,'FFF0D80E7CC344198499B3892A17499D',1,'销售人员代购',sysdate,add_months(sysdate,12),In_Saler,temp_OutMess);
        commit;
        if temp_OutMess='订购成功！！【找货宝】' then
            temp_OutMess:='手机号码：'||In_QQ||'已成功开通12个月！【找货宝】';
        end if;
        goto ret_end;
   end if;
   ---------------结束点-----------------
   <<ret_end>>
   Out_mess:=temp_OutMess;
 end;
/

prompt
prompt Creating procedure WL_SMS_ZST_OPEARE
prompt ====================================
prompt
create or replace procedure WL_SMS_ZST_OPEARE(
  In_Phone in varchar2,--手机号码
  In_Opear in varchar2,--操作 order,exit, other(BDQQ,BDXS,getMess)
  In_Mess in varchar2,--手机发送过来的信息和一些操作指令 如 
  ret_mess out varchar2--发给手机的信息
)as
  tmp_out_message varchar2(8000):='';--临时储存输出信息
  temp_count    number(5):=0;--临时储存数组
  temp_str varchar2(500):='';
  startStr varchar2(100):='';--临时储存起始地
  endStr varchar2(100):='';--临时储存目的地
 begin
   -------------判断手机号码是否正确----------------------------
   if regexp_instr(In_Phone,'1(3|4|5|6|7|8)\d{9}')=0 then
      tmp_out_message:='手机号码有误';
      goto ret_end;
   end if;
   ---------------------判断该用户是否存在----------------------
   select count(*) into temp_count from Wl_Cust_Mobile where nbr=In_Phone;
   ------------------------订购业务-----------------------------
   if In_Opear='order' then
      if temp_count>0 then
         update Wl_Cust_Mobile set Isorderssxy=1,Eff_Date=add_months(sysdate,to_number(1)) where nbr=In_Phone;
         select max(cust_ID) into temp_str from Wl_Cust_Mobile where nbr=In_Phone;
      else
         insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy) 
           values(In_Phone,sysdate,add_months(sysdate,1),1) return CUST_ID into temp_str;
      end if;
         insert into WL_SMS_ZST_SERVICE(PhoneNum,BizSate) values(In_Phone,'订购业务');
         wl_cust_order(temp_str,'96B6F04443E64C21819C9C68822E07F4',1,'自订购',sysdate,null,In_Phone,tmp_out_message);
         tmp_out_message :='感谢您订购找货宝业务!请回复“RJ”下载安卓手机软件;短信查询:请回复“某地到某地”。QQ查询:请加2577556895为好友。【找货宝】';
      goto ret_end;
   end if;
   ----------------------取消业务---------------------------------
   if In_Opear='exit' then 
      if temp_count>0 then
         update Wl_Cust_Mobile set Isorderssxy=0,Eff_Date=sysdate where nbr=In_Phone;
      else
         insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy) 
         values(In_Phone,sysdate,sysdate,0);
      end if;
         insert into WL_SMS_ZST_SERVICE(PhoneNum,BizSate) values(In_Phone,'取消业务');
          WL_CUST_CannelOrder(In_Phone,tmp_out_message);
         tmp_out_message :='手机号码：'||In_Phone||'取消成功【找货宝】';
      goto ret_end;
   end if;
   /*------------------------其他业务------------------------------*/
   if In_Opear='other' then
      -----------------------提帮助----------------------------------
       if regexp_instr(In_Mess,'\s*帮助\s*')>0 then
           tmp_out_message:='1.绑定QQ 请发送:bdqq QQ号 ; 2.绑定销售编号 请发送:bdxs 销售编号 ;【找货宝】';
           goto ret_end;   
       end if;
        ----------------------绑定QQ--------------------------------
       if  regexp_like(In_Mess,'\s*bdqq\s*\d{5,12}','i') then
           temp_str:=regexp_replace(In_Mess,'\s*bdqq\s*','',1,1,'i');
           WL_SMS_ZST_OPEARE_OpDetail(In_Phone,'bdqq',temp_str,'', tmp_out_message);
           goto ret_end;   
       end if;
       -------------------------绑定销售编号------------------------
        if regexp_like(In_Mess,'\s*bdxs\s*\d{6}','i')  then
           temp_str:=regexp_replace(In_Mess,'\s*bdxs\s*','',1,1,'i');
           WL_SMS_ZST_OPEARE_OpDetail(In_Phone,'bdxs','',temp_str, tmp_out_message);
           goto ret_end;   
        end if;
        -------------------------销售人员开通司机手机号---------------
        if regexp_like(In_Mess,'\s*ktsj\s*1(3|4|5|8)\d{9}\s+[1-9]\d{5}\s*','i') then
           temp_str:=regexp_substr(In_Mess,'\s*1(3|4|5|8)\d{9}\s+');--要开通的司机手机号码
           temp_str:=trim(temp_str);
           startStr:=regexp_replace(In_Mess,'\s*1(3|4|5|8)\d{9}\s+','');
           startStr:=regexp_substr(startStr,'\s*[1-9]\d{5}\s*');--销售便号
           startStr:=trim(startStr);
           WL_SMS_ZST_OPEARE_OpDetail(In_Phone,'ktsj',temp_str,startStr, tmp_out_message);
           goto ret_end;  
        end if;
        -----------------------判断该用户是否订购业务-------------------------------
       select max(isorderssxy) into temp_count from wl_cust_mobile where nbr=In_Phone;
       if temp_count=0 or temp_count is null then
           tmp_out_message:='您尚未订购找货宝短信业务，无法使用该功能，若要使用请发送5137到10669588订购【找货宝】';
           goto ret_end;
        end if;
        if temp_count=2 or temp_count is null then
           tmp_out_message:='您未开通找货宝短信业务,请和您的销售人员联系【找货宝】';
           goto ret_end;
        end if;
        -------------------------查询信息----------------------------
         WL_Cust_ZHBQQ_GetAddress(In_Mess,startStr,endStr);
         -----------------------地址格式有误-------------------------
         if startStr is null or endStr is null then
             tmp_out_message :='您输入的格式有误请输入:某地到某地【找货宝】';
             goto ret_end;
         end if;
         -----------------------地址格式正确-----------------------------
         tmp_out_message:=get_line_info_TXH_top(In_Phone,1,startStr,endStr);
         ------------------------更新最后一次查询时间--------------------
         update wl_cust_mobile set 
              last_position_date=sysdate ,
              last_search=startStr||'到'||endStr,
              last_serch_data=Sysdate
              where  nbr=In_Phone;
         commit; 
         -----------------------插入到短信记录中去------------------------
         insert into WL_SMS_ZST_Messages(PHONENUM,IN_MESS,OUT_MESS)
               values(In_Phone,In_Mess,tmp_out_message);
         commit;
          -------------------------查询记录条数-----------------------------
         select count(*) into temp_count from WL_info_data
               where info_start like startStr||'%' 
               and info_end like endStr||'%';
          tmp_out_message:=tmp_out_message||'相关信息共有'||temp_count||'条,继续发送获取其他信息【找货宝】';
   end if;
   -------结束点--------
 <<ret_end>>
ret_mess   :=tmp_out_message;
end;
/

prompt
prompt Creating procedure WL_SMS_ZST_OPEARE_OPDETAIL_1
prompt ===============================================
prompt
create or replace procedure WL_SMS_ZST_OPEARE_OPDETAIL_1(
  MessID in varchar2,--消息ID
  In_phoneNum in varchar2,--手机号码
  In_Op in varchar2,--操作
  In_Mess in varchar2,--输入的内容
  Out_mess out varchar2--输出信息
  )
as
 temp_Count number(5):=0;--临时储存记录数
 temp_OutMess varchar2(300):='';--临时储存输出信息
 temp_Str varchar2(300):='';--临时储存字符串
 temp_str1 varchar2(300):='';--临时处理字符串2
 startStr varchar2(500):='';--起始地
 endStr varchar2(500):='';--目的地
 In_obj varchar2(500):='';--临时对象
 begin

  ----------------------判断该用户是否存在----------------
  select count(*) into temp_count from Wl_Cust_Mobile where nbr=In_phoneNum;

  --------------------获取客户端地址链接------------------
  if regexp_like(In_Op,'getrj','i') then
      temp_OutMess:='http://down.zhaohuobao.cn/ZHBao.apk';
      sp_down_insert(MessID,In_phoneNum ,'mandao',temp_OutMess,0 ,temp_count);
  end if;

  ------------------------订购业务------------------------
  if regexp_like(In_Op,'order','i') then
    ----该用户存在
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=1,Eff_Date=add_months(sysdate,to_number(30)) where nbr=In_phoneNum;
      select max(cust_ID) into temp_str from Wl_Cust_Mobile where nbr=In_phoneNum;
     --该用户不存在
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,add_months(sysdate,1),1) return CUST_ID into temp_str;
    end if;
     --生成订单
      wl_cust_order(temp_str,'10元包月',1,'自订购',sysdate,null,In_phoneNum,temp_OutMess);
      temp_OutMess :='感谢您订购找货宝业务!请回复“RJ”下载安卓手机软件;短信查询:请回复“某地到某地”。QQ查询:请加2577556895为好友。【找货宝】';
      sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------------取消业务--------------------------
  if regexp_like(In_Op,'exit','i') then
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=0,Eff_Date=sysdate where nbr=In_phoneNum;
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,sysdate,0);
    end if;
    insert into WL_SMS_ZST_SERVICE(PhoneNum,BizSate) values(In_phoneNum,'取消业务');
    WL_CUST_CannelOrder(In_phoneNum,temp_OutMess);
    temp_OutMess :='手机号码：'||In_phoneNum||'取消成功【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------绑定QQ----------------------------------
  if In_Op='bdqq' then
       In_obj:=regexp_substr(In_Mess,'\d+');
      ----------------查找是否存在其他人占用此QQ----------
      select count(*) into temp_Count from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------没有其他人使用此qq-----------------------
      if temp_Count=0 then
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
      ---------------查看存在该QQ的电话是否为空-----------
      select max(nbr) into temp_Str from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------存在qq 的电话为空则删除该条信息并将qq绑定到该号码下-----
      if temp_Str is null then
         delete  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum and (nbr is null);
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      else--否则去掉对方的qq并绑定自己QQ
         update Wl_Cust_Mobile set qq='' where qq=In_obj and nbr<>In_phoneNum;
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
   end if;

  ---司机的手机绑定销售编号 成为该销售编号的意向用户-----
  if regexp_like(In_op,'bdxs','i') then
     In_obj:=regexp_substr(In_Mess,'\d+');
     select max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       if temp_Str is null then
          insert into Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Last_Position_Date,Isorderssxy)values(In_phoneNum,sysdate,sysdate,sysdate,2);
           commit;
       end if;
       select max(CUST_ID) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       select count(*) into temp_Count  from wl_xs_salesach where oppsiteid=temp_Str;
       if temp_Count>0 then
           temp_OutMess:='该司机已被其他销售人员绑定';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
       else
           select max(saleid) into temp_count from wl_xs_salesman where sphone=In_obj;
           if temp_count is null then
               temp_OutMess:='此销售编号有误！';
               sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
               goto ret_end;
            else
                insert into wl_xs_salesach(saleid,xstype,oppsiteid,xstime,translation) values(temp_count,'司机',temp_Str,sysdate,0);
                 temp_OutMess:='绑定成功！';
               sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
                commit;
           end if;
       end if;
   end if;

  -----------------销售人员开通司机手机号-----------------
  if regexp_like(In_op,'ktsj','i') then

       -------------验证销售编号-----------------
        select count(*) into temp_Count from Wl_Xs_Salesman where sphone=In_phoneNum;
        if temp_Count=0 then
           temp_OutMess:='您发短信的手机非找货宝销售人员手机，不能为此号码开通找货宝业务，请让销售人员帮您开通，感谢您对找货宝的支持4006282515【找货宝】';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
           goto ret_end;
        end if;
        In_obj:=regexp_substr(In_Mess,'\d+');
        -------------判断该司机是否存在本库中--------
        select Max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_obj;
        if temp_Str is null  then
           insert into Wl_Cust_Mobile(Nbr,Create_Date,Isorderssxy,Isreg,Serchtimes)values(In_obj,sysdate,0,0,0)
            return CUST_ID into temp_Str;
        end if;

        ------判断该司机是否重复订购------
        select count(*) into temp_Count from wl_orderinfo where
             cust_id=temp_str and (expiry_date is null or expiry_date>sysdate);
        if temp_Count>0 then
            temp_OutMess:='销售代理您好，该号码已开通找货宝业务，无需重新开通，如有疑问，请咨询总部工作人员';
            sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
            goto ret_end;
        end if;
        --------------判断该手机号码是否被销售编号绑定----
        select count(*) into temp_Count from wl_xs_salesach where  oppsiteid=temp_Str;
        if temp_Count>0 then
             ---获取该的所属销售员
             select max(sphone) into temp_str1 from wl_xs_salesman where SaleId in(
                select Max(SALEID) from Wl_Xs_Salesach  where oppsiteid=temp_Str
             );

             ----给该销售员发送信息
             temp_OutMess:='手机号为15129030647已被其他销售员开通，将不再是您的意向用户';
             sp_down_insert('',temp_str1 ,'mandao' ,temp_OutMess ,0 ,temp_count);
             ---删除原来有的意向用户
            delete wl_xs_salesach where oppsiteid=temp_Str;
        end if;

        ------------------给司机开通12个月----------
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(12)), isORDERSSXY=1 where nbr=In_obj;
         commit;

         ----------根据手机号码获取销售人员的销售编号--
         select max(saleid) into temp_str1 from wl_xs_salesman where sphone=In_phoneNum;

         ---------------------生成订单----------------------
         wl_cust_order(temp_Str,'120元包年',1,'销售人员代购',sysdate,add_months(sysdate,12),temp_str1,temp_OutMess);
         --给销售代理下发信息
         temp_OutMess:='销售代理您好,您已经帮手机号码为'||In_obj||'的用户开通找货宝业务，请指导用户使用此业务，感谢对找货宝的支持 ';
         sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
         --给司机下发信息
         temp_OutMess:='尊敬的用户您好！您已经成功开通找货宝业务，感谢对找货宝的支持，请让销售人员指导您使用此业务，如有疑问，请咨询4006282515';
         sp_down_insert('',In_obj ,'mandao' ,temp_OutMess ,0 ,temp_count);

   end if;

  ------------------获取货源信息--------------------------
  if regexp_like(In_Op,'getinfo','i') then

     -----------------------判断该用户是否订购业务-----------------------
    select max(isorderssxy) into temp_count from wl_cust_mobile where nbr=In_phoneNum;
    if temp_count=0 or temp_count is null then
      temp_OutMess:='您尚未订购找货宝短信业务，无法使用该功能，若要使用请发送5137到10669588订购【找货宝】';
      goto ret_end;
    end if;

    -------------------------查询信息----------------------------
    WL_Cust_ZHBQQ_GetAddress(In_Mess,startStr,endStr);

    -----------------------地址格式有误-------------------------
    if startStr is null or endStr is null then
      temp_OutMess :='您输入的格式有误请输入:某地到某地【找货宝】';
      goto ret_end;
    end if;

    -----------------------地址格式正确-----------------------------
    temp_OutMess:=get_line_info_TXH_top(In_phoneNum,1,startStr,endStr);

    ------------------------更新最后一次查询时间--------------------
    update wl_cust_mobile set
           last_position_date=sysdate ,
           last_search=startStr||'到'||endStr,
           last_serch_data=Sysdate
           where  nbr=In_phoneNum;
    commit;

    -----------------------插入到短信记录中去------------------------
    insert into WL_SMS_ZST_Messages(PHONENUM,IN_MESS,OUT_MESS)
           values(In_phoneNum,In_Mess,temp_OutMess);
    commit;

    -------------------------查询记录条数-----------------------------
    select count(*) into temp_count from WL_info_data
               where info_start like startStr||'%'
               and info_end like endStr||'%';
    temp_OutMess:=temp_OutMess||'相关信息共有'||temp_count||'条,继续发送获取其他信息【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
   end if;
   ---------------结束点-----------------
   <<ret_end>>
   Out_mess:=temp_OutMess;
 end;
/

prompt
prompt Creating procedure WL_TABLE_BF
prompt ==============================
prompt
create or replace procedure wl_table_bf
Authid Current_User is
month_1 integer;
month_0 integer;
today_0 integer;
date_0  integer;
date_1  integer;
wl_sql varchar2(2000);

 begin
---------------------------------

---计算月天
select to_char(add_months(sysdate,-1),'yyyymm')  into month_1 from dual;--上月
select to_char(sysdate,'yyyymm')    into month_0  from dual;--当月
select to_char(sysdate,'yyyymmdd')  into date_0   from dual;--当天
select to_char(sysdate,'dd')        into today_0  from dual;--天
select to_char(sysdate-1,'yyyymmdd')into date_1  from dual;--昨天

------------------------每月一号---------------
if today_0='01' then

--表1：wl_info_data
wl_sql:='insert into wl_info_data_'||month_1||' 
 select * from wl_info_data where create_date<
 to_date('||date_0||','||'''yyyymmdd'''||')
 minus
 select * from wl_info_data_'||month_1||' ';
execute immediate wl_sql;
commit;

--表2:wl_info_data_log
wl_sql:='insert into wl_info_data_log_'||month_1||' 
 select * from wl_info_data_log where create_date<
 to_date('||date_0||','||'''yyyymmdd'''||')
 minus
 select * from wl_info_data_log_'||month_1||' ';
execute immediate wl_sql;
commit;

--表3：备份手机客户表：wl_cust_mobile
wl_sql:='create table wl_cust_mobile_'||month_1||' as
        select * from wl_cust_mobile';
execute immediate wl_sql;

--表4：备份信息部客户表:wl_cust_department
wl_sql:='create table wl_cust_department_'||month_1||' as
        select * from wl_cust_department';
execute immediate wl_sql;

   goto ret_end;
end if;


------------------------每月二号及以上---------------
--表1：wl_info_data
wl_sql:='insert into wl_info_data_'||month_0||' 
 select * from wl_info_data where create_date<
 to_date('||date_0||','||'''yyyymmdd'''||')
 minus
 select * from wl_info_data_'||month_0||' ';
execute immediate wl_sql;
commit;

--表2:wl_info_data_log
wl_sql:='insert into wl_info_data_log_'||month_0||' 
 select * from wl_info_data_log where create_date<
 to_date('||date_0||','||'''yyyymmdd'''||')
 minus
 select * from wl_info_data_log_'||month_0||' ';
execute immediate wl_sql;
commit;



/*insert into wl_info_data_201204
select * from wl_info_data where create_date<to_date('20120424','yyyymmdd')
minus
select * from wl_info_data_201204;*/

<<ret_end>>

--*************需要DELETE 原表*******************

--1.wl_info_data表每天删除，只保留昨天12点以后内容
/*wl_sql:='delete wl_info_data where create_date<
 to_date('||date_0||','||'''yyyymmdd'''||')';*/

wl_sql:='delete wl_info_data where create_date<trunc(sysdate-1)+0.5';
execute immediate wl_sql;
commit;


--2.wl_info_data_log表删除上月记录，只保留当月内容
wl_sql:='delete wl_info_data_log where create_date<
 to_date('||month_0||','||'''yyyymm'''||')';
execute immediate wl_sql;
commit;
end;
/

prompt
prompt Creating procedure WL_WEB_DATA_GETONTIME
prompt ========================================
prompt
create or replace procedure WL_Web_Data_GetOnTime(
/*定时获取数据库中更新的数据*/
 FromA in varchar2,--起始地
 toB in varchar2,--目的地
 oldCount in number,--未更新前的条数
 newCount out number,--信息数据
 retCur out wl_cust_department_getinfo.t_info--返回数据
)as
tempCount number(6):=0;
tempStart varchar2(100):='';
tempEnd varchar2(100):='';
begin
    ----获取起始地地址----
     if FromA is not null then
        tempStart:=WL_GetComAddress(FromA);
     end if;
     if toB is not null then
        tempEnd:=WL_GetComAddress(toB);
     end if;
  -----获取总记录数-----
     select count(*) into tempCount
      from Wl_Info_Data
      where 1=1 
      and info_start like trim(tempStart)||'%' 
      and info_end like trim(tempEnd)||'%'; 
      newCount:=tempCount;              
  ----查询该数据---
  open retCur for select s.* from 
      (
          select Id,c_name,create_date,Info_nbr1,info 
             from Wl_Info_Data 
             where info_start like trim(tempStart)||'%' 
             and info_End like trim(tempEnd)||'%'
             order by Create_date desc
       ) s 
       where rownum<=tempCount-oldCount;
  end;
/

prompt
prompt Creating procedure WL_XS_BINDDEP
prompt ================================
prompt
create or replace procedure wl_XS_BindDep(
  /*销售人员绑定信息部*/
  op in varchar2,
  In_saleid in number,
  In_depNbr in varchar2,
  In_depPass in varchar2,
  out_ret out varchar2
)
as
temp_count number(2):=0;
temp_str varchar2(80):='';
begin
   select count(*) into temp_count from Wl_Cust_Department where nbr=In_depNbr and c_password=In_depPass;
   if temp_count=0 then
       temp_str:='绑定失败：用户名或密码错误！';
       goto ret_end;
   end if;
   if In_saleid is null then
      temp_str:='销售编号有误！';
      goto ret_end;
   end if;
   if regexp_like(op,'bind') then
      update Wl_Cust_Department set saleId=In_saleid  where nbr=In_depNbr and c_password=In_depPass;
      commit;
      temp_str:='绑定成功！';
       goto ret_end;
   end if;
   if regexp_like(op,'exitBind') then
      update Wl_Cust_Department set saleId=''  where nbr=In_depNbr and c_password=In_depPass;
      commit;
      temp_str:='取消成功！';
   end if;
   <<ret_end>>
   out_ret:= temp_str;
end;
/

prompt
prompt Creating procedure WL_XS_GETBYMSALE
prompt ===================================
prompt
create or replace procedure WL_XS_GetByMSale(
sender in varchar2,
ret_cur out wl_get_cust_dep.t_info)
as
   ret_num number(5):=0; 
begin
   -----------------------管理员---------------------
   if regexp_like(sender,'admin','i') then
       open ret_cur for
            select saleId sID,
                   wl_xs_GetXS_Info(saleId,'name') sName,
                   wl_xs_GetXS_Info(saleId,'phone') sPhone,
                   wl_xs_getsecular('admin',saleId) shoudUP,
                   wl_Xs_getPayMent(saleId) sUped,
                   wl_xs_getsecular('admin',saleId)-wl_Xs_getPayMent(saleId) sNoup     
            from wl_xs_salesach
            where xstype='司机'
            and regexp_like(saleid,'^\d{3}000$')
            group by saleId; 
            goto ret_end;
   end if;
     -----------------一级代理查看二级代理销售状态级--------
   if regexp_like(sender,'^\d{3}000$','i') then
        open ret_cur for
            select saleId sID,
            wl_xs_GetXS_Info(saleId,'name') sName, 
            wl_xs_GetXS_Info(saleId,'phone') sPhone,
             wl_xs_getsecular(sender,saleId) shoudUP,
            wl_Xs_getPayMent(saleId) sUped,
            wl_xs_getsecular(sender,saleId)-wl_Xs_getPayMent(saleId) sNoup
            from wl_xs_salesach
            where saleid<to_number(sender)-(mod(to_number(sender),100))+1000
            and saleid>to_number(sender)-(mod(to_number(sender),100))
            and xstype='司机'
            group by saleid ;
            goto ret_end;
    end if;
    ------------------------代理查看自己的销售量情况-----------
    <<ret_other>>
    open ret_cur for 
        select c.nbr cPhone,c.c_name cName,s.xstime sDate 
         from wl_xs_salesach s 
         inner join wl_cust_mobile c
         on c.cust_id=s.oppsiteid
         where s.saleid=sender;
    <<ret_end>>
    ret_num:=0;
end;
/

prompt
prompt Creating procedure WL_XS_GETMYEARN
prompt ==================================
prompt
create or replace procedure WL_xs_GetMyEarn(
/*输入编号获取该销售人员盈利金额*/
 sender in varchar2,---销售ID
 outStr out varchar2--输入字符串
)as
temp_out varchar2(500):='';
temp_count number(8):=0;
temp_money number(8):=0;
temp_money_1 number(8):=0;
temp_money_up number(8):=0;
temp_all_money number(8):=0;
temp_shoud_inf number(8):=0;
begin
     /*管理员查看自己盈利状态*/
     if regexp_like(sender,'admin','i') then
          select count(*) into temp_count
             from Wl_Xs_Salesach
               where xstype='司机'
               and Translation=1;
               temp_all_money:=temp_count*120;
         temp_out:='总收入:'||to_char(temp_all_money)||'元;';
         /*获取一级销售人数销售的司机个数*/
         ---------------应收--------------------
         select count(*) into temp_count
             from Wl_Xs_Salesach
               where regexp_like(saleid,'\d{3}000','i')
               and xstype='司机'
               and Translation=1;
         temp_money:=temp_count*70;
         /*获取二级销售的销售司机个数*/
          select count(*) into temp_count
             from Wl_Xs_Salesach
               where regexp_instr(saleid,'\d{3}000')=0
               and xstype='司机'
               and Translation=1;
          temp_money:=temp_money+(temp_count*60);
          temp_shoud_inf:=temp_money;
           /*获取公司总收入*/
          temp_out:=temp_out||'应收:'||to_char(temp_money)||'元;';
          temp_out:=temp_out||'支出:'||to_char(temp_all_money-temp_money)||'元;';
           /*获取公司实际收入*/
          select sum(paymentamount) into temp_money
          from wl_xs_payment
          where regexp_like(saleid,'\d{3}000','i');
          if temp_money is null or trim(temp_money)='' then
              temp_money:=0;
          end if;
          temp_out:=temp_out||'已收:'||to_char(temp_money)||'元;';
          temp_out:=temp_out||'未收:'||to_char(temp_shoud_inf-temp_money)||'元;';
         goto ret_end;
     end if;
     /*一级销售人员查看自己销售多少个，销售金额是多少，二级销售工上交多少 一级销售应该上交多少，盈利多少*/
     if regexp_like(sender,'\d{3}000','i') then
        /*查看自己销售多少司机*/
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid=to_number(sender)
        and xstype='司机'
        and Translation=1;
        temp_out:='我总共销售司机:'||to_char(temp_count)||'个;';
        temp_out:=temp_out||'销售总金额:'||to_char(temp_count*120)||'元;';
        temp_money_1:=temp_count*50;--自己盈利金额
        temp_money_up:=temp_count*70;--应该上交
        temp_out:=temp_out||'盈利金额:'||to_char(temp_money_1)||'元;';
        temp_out:=temp_out||'应上交金额:'||to_char(temp_money_up)||'元;';
        /*查看该一级销售人员的二级销售人员销售司机个数*/
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid>to_number(sender)
        and saleid<to_number(sender)+1000
        and xstype='司机'
        and Translation=1;
        temp_out:=temp_out||'我的二级销售应该交我:'||to_char(temp_count*70)||'元;';
        temp_out:=temp_out||'我应该转交上级:'||to_char(temp_count*60)||'元;';
        temp_out:=temp_out||'从二级盈利:'||to_char(temp_count*10)||'元;';
        temp_out:=temp_out||'总上交:'||to_char(temp_count*60+temp_money_up)||'元;';
        -------------------------已经上交金额---------------------------------------
         temp_out:=temp_out||'已上交:'||to_char(wl_Xs_getPayMent(sender))||'元;';
          temp_out:=temp_out||'还未上交:'||to_char(wl_xs_getsecular('admin',sender)-wl_Xs_getPayMent(sender))||'元;';
        -------------------------未上交上交金额-------------------------------------
        temp_out:=temp_out||'我的总利润:'||to_char(temp_count*10+temp_money_1)||'元;';
        select sum(paymentamount) into temp_money
        from wl_xs_payment
        where saleid>to_number(sender)
        and saleid<to_number(sender)+1000;
        if temp_money is null or trim(temp_money)='' then
           temp_money:=0;
        end if;
        temp_out:=temp_out||'已获得的利润:'||to_char(temp_money_1+temp_money)||'元。';
        goto ret_end;
     end if;
     /*二级销售人员查看自己的销售总金额，应上交金额，已经上交金额， 未上交金额 ，总利润*/
     if regexp_instr(sender,'\d{3}000')=0 and regexp_instr(sender,'^\d{6}$')>0 then
        select count(*) into  temp_count
        from Wl_Xs_Salesach
        where saleid=to_number(sender)
        and xstype='司机'
        and Translation=1;
        temp_out:='销售总金额: '||to_char(temp_count*120)||'元;';
        temp_money_up:=temp_count*70;
        temp_money_1:=temp_count*50;
        temp_out:=temp_out||'应上交金额:'||to_char(temp_money_up)||'元;';
        /*已经上交金额*/
        select sum(paymentamount) into temp_money
        from wl_xs_payment
        where saleid=to_number(sender);
         if temp_money is null or trim(temp_money)='' then
           temp_money:=0;
        end if;
        temp_out:=temp_out||'已经上交金额:'||to_char(temp_money)||'元;';
        temp_out:=temp_out||'未上交金额:'||to_char(temp_money_up-temp_money)||'元;';
        temp_out:=temp_out||'总利润:'||to_char(temp_money_1)||'元;';
     end if;
    <<ret_end>>
    outStr:=temp_out;
  end;
/

prompt
prompt Creating procedure WL_XS_HANDONHISTORY
prompt ======================================
prompt
create or replace procedure WL_XS_handOnHistory(
   /*获取缴费和收费账单*/
   sender in varchar2,--查询者
   op in varchar2,--操作 in 收费账单 out 缴费账单
   ret_cur out wl_cust_dep_getinfo.t_info--查询结果
)
as
begin

  if regexp_like(op,'in','i') then
      if regexp_like(sender,'admin','i') then
       open ret_cur for  select a.sname 姓名,a.saleid 编号,a.sphone 电话,p.paymentamount 上交金额,p.paydate 上交时间,
          case
             when p.note='admin' then '找货宝公司'
          end 收款对象,
          case
              when p.note='admin' then '029-88764408'
          end 收款电话
          from wl_xs_payment p
           inner join wl_xs_salesman a
           on a.saleid=p.saleid
            where p.note=sender;
      else
         open ret_cur for  select a.sname 姓名,a.saleid 编号,a.sphone 电话,p.paymentamount 上交金额,p.paydate 上交时间
          from wl_xs_payment p
           inner join wl_xs_salesman a
           on a.saleid=p.saleid
            where p.note=sender;
      end if;
  end if;
  if regexp_like(op,'out','i') then
     open  ret_cur for  select p.saleid 编号,p.paymentamount 上交金额,p.paydate 上交时间,
          case
             when p.note='admin' then '找货宝公司'
             else wl_xs_getxs_info(p.note,'name')
          end 收款对象,
          case
              when p.note='admin' then '029-88764408'
               else wl_xs_getxs_info(p.note,'phone')
          end 收款电话
        from wl_xs_payment p
       where p.saleid=sender;
  end if;
end;
/

prompt
prompt Creating procedure WL_XS_PAYMENTM
prompt =================================
prompt
create or replace procedure wl_xs_PayMentM(
   /*销售人员交税*/
   saleid1 in varchar2,--销售编号
   payAmount in number,--缴费金额
   note1 in varchar2,--备注
   ret_out out varchar2--输入信息--一级销售编号
)as
temp_str varchar2(800):='';
temp_count number(8):=0;
begin
    if saleid1 is null or trim(saleid1)='' then
       temp_str:='销售编号无效';
       goto ret_end;
    end if;
     if regexp_like(saleid1,'\d{6}')=false then
        temp_str:='销售编号无效';
       goto ret_end;
     end if;
     select count(*) into temp_count from wl_xs_salesman 
     where saleid=saleid1;
     if temp_count=0 then
        temp_str:='销售编号无效';
         goto ret_end;
     end if;
     if regexp_like(note1,'\d{3}000') then
        if to_number(saleid1)<to_number(note1) or to_number(saleid1)>=to_number(note1)+1000 then
         temp_str:='缴费失败。该销售编号不属于您的二级代理';
         goto ret_end;
        end if;
     end if;
     if regexp_like(note1,'\d{3}000')=false and regexp_like(note1,'\d{6}') then
         temp_str:='您无权操作';
         goto ret_end;
     end if;
     insert into WL_XS_PayMent(saleid,Paymentamount,payDate,note)
       values(saleid1,payAmount,sysdate,note1);
       temp_str:='缴费成功';
    <<ret_end>>
    ret_out:=temp_str;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_ADDSIJI
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_addSiJi(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   in_phone varchar2,--司机司机手机号
   in_idCard varchar2,--司机身份证号码
   in_cname varchar2,--司机姓名
   in_carNumber varchar2,--司机车牌号
   in_carType in varchar2,--司机车类型
   in_like1 in varchar2,--司机长袍路线
   in_qq in varchar2 ,--司机QQ
   Out_ret out varchar2  --返回结果
)
as
 temp_cust_id varchar2(200):='';
 temp_str varchar2(400):='';
 temp_count number(3):=0;
begin
  if sxid is null then
     temp_str:='销售编号有误！';
      goto ret_end;
  end if;
  select count(*) into temp_count from wl_xs_salesman 
  where saleid =sxid;
   if temp_count=0 then
      temp_str:='销售编号有误！';
      goto ret_end;
   end if;
   if in_phone is null then 
       temp_str:='电话号码不能为空！';
      goto ret_end;
   end if;
   select max(cust_id) into temp_cust_id  from wl_cust_mobile 
     where nbr =in_phone;
   if temp_cust_id is null then
        insert into Wl_Cust_Mobile(
               nbr,Id_card,c_name,car_number,
               Car_Model,Like1,qq,create_date,eff_date
         )values
         (
            in_phone,in_idCard,in_cname,in_carNumber,
            in_carType,in_like1,in_qq,sysdate,sysdate
         ) return cust_id into temp_cust_id;
         commit;
    end if;
     wl_xs_sach_add(SXID,'司机',temp_cust_id,temp_count);
         if temp_count=0 then
            temp_str:='添加成功！';
            goto ret_end;
         elsif temp_count=4 then
            temp_str:='不能重复添加！';
            goto ret_end;
         elsif temp_count=5 then
             temp_str:='该用户已经被其他用户添加！';
              goto ret_end;
         end if;
   <<ret_end>>
  Out_ret:=temp_str;  
end;
/

prompt
prompt Creating procedure WL_XS_SACH_GETSIJI
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_GetSiJi(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   SiJiType number,--司机类型 0 所有用户 1 包年用户 2  包月用户 3 潜在用户
   XSType1 in varchar2,--销售类型
   keyWords in varchar2,--搜索相关的数据
   retCursor out wl_cust_dep_getinfo.t_info--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
         select c.* from (
           select * from wl_cust_department
               where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch
                  where SaleId=SXID and XSType='信息部'
               )
           )  c
           where c.NBR like ''||keyWords||'%'
              or c.C_NAME like ''||keyWords||'%'
              or c.ID_CARD like ''||keyWords||'%'
              or c.City like ''||keyWords||'%'
              or c.CHIEF like ''||keyWords||'%'
              or c.CONTACT like ''||keyWords||'%'
              or c.ADDRESS like ''||keyWords||'%'
              or c.SMSSIGN like ''||keyWords||'%';
      end if;
      if XSType1='司机' then
        if SiJiType=0 then
            open retCursor for  
               select m.*,t.* from (
                  select a.*,b.* from wl_orderinfo a 
                     inner join wl_productinfo b
                     on a.product_id=b.productid
                     ) t right join (
                        select * from Wl_Cust_Mobile where cust_id in
                    ( select oppsiteid from Wl_Xs_Salesach
                          where saleid=SXID and xstype='司机'
                    )) m
                  on t.cust_id=m.cust_id
                where m.NBR like ''||keyWords||'%'
               or m.C_NAME like ''||keyWords||'%'
               or m.City like ''||keyWords||'%'
               or m.CAR_NUMBER like ''||keyWords||'%'
               or m.CAR_MODEL like ''||keyWords||'%'
               or m.LIKE1 like ''||keyWords||'%' 
               or m.QQ like ''||keyWords||'%';
           elsif SiJiType=1 then
              open retCursor for  
                    select m.*,t.* 
                    from wl_cust_mobile m
                         inner join 
                         (
                         select o.cust_id,o.order_date,o.expiry_date,
                             o.order_count,o.order_type,o.order_operator,
                             p.product_name,p.product_type,p.product_value,
                             p.product_note from wl_orderinfo o 
                             inner join wl_productinfo p
                             on p.productid=o.product_id
                             where o.product_id='FFF0D80E7CC344198499B3892A17499D'
                           ) t 
                         on t.cust_id=m.cust_id
                 where t.order_operator=SXID;
            elsif SiJiType=2 then
                open retCursor for  
                    select m.*,t.* 
                    from wl_cust_mobile m
                         inner join 
                         (
                         select o.cust_id,o.order_date,o.expiry_date,
                             o.order_count,o.order_type,o.order_operator,
                             p.product_name,p.product_type,p.product_value,
                             p.product_note from wl_orderinfo o 
                             inner join wl_productinfo p
                             on p.productid=o.product_id
                             where o.product_id='96B6F04443E64C21819C9C68822E07F4'
                           ) t 
                         on t.cust_id=m.cust_id
                 where t.order_operator=SXID;
            elsif SiJiType=3 then
              open retCursor for  
               select m.*,t.* from (
                  select a.*,b.* from wl_orderinfo a 
                     inner join wl_productinfo b
                     on a.product_id=b.productid
                     ) t right join (
                        select * from Wl_Cust_Mobile where cust_id in
                    ( select oppsiteid from Wl_Xs_Salesach
                          where saleid=SXID and xstype='司机'
                          and oppsiteid not in (
                               select cust_id from wl_orderinfo 
                              where order_operator=SXID
                    ))) m
                  on t.cust_id=m.cust_id
                  ;
            end if;
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_SELECT
prompt ====================================
prompt
create or replace procedure WL_XS_SACH_Select(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   XSType1 in varchar2,--销售类型
   retCursor out wl_cust_dep_getinfo.t_info--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
           select * from wl_cust_department where Cust_ID in (
              select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='信息部'
           );
      end if;
      if XSType1='司机' then
        open retCursor for
           select * from wl_cust_mobile where Cust_ID in (
              select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='司机'
           );
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SACH_SELECT1
prompt =====================================
prompt
create or replace procedure WL_XS_SACH_Select1(
   /*查询我的司机或者我的信息部*/
   SXID in number,--销售ID编号
   XSType1 in varchar2,--销售类型
   keyWords in varchar2,--搜索相关的数据
   retCursor out wl_cust_dep_getinfo.t_info--返回结果集
)
as
begin
    if XSType1='信息部' then
        open retCursor for
         select c.* from (
           select * from wl_cust_department
               where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch
                  where SaleId=SXID and XSType='信息部'
               )
           )  c
           where c.NBR like ''||keyWords||'%'
              or c.C_NAME like ''||keyWords||'%'
              or c.ID_CARD like ''||keyWords||'%'
              or c.City like ''||keyWords||'%'
              or c.CHIEF like ''||keyWords||'%'
              or c.CONTACT like ''||keyWords||'%'
              or c.ADDRESS like ''||keyWords||'%'
              or c.SMSSIGN like ''||keyWords||'%';
      end if;
      if XSType1='司机' then
        open retCursor for
           select m.* from(
               select * from wl_cust_mobile where Cust_ID in (
                  select OppsiteId from WL_XS_salesAch where SaleId=SXID and XSType='司机'
               )
           )  m
           where m.NBR like ''||keyWords||'%'
           or m.C_NAME like ''||keyWords||'%'
           or m.City like ''||keyWords||'%'
           or m.CAR_NUMBER like ''||keyWords||'%'
           or m.CAR_MODEL like ''||keyWords||'%'
           or m.LIKE1 like ''||keyWords||'%' 
           or m.QQ like ''||keyWords||'%';
      end if;
  end;
/

prompt
prompt Creating procedure WL_XS_SALES_ADDUPDATE
prompt ========================================
prompt
CREATE OR REPLACE PROCEDURE WL_XS_SALES_AddUpdate(
  /*销售人员的管理的存储过程*/
    snId IN NUMBER,--销售编号
    sName1 IN varchar2,--销售人员姓名
    sGender1 IN varchar2,--销售人员性别
    sIDCard1 IN varchar2,--销售人员身份证号码
    sPhone1 IN varchar2,--电话号码
    sNode1 IN varchar2,--备注
    sOp in number,--操作编号 0 添加 1 修改
    ret out varchar2--结果编号 返回编号 添加成功  0 更新成功 1 销售编号无效  2 电话号码无效 
)as
retcount number(4):=0;
retStr varchar2(100):='';
begin
     /*添加*/
     if sOp=0 then
         --电话号码为空
         if sPhone1 is null or trim(sPhone1)='' then
            retStr:='电话号码为空';
            goto  ret_end;
          end if;
          select count(*) into retcount from WL_XS_salesman where saleId=snId;
          if retcount>0 then
            retStr:='该销售编号已存在！';
            goto  ret_end;
          end if;
          --添加信息并返回编号
          insert into WL_XS_salesman(saleId,sName,sGender,sIDCard,sPhone,sNode)
              values(snId,sName1,sGender1,sIDCard1,sPhone1,sNode1);
            retStr:='添加成功！'; 
            goto ret_end;
       end if;
       /*更新*/
       if sOp=1 then
            --电话号码为空
         if sPhone1 is null or trim(sPhone1)='' then
            retStr:='电话号码为空';
            goto  ret_end;
          end if;
          --判断该编号是否存在
           select count(saleId) into retcount from WL_XS_salesman where saleId=snId;
           --不存在
           if retcount=0 then
                retStr:='销售编号不存在';
                goto  ret_end;
           end if;
           --存在则更新
           update WL_XS_salesman set sName=sName1,sGender=sGender1,sIDCard=sIDCard1,sPhone=sPhone1,sNode=sNode1
              where saleId=snId;
              retStr:='修改成功！';
               goto ret_end;
         end if;
     <<ret_end>>
     ret:=retStr;
  end;
/

prompt
prompt Creating procedure WL_XS_SALES_SELECTDELE
prompt =========================================
prompt
create or replace procedure WL_XS_SALES_SelectDele(
    /*操作销售元列表 查询和删除*/
    snId in number,--销售人员Id
    keywords in varchar2,--搜索关键字
    Sop in number,--操作编号 0 删除 1 查询
    ret out number,--0 删除成功 1 销售编号无效 2 关键字无效
    retcur out WL_GET_CUST_DEP.t_info--返回的结果集
  )as
  retcount number(4):=0;
  begin
    /*执行删除操作*/
      if Sop=0 then
          --编号为空
          if snId is null then
              retcount:=1;
              goto ret_end;
            end if;
          --编号为‘’
          if trim(to_char(snId))='' then
               retcount:=1;
               goto ret_end;
            end if;
            --是否存在
            select count(saleId) into retcount from WL_XS_salesman where saleId=snId;
            --编号不存在
            if retcount=0 then
                 retcount:=1;
                 goto ret_end;
              end if;
             --删除该用户
             delete WL_XS_salesman where saleId=snId;
             delete WL_XS_salesAch where SaleId=snId;
              retcount:=0;
             goto ret_end;
        end if;
       /*执行查询操作*/
       if Sop=1 then
           --关键字为空查询全部
           if keywords is null or trim(keywords)='' then
               open retcur for select * from WL_XS_salesman order by sCreateDate desc;
               goto ret_end;
            end if;
            --关键字不为空蘑菇查询
           open retcur for
               select * from WL_XS_salesman where
               to_char(saleId) like ''||keywords||'%'
               or sName like ''||keywords||'%'
               or sGender like ''||keywords||'%'
               or sIDCard like ''||keywords||'%'
               or sPhone like ''||keywords||'%'
               or sNode like ''||keywords||'%' order by sCreateDate desc;
        end if;
      <<ret_end>>
      ret:=retcount;
    end;
/

prompt
prompt Creating procedure WL_XS_SALE_DEP_REG
prompt =====================================
prompt
create or replace procedure WL_XS_SALE_DEP_REG(
   /*改存储过程是信息部手机信息注册用户使用的*/
    phoneNum in varchar2,--手机号码
    Pwd in varchar2,--密码
    XSId in number,--销售人员ID
    Ret out number--返回值 0 成功 1 手机号码为空 2 密码为空 3 销售编号无效 4 手机号已经被注册过
)as
retcount number(6):=0;
opsiId varchar2(64):='';
begin
  /*判断手机号码*/
   if phoneNum is null then
     retcount:=1;
     goto ret_end;
   end if;
   select count(nbr) into retcount from wl_cust_department where nbr=phoneNum;
   if retcount>0 then
       retcount:=4;
       goto ret_end;
   end if;
   /*判断密码*/
   if Pwd is null then
     retcount:=2;
     goto ret_end;
   end if;
   /*注册信息部*/
   insert into wl_cust_department (nbr,contact,c_password,freesms,billingsms,issuperadmin,smssign)
     values(phoneNum,PhoneNum,Pwd,0,0,0,'【找货宝】') return CUST_ID into OpsiId;
   /*判断ID是否为空*/
    if XSId is null then
       retcount:=3;
       goto ret_end;
    end if;
    /*判断销售ID是否存在*/
    select count(SALEID) into retcount from  wl_xs_salesman where SALEID=XSId;
    if retcount=0 then
       retcount:=3;
       goto ret_end;
    end if;
    /*给销售人员插入对应的部门ID*/
    insert into WL_XS_salesAch(SaleId,XSType,OppsiteId)
        values(XSId,'信息部',opsiId);
      retcount:=0;
    /*给该部门分配一个信息销售编号*/
    select max(Saleid)+1 into retcount from Wl_Xs_Salesman where Saleid<(XSId+100000) and Saleid>=XSId;
    /*将销售编号放入销售人员列表*/
    insert into Wl_Xs_Salesman(SALEID,SPWD,Sname,Sgender,Sidcard,Sphone,Snode,Screatedate)
    values(retcount,'0000','','','',phoneNum,'',sysdate);
    /*将信息部和自身的销售编号绑定*/
    update wl_cust_department set saleId=retcount where cust_id=opsiId;
    commit;
  <<ret_end>>
  Ret:=retcount;
end;
/

prompt
prompt Creating package body WL_CUST_DEPARTMENT_GETINFO
prompt ================================================
prompt
create or replace package body wl_cust_department_getinfo as
procedure p_info(in_nbr in varchar2,cur_ref out t_info) is
--3.显示用户已发送信息（显示自己所有发布的信息）
--输入参数：in_nbr 号码
--输出游标进行显示
begin
open cur_ref for 'select * from ( select * from wl_info_from_department where nbr='||in_nbr||' order by create_date desc) where rownum<=20 ';
end p_info;

end wl_cust_department_getinfo;
/

prompt
prompt Creating package body WL_CUST_DEPARTMENT_GETTRUCK
prompt =================================================
prompt
create or replace package body wl_cust_department_gettruck as
procedure p_truck(in_nbr in varchar2,in_km in number,cur_ref out t_info) is
--4.显示附近车辆信息（显示信息部客户表的一个号码,对应**公里以内的手机客户信息）
--输入参数：in_nbr,IN_KM  信息部中某一个号码,公里数
--输出游标进行显示
  tmp_ct    number(10);
  nbr_lng   number(11,8);
  nbr_lat   number(11,8);
  tmp_str   varchar2(500);
  
begin

  -----判断号码存在与否--------
  select count(*) into tmp_ct  from wl_cust_department 
  where nbr=in_nbr and lng is not null and lat is not null;

  if tmp_ct = 0 then
       goto ret_end;
  end if;
 
  -----附nbr_lng,nbr_lat，找出in_km内的手机客户信息----
 select lng,lat into nbr_lng,nbr_lat from wl_cust_department 
 where nbr=in_nbr and lng is not null and lat is not null;
 
 
 tmp_str:='select get_distance('
 ||nbr_lng||','||nbr_lat||',last_lng,last_lat) km1,A.*
 from wl_cust_mobile a where get_distance('
 ||nbr_lng||','||nbr_lat||',last_lng,last_lat)<'||in_km;
 tmp_str:=tmp_str||' order by get_distance('||nbr_lng||','||nbr_lat||',last_lng,last_lat) ';
 goto real_end;
  -------结束点---------

<<ret_end>>

tmp_str:='select 0,a.* from wl_cust_mobile a where 1=0';

<<real_end>>
          
  ----------返回记录集---------------
open cur_ref for tmp_str;

end p_truck;

end wl_cust_department_gettruck;
/

prompt
prompt Creating package body WL_CUST_DEP_GETINFO
prompt =========================================
prompt
create or replace package body wl_cust_dep_getinfo as
procedure p_info
--F2：显示当前位置附近信息部、停车场的信息
--输入参数：in_sim,in_nbr,in_esn,in_imsi,in_lng,in_lat,in_city,in_km,c_type,key_wd
--1输入参数in_sim或in_lng或in_lat任一为空
--2客户不存在
--0成功
(in_sim  in varchar2  ---用户sim卡号（必选字段）
,in_nbr  in varchar2  ---用户号码
,in_esn  in varchar2  ---用户手机设备号码
,in_imsi in varchar2
,in_lng  in number    ---用户所在位置经度信息
,in_lat  in number    ---用户所在位置纬度信息
,in_city in varchar2  ---城市名称，可为空
,in_km   in number    ---附近公里数,可为空
,in_c_type  in varchar2  ---类型表示信息部或停车场
,in_key_wd  in varchar2  ---信息部客户表的模糊客户名称
,in_send_nbr  in varchar2  ----信息发送者名字
,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6成功
,cur_ref out t_info   ---返回记录集
 )
is
  ct              number(10);
  tmp_str         varchar2(500);
  tmp_km          number(10);
  tmp_in_city     varchar2(64);
  tmp_in_c_type   varchar2(64);
  tmp_in_key_wd   varchar2(64);
  tmp_in_send_nbr varchar2(30);
  begin
  -----参数空值校验---------
  if in_sim is null or in_lng is null or in_lng<0 
     or in_lat is null or in_lat<0 then 
       ret:=1;
       goto ret_end;
  end if;
  -----判断客户存在与否--------
  select count(*) into ct
  from wl_cust_mobile where sim=in_sim;
 
  if ct= 0 then   
     ret:=2;
     goto ret_end;
  end if;
  -----判断公里数是否为空---------
  if in_km is null or in_km<0 then tmp_km:=100;
  else tmp_km:=in_km;
  end if;
  -----生成游标动态语句---------
  tmp_str:='select get_distance('||in_lng||','||in_lat||',lng,lat) km ,A.*
            from wl_cust_department a 
            where get_distance('||in_lng||','||in_lat||',lng,lat)<'||tmp_km;
  -----判断in_city\in_c_type\in_key_wd是否为空---------
  tmp_in_city:=trim(in_city);
  if tmp_in_city is not null   then 
       tmp_str:=   tmp_str||' and city='||chr(39)||tmp_in_city||chr(39);
  end if;
  
  tmp_in_c_type:=trim(in_c_type);
  if tmp_in_c_type is not null  then 
       tmp_str:=   tmp_str||' and c_type='||chr(39)||tmp_in_c_type||chr(39);
  end if;

  tmp_in_key_wd:=trim(in_key_wd);
  if tmp_in_key_wd is not null  then 
       tmp_str:=   tmp_str||' and c_name like '||chr(39)||'%'||tmp_in_key_wd||'%'||chr(39);
  end if;

   tmp_in_send_nbr:=trim(in_send_nbr);
  if tmp_in_send_nbr is not null  then 
       tmp_str:=   tmp_str||' and nbr='||chr(39)||tmp_in_send_nbr||chr(39);
  end if;
  
  
   tmp_str:=  tmp_str||' order by km';

  ret:=0;

  
     -------结束点---------
  goto real_end;
 

<<ret_end>>

tmp_str:='select 0 km ,a.*
          from wl_cust_department a where 1=0';
<<real_end>>

  ----------返回记录集---------------
  open cur_ref for 'select * from ( '||tmp_str||' ) where rownum<=100  ';
  
ret:=ret;

end p_info;

end wl_cust_dep_getinfo;
/

prompt
prompt Creating package body WL_CUST_FRIEND_LIST
prompt =========================================
prompt
create or replace package body wl_cust_friend_list as
 --好友分组展示
 --输入参数：客户ID
 --输出参数：好友游标集 ret 1输入参数为空,2无此客户,0成功
  procedure p_info
    (in_cust_id        in varchar2
    ,ret               out number   ---返回参数
    ,cur_ref           out t_info   ---返回记录集
    )
is
  tmp_ct            number(10);
  tmp_str           varchar2(500);
begin

  -----输入参数空值校验---------
  if in_cust_id is null
  then ret:=1;
       goto ret_end;
  end if;
  -----cust_id记录校验-----------------
  select count(*) into tmp_ct from  wl_cust_department
  where cust_id=in_cust_id;

  if tmp_ct<>1
  then ret:=2;
       goto ret_end;
  end if;
  ------输出好友游标集----------
  tmp_str:='';

  tmp_str:=tmp_str||'select friend_group,count(*)
  from wl_cust_friend where cust_id='||chr(39)
  ||in_cust_id||chr(39)||'group by friend_group';


  --dbms_output.put_line(tmp_str);
  open cur_ref for tmp_str;
  ret:=0;
  goto real_end;
  -------结束点---------
<<ret_end>>
open cur_ref for 'select friend_group,count(*) 
from wl_cust_friend where 1=0 group by friend_group';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_friend_list;
/

prompt
prompt Creating package body WL_CUST_FRIEND_QUERY
prompt ==========================================
prompt
create or replace package body wl_cust_friend_query as
 --好友列表展示
 --输入参数：客户ID
 --输出参数：好友游标集 ret 1输入参数为空,2无此客户,0成功
  procedure p_info
    (in_cust_id        in varchar2
    ,ret               out number   ---返回参数
    ,cur_ref           out t_info   ---返回记录集
    )
is
  tmp_ct            number(10);
  tmp_str           varchar2(500);
begin

  -----输入参数空值校验---------
  if in_cust_id is null
  then ret:=1;
       goto ret_end;
  end if;
  -----cust_id记录校验-----------------
  select count(*) into tmp_ct from  wl_cust_department
  where cust_id=in_cust_id;

  if tmp_ct<>1
  then ret:=2;
       goto ret_end;
  end if;
  ------输出好友游标集----------
  tmp_str:='';

  tmp_str:=tmp_str||'select * from wl_cust_friend where 
           cust_id='||chr(39)||in_cust_id||chr(39);

  --dbms_output.put_line(tmp_str);
  open cur_ref for tmp_str;
  ret:=0;
  goto real_end;
  -------结束点---------
<<ret_end>>
open cur_ref for 'select * from wl_cust_friend where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_friend_query;
/

prompt
prompt Creating package body WL_CUST_GETINFO
prompt =====================================
prompt
create or replace package body wl_cust_getinfo as
/*
  XueHaiFeng
  LastModified:2013-03-27
*/
procedure p_info
  (in_imsi in varchar2,  ---用户imsi卡号（必选字段）
   in_line in varchar2,  ---指定线路:可空 包括：省市省市    陕西,榆林,河南,河南省的全部检索(陕西,全部,全部,全部)
   ret     out number,   ---返回参数0为正常返回。1、in_imsi为空或试用；2、3、4为措施。9、8、7、6
   cur_ref out t_info   ---返回记录集
  )
  is
  ct number(10);
  tmp_str varchar2(2000);
  tmp_infoid_str varchar2(1000);
  tmp_in_ct varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  begin
    tmp_str:='';
    -----参数空值校验---------
    if trim(in_imsi) is null then
         ret:=1;
         goto ret_end;
    end if;
    -----判断用户，老用户更新位置，新用户插入记录-----
    select count(*) into ct
       from wl_cust_mobile where imsi=in_imsi;
    if ct> 0 then
       update wl_cust_mobile
           set --last_lng=in_lng,last_lat=in_lat,
               last_position_date=sysdate
              where imsi =in_imsi;
       commit;
       --ret:=0;---更新用户
    end if;
  
    ----------返回记录集
    --按指定线路返回
    if trim(in_line) is not null then
      tmp_in_ct:= 15;
      -------in_line输入字符串拆分----------
      --格式:陕西,榆林,陕西,西安 (陕西,全部,全部,全部)
      from_province:=trim(substr(in_line,1,instr(in_line,',',1,1)-1));
      from_city:=trim(substr(in_line,instr(in_line,',',1,1)+1,instr(in_line,',',1,2)-instr(in_line,',',1,1)-1));
      to_province:=trim(substr(in_line,instr(in_line,',',1,2)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
      to_city:=trim(substr(in_line,instr(in_line,',',1,3)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
      tmp_infoid_str:=get_line_info_id_top(in_imsi,tmp_in_ct,from_province,from_city,to_province,to_city);

      if from_province is null or from_province='全部' then
        ret:=2;
        goto ret_end;
      end if ;

      if tmp_infoid_str is null then
        ret:=7;
        goto ret_end;
      end if;
      -------更新最后查询的条件--------------------------
      if from_province is not null and from_city is not null and
        to_province is not null and to_city is not null then
        update wl_cust_mobile
          set last_position_date=sysdate ,last_search=from_province||from_city||'到'||to_province||to_city
            where  sim=to_char(in_imsi);
        commit;  ---更新用户 
      end if ;
    end if;
    tmp_str:='';
    tmp_str:=tmp_str||'select * from wl_info_data where  id in ('||tmp_infoid_str||')
                      order by create_date desc';
    open cur_ref for tmp_str;
    ------调整信息权值-----------------------------
    tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_infoid_str||' )';
    execute immediate tmp_str;
    commit;

    ------记录访问信息------无记录的插入，更新记录时间-----------------------
    tmp_str:='';
    tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
    tmp_str:=tmp_str||'select id, '||chr(39)||in_imsi||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_infoid_str||' )';
    tmp_str:=tmp_str||' minus ';
    tmp_str:=tmp_str||'select info_id, '||chr(39)||in_imsi||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_infoid_str||')
                      and   sim='||chr(39)||in_imsi||chr(39);
   --dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_infoid_str||') and sim= '||chr(39)||in_imsi||chr(39);
     --dbms_output.put_line(tmp_str);
     execute immediate tmp_str;
   commit;

goto real_end;

   -------结束点---------
<<ret_end>>
open cur_ref for 'SELECT * FROM wl_info_data where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_getinfo;
/

prompt
prompt Creating package body WL_CUST_GETINFO_T
prompt =======================================
prompt
create or replace package body wl_cust_getinfo_t as
/*
  XueHaiFeng
  LastModified:2013-03-27
*/
procedure p_info
  (in_imsi in varchar2,  ---用户imsi卡号（必选字段）
   in_line in varchar2,  ---指定线路:可空 包括：省市省市    陕西,榆林,河南,河南省的全部检索(陕西,全部,全部,全部)
   ret     out number,   ---返回参数0为正常返回。1、in_imsi为空或试用；2、3、4为措施。9、8、7、6
   cur_ref out t_info   ---返回记录集
  )
  is
  ct number(10);
  tmp_str varchar2(2000) :='';
  tmp_infoid_str varchar2(1000);
  tmp_in_ct varchar2(4) :=15;
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  rsign number(10) :=0;
  begin
    --tmp_str:='';
    -----参数空值校验---------
    if in_imsi is null or trim(in_imsi)='' then
      ret:=1;
      goto ret_end;
    end if;
    
    if in_line is null or trim(in_line)='' then
      ret:=2;
      goto ret_end;
    end if;
    
    select ISORDERSSXY into rsign from Wl_Cust_Mobile where imsi=in_imsi;
    if rsign=0 then
      --未订购定购时尚心语业务
      rsign:=5;
      tmp_in_ct:=5;
      goto ret_end;
    end if;

    --按指定线路返回返回记录集
    --in_line字符串拆分。格式:陕西,榆林,陕西,西安 (陕西,全部,全部,全部)
    from_province:=trim(substr(in_line,1,instr(in_line,',',1,1)-1));
    from_city:=trim(substr(in_line,instr(in_line,',',1,1)+1,instr(in_line,',',1,2)-instr(in_line,',',1,1)-1));
    to_province:=trim(substr(in_line,instr(in_line,',',1,2)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
    to_city:=trim(substr(in_line,instr(in_line,',',1,3)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
    tmp_infoid_str:=get_line_info_id_top(in_imsi,tmp_in_ct,from_province,from_city,to_province,to_city);

    if from_province is null or from_province='全部' then
      ret:=2;
      goto ret_end;
    end if ;

    if tmp_infoid_str is null then
      ret:=7;
      goto ret_end;
    end if;

    tmp_str:='';
    tmp_str:=tmp_str||'select * from wl_info_data where  id in ('||tmp_infoid_str||')
                      order by create_date desc';
    open cur_ref for tmp_str;
    ------调整信息权值-----------------------------
    tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_infoid_str||' )';
    execute immediate tmp_str;
    commit;

    ------记录访问信息------无记录的插入，更新记录时间-----------------------
    tmp_str:='';
    tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
    tmp_str:=tmp_str||'select id, '||chr(39)||in_imsi||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_infoid_str||' )';
    tmp_str:=tmp_str||' minus ';
    tmp_str:=tmp_str||'select info_id, '||chr(39)||in_imsi||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_infoid_str||')
                      and   sim='||chr(39)||in_imsi||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_infoid_str||') and sim= '||chr(39)||in_imsi||chr(39);
   dbms_output.put_line(tmp_str);
     execute immediate tmp_str;
   commit;

goto real_end;

   -------结束点---------
<<ret_end>>
open cur_ref for 'SELECT * FROM wl_info_data where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_getinfo_t;
/

prompt
prompt Creating package body WL_CUST_MOBILE_CITYLIST
prompt =============================================
prompt
create or replace package body wl_cust_mobile_citylist as
procedure p_info
--F6:显示当前位置货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     /*0更新用户位置成功
       9新用户创建位置成功*/ 
--8按经纬查无货源信息
--7按线路查无货源信息
(in_sim  in varchar2  ---用户sim卡号（必选字段）
,in_nbr  in varchar2  ---用户号码
,in_esn  in varchar2  ---用户手机设备号码
,in_imsi in varchar2
,in_lng  in number    ---用户所在位置经度信息
,in_lat  in number    ---用户所在位置纬度信息
,in_line in varchar2  ---指定线路:可空 包括：省市省市    陕西,榆林,河南,河南省的全部检索(陕西,全部,全部,全部)
,in_ct   in varchar2  ---需要返回行数
,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
,cur_ref out t_info   ---返回记录集
 )

is
  ct number(10);
  tmp_str varchar2(2000);
  tmp_infoid_str varchar2(1000);
  tmp_in_ct varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  begin
  tmp_str:='';
 -- type cursor_type is ref cursor;
--  c1 cursor_type;
 -- v_info_date hr.wl_info_data%ROWTYPE;
  -----参数空值校验---------
  if trim(in_sim) is null or in_lng is null or in_lng<=0
                    or in_lat is null or in_lat<=0
     then
       ret:=1;
       goto ret_end;
  end if;
  -----判断用户，老用户更新位置，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where sim=in_sim;
  if ct> 0 then
     update wl_cust_mobile set last_lng=in_lng,last_lat=in_lat,last_position_date=sysdate
     where sim=in_sim;
     commit;
     --ret:=0;---更新用户
  else
     insert into wl_cust_mobile(sim,nbr,esn,imsi,last_lng,last_lat,last_position_date)
     values (in_sim,in_nbr,in_esn,in_imsi,in_lng,in_lat,sysdate);
     commit;
     --ret:=9;---插入用户
  end if;
  ---------输出附近公里货源信息
  ----------返回记录集
 
  
  --按指定线路返回
  if trim(in_line) is not null and regexp_instr(lxf_replace_place_name(in_line),'#P\d+#')>0 then   
   tmp_str:='select (case when substr(info_end,1,2)='||chr(39)||'黑龙%'||Chr(39)||' then '||Chr(39)||'黑龙江'||Chr(39)
       ||'             when substr(info_end,1,2)='||chr(39)||'内蒙%'||Chr(39)||' then '||Chr(39)||'内蒙古'||Chr(39)||'  
                        else substr(info_end,1,2) end) 省份,count(*)  条数
          from wl_info_data 
          where info_start like '||chr(39)||'%'||in_line||'%'||Chr(39)||'
          group by  substr(info_end,1,2)
          order by count(*) desc';
  open cur_ref for tmp_str;
      else
  open cur_ref for 'SELECT substr(info_end,1,2) 省份,count(*) 条数 FROM wl_info_data where 1=0  group by  substr(info_end,1,2)';
  end if ;
   -------结束点---------

<<ret_end>>
<<real_end>>
ret:=ret;
end p_info;

end wl_cust_mobile_citylist;
/

prompt
prompt Creating package body WL_CUST_MOBILE_GETINFO
prompt ============================================
prompt
create or replace package body wl_cust_mobile_getinfo as
procedure p_info(instr in varchar2,cur_ref out t_info) is

begin

open cur_ref for

'SELECT * FROM wl_cust_mobile WHERE nbr='||instr||'
or sim='||instr;


end p_info;

end wl_cust_mobile_getinfo;
/

prompt
prompt Creating package body WL_CUST_MOBILE_GETINFO6
prompt =============================================
prompt
create or replace package body wl_cust_mobile_getinfo6 as
procedure p_info
--F6:显示当前位置货源信息
--1输入sim为空或经纬为空或经纬负值
--2起始省为空
     /*0更新用户位置成功
       9新用户创建位置成功*/
--8按经纬查无货源信息
--7按线路查无货源信息
(in_sim  in varchar2  ---用户sim卡号（必选字段）
,in_nbr  in varchar2  ---用户号码
,in_esn  in varchar2  ---用户手机设备号码
,in_imsi in varchar2
,in_lng  in number    ---用户所在位置经度信息
,in_lat  in number    ---用户所在位置纬度信息
,in_line in varchar2  ---指定线路:可空 包括：省市省市    陕西,榆林,河南,河南省的全部检索(陕西,全部,全部,全部)
,in_ct   in varchar2  ---需要返回行数
,ret     out number   ---返回参数0为正常返回。1、2、3、4为措施。9、8、7、6
,cur_ref out t_info   ---返回记录集
 )

is
  ct number(10);
  tmp_str varchar2(2000);
  tmp_infoid_str varchar2(1000);
  tmp_in_ct varchar2(4);
  from_province   varchar2(64);   ----起始省份
  from_city       varchar2(64);   ----起始城市
  to_province     varchar2(64);   ----目的省份
  to_city         varchar2(64);   ----目的城市
  begin
  tmp_str:='';
 -- type cursor_type is ref cursor;
--  c1 cursor_type;
 -- v_info_date hr.wl_info_data%ROWTYPE;
  -----参数空值校验---------
  if trim(in_sim) is null --or in_lng is null or in_lng<=0
                   -- or in_lat is null or in_lat<=0
     then
       ret:=1;
       goto ret_end;
  end if;
  -----判断用户，老用户更新位置，新用户插入记录-----
  select count(*) into ct
  from wl_cust_mobile where sim=in_sim;
  if ct> 0 then
     update wl_cust_mobile set last_lng=in_lng,last_lat=in_lat,last_position_date=sysdate
     where sim=in_sim;
     commit;
     --ret:=0;---更新用户
  else
     insert into wl_cust_mobile(sim,nbr,esn,imsi,last_lng,last_lat,last_position_date,create_date,eff_date)
     values (in_sim,in_nbr,in_esn,in_imsi,in_lng,in_lat,sysdate,sysdate,sysdate+30);
     commit;
     --ret:=9;---插入用户
  end if;
  ---------输出附近公里货源信息
  ----------返回记录集
  --返回条数
  if trim(in_ct)is not null and trim(in_ct)>0 and trim(in_ct)<20
      then tmp_in_ct :=trim(in_ct);
   else   tmp_in_ct :='20';
  end if;

  --按经纬度返回
  if trim(in_line) is null then
     tmp_infoid_str:=get_info_id_top(in_sim,in_lng,in_lat,tmp_in_ct);

      if tmp_infoid_str is null then
         ret:=8;
         goto ret_end;
      end if;
   end if;

  --按指定线路返回
  if trim(in_line) is not null then
      -------in_line输入字符串拆分----------
      --格式:陕西,榆林,陕西,西安 (陕西,全部,全部,全部)
   from_province:=trim(substr(in_line,1,instr(in_line,',',1,1)-1));
   from_city:=trim(substr(in_line,instr(in_line,',',1,1)+1,instr(in_line,',',1,2)-instr(in_line,',',1,1)-1));
   to_province:=trim(substr(in_line,instr(in_line,',',1,2)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
   to_city:=trim(substr(in_line,instr(in_line,',',1,3)+1,instr(in_line,',',1,3)-instr(in_line,',',1,2)-1));
   tmp_infoid_str:=get_line_info_id_top
     (in_sim,tmp_in_ct,from_province,from_city,to_province,to_city);

      if from_province is null or from_province='全部' then
         ret:=2;
         goto ret_end;
      end if ;

      if tmp_infoid_str is null then
         ret:=7;
         goto ret_end;
      end if;
      -------更新最后查询的条件--------------------------
   if from_province is not null  and  from_city is not null  
      and to_province is not null  and  to_city is not null  then
      update wl_cust_mobile set last_position_date=sysdate ,last_search=from_province||from_city||'到'||to_province||to_city
      where  sim=to_char(in_sim) ;
      commit;  ---更新用户    
   end if ;
      
      
      
  end if;


   tmp_str:='';
   tmp_str:=tmp_str||'select * from wl_info_data where  id in ('||tmp_infoid_str||')
                      order by create_date desc';

  open cur_ref for tmp_str;

   ------调整信息权值-----------------------------
  tmp_str:= 'update wl_info_data set num=num-1 where id  in( '||tmp_infoid_str||' )';
  execute immediate tmp_str;
  commit;

   ------记录访问信息------无记录的插入，更新记录时间-----------------------
   tmp_str:='';
   tmp_str:=tmp_str||'insert into wl_info_data_log (info_id,sim)';
   tmp_str:=tmp_str||'select id, '||chr(39)||in_sim||chr(39)||' in_sim from  wl_info_data where id in ('||tmp_infoid_str||' )';
   tmp_str:=tmp_str||' minus ';
   tmp_str:=tmp_str||'select info_id, '||chr(39)||in_sim||chr(39)||'  in_sim  from wl_info_data_log
                      where info_id in ('||tmp_infoid_str||')
                      and   sim='||chr(39)||in_sim||chr(39);
   dbms_output.put_line(tmp_str);
   execute immediate tmp_str;
   commit;

   tmp_str:='';
   tmp_str:=tmp_str||'update wl_info_data_log set create_date =sysdate ';
   tmp_str:=tmp_str||' where info_id in ('||tmp_infoid_str||') and sim= '||chr(39)||in_sim||chr(39);
    dbms_output.put_line(tmp_str);
     execute immediate tmp_str;
   commit;

goto real_end;

   -------结束点---------
<<ret_end>>
open cur_ref for 'SELECT * FROM wl_info_data where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_mobile_getinfo6;
/

prompt
prompt Creating package body WL_CUST_SMS_FIND
prompt ======================================
prompt
create or replace package body wl_cust_sms_find as
 --短信2:找出待发短信记录
 --输入参数：多少个短信ID,
 --输出参数：游标集：短信ID,收信人号码,短信内容,
 --ret 0为正常返回。1输入条数为空或小于1或大于100，2无待发数据
  procedure p_info
    (in_ct   in number    ---输入索取待发多少条短信
    ,ret     out number   ---返回参数
    ,cur_ref out t_info   ---返回记录集
    )
is
  tmp_str       varchar2(5000);
  tmp_msgid_str varchar2(5000);

  begin

  -----输入参数空值校验---------
  if in_ct is null or in_ct<1 or in_ct>100
  then ret:=1;
       goto ret_end;
  end if;
  ------找出待发送的----------          
  tmp_msgid_str:=get_line_msg_id_top(in_ct);--前多少条短信
   
/* state:-1待发,0成功,2正在发送,1失败,-2过期
   get_line_msg_id_top:
   select chr(39)||msg_id||chr(39) from 
   (select msg_id from wl_cust_sms where state=-1 group by msg_id)
   where rownum<='||in_ct;;
*/
       
  if tmp_msgid_str is null 
  then ret:=2;
       goto ret_end;
  end if;

  
  tmp_str:='';
  tmp_str:=tmp_str||'select msg_id,receiver_nbr,contents from wl_cust_sms 
                     where state=-1 and msg_id in ('||tmp_msgid_str||')';
  dbms_output.put_line(tmp_str);
  open cur_ref for tmp_str;

  ------调整信息权值-----------------------------
  tmp_str:= 'update wl_cust_sms set state=2,fail=fail-1 
             where state=-1 and msg_id in( '||tmp_msgid_str||' )';
  execute immediate tmp_str;
  commit;
  ret:=0;
  goto real_end;

   -------结束点---------
<<ret_end>>
open cur_ref for 'select msg_id,receiver_nbr,contents from 
                  wl_cust_sms where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_cust_sms_find;
/

prompt
prompt Creating package body WL_GET_CUST_DEP
prompt =====================================
prompt
create or replace package body wl_get_cust_dep as
 --输入cust_id,
 --输出参数：信息部客户表:wl_cust_department游标集
 --ret 1输入客户ID为空，2无此记录，0为正常返回
  procedure p_info
    (in_cust_id   in varchar2
    ,ret          out number
    ,cur_ref      out t_info   ---返回记录集
    )
is
  tmp_ct           number(10);
  tmp_str       varchar2(500);


  begin

  -----输入参数空值校验---------
  if in_cust_id is null
  then ret:=1;
       goto ret_end;
  end if;
  -----记录校验-----------------
  select count(*) into tmp_ct from  wl_cust_department
  where cust_id=in_cust_id;

  if tmp_ct<>1
  then ret:=2;
       goto ret_end;
  end if;
  ------输出客户表游标集----------
  tmp_str:='';
  tmp_str:=tmp_str||'select * from wl_cust_department
                     where cust_id='||chr(39)||in_cust_id||chr(39);

  --dbms_output.put_line(tmp_str);
  open cur_ref for tmp_str;
  ret:=0;
  goto real_end;

   -------结束点---------
<<ret_end>>
open cur_ref for 'select * from wl_cust_department where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_get_cust_dep;
/

prompt
prompt Creating package body WL_MOBILE_RING_QUERY
prompt ==========================================
prompt
create or replace package body wl_mobile_ring_query as
procedure p_info
--外呼管理
--1.随机获取5条，最近外呼时间最早 并且最近外呼时间大于4小时 并且 状态字段为1
--的五条信息，然后更改此5条记录的“最近外呼时间”
--存储过程获取 联系人电话 姓名 车号 QQ号码 车型 常跑路线  手机类型  
--外呼人  最近外呼时间。
--输入参数:in_ct=1
--输出参数:ret 0成功输入,1输入参数错,2无符合记录

(in_ct   in varchar2  ---需要返回行数
,ret     out number   ---返回参数0为正常返回。1输入参数错，2无符合记录
,cur_ref out t_info   ---返回记录集
 )

is

  tmp_ct            number(10);
  tmp_nbr           varchar2(500);
  tmp_str           varchar2(500);
  tmp_date          date;
  
begin

  -----输入参数空值校验---------
  if in_ct is null or in_ct>1
  then ret:=1;
       goto ret_end;
  end if;
  -----记录校验-----------------
  select count(*) into tmp_ct from wl_mobile_ring 
  where (sysdate-last_ring_date)*24>4 and state=1;
  
  if tmp_ct=0
  then ret:=2;
       goto ret_end;
  end if;
  -----随机获取1条-----------------
  select nbr into tmp_nbr from
  (select * from wl_mobile_ring 
  where (sysdate-last_ring_date)*24>4 and state=1 
  order by last_ring_date)
  where rownum<=1;

dbms_output.put_line(tmp_nbr);
  -----更新外呼司机表时间-------------------
  tmp_date:=sysdate;

  update wl_mobile_ring set last_ring_date=tmp_date where nbr=tmp_nbr;
  commit;
   
  ------输出外呼司机游标集----------
  tmp_str:='';

  tmp_str:=tmp_str||'select * from wl_mobile_ring where 
           nbr  in( '||chr(39)||tmp_nbr||chr(39)||' )';

  dbms_output.put_line(tmp_str);
  open cur_ref for tmp_str;
  ret:=0;
  goto real_end;
  -------结束点---------
<<ret_end>>
open cur_ref for 'select * from wl_mobile_ring where 1=0';

<<real_end>>
ret:=ret;
end p_info;

end wl_mobile_ring_query;
/


spool off
