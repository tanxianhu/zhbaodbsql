----------------------------------------------
-- Export file for user HR                  --
-- Created by Yangfan on 2013/6/7, 16:43:59 --
----------------------------------------------

set define off
spool sh_zhbao_UpdateRollBack_v1_0_2.log

alter table wl_xs_salesman drop column sumsale;

alter table wl_xs_salesman drop column sumup;

drop function GETSHOULDUPCOUNT;

drop procedure WL_SALE_ADD_DRIVER;

drop procedure WL_SALE_DELETE;

drop procedure WL_SALE_GETCUSTMOBILE;

drop procedure WL_SALE_GETPAYMENT;

drop procedure WL_SALE_LOOK_SIJI;

drop procedure WL_SALE_PAYMENTM;

drop procedure WL_SALE_REGISTER;

drop procedure WL_SALE_SACH_DELETE;

drop procedure WL_SALE_SELECTSALE;

drop procedure WL_SALE_SELECT_LOGIN;

drop procedure WL_SALE_UPDATE;

drop procedure WL_SALE_UPDATECUSTMOBILE;


prompt
prompt Creating procedure SP_DOWN_INSERT
prompt =================================
prompt
create or replace procedure SP_Down_Insert(
   In_MessId in varchar2,--短信Id
   In_ReceiVer in varchar2,--接收者
   In_Sender in varchar2,--发送者
   In_content in varchar2,--发送的内容
   Priority in number,--优先级别
   ret_out out number--操作结果 0 成功 1 接收者为空 2 发送者为空 3 内容为空
)
as
temp_out number(5);
temp_Priority number(2);
begin
   temp_Priority:=Priority;
   --验证接收者
   if In_ReceiVer is null then
      temp_out:=1;
      goto ret_end;
   end if;
   --验证发送者
   if In_Sender is null then
       temp_out:=2;
      goto ret_end;
   end if;
   --验证内容
   if In_content is null then
        temp_out:=3;
      goto ret_end;
   end if;
   --验证优先级
   if temp_Priority is null then
      temp_Priority:=0;
   end if;
   insert into SP_DOWN(ID,MessageId,ReCEIVER,Sender,Content,sendDate,priority,state)
      values(sp_down_ID.Nextval,In_MessId,In_ReceiVer,In_Sender,In_content,Sysdate,temp_Priority,0);
      commit;
       temp_out:=0;
   <<ret_end>>
   ret_out:=temp_out;
end;
/

prompt
prompt Creating procedure SP_GETSPCODE
prompt ===============================
prompt
create or replace procedure SP_GetSPCODE(
  ret_cur out sys_refcursor--返回结果集
)
as
begin
  open ret_cur for select * from sp_code;
end;
/

prompt
prompt Creating procedure SP_GET_DOWNMES
prompt =================================
prompt
create or replace procedure SP_Get_downMes(
  /*获取下行预发送短信*/
  in_count in number,--一次获取条数
  ret_out out sys_refcursor--返回的结构集
)
as
begin
    /*0未发送，1发送成功，2正在发送，3发送失败,4 已发送未返回状态*/
    update sp_down set state=4 where state=2;
    commit;
    update sp_down set state=2 where sp_down.id in(
      select id from (
           select sp_down.id from sp_down where state=0
            order by priority desc
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_down where state=2;
  end;
/

prompt
prompt Creating procedure SP_GET_UPSMS
prompt ===============================
prompt
create or replace procedure SP_Get_UPSMS(
  in_count in number,--获取条数
  ret_out out sys_refcursor--返回结果集
)
as
begin
    --状态 0 为处理 1 正在处理 2 已处理
    update sp_up set state=2 where state=1;
    commit;
    update sp_up set state=1 where sp_up.id in(
      select id from (
           select sp_up.id from sp_up where state=0
      )
    )and rownum<=in_count;
    commit;
    open ret_out for
        select * from sp_up where state=1;
end;
/

prompt
prompt Creating procedure SP_INSERT_UP
prompt ===============================
prompt
create or replace procedure SP_Insert_Up(
 /*短信SP上行*/
  In_Sender in varchar2,--发送者
  In_SP_Code in  varchar2,--sp指令
  In_MessID in varchar2,--消息ID
  In_Content in  varchar2,--短信类容
  Out_ret out varchar2--操作结构
)
as
temp_out varchar2(50);
temp_SP_Code varchar2(5000);
begin
    temp_SP_Code:=In_SP_Code;
     --发送者不能为空
     if In_Sender is null then
       temp_out:='发送者不能为空';
       goto ret_end;
     end if;
     --从类容中获取指令
     if temp_SP_Code is null then
         if In_Content is null then
             temp_out:='指令和内容不能同时为空';
             goto ret_end;
          end if;
        select max(IN_code) into temp_SP_Code from sp_code where regexp_like(In_Content,format,'i');
     end if;
     --判断指令
     if temp_SP_Code is null then
          temp_out:='无法获取指令';
          goto ret_end;
     end if;
     --插入上行信息
     insert into sp_up(ID,MessageID,sender,sp_code,content,state,create_date)
       values(sp_up_id.nextval,In_MessID,In_Sender,temp_SP_Code,In_Content,0,sysdate);
       commit;
      temp_out:='添加成功！';
     <<ret_end>>
     Out_ret:=temp_out;
  end;
/

prompt
prompt Creating procedure SP_UPDATE_DOWNSTATE
prompt ======================================
prompt
create or replace procedure SP_Update_DownState(
   /*更新下行短信状态*/
   In_SMSID in varchar2,--短信ID
   In_state in number,--状态
   Ret_out out number--返回状态 0 成功 1 短信ID为空 2 该短信不存在 3状态为空
)
as
temp_num number(5);
begin
    ---验证短信ID 非空
    if In_SMSID is null then
       temp_num:=1;
       goto ret_end;
    end if;
    --验证状态非空
    if In_state is null then
        temp_num:=3;
       goto ret_end;
    end if;
    --验证短信是否存在
    select count(*) into temp_num from sp_down where id=In_SMSID;
    if temp_num=0 then
       temp_num:=2;
       goto ret_end;
    end if;
    ---更新短信状态
    update sp_down set state=In_state where id=In_SMSID;
    commit;
   <<ret_end>>
   Ret_out:=temp_num;
end;
/

prompt
prompt Creating procedure WL_CUST_CANNELORDER
prompt ======================================
prompt
create or replace procedure WL_CUST_CANNELORDER(
   Phones in varchar2,
   ret_out out varchar2)
as
temp_str varchar2(500):='';
begin
  select max(CUST_ID) into temp_str from Wl_Cust_Mobile where nbr=phones;
  if temp_str is not  null then
     update wl_orderinfo set Expiry_date=sysdate where CUST_ID=temp_str and Expiry_date is null;
     commit;
     update wl_cust_mobile set isorderssxy=0,eff_date=sysdate where nbr=Phones;
     commit;
  end if;
  temp_str:='取消成功！！';
  <<ret_end>>
  ret_out:=temp_str;
end;
/

prompt
prompt Creating procedure WL_CUST_ORDER
prompt ================================
prompt
create or replace procedure WL_CUST_Order
  /*用户套餐订购*/
  (custId in varchar2
  --订购用户
  ,ProductId1 in varchar2
  --商品ID
  ,OrderCount in number
  --商品数量
  ,OrderType  in varchar2
  --订购类型  1.自定购 2.销售人员代购 3.信息部代购 4.公司代购
  ,CreateDate in date
  --订购时间
  ,ExprityDate in date
  --失效时间
  ,Op_sender in varchar2
  --操作人Id
  ,ret_out  out varchar2
  --返回操作状态
  )
as
  tmp_out varchar2(500):='';
  temp_OrderType varchar2(500):='';
  temp_Op_sender varchar2(500):='';
  temp_count number(5):=0;
begin
   temp_OrderType:=OrderType;
   temp_Op_sender:=Op_sender;
  ---------订购用户验证----------
  if custId is null then
     tmp_out:='订购用户不能为空【找货宝】';
     goto ret_end;
  end if;
  -------------商品验证-------------
  if ProductId1 is null then
    tmp_out:='购买商品不能为空【找货宝】';
    goto ret_end;
  end if;
  ---------------该商品是否存在------
  select count(*) into temp_count from wl_productinfo
     where productid=ProductId1;
  /*if temp_count=0 then
     tmp_out:='该套餐不存在【找货宝】';
     goto ret_end;
  end if;*/
  ----------------订购类型验证---------
  if temp_OrderType is null then
     tmp_out:='订购类型验证【找货宝】';
     goto ret_end;
  end if;
  ------查看是否有销售人员绑定该号码----
  select max(saleid) into tmp_out from wl_xs_salesach where oppsiteid=custId;
  if tmp_out is not null then
      temp_Op_sender:=to_char(tmp_out);
      temp_OrderType:='销售人员代购【找货宝】';
  end if;
  -------------添加订单-----------------
   insert into Wl_Orderinfo
    (
        Cust_id,Product_Id,Order_Count,Order_Date,Expiry_Date,Order_Type,Order_Operator
    )
    values
    (
        custId,ProductId1,OrderCount,CreateDate,ExprityDate,temp_OrderType,temp_Op_sender
     );
  commit;
  tmp_out:='订购成功！！【找货宝】';
  <<ret_end>>
  ret_out:=tmp_out;
end;
/

prompt
prompt Creating procedure WL_SMS_ZST_OPEARE_OPDETAIL_1
prompt ===============================================
prompt
create or replace procedure WL_SMS_ZST_OPEARE_OPDETAIL_1(
  MessID in varchar2,--消息ID
  In_phoneNum in varchar2,--手机号码
  In_Op in varchar2,--操作
  In_Mess in varchar2,--输入的内容
  Out_mess out varchar2--输出信息
  )
as
 temp_Count number(5):=0;--临时储存记录数
 temp_OutMess varchar2(300):='';--临时储存输出信息
 temp_Str varchar2(300):='';--临时储存字符串
 temp_str1 varchar2(300):='';--临时处理字符串2
 startStr varchar2(500):='';--起始地
 endStr varchar2(500):='';--目的地
 In_obj varchar2(500):='';--临时对象
 begin

  ----------------------判断该用户是否存在----------------
  select count(*) into temp_count from Wl_Cust_Mobile where nbr=In_phoneNum;

  --------------------获取客户端地址链接------------------
  if regexp_like(In_Op,'getrj','i') then
      temp_OutMess:='http://down.zhaohuobao.cn/ZHBao.apk';
      sp_down_insert(MessID,In_phoneNum ,'mandao',temp_OutMess,0 ,temp_count);
  end if;

  ------------------------订购业务------------------------
  if regexp_like(In_Op,'order','i') then
    ----该用户存在
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=1,Eff_Date=add_months(sysdate,to_number(30)) where nbr=In_phoneNum;
      select max(cust_ID) into temp_str from Wl_Cust_Mobile where nbr=In_phoneNum;
     --该用户不存在
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,add_months(sysdate,1),1) return CUST_ID into temp_str;
    end if;
     --生成订单
      wl_cust_order(temp_str,'10元包月',1,'自订购',sysdate,null,In_phoneNum,temp_OutMess);
      temp_OutMess :='感谢您订购找货宝业务!请回复“RJ”下载安卓手机软件;短信查询:请回复“某地到某地”。QQ查询:请加2577556895为好友。【找货宝】';
      sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------------取消业务--------------------------
  if regexp_like(In_Op,'exit','i') then
    if temp_count>0 then
      update Wl_Cust_Mobile set Isorderssxy=0,Eff_Date=sysdate where nbr=In_phoneNum;
    else
      insert into  Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Isorderssxy)
      values(In_phoneNum,sysdate,sysdate,0);
    end if;
    insert into WL_SMS_ZST_SERVICE(PhoneNum,BizSate) values(In_phoneNum,'取消业务');
    WL_CUST_CannelOrder(In_phoneNum,temp_OutMess);
    temp_OutMess :='手机号码：'||In_phoneNum||'取消成功【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
    goto ret_end;
  end if;

  ----------------绑定QQ----------------------------------
  if In_Op='bdqq' then
       In_obj:=regexp_substr(In_Mess,'\d+');
      ----------------查找是否存在其他人占用此QQ----------
      select count(*) into temp_Count from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------没有其他人使用此qq-----------------------
      if temp_Count=0 then
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
      ---------------查看存在该QQ的电话是否为空-----------
      select max(nbr) into temp_Str from  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum;
      -----------存在qq 的电话为空则删除该条信息并将qq绑定到该号码下-----
      if temp_Str is null then
         delete  Wl_Cust_Mobile where qq=In_obj and nbr<>In_phoneNum and (nbr is null);
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      else--否则去掉对方的qq并绑定自己QQ
         update Wl_Cust_Mobile set qq='' where qq=In_obj and nbr<>In_phoneNum;
         update Wl_Cust_Mobile set qq=In_obj where nbr=In_phoneNum;
         commit;
         temp_OutMess:='QQ号:'||In_obj||'绑定成功！【找货宝】';
         sp_down_insert(MessID,In_phoneNum ,'sp',temp_OutMess,0 ,temp_count);
         goto ret_end;
      end if;
   end if;

  ---司机的手机绑定销售编号 成为该销售编号的意向用户-----
  if regexp_like(In_op,'bdxs','i') then
     In_obj:=regexp_substr(In_Mess,'\d+');
     select max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       if temp_Str is null then
          insert into Wl_Cust_Mobile(nbr,Create_Date,Eff_Date,Last_Position_Date,Isorderssxy)values(In_phoneNum,sysdate,sysdate,sysdate,2);
           commit;
       end if;
       select max(CUST_ID) into temp_Str from Wl_Cust_Mobile where nbr=In_phoneNum;
       select count(*) into temp_Count  from wl_xs_salesach where oppsiteid=temp_Str;
       if temp_Count>0 then
           temp_OutMess:='该司机已被其他销售人员绑定';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
       else
           select max(saleid) into temp_count from wl_xs_salesman where sphone=In_obj;
           if temp_count is null then
               temp_OutMess:='此销售编号有误！';
               sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
               goto ret_end;
            else
                insert into wl_xs_salesach(saleid,xstype,oppsiteid,xstime,translation) values(temp_count,'司机',temp_Str,sysdate,0);
                 temp_OutMess:='绑定成功！';
               sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
                commit;
           end if;
       end if;
   end if;

  -----------------销售人员开通司机手机号-----------------
  if regexp_like(In_op,'ktsj','i') then

       -------------验证销售编号-----------------
        select count(*) into temp_Count from Wl_Xs_Salesman where sphone=In_phoneNum;
        if temp_Count=0 then
           temp_OutMess:='您发短信的手机非找货宝销售人员手机，不能为此号码开通找货宝业务，请让销售人员帮您开通，感谢您对找货宝的支持4006282515【找货宝】';
           sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
           goto ret_end;
        end if;
        In_obj:=regexp_substr(In_Mess,'\d+');
        -------------判断该司机是否存在本库中--------
        select Max(cust_id) into temp_Str from Wl_Cust_Mobile where nbr=In_obj;
        if temp_Str is null  then
           insert into Wl_Cust_Mobile(Nbr,Create_Date,Isorderssxy,Isreg,Serchtimes)values(In_obj,sysdate,0,0,0)
            return CUST_ID into temp_Str;
        end if;

        ------判断该司机是否重复订购------
        select count(*) into temp_Count from wl_orderinfo where
             cust_id=temp_str and (expiry_date is null or expiry_date>sysdate);
        if temp_Count>0 then
            temp_OutMess:='销售代理您好，该号码已开通找货宝业务，无需重新开通，如有疑问，请咨询总部工作人员';
            sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
            goto ret_end;
        end if;
        --------------判断该手机号码是否被销售编号绑定----
        select count(*) into temp_Count from wl_xs_salesach where  oppsiteid=temp_Str;
        if temp_Count>0 then
             ---获取该的所属销售员
             select max(sphone) into temp_str1 from wl_xs_salesman where SaleId in(
                select Max(SALEID) from Wl_Xs_Salesach  where oppsiteid=temp_Str
             );

             ----给该销售员发送信息
             temp_OutMess:='手机号为15129030647已被其他销售员开通，将不再是您的意向用户';
             sp_down_insert('',temp_str1 ,'mandao' ,temp_OutMess ,0 ,temp_count);
             ---删除原来有的意向用户
            delete wl_xs_salesach where oppsiteid=temp_Str;
        end if;

        ------------------给司机开通12个月----------
         update wl_cust_mobile set eff_date=add_months(sysdate,to_number(12)), isORDERSSXY=1 where nbr=In_obj;
         commit;

         ----------根据手机号码获取销售人员的销售编号--
         select max(saleid) into temp_str1 from wl_xs_salesman where sphone=In_phoneNum;

         ---------------------生成订单----------------------
         wl_cust_order(temp_Str,'120元包年',1,'销售人员代购',sysdate,add_months(sysdate,12),temp_str1,temp_OutMess);
         --给销售代理下发信息
         temp_OutMess:='销售代理您好,您已经帮手机号码为'||In_obj||'的用户开通找货宝业务，请指导用户使用此业务，感谢对找货宝的支持 ';
         sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
         --给司机下发信息
         temp_OutMess:='尊敬的用户您好！您已经成功开通找货宝业务，感谢对找货宝的支持，请让销售人员指导您使用此业务，如有疑问，请咨询4006282515';
         sp_down_insert('',In_obj ,'mandao' ,temp_OutMess ,0 ,temp_count);

   end if;

  ------------------获取货源信息--------------------------
  if regexp_like(In_Op,'getinfo','i') then

     -----------------------判断该用户是否订购业务-----------------------
    select max(isorderssxy) into temp_count from wl_cust_mobile where nbr=In_phoneNum;
    if temp_count=0 or temp_count is null then
      temp_OutMess:='您尚未订购找货宝短信业务，无法使用该功能，若要使用请发送5137到10669588订购【找货宝】';
      goto ret_end;
    end if;

    -------------------------查询信息----------------------------
    WL_Cust_ZHBQQ_GetAddress(In_Mess,startStr,endStr);

    -----------------------地址格式有误-------------------------
    if startStr is null or endStr is null then
      temp_OutMess :='您输入的格式有误请输入:某地到某地【找货宝】';
      goto ret_end;
    end if;

    -----------------------地址格式正确-----------------------------
    temp_OutMess:=get_line_info_TXH_top(In_phoneNum,1,startStr,endStr);

    ------------------------更新最后一次查询时间--------------------
    update wl_cust_mobile set
           last_position_date=sysdate ,
           last_search=startStr||'到'||endStr,
           last_serch_data=Sysdate
           where  nbr=In_phoneNum;
    commit;

    -----------------------插入到短信记录中去------------------------
    insert into WL_SMS_ZST_Messages(PHONENUM,IN_MESS,OUT_MESS)
           values(In_phoneNum,In_Mess,temp_OutMess);
    commit;

    -------------------------查询记录条数-----------------------------
    select count(*) into temp_count from WL_info_data
               where info_start like startStr||'%'
               and info_end like endStr||'%';
    temp_OutMess:=temp_OutMess||'相关信息共有'||temp_count||'条,继续发送获取其他信息【找货宝】';
    sp_down_insert(MessID,In_phoneNum ,'mandao' ,temp_OutMess ,0 ,temp_count);
   end if;
   ---------------结束点-----------------
   <<ret_end>>
   Out_mess:=temp_OutMess;
 end;
/


spool off
