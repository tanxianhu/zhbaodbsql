-----------------------------------------------------
-- Export file for user ZHBAO                      --
-- Created by Administrator on 2013-6-17, 15:36:42 --
-----------------------------------------------------

spool D:\app\ZHBaoDBSql\Log\sh_zhbao_v1_0_3_Update_RollBack.sql


alter table WL_CUST_DEPARTMENT DROP COLUMN IMG_IDCARD ;
alter table WL_CUST_DEPARTMENT DROP COLUMN STATE_IDCARD;

alter table WL_CUST_DEPARTMENT DROP COLUMN IMG_BUSINESS ;
alter table WL_CUST_DEPARTMENT DROP COLUMN STATE_BUSINESS ;

alter table WL_CUST_DEPARTMENT DROP COLUMN IMG_HOUSE ;
alter table WL_CUST_DEPARTMENT DROP COLUMN STATE_HOUSE ;

drop procedure WL_CUST_DEPARTMENT_UPDATE;

drop procedure WL_GetDepInfo_1;

prompt
prompt Creating procedure WL_SALE_LOOK_SIJI
prompt ====================================
prompt
create or replace procedure wl_sale_look_siji
(
   In_CurrentId in varchar2,--当前登录用户ID
   in_saleid in varchar2,--被查看销售编号
   in_sijiType in varchar2,--司机类型 0 意向用户 1 包年司机 2 包月司机
   out_cursor out sys_refcursor--返回结果级
)
as
begin
    if in_sijiType='0' then
       open out_cursor for
          select * from wl_cust_mobile where cust_id in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid=in_saleid
          );
    end if;
    if in_sijiType='1' then
      if in_saleid is null then
        open out_cursor for
           select *from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator in (
                 select saleid from wl_xs_salesman  where saleid=In_CurrentId
                 or fsaleid=In_CurrentId
             )
          );
       else
         open out_cursor for
           select * from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator=in_saleid
          );
       end if;
    end if;

    if in_sijiType='2' then
       open out_cursor for
          select *from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
             where order_operator=in_saleid and
             product_id='10元包月'
          );
    end if;
  end;
/

prompt
prompt Creating procedure WL_SALE_UPDATE
prompt =================================
prompt
create or replace procedure WL_sale_update(
    in_saleID in  number,
    in_fsaleid in number,
    in_loginname in varchar2,
    in_spwd in varchar2,
    in_sname in varchar2,
    in_sgender in varchar2,
    in_sidcard in varchar2,
    in_sPhone in varchar2,
    in_address in varchar2,
    in_snode in varchar2,
    in_sumsale in number,
    in_sumup in varchar2,
    ret_out out  varchar2 --0 修改成功 -1 当前用户获取失败 -2 登陆名不能为空 -3 电话号码为空
)
as
  retcount number(4):=0;
  retStr varchar2(100):='';
begin
   --销售编号
    if in_saleID is null then
      retStr:='-1';
      goto  ret_end;
    end if;
    --电话号码
    if in_loginname is null then
      retStr:='-2';
      goto  ret_end;
    end if;
    --电话号码
    if in_sPhone is null then
      retStr:='-3';
      goto  ret_end;
    end if;

    select count(*) into retcount  from WL_XS_salesman where saleId=in_saleID;
    if retcount=0 then
        retStr:='-1';
      goto  ret_end;
    end if;
    --存在则更新
    update WL_XS_salesman set Fsaleid=in_fsaleid, Loginname=in_loginname, spwd=in_spwd,saddress=in_address, sName=in_sname,sGender=in_sgender,
      sIDCard=in_sidcard,sPhone=in_sPhone,sNode=in_snode,sumsale=in_sumsale,sumup=in_sumup
      where saleId=in_saleID;
      retStr:='0';
      goto ret_end;
  <<ret_end>>
  ret_out:=retStr;
end;
/


spool off
