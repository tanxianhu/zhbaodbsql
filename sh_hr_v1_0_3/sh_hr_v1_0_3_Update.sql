-------------------------------------------------
-- Export file for user HR                     --
-- Created by Admin on 2013/6/17 星期一, 15:24:15 --
-------------------------------------------------

set define off
spool D:\app\ZHBaoDBSql\Log\sh_zhbao_v1_0_3_Update.sql

alter table WL_CUST_DEPARTMENT add(IMG_IDCARD BLOB default empty_blob());
alter table WL_CUST_DEPARTMENT add(STATE_IDCARD NUMBER(4) default 0);

alter table WL_CUST_DEPARTMENT add(IMG_BUSINESS BLOB default empty_blob());
alter table WL_CUST_DEPARTMENT add(STATE_BUSINESS NUMBER(4) default 0);

alter table WL_CUST_DEPARTMENT add(IMG_HOUSE BLOB default empty_blob());
alter table WL_CUST_DEPARTMENT add(STATE_HOUSE NUMBER(4) default 0);

prompt
prompt Creating procedure WL_GETDEPINFO_1
prompt ==================================
prompt
create or replace procedure WL_GetDepInfo_1(
     PageSize in number,
     pageIndex in number,
     keys in varchar2,
     in_state in number,---1 全部 0 为上传 1 带审核 2 通过 3 未通过
     pageCount out number,
     recur out sys_refcursor--Wl_Cust_Dep_Getinfo.t_info
  )as

begin
  if in_state=-1 then
      open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department 
            where CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  else
    open recur for
    select k.* from (
     select t.* ,rownum run from
       ( select * from WL_Cust_department 
            where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%')
            order by R_DATE desc
        ) t where regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null
    ) k
    where k.run<=pageIndex*pageSize and k.run>(pageIndex-1)*pageSize ;
    ---获取总记录数
    select count(*) into pageCount from WL_Cust_department
      where (
               State_Idcard=in_state or
               State_Business=in_state or
               State_House=in_state
            ) and (CUST_ID like ''||keys||'%'
            or NBR like ''||keys||'%'
            or C_NAME like ''||keys||'%'
            or CONTACT like ''||keys||'%'
            ) and regexp_substr(nbr,'^1(3|4|5|6|8)\d{9}$')is not null;
  end if;
end;
/
prompt
prompt Creating procedure WL_CUST_DEPARTMENT_UPDATE
prompt ============================================
prompt
create or replace procedure WL_CUST_DEPARTMENT_UPDATE(
    in_cust_Id in varchar2,--主键ID
    in_nbr in varchar2,--账号
    in_id_card in varchar2,--身份证号码
    in_c_name in varchar2,--信息部名称
    in_c_passWord in varchar2,--密码
    in_city in varchar2,--注册城市
    in_c_type in varchar2,--类型
    in_chief in varchar2,--负责人
    in_contact in varchar2,--联系方式
    in_address in varchar2,--注册地址
    in_lng in number,--信息部经度
    in_lat in number,--信息部纬度
    in_state in number,--状态
    in_note in varchar2,--备注
    in_default_goods in varchar2,--默认获取
    in_freesms in number,--免费短信条数
    in_billingsms in number,--计费短信条数
    in_issuperadmin in number,--是否是超级管理员
    in_smssing in varchar2,--短信签名
    in_qq in varchar2,--qq
    in_saleid in varchar2,--销售Id
    in_state_idcard in number,--身份证状态 0 未上传 1 等待审核  2 未通过 3 通过
    in_state_business in number,--营业执照状态 0 未上传 1 等待审核  2 未通过 3 通过
    in_state_house in number,--门头照片状态 0 未上传 1 等待审核  2 未通过 3 通过
    out_state out number--0 修改成功 -1 主键不存在 -2 账号为空 -3 密码为空
  )
as
  tempCount number(6);
  tempStr number(4);
begin
  
   select count(*) into tempCount from wl_cust_department where cust_id = in_cust_Id;
   if tempCount = 0 then 
     tempStr:=-1;
     goto ret_end;
   end if;
     
   if in_nbr is null then
      tempStr:=-2;
      goto ret_end;
   end if;
     
   if in_c_passWord is null then
      tempStr:=-3;
      goto ret_end;
   end if;
     
   update  wl_cust_department set 
            nbr = in_nbr,
            id_card = in_id_card,
            c_name = in_c_name,
            c_password = in_c_passWord,city = in_city,c_type = in_c_type,chief = in_chief,
            contact = in_contact,address = in_address,lng = in_lng,lat = in_lat,state = in_state,
            note = in_note,default_goods = in_default_goods,freesms = in_freesms,
            billingsms = in_billingsms,issuperadmin = in_issuperadmin,smssign = in_smssing,
            qq = in_qq,saleid = in_saleid,state_idcard = in_state_idcard,state_business = in_state_business,
            state_house = in_state_house where cust_id = in_cust_Id;
    commit;
  tempStr:=0;
 <<ret_end>>
 out_state := tempStr;
end;
/

prompt
prompt Creating procedure WL_SALE_LOOK_SIJI
prompt ====================================
prompt
create or replace procedure wl_sale_look_siji
(
   In_CurrentId in varchar2,--当前登录用户ID
   in_saleid in varchar2,--被查看销售编号
   in_sijiType in varchar2,--司机类型 0 意向用户 1 包年司机 2 包月司机
   in_pageIndex in number,--当前页数
   in_pageSize in number,--每页显示的条数
   out_count out number,--符合的总记录书
   out_cursor out sys_refcursor--返回结果级
)
as
begin
    if in_sijiType='0' then
     if in_saleid='0' then    
         ---获取结构集
       open out_cursor for
          select * from (select rownum nu,CUST_ID,sim,nbr,esn,imsi,Id_card,c_name,
            C_password,City,car_number,car_model,like1,
            last_lng,last_lat,last_position_date,state,note,
            qq,create_date,eff_date,last_search,
            isorderssxy,serchtimes,isreg from wl_cust_mobile where cust_id not in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid<>0
          )) t
          where t.nu>(in_pageIndex-1)*in_pageSize and t.nu<=in_pageIndex*in_pageSize;
      --获取总记录数
         select count(*)  into out_count from wl_cust_mobile where cust_id not in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid<>0
          );
      else
      ---获取结构集
       open out_cursor for
          select * from (select rownum nu,CUST_ID,sim,nbr,esn,imsi,Id_card,c_name,
            C_password,City,car_number,car_model,like1,
            last_lng,last_lat,last_position_date,state,note,
            qq,create_date,eff_date,last_search,
            isorderssxy,serchtimes,isreg from wl_cust_mobile where cust_id in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid=in_saleid
             union
             select cust_id from wl_orderinfo
          )) t
          where t.nu>(in_pageIndex-1)*in_pageSize and t.nu<=in_pageIndex*in_pageSize;
      --获取总记录数
         select count(*)  into out_count from wl_cust_mobile where cust_id in (
             select oppsiteid cust_id from wl_xs_salesach
             where saleid=in_saleid
             union 
             select cust_id from wl_orderinfo
          );
          end if;
    end if;
    if in_sijiType='1' then
      ---获取结构集
      if in_saleid is null then
        open out_cursor for
        select * from (
          select rownum nu,CUST_ID,sim,nbr,esn,imsi,Id_card,c_name,
            C_password,City,car_number,car_model,like1,
            last_lng,last_lat,last_position_date,state,note,
            qq,create_date,eff_date,last_search,
            isorderssxy,serchtimes,isreg from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator in (
                 select saleid from wl_xs_salesman  where saleid=In_CurrentId
                 or fsaleid=In_CurrentId
             )
          )
          ) t
          where t.nu>(in_pageIndex-1)*in_pageSize and t.nu<=in_pageIndex*in_pageSize;
          --获取总计路数

          select count(*)  into out_count from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator in (
                 select saleid from wl_xs_salesman  where saleid=In_CurrentId
                 or fsaleid=In_CurrentId
             )
          );

       else

         open out_cursor for
          select * from (
           select rownum nu,CUST_ID,sim,nbr,esn,imsi,Id_card,c_name,
            C_password,City,car_number,car_model,like1,
            last_lng,last_lat,last_position_date,state,note,
            qq,create_date,eff_date,last_search,
            isorderssxy,serchtimes,isreg from wl_cust_mobile where cust_id in  (
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator=in_saleid
          )
          ) t where t.nu>(in_pageIndex-1)*in_pageSize and t.nu<=in_pageIndex*in_pageSize;
           --获取总计路数
          select count(*)  into out_count from wl_cust_mobile where cust_id in(
             select cust_id from wl_orderinfo
              where  product_id='120元包年' and order_operator=in_saleid
          );
       end if;
    end if;

    if in_sijiType='2' then
       open out_cursor for
         select * from(
           select rownum nu,CUST_ID,sim,nbr,esn,imsi,Id_card,c_name,
            C_password,City,car_number,car_model,like1,
            last_lng,last_lat,last_position_date,state,note,
            qq,create_date,eff_date,last_search,
            isorderssxy,serchtimes,isreg from wl_cust_mobile where cust_id in (
             select cust_id from wl_orderinfo
             where order_operator=in_saleid and
             product_id='10元包月'
          )) t
          where t.nu>(in_pageIndex-1)*in_pageSize and t.nu<=in_pageIndex*in_pageSize;
          --获取总记录数
           select count(*)  into out_count from wl_cust_mobile where cust_id in(
              select cust_id from wl_orderinfo
             where order_operator=in_saleid and
             product_id='10元包月'
           );

    end if;
  end;
/

prompt
prompt Creating procedure WL_SALE_UPDATE
prompt =================================
prompt
create or replace procedure WL_sale_update(
    in_saleID in  number,
    in_fsaleid in number,
    in_loginname in varchar2,
    in_spwd in varchar2,
    in_sname in varchar2,
    in_sgender in varchar2,
    in_sidcard in varchar2,
    in_sPhone in varchar2,
    in_address in varchar2,
    in_snode in varchar2,
    in_sumsale in number,
    in_sumup in varchar2,
    in_state in number,
    ret_out out  varchar2 --0 修改成功 -1 当前用户获取失败 -2 登陆名不能为空 -3 电话号码为空 
)
as
  retcount number(4):=0;
  retStr varchar2(100):='';
begin
   --销售编号
    if in_saleID is null then
      retStr:='-1';
      goto  ret_end;
    end if;   
    --电话号码
    if in_loginname is null then
      retStr:='-2';
      goto  ret_end;
    end if;    
    --电话号码
    if in_sPhone is null then
      retStr:='-3';
      goto  ret_end;
    end if;
    
    select count(*) into retcount  from WL_XS_salesman where saleId=in_saleID;
    if retcount=0 then
        retStr:='-1';
      goto  ret_end;
    end if;   
    --存在则更新
    update WL_XS_salesman set Fsaleid=in_fsaleid, Loginname=in_loginname, spwd=in_spwd,saddress=in_address, 
       sName=in_sname,sGender=in_sgender, sIDCard=in_sidcard,sPhone=in_sPhone,sNode=in_snode,sumsale=in_sumsale,
       sumup=in_sumup,state=in_state
      where saleId=in_saleID;
      retStr:='0';
      goto ret_end;
  <<ret_end>>
  ret_out:=retStr;
end;
/


spool off
