-------------------------------------------------
-- Export file for user HGER                   --
-- Created by Admin on 2013/7/25 星期四, 10:52:06 --
-------------------------------------------------

set define off
spool xa_hger_v1_0_5.log

prompt
prompt Creating table CUST_CONTACT
prompt ===========================
prompt
create table CUST_CONTACT
(
  dep_id       VARCHAR2(64),
  contact      VARCHAR2(128),
  contact_type VARCHAR2(32),
  contact_note VARCHAR2(256),
  create_date  DATE
)
;

prompt
prompt Creating table CUST_IDCARD
prompt ==========================
prompt
create table CUST_IDCARD
(
  id_card      VARCHAR2(192) not null,
  id_card_name VARCHAR2(192),
  photo        BLOB,
  gender       VARCHAR2(6),
  birthday     VARCHAR2(30),
  nationality  VARCHAR2(60),
  address      VARCHAR2(600),
  signunit     VARCHAR2(192),
  degree       VARCHAR2(192),
  career       VARCHAR2(90),
  addresscode  VARCHAR2(90),
  addresscount NUMBER(3),
  create_date  DATE,
  state        NUMBER(1),
  note         VARCHAR2(1500)
)
;

prompt
prompt Creating table LXF_LOCAL_PLACE
prompt ==============================
prompt
create table LXF_LOCAL_PLACE
(
  id       NUMBER(10),
  pro      VARCHAR2(50),
  city     VARCHAR2(50),
  county   VARCHAR2(50),
  region   VARCHAR2(50),
  state    VARCHAR2(200),
  lng      NUMBER(11,8),
  lat      NUMBER(11,8),
  serial   NUMBER(10),
  citycode VARCHAR2(800)
)
;

prompt
prompt Creating table REGION_CN
prompt ========================
prompt
create table REGION_CN
(
  r_id      NUMBER(8),
  f_id      NUMBER(8),
  r_code    NUMBER(6),
  r_name    VARCHAR2(256),
  ra_code   VARCHAR2(8),
  rp_length VARCHAR2(8),
  rs_name   VARCHAR2(256),
  rl_name   VARCHAR2(256),
  r_state   VARCHAR2(256),
  r_sort    NUMBER(8),
  remark    VARCHAR2(512),
  created   DATE,
  r_depth   NUMBER(8),
  zip_code  VARCHAR2(6),
  serial    NUMBER(10),
  r_lng     NUMBER(11,8),
  r_lat     NUMBER(11,8)
)
;

prompt
prompt Creating table WL_CUST_DEPARTMENT
prompt =================================
prompt
create table WL_CUST_DEPARTMENT
(
  cust_id        VARCHAR2(64) default sys_guid() not null,
  nbr            VARCHAR2(30),
  id_card        VARCHAR2(64),
  c_name         VARCHAR2(128),
  c_password     VARCHAR2(64),
  city           VARCHAR2(64),
  c_type         VARCHAR2(64),
  chief          VARCHAR2(64),
  contact        VARCHAR2(500),
  address        VARCHAR2(500),
  lng            NUMBER(20,8),
  lat            NUMBER(20,8),
  state          NUMBER(1),
  note           VARCHAR2(500),
  default_goods  VARCHAR2(50),
  freesms        NUMBER default 0,
  billingsms     NUMBER default 0,
  issuperadmin   NUMBER default 0,
  smssign        VARCHAR2(50) default '【找货宝】',
  r_date         DATE default sysdate not null,
  qq             VARCHAR2(20),
  saleid         NUMBER(8),
  state_idcard   NUMBER(4) default 0,
  img_business   BLOB default empty_blob() not null,
  state_business NUMBER(4) default 0,
  img_house      BLOB default empty_blob(),
  state_house    NUMBER(4) default 0,
  img_idcard     BLOB default empty_blob()
)
;
comment on table WL_CUST_DEPARTMENT
  is '信息部客户表';
comment on column WL_CUST_DEPARTMENT.cust_id
  is '客户ID';
comment on column WL_CUST_DEPARTMENT.nbr
  is '手机号码';
comment on column WL_CUST_DEPARTMENT.id_card
  is '身份证号';
comment on column WL_CUST_DEPARTMENT.c_name
  is '姓名';
comment on column WL_CUST_DEPARTMENT.c_password
  is '密码';
comment on column WL_CUST_DEPARTMENT.city
  is '注册城市';
comment on column WL_CUST_DEPARTMENT.c_type
  is '类型停车场或信息部';
comment on column WL_CUST_DEPARTMENT.chief
  is '负责人';
comment on column WL_CUST_DEPARTMENT.contact
  is '联系人';
comment on column WL_CUST_DEPARTMENT.address
  is '地址';
comment on column WL_CUST_DEPARTMENT.lng
  is '经度';
comment on column WL_CUST_DEPARTMENT.lat
  is '纬度';
comment on column WL_CUST_DEPARTMENT.state
  is '状态';
comment on column WL_CUST_DEPARTMENT.note
  is '备注';
comment on column WL_CUST_DEPARTMENT.freesms
  is '免费信息条数';
comment on column WL_CUST_DEPARTMENT.billingsms
  is '计费短信条数';
comment on column WL_CUST_DEPARTMENT.issuperadmin
  is '是否是超级管理员';
comment on column WL_CUST_DEPARTMENT.smssign
  is '短信签名';
comment on column WL_CUST_DEPARTMENT.r_date
  is '注册时间';
comment on column WL_CUST_DEPARTMENT.qq
  is 'QQ号';
comment on column WL_CUST_DEPARTMENT.img_business
  is '营业执照';
comment on column WL_CUST_DEPARTMENT.img_house
  is '门头照片';
alter table WL_CUST_DEPARTMENT
  add primary key (CUST_ID);

prompt
prompt Creating table WL_FILTER
prompt ========================
prompt
create table WL_FILTER
(
  filterid NUMBER(8) not null,
  fwords   VARCHAR2(64),
  rwords   VARCHAR2(64),
  flevel   NUMBER(2),
  fstate   NUMBER(1),
  user_id  VARCHAR2(64),
  created  DATE,
  note     VARCHAR2(128)
)
;
alter table WL_FILTER
  add primary key (FILTERID);

prompt
prompt Creating table WL_INFO
prompt ======================
prompt
create table WL_INFO
(
  id          VARCHAR2(32),
  info        VARCHAR2(1024),
  state       NUMBER(1),
  dep_id      VARCHAR2(64),
  create_date DATE,
  note        VARCHAR2(500)
)
;
create index WL_INFO_IND on WL_INFO (ID);
create index WL_INFO_NOTE on WL_INFO (NOTE);

prompt
prompt Creating table WL_INFO_DATA
prompt ===========================
prompt
create table WL_INFO_DATA
(
  id          NUMBER(30) not null,
  num         NUMBER(30) default 0,
  nbr         VARCHAR2(30),
  c_name      VARCHAR2(200),
  city        VARCHAR2(64),
  create_date DATE,
  info        VARCHAR2(500),
  info_start  VARCHAR2(64),
  info_end    VARCHAR2(64),
  info_nbr1   VARCHAR2(30),
  info_nbr2   VARCHAR2(30),
  info_nbr3   VARCHAR2(30),
  info_truck  VARCHAR2(64),
  info_goods  VARCHAR2(64),
  info_note   VARCHAR2(500),
  info_from   VARCHAR2(64),
  state       NUMBER(1),
  note        VARCHAR2(500),
  lng_start   NUMBER(11,8),
  lat_start   NUMBER(11,8),
  lng_end     NUMBER(11,8),
  lat_end     NUMBER(11,8),
  early_id    VARCHAR2(32)
)
;
comment on table WL_INFO_DATA
  is '求车信息表';
comment on column WL_INFO_DATA.id
  is '信息ID';
comment on column WL_INFO_DATA.num
  is '信息被推送级别，默认0';
comment on column WL_INFO_DATA.nbr
  is '手机号码';
comment on column WL_INFO_DATA.c_name
  is '姓名';
comment on column WL_INFO_DATA.city
  is '注册城市/发布城市';
comment on column WL_INFO_DATA.create_date
  is '创建时间';
comment on column WL_INFO_DATA.info
  is '拉货信息';
comment on column WL_INFO_DATA.info_start
  is '货物起始地';
comment on column WL_INFO_DATA.info_end
  is '货物目的地';
comment on column WL_INFO_DATA.info_nbr1
  is '拉货联系电话1';
comment on column WL_INFO_DATA.info_nbr2
  is '拉货联系电话2';
comment on column WL_INFO_DATA.info_nbr3
  is '拉货联系电话3';
comment on column WL_INFO_DATA.info_truck
  is '需要车型';
comment on column WL_INFO_DATA.info_goods
  is '货物';
comment on column WL_INFO_DATA.info_note
  is '拉货备注信息';
comment on column WL_INFO_DATA.info_from
  is '信息来源';
comment on column WL_INFO_DATA.state
  is '状态';
comment on column WL_INFO_DATA.note
  is '备注';
comment on column WL_INFO_DATA.lng_start
  is '起始地经度';
comment on column WL_INFO_DATA.lat_start
  is '起始地纬度';
comment on column WL_INFO_DATA.lng_end
  is '目的地经度';
comment on column WL_INFO_DATA.lat_end
  is '目的地纬度';
create index LNG_LAT_START_IDX on WL_INFO_DATA (LNG_START, LAT_START);
alter table WL_INFO_DATA
  add primary key (ID);

prompt
prompt Creating table WL_INFO_DATA_GOODS
prompt =================================
prompt
create table WL_INFO_DATA_GOODS
(
  id    NUMBER(10),
  goods VARCHAR2(200),
  desc1 VARCHAR2(100)
)
;

prompt
prompt Creating table WL_INFO_DATA_HISTORY
prompt ===================================
prompt
create table WL_INFO_DATA_HISTORY
(
  id          VARCHAR2(64) not null,
  text1       VARCHAR2(1500),
  create_date DATE,
  state       NUMBER(5),
  city        VARCHAR2(30),
  info_id     NUMBER(30),
  note        VARCHAR2(500)
)
;

prompt
prompt Creating table WL_INFO_DATA_NOTE
prompt ================================
prompt
create table WL_INFO_DATA_NOTE
(
  id    NUMBER(10),
  note  VARCHAR2(200),
  desc1 VARCHAR2(100)
)
;

prompt
prompt Creating table WL_INFO_DATA_REGION
prompt ==================================
prompt
create table WL_INFO_DATA_REGION
(
  id       NUMBER(10),
  pro      VARCHAR2(50),
  city     VARCHAR2(50),
  county   VARCHAR2(50),
  region   VARCHAR2(50),
  state    VARCHAR2(200),
  lng      NUMBER(11,8),
  lat      NUMBER(11,8),
  serial   NUMBER(10),
  citycode VARCHAR2(32)
)
;

prompt
prompt Creating table WL_INFO_DATA_RULE
prompt ================================
prompt
create table WL_INFO_DATA_RULE
(
  id      NUMBER(10),
  find    VARCHAR2(2000),
  replace VARCHAR2(2000),
  note    VARCHAR2(2000)
)
;

prompt
prompt Creating table WL_INFO_DATA_TRUCK
prompt =================================
prompt
create table WL_INFO_DATA_TRUCK
(
  id    NUMBER(10),
  truck VARCHAR2(200),
  desc1 VARCHAR2(100)
)
;

prompt
prompt Creating table WL_INFO_FROM_IN_EARLY
prompt ====================================
prompt
create table WL_INFO_FROM_IN_EARLY
(
  id          VARCHAR2(64) default sys_guid() not null,
  text1       VARCHAR2(1500),
  create_date DATE default sysdate,
  state       NUMBER(5) default 0,
  city        VARCHAR2(30) default '榆林',
  info_id     NUMBER(30),
  note        VARCHAR2(500)
)
;
create index WL_INFO_EARY_CITY_IDX on WL_INFO_FROM_IN_EARLY (CITY);
create index WL_INFO_EARY_IDX on WL_INFO_FROM_IN_EARLY (STATE);

prompt
prompt Creating table WL_INFO_LINE
prompt ===========================
prompt
create table WL_INFO_LINE
(
  id        VARCHAR2(32),
  from_area VARCHAR2(225),
  to_area   VARCHAR2(225)
)
;
create index WL_INFO_LINE_IND on WL_INFO_LINE (ID);

prompt
prompt Creating sequence EARLY_MSG_SEQUENCE
prompt ====================================
prompt
create sequence EARLY_MSG_SEQUENCE
minvalue 1
maxvalue 9999999999999999999999999999
start with 25978984
increment by 1
cache 10;

prompt
prompt Creating sequence SEQSALEID
prompt ===========================
prompt
create sequence SEQSALEID
minvalue 1000
maxvalue 999999999999999999999999999
start with 1040
increment by 1
cache 20;

prompt
prompt Creating sequence SEQU_FILTER_ID
prompt ================================
prompt
create sequence SEQU_FILTER_ID
minvalue 1
maxvalue 99999999
start with 21
increment by 1
cache 20;

prompt
prompt Creating sequence WL_INFO_DATA_ID
prompt =================================
prompt
create sequence WL_INFO_DATA_ID
minvalue 2000000
maxvalue 999999999999999999999
start with 5450313
increment by 1
cache 20;

prompt
prompt Creating view DATA_TABLE_MSG
prompt ============================
prompt
CREATE OR REPLACE FORCE VIEW DATA_TABLE_MSG AS
SELECT 'XA_EARLY' DATA_TABLE, NOTE INFO_FROM,count(*) INFO_COUNT, max(CREATE_DATE) Last_Time
  FROM hger.WL_INFO_FROM_IN_EARLY
    WHERE sysdate- create_date<1/24
      GROUP BY NOTE
UNION ALL
SELECT 'XA_DATE' DATA_TABLE, INFO_FROM,count(*) INFO_COUNT, max(CREATE_DATE) Last_Time
  FROM hger.Wl_Info_Data
    WHERE sysdate- create_date<1/24
      GROUP BY INFO_FROM
UNION ALL
SELECT 'SH_DATE' DATA_TABLE, INFO_FROM,count(*) INFO_COUNT, max(CREATE_DATE) Last_Time
  FROM Wl_Info_Data@Sh
    WHERE sysdate- create_date<1/24
      GROUP BY INFO_FROM;

prompt
prompt Creating view V_DATA_INFO
prompt =========================
prompt
create or replace force view v_data_info as
select
        l.From_area,
        l.to_area,
        i.id,
        i.info,
        i.state,
        i.create_date,
        i.dep_id
   from wl_info_line l,
        wl_info i
  where i.id=l.id;

prompt
prompt Creating view V_DEPARTMENT_CONTACT
prompt ==================================
prompt
create or replace force view v_department_contact as
select
    c.dep_id,
    c.contact_type,
    w.C_NAME,
    w.state_business,
    w.state_house,
    w.state_idcard,
    c.CONTACT
    from
       WL_CUST_DEPARTMENT w,
       CUST_CONTACT c
    where
       w.CUST_ID=c.DEP_ID;

prompt
prompt Creating view V_WL_FILTER
prompt =========================
prompt
CREATE OR REPLACE FORCE VIEW V_WL_FILTER AS
SELECT
       FILTERID,
       FWORDS,
       RWORDS,
       FLEVEL,
       FSTATE,
       USER_ID,
       CREATED,
       NOTE
    FROM WL_filter;

prompt
prompt Creating procedure DATA_TO_SH
prompt =============================
prompt
create or replace procedure Data_To_SH
as
begin

  insert into wl_Info@Sh select * from wl_info bd
  where note is null and not exists(
         select id from wl_Info@Sh sh where sh.id=bd.id
  );
  
  insert into WL_info_line@Sh select * from wl_info_line bd
  where  not exists(
    select id from WL_info_line@Sh sh where sh.id=bd.id
  );
  
  update wl_info set note =2
         where (sysdate - create_date)*24*60 > 2;
         
  commit;
  
end;
/

prompt
prompt Creating procedure GET_CITYCODE_COUNT
prompt =====================================
prompt
create or replace procedure Get_CityCode_count(
   In_CityCode in varchar2,--城市区号
   Out_NumberCount out number,--区号对应得位数
   Out_cur out sys_refcursor
)
as
begin
   if In_CityCode is null then
       open Out_cur for
            select RA_CODE,RP_LENGTH
               from region_cn
                  where RP_LENGTH=8
               group by RA_CODE, RP_LENGTH order by RA_CODE;
    else
        select max(RP_LENGTH) into Out_NumberCount from region_cn where RA_CODE=In_CityCode;
    end if;
end;
/

prompt
prompt Creating procedure LOCADATATOSHDATA
prompt ===================================
prompt
create or replace procedure LocaDataToShData
  (numa in number)
is

begin
  --Insert到上海
  insert into wl_info_data@sh
    select * from Wl_Info_Data lo
      where state = 0 and not exists(
            select * from wl_info_data@sh sh where lo.id=sh.id
            );
  commit;
  --Update本地
  update wl_info_data set state = 1
         where state = 0 and (sysdate - create_date)*24*60 > 2;
  commit;
end;
/

prompt
prompt Creating procedure WL_CLEAN_LOG
prompt ===============================
prompt
create or replace procedure WL_Clean_log(
   /*定时清空元数据信息和发布的信息
     备份删除的源信息
   */
      sign in number
)as
begin
    /*备份要删除的元数据到WL_INfo_DATA_HISTORY 表中*/
    insert into WL_INfo_DATA_HISTORY select * from Wl_Info_From_In_Early where create_date< trunc(sysdate)+0.5 ;
    /*在23:59:59：删除 当天12:00之前的发布数据*/
    delete from Wl_Info_Data where create_date< trunc(sysdate)+0.5;
    delete from Wl_Info_Data@Sh where create_date< trunc(sysdate)+0.5;
    /*在23:59:59：删除 当天12:00之前的原始数据*/
    delete from Wl_Info_From_In_Early where create_date< trunc(sysdate)+0.5 ;
    
    /*在23:59:59 删除当天12点之前的wl_info 本地数据*/
    delete wl_info_line where id in(
       select id from wl_info where create_date< trunc(sysdate)+0.5
    );
    delete  wl_info where create_date< trunc(sysdate)+0.5;
     
    /*在23:59:59 删除当天12点之前的wl_info 上海数据*/
    delete wl_info_line@sh where id in(
       select id from wl_info@sh where create_date< trunc(sysdate)+0.5
    );
    delete wl_info@sh where create_date< trunc(sysdate)+0.5;
    commit; 
 end;
/

prompt
prompt Creating procedure WL_FILTER_ADD
prompt ================================
prompt
create or replace procedure WL_FILTER_Add(
   /*敏感词添加*/
   IN_FWORDS in VARCHAR2,--敏感词
   IN_RWORDS in VARCHAR2,--要替换的词
   IN_FLEVEL in NUMBER,--优先级
   IN_FSTATE in NUMBER,--状态
   IN_USER_ID in VARCHAR2,--操作者
   IN_NOTE in VARCHAR2,--备注
   OUT_STATE out NUMBER--操作结果 0 添加成功 -1 敏感词为空 -2 敏感词已经存在
)
as
temp_count number(2);
temp_state number(1);
temp_flevel number(2);
temp_fstate number(2);
begin

   temp_flevel:=IN_FLEVEL;
   temp_fstate:=IN_FSTATE;

   --判断敏感词是否为空
   if IN_FWORDS is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   --判断该敏感词是否存在
   select count(*) into temp_count from WL_FILTER where FWORDS=IN_FWORDS;
   if temp_count>0 then
      temp_state:=-2;
      goto ret_end;
   end if;

   --判断状态
   if temp_fstate is null then
      temp_fstate:=0;
   end if;

   --判断优先级
   if temp_flevel is null or temp_flevel<0 then
       temp_flevel:=1;
   end if;
   insert into WL_FILTER(
             FilterID,
             FWords,
             RWords,
             FLEVEL,
             FSTATE,
             USER_ID,
             CREATED,NOTE
             )
        values(
            SEQU_FILTER_ID.Nextval,
            IN_FWORDS,
            IN_RWORDS,
            temp_flevel,
            temp_fstate,
            IN_USER_ID,
            sysDate,
            IN_NOTE
            );
     commit;
      temp_state:=0;
   <<ret_end>>
   OUT_STATE:=temp_state;
end;
/

prompt
prompt Creating procedure WL_FILTER_UPDATE
prompt ===================================
prompt
create or replace procedure WL_FILTER_UPDATE(
   /*敏感词修改*/
   IN_FILTER_ID in number,--ID
   IN_FWORDS in VARCHAR2,--敏感词
   IN_RWORDS in VARCHAR2,--要替换的词
   IN_FLEVEL in NUMBER,--优先级
   IN_FSTATE in NUMBER,--状态
   IN_USER_ID in VARCHAR2,--操作者
   IN_NOTE in VARCHAR2,--备注
   OUT_STATE out NUMBER--操作结果 0 修改成功 -1 敏感词为空
)
as
temp_count number(2);
temp_state number(1);
temp_flevel number(2);
temp_fstate number(2);
begin

   temp_flevel:=IN_FLEVEL;
   temp_fstate:=IN_FSTATE;

   --判断敏感词是否为空
   if IN_FWORDS is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   --判断状态
   if temp_fstate is null then
      temp_fstate:=0;
   end if;

   --判断优先级
   if temp_flevel is null or temp_flevel<0 then
       temp_flevel:=1;
   end if;
     update WL_FILTER
        set FWORDS=IN_FWORDS,
            RWORDS=IN_RWORDS,
            FLEVEL=IN_FLEVEL,
            FSTATE=IN_FSTATE,
            USER_ID=IN_USER_ID,
            NOTE=IN_NOTE
         where FilterID=IN_FILTER_ID;
    commit;
      temp_state:=0;
   <<ret_end>>
   OUT_STATE:=temp_state;
end;
/

prompt
prompt Creating procedure WL_GET_BASIC_DATA
prompt ====================================
prompt
create or replace procedure WL_Get_Basic_data(
  In_count in number,--获取条数
  Out_Cur out Sys_Refcursor
)
as
begin
  -----清理余孽----------------
  update wl_info_from_in_early set state=2 where state=1;
  commit;
  -------建立新的队列-----------
  update wl_info_from_in_early set state=1
    where  state=0 and rownum<=In_count;
  commit;
  open Out_Cur for select text1,city,note,Id from wl_info_from_in_early where state=1 and rownum<=In_count group by text1,city,note,Id;
end;
/

prompt
prompt Creating procedure WL_GET_BASIC_DATA1
prompt =====================================
prompt
create or replace procedure WL_Get_Basic_data1(
  In_count in number,--获取条数
  Out_Cur out Sys_Refcursor
)
as
begin
  -----清理余孽----------------
  update wl_info_from_in_early set state=4 where state=3  and rownum<=In_count;
  commit;
  -------建立新的队列-----------
  update wl_info_from_in_early set state=3
    where  state=2 and rownum<=In_count;
  commit;
  open Out_Cur for select text1,city,note,Id from wl_info_from_in_early where state=3 and rownum<=In_count group by text1,city,note,Id;
end;
/

prompt
prompt Creating procedure WL_GET_INFO_TEST
prompt ===================================
prompt
create or replace procedure wl_get_info_test
--输入参数：in_text,in_city 信息,城市
--输出ret:1输入信息串为空或过长过短,2输入城市为空，3经预处理规则后信息串过长过短
--4无目的地，5无联系电话，6已有记录修改时间为当前时间
(in_text  in varchar2,in_city  in varchar2,ret   out number)
is
 
  tmp_pre_treatment varchar2(500);

  begin
  -----参数空值校验---------
  if in_text is null  or  length(in_text)<5 or  length(in_text)>500  then
       ret:=-1;-------
       goto ret_end;
  end if;
  if  trim(in_city) is null then
       ret:=-2;
       goto ret_end;
  end if;
  --ret:= lxf_get_qq_msg_back1('342143923','0',in_city,in_text);
 ret:=100 ;
 if ret<0 then
     goto ret_end;
  end if;
 goto real_end;
 <<ret_end>>
 -----插入原始表--------------------------
 
 <<real_end>>
  insert into wl_info_from_in_early (text1,  city,   note,              info_id)
                             values (in_text,in_city,substr(in_city,-3),early_msg_sequence.nextval);
  commit;
  /* 到测试表
  insert into  hr.wl_info_from_in_early (text1,city,note)
  values (in_text,SUBSTR(in_city,1,instr(in_city,'_',1,1)-1),substr(in_city,-3));
  commit;
  */
  ret:=ret;
end;
/

prompt
prompt Creating procedure WL_INFO_ADD
prompt ==============================
prompt
create or replace procedure WL_INFO_ADD(
   In_ID in varchar2,--信息ID
   In_Info in varchar2,--消息内容
   In_State in number,--消息状态
   In_dep_id in varchar2,--信息部ID
   In_Note in varchar2,--备注信息
   Out_state out number --返回状态 0 添加成功，-1 Id 为空 -2 信息内容为空 -3 该条信息已经存在
)
as
temp_count number;
begin
   ----验证ID
   if In_ID is null then
      temp_count:=-1;
      goto ret_end;
   end if;

   --验证信息是否为空
   if In_Info is null then
       temp_count:=-2;
      goto ret_end;
   end if;

   --验证信息是否存在
   select count(*) into temp_count
        from wl_info  where
        --DEP_ID=In_dep_id and
        info=In_Info;
    if temp_count>0 then
        update wl_info set create_date=sysdate
                where
                --DEP_ID=In_dep_id and
                info=In_Info;
        commit;
        temp_count:=-3;
    else
        insert into wl_info values(In_ID,In_Info,In_State,In_dep_id,sysdate,In_Note);
        commit;
        temp_count:=0;
    end if;

  <<ret_end>>
  Out_state:=temp_count;
end;
/

prompt
prompt Creating procedure WL_INFO_LINE_ADD
prompt ===================================
prompt
create or replace procedure WL_INFO_LINE_ADD(
  IN_ID in varchar2,--ID
  IN_From_Area in varchar2,--起始地
  In_To_Area in varchar2,--目的地
  Out_state out number--0 添加成 -1 id 为空 -2 起始地为空 -3 目的地为空 -4该条记录已经存在 -5 起始地目的相同
)
as
  temp_state number(5);
begin

   if IN_ID is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   if IN_From_Area is null then
       temp_state:=-2;
       goto ret_end;
   end if;

   if In_To_Area is null then
       temp_state:=-3;
       goto ret_end;
   end if;

   if In_To_Area=IN_From_Area then
       temp_state:=-5;
       goto ret_end;
   end if;

   select count(*) into temp_state  from wl_info_line
           where id=IN_ID
           and from_area=IN_From_Area
           and to_area=In_To_Area;

   if temp_state >0 then
        temp_state:=-4;
       goto ret_end;
   end if;

   insert into wl_info_line values(IN_ID,IN_From_Area,In_To_Area);
   commit;
   temp_state:=0;

   <<ret_end>>
   Out_state:=temp_state;
end;
/

prompt
prompt Creating procedure WL_PRO_INFO_INSERT_DATA
prompt ==========================================
prompt
create or replace procedure WL_Pro_INFO_Insert_Data(
  In_nbr in varchar2,--账号
  In_cname in varchar2,--信息部名称
  In_start in varchar2,--起始地
  In_end in varchar2,--目的地
  In_Info in varchar2,--信息主体
  In_city in varchar2,--城市
  In_plant in varchar2,--平台
  In_truck in varchar2,--车型
  In_goods in varchar2,--货物
  In_Note in varchar2,--备注
  In_phone in varchar2,--电话号码
  In_earyId in varchar2,--原始ID
  Out_Mess out varchar2--输出信息
)
as
  temp_count number(5):=0;
  temp_out varchar2(100):='';
  temp_cname varchar2(200):='';
begin
  ---------------验证账号---------------------
  if In_nbr is null or trim(In_nbr)='' then
    temp_out:='账号不能为空';
    temp_count:=0;
    goto ret_end;
  end if;
  ----------------验证电话号码-----------------
  if In_phone is null or trim(In_phone)='' then
    temp_out:='电话不能为空';
    temp_count:=0;
    goto ret_end;
  end if;
  -------------------验证起始地目的地----------
  if In_start is null or trim(In_start)='' then
    temp_out:='起始地不能为空';
    temp_count:=0;
    goto ret_end;
  end if;
  if In_end is null or trim(In_end)='' then
    temp_out:='目的地不能为空';
    temp_count:=0;
    goto ret_end;
  end if;
  -------------------验证相同-------------------
  if In_start=In_end then
    temp_out:='起始地和目的地不能相同';
    temp_count:=0;
    goto ret_end;
  end if;
  ---------------------信息主体----------------
  if In_Info is null or trim(In_Info)='' then
    temp_out:='信息主体不能为空';
    temp_count:=0;
    goto ret_end;
  end if;
  


  ---------判断元数据是否存在存在则更新发布时间-----
  select count(*) into temp_count from wl_info_data where info=In_start||'-->>'||In_end||In_Info;
  if temp_count>0 then
    temp_count:=0;
    update wl_info_data set create_date=sysdate where info=In_start||'-->>'||In_end||In_Info;
    commit;
    goto ret_end;
  end if;
  -------------------插入主表------------------
  insert into Wl_Info_Data(id,num,nbr,c_name,city,Create_date,Info,info_start,Info_end,Info_Nbr1,
                          Info_truck,info_goods,Info_Note,Info_from,state,Note,EARLY_ID)
                     values
                          (wl_info_data_id.NEXTVAL,0,In_nbr,In_cname,In_city,sysdate,In_start||'-->>'||In_end||In_Info,
                          In_start,In_end,In_phone,In_truck,In_goods,In_Note,In_plant,0,In_phone,In_earyId);
  commit;
  temp_count:=1;

  <<ret_end>>
  Out_Mess:=temp_out;
end;
/


spool off
