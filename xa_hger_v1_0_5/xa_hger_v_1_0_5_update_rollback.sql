-------------------------------------------------
-- Export file for user HR                     --
-- Created by Admin on 2013/6/27 ������, 11:52:14 --
-------------------------------------------------

set define off
spool DataHrBack.log

prompt
prompt drop table CUST_CONTACT
prompt ===============================
prompt
drop table CUST_CONTACT;

prompt
prompt drop view V_DEPARTMENT_CONTACT
prompt ==================================
prompt
drop view v_department_contact;

drop procedure Data_To_SH;
drop index wl_info_ind;
drop index wl_info_note;
drop index wl_info_line_ind;

spool off
