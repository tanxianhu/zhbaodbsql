-------------------------------------------------
-- Export file for user HR                     --
-- Created by Admin on 2013/6/27 ������, 11:52:14 --
-------------------------------------------------

set define off
spool DataHr.log

prompt
prompt Creating table CUST_CONTACT
prompt ===============================
prompt
create table CUST_CONTACT
(
  dep_id       VARCHAR2(64),
  contact      VARCHAR2(128),
  contact_type VARCHAR2(32),
  contact_note VARCHAR2(256),
  create_date  DATE
)
;

prompt
prompt Creating view V_DEPARTMENT_CONTACT
prompt ==================================
prompt
create or replace force view v_department_contact as
select
    c.dep_id,
    c.contact_type,
    w.C_NAME,
    c.CONTACT
    from
       WL_CUST_DEPARTMENT w,
       CUST_CONTACT c
    where
       w.CUST_ID=c.DEP_ID;

prompt
prompt Creating Procedure Data_To_SH
prompt ==================================
prompt
create or replace procedure Data_To_SH
as
begin

  insert into wl_Info@Sh select * from wl_info bd
  where note is null and not exists(
         select id from wl_Info@Sh sh where sh.id=bd.id
  );
  
  insert into WL_info_line@Sh select * from wl_info_line bd
  where  not exists(
    select id from WL_info_line@Sh sh where sh.id=bd.id
  );
  
  update wl_info set note =2
         where (sysdate - create_date)*24*60 > 2;
         
  commit;
end;
/

create index wl_info_ind on wl_info(id);
create index wl_info_note on wl_info(note);
create index wl_info_line_ind on wl_info_line(id); 

spool off
