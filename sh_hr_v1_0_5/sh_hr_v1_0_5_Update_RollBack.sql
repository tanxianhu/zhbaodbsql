------------------------------------------------
-- Export file for user HR                    --
-- Created by Admin on 2013/7/8 ����һ, 14:13:06 --
------------------------------------------------

set define off
spool sh_zhbao_v1_0_5_Update_RollBack.log

prompt
prompt drop table CUST_CONTACT
prompt ===========================
prompt
drop table CUST_CONTACT;

prompt
prompt drop table WL_FILTER
prompt ========================
prompt
drop table WL_FILTER;

prompt
prompt drop table WL_INFO
prompt ======================
prompt
drop table WL_INFO;

prompt
prompt drop table WL_INFO_LINE
prompt ===========================
prompt
drop table WL_INFO_LINE;

prompt
prompt drop sequence SEQU_FILTER_ID
prompt ================================
prompt
drop sequence SEQU_FILTER_ID;

prompt
prompt drop view V_DATA_INFO
prompt =========================
prompt
drop view v_data_info;

prompt
prompt drop view V_DEPARTMENT_CONTACT
prompt ==================================
prompt
drop view v_department_contact;

prompt
prompt drop view V_WL_FILTER
prompt =========================
prompt
drop VIEW V_WL_FILTER;

prompt
prompt drop procedure WL_FILTER_ADD
prompt ================================
prompt
drop procedure WL_FILTER_Add;

prompt
prompt drop procedure WL_FILTER_UPDATE
prompt ===================================
prompt
drop procedure WL_FILTER_UPDATE;

prompt
prompt drop procedure WL_INFO_ADD
prompt ==============================
prompt
drop procedure WL_INFO_ADD;

prompt
prompt drop procedure WL_INFO_LINE_ADD
prompt ===================================
prompt
drop procedure WL_INFO_LINE_ADD;

prompt
prompt drop procedure WL_CUST_GETINFODATA1
prompt ======================================
prompt
drop  procedure WL_CUST_GETINFODATA1;

prompt
prompt drop procedure WL_INFO_DATA_GETDATA1
prompt =======================================
prompt
drop procedure WL_Info_data_GetData1;

prompt
prompt drop procedure WL_WEB_DATA_GETONTIME1
prompt ========================================
prompt
drop procedure WL_Web_Data_GetOnTime1;

spool off
