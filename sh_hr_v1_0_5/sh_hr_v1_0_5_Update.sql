------------------------------------------------
-- Export file for user HR                    --
-- Created by Admin on 2013/7/8 星期一, 14:13:06 --
------------------------------------------------

set define off
spool sh_zhbao_v1_0_5_Update.log

prompt
prompt Creating table CUST_CONTACT
prompt ===========================
prompt
create table CUST_CONTACT
(
  dep_id       VARCHAR2(64),
  contact      VARCHAR2(128),
  contact_type VARCHAR2(32),
  contact_note VARCHAR2(256),
  create_date  DATE
)
;

prompt
prompt Creating table WL_FILTER
prompt ========================
prompt
create table WL_FILTER
(
  filterid NUMBER(8) not null,
  fwords   VARCHAR2(64),
  rwords   VARCHAR2(64),
  flevel   NUMBER(2),
  fstate   NUMBER(1),
  user_id  VARCHAR2(64),
  created  DATE,
  note     VARCHAR2(128)
)
;
alter table WL_FILTER
  add primary key (FILTERID);

prompt
prompt Creating table WL_INFO
prompt ======================
prompt
create table WL_INFO
(
  id          VARCHAR2(32),
  info        VARCHAR2(1024),
  state       NUMBER(1),
  dep_id      VARCHAR2(64),
  create_date DATE,
  note        VARCHAR2(500)
)
;

prompt
prompt Creating table WL_INFO_LINE
prompt ===========================
prompt
create table WL_INFO_LINE
(
  id        VARCHAR2(32),
  from_area VARCHAR2(225),
  to_area   VARCHAR2(225)
)
;

prompt
prompt Creating sequence SEQU_FILTER_ID
prompt ================================
prompt
create sequence SEQU_FILTER_ID
minvalue 1
maxvalue 99999999
start with 21
increment by 1
cache 20;

prompt
prompt Creating view V_DATA_INFO
prompt =========================
prompt
create or replace force view v_data_info as
select
        l.From_area,
        l.to_area,
        i.id,
        i.info,
        i.state,
        i.create_date,
        i.dep_id
   from wl_info_line l,
        wl_info i
  where i.id=l.id;

prompt
prompt Creating view V_DEPARTMENT_CONTACT
prompt ==================================
prompt
create or replace force view v_department_contact as
select
    c.dep_id,
    c.contact_type,
    w.C_NAME,
    w.state_business,
    w.state_house,
    w.state_idcard,
    c.CONTACT
    from
       WL_CUST_DEPARTMENT w,
       CUST_CONTACT c
    where
       w.CUST_ID=c.DEP_ID;

prompt
prompt Creating view V_WL_FILTER
prompt =========================
prompt
CREATE OR REPLACE FORCE VIEW V_WL_FILTER AS
SELECT
       FILTERID,
       FWORDS,
       RWORDS,
       FLEVEL,
       FSTATE,
       USER_ID,
       CREATED,
       NOTE
    FROM WL_filter;


prompt
prompt Creating procedure WL_CUST_GETINFODATA1
prompt ======================================
prompt
create or replace procedure WL_CUST_GETINFODATA1(
  MID in number,--信息编号
  retcur out sys_refcursor, --,--查询结果集
  ret out number--返回编号 0 成功 1 信息编号无效
  )
as
  retcount number:=0;
begin
  /*验证信息编号*/
  if MID is null then
    retcount:=1;
    goto ret_end;
  end if;
  open retcur for select w.C_name,
    w.Create_Date,
    w.INfo,w.Info_NBR1,
    w.Info_TRUCK,w.INFO_note,d.conm
    from wl_info_data w
    left join (
      select e.conm, c.c_Name,c.nbr,c.contact
      from wl_Cust_department c
      inner join wl_cust_deptnote e
      on c.cust_id=e.depid
    ) d
    on d.nbr=w.info_nbr1 or d.nbr=w.nbr
    where W.id=MID;
    retcount:=0;
    goto ret_end;
    <<ret_end>>
    ret:=retcount;
end;
/

prompt
prompt Creating procedure WL_FILTER_ADD
prompt ================================
prompt
create or replace procedure WL_FILTER_Add(
   /*敏感词添加*/
   IN_FWORDS in VARCHAR2,--敏感词
   IN_RWORDS in VARCHAR2,--要替换的词
   IN_FLEVEL in NUMBER,--优先级
   IN_FSTATE in NUMBER,--状态
   IN_USER_ID in VARCHAR2,--操作者
   IN_NOTE in VARCHAR2,--备注
   OUT_STATE out NUMBER--操作结果 0 添加成功 -1 敏感词为空 -2 敏感词已经存在
)
as
temp_count number(2);
temp_state number(1);
temp_flevel number(2);
temp_fstate number(2);
begin

   temp_flevel:=IN_FLEVEL;
   temp_fstate:=IN_FSTATE;

   --判断敏感词是否为空
   if IN_FWORDS is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   --判断该敏感词是否存在
   select count(*) into temp_count from WL_FILTER where FWORDS=IN_FWORDS;
   if temp_count>0 then
      temp_state:=-2;
      goto ret_end;
   end if;

   --判断状态
   if temp_fstate is null then
      temp_fstate:=0;
   end if;

   --判断优先级
   if temp_flevel is null or temp_flevel<0 then
       temp_flevel:=1;
   end if;
   insert into WL_FILTER(
             FilterID,
             FWords,
             RWords,
             FLEVEL,
             FSTATE,
             USER_ID,
             CREATED,NOTE
             )
        values(
            SEQU_FILTER_ID.Nextval,
            IN_FWORDS,
            IN_RWORDS,
            temp_flevel,
            temp_fstate,
            IN_USER_ID,
            sysDate,
            IN_NOTE
            );
     commit;
      temp_state:=0;
   <<ret_end>>
   OUT_STATE:=temp_state;
end;
/

prompt
prompt Creating procedure WL_FILTER_UPDATE
prompt ===================================
prompt
create or replace procedure WL_FILTER_UPDATE(
   /*敏感词修改*/
   IN_FILTER_ID in number,--ID
   IN_FWORDS in VARCHAR2,--敏感词
   IN_RWORDS in VARCHAR2,--要替换的词
   IN_FLEVEL in NUMBER,--优先级
   IN_FSTATE in NUMBER,--状态
   IN_USER_ID in VARCHAR2,--操作者
   IN_NOTE in VARCHAR2,--备注
   OUT_STATE out NUMBER--操作结果 0 修改成功 -1 敏感词为空
)
as
temp_count number(2);
temp_state number(1);
temp_flevel number(2);
temp_fstate number(2);
begin

   temp_flevel:=IN_FLEVEL;
   temp_fstate:=IN_FSTATE;

   --判断敏感词是否为空
   if IN_FWORDS is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   --判断状态
   if temp_fstate is null then
      temp_fstate:=0;
   end if;

   --判断优先级
   if temp_flevel is null or temp_flevel<0 then
       temp_flevel:=1;
   end if;
     update WL_FILTER
        set FWORDS=IN_FWORDS,
            RWORDS=IN_RWORDS,
            FLEVEL=IN_FLEVEL,
            FSTATE=IN_FSTATE,
            USER_ID=IN_USER_ID,
            NOTE=IN_NOTE
         where FilterID=IN_FILTER_ID;
    commit;
      temp_state:=0;
   <<ret_end>>
   OUT_STATE:=temp_state;
end;
/

prompt
prompt Creating procedure WL_INFO_ADD
prompt ==============================
prompt
create or replace procedure WL_INFO_ADD(
   In_ID in varchar2,--信息ID
   In_Info in varchar2,--消息内容
   In_State in number,--消息状态
   In_dep_id in varchar2,--信息部ID
   In_Note in varchar2,--备注信息
   Out_state out number --返回状态 0 添加成功，-1 Id 为空 -2 信息内容为空 -3 该条信息已经存在
)
as
temp_count number;
begin
   ----验证ID
   if In_ID is null then
      temp_count:=-1;
      goto ret_end;
   end if;

   --验证信息是否为空
   if In_Info is null then
       temp_count:=-2;
      goto ret_end;
   end if;

   --验证信息是否存在
   select count(*) into temp_count
        from wl_info  where 
        --DEP_ID=In_dep_id and 
        info=In_Info;
    if temp_count>0 then
        update wl_info set create_date=sysdate
                where 
                --DEP_ID=In_dep_id and 
                info=In_Info;
        commit;
        temp_count:=-3;
    else
        insert into wl_info values(In_ID,In_Info,In_State,In_dep_id,sysdate,In_Note);
        commit;
        temp_count:=0;
    end if;

  <<ret_end>>
  Out_state:=temp_count;
end;
/

prompt
prompt Creating procedure WL_INFO_DATA_GETDATA1
prompt =======================================
prompt
create or replace procedure WL_INFO_DATA_GETDATA1(
  pageIndex in number,--当前第几页
  pageSize in number,--每页显示的条数
  keyStart in varchar2,--起点城市
  keyEnd in varchar2,--目的城市
  retCount out number,--总记录数
  retCur out sys_refcursor --Wl_Cust_Dep_Getinfo.t_info--输出信息游标
  )
as
tempStart varchar2(100):='';
tempEnd varchar2(100):='';
begin
  ----获取起始地地址----
     if keyStart is not null then
       tempStart:=WL_GetComAddress(keyStart);
     end if;

     if keyEnd is not null then
       tempEnd:=WL_GetComAddress(keyEnd);
     end if;
  -----获取总记录数-----
        select count(*) Into retCount from
                   (
                    select w.id,w.info,w.state,w.dep_id,w.create_date,
                            e.C_name,e.state_IDCARD,e.state_Business,e.state_house
                     from wl_info w,wl_cust_department e
                     where e.cust_id=w.dep_id and w.state=1
                     ) t
       where t.id in (
            select id from  wl_info_line where from_area like  tempStart||'%' and To_Area like tempEnd||'%'
       );
  ----查询该数据---
  open retCur for
    select * from (
       select rownum run, f.* from (
      select id,info,state,dep_id,create_date,c_name,state_idCard,state_business,State_house 
            from (
                    select w.id,w.info,w.state,w.dep_id,w.create_date,
                            e.C_name,e.state_IDCARD,e.state_Business,e.state_house
                     from wl_info w,wl_cust_department e
                     where e.cust_id=w.dep_id and w.state=1
                  ) t
            where t.id in 
            (
                select id from  wl_info_line 
                   where from_area like  ''||'%' and To_Area like ''||'%'
            ) 
          Order by Create_date desc) f
     ) c
    where c.run<=pageIndex*pageSize  and c.run>(pageIndex-1)*pageSize ;
end;

/

prompt
prompt Creating procedure WL_INFO_LINE_ADD
prompt ===================================
prompt
create or replace procedure WL_INFO_LINE_ADD(
  IN_ID in varchar2,--ID
  IN_From_Area in varchar2,--起始地
  In_To_Area in varchar2,--目的地
  Out_state out number--0 添加成 -1 id 为空 -2 起始地为空 -3 目的地为空 -4该条记录已经存在 -5 起始地目的相同
)
as
  temp_state number(5);
begin

   if IN_ID is null then
      temp_state:=-1;
      goto ret_end;
   end if;

   if IN_From_Area is null then
       temp_state:=-2;
       goto ret_end;
   end if;

   if In_To_Area is null then
       temp_state:=-3;
       goto ret_end;
   end if;
  
   if In_To_Area=IN_From_Area then
       temp_state:=-5;
       goto ret_end;
   end if;

   select count(*) into temp_state  from wl_info_line
           where id=IN_ID
           and from_area=IN_From_Area
           and to_area=In_To_Area;

   if temp_state >0 then
        temp_state:=-4;
       goto ret_end;
   end if;

   insert into wl_info_line values(IN_ID,IN_From_Area,In_To_Area);
   commit;
   temp_state:=0;

   <<ret_end>>
   Out_state:=temp_state;
end;
/

prompt
prompt Creating procedure WL_WEB_DATA_GETONTIME1
prompt ========================================
prompt
create or replace procedure WL_WEB_DATA_GETONTIME1(
  FromA in varchar2,--起始地
  toB in varchar2,--目的地
  oldCount in number,--未更新前的条数
  newCount out number,--信息数据
  retCur out sys_refcursor --wl_cust_department_getinfo.t_info--返回数据
  )
as
  tempCount number(6):=0;
  tempStart varchar2(100):='';
  tempEnd varchar2(100):='';
begin
  ----获取起始地地址----
  if FromA is not null then
        tempStart:=WL_GetComAddress(FromA);
  end if;
  if toB is not null then
        tempEnd:=WL_GetComAddress(toB);
  end if;
  -----获取总记录数-----
     select count(*) into tempCount from 
                   (
                    select w.id,w.info,w.state,w.dep_id,w.create_date,
                            e.C_name,e.state_IDCARD,e.state_Business,e.state_house 
                     from wl_info w,wl_cust_department e 
                     where e.cust_id=w.dep_id and w.state=1
                     ) t
       where t.id in (
            select id from  wl_info_line where from_area like  tempStart||'%' and To_Area like tempEnd||'%'
       );
    newCount:=tempCount;
  ----查询该数据---
  open retCur for select s.* from
    (
       select rownum run,id,info,state,dep_id,create_date,c_name,state_idCard,state_business,State_house from 
                   (
                    select w.id,w.info,w.state,w.dep_id,w.create_date,
                            e.C_name,e.state_IDCARD,e.state_Business,e.state_house 
                     from wl_info w,wl_cust_department e 
                     where e.cust_id=w.dep_id and w.state=1
                     ) t
       where t.id in (
            select id from  wl_info_line where from_area like  tempStart||'%' and To_Area like tempEnd||'%'
       ) order by create_date desc
       ) s
  where rownum<=tempCount-oldCount;
end;
/


spool off
